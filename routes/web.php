<?php

use Illuminate\Support\Facades\Route;
Auth::routes();

Route::get('/renew_pass','Auth\LoginController@renew_pass');
Route::get('/renew_pass_dosen','Auth\LoginController@renew_pass_dosen');
Route::post('/cek_login','Auth\LoginController@cek_login')->name('cek_login');
Route::get('/beranda','Auth\LoginController@beranda')->name('beranda');
// Route::get('/landing', function () { return view('landing');});

Route::get('/log', function () {
    return view('auth.the_login');
});

Route::get('/cek1', function () {
    return view('calon_anggota.pdf_hasil_test');
});

//================================= halaman Calon Anggota =================================
Route::get('/', function () {
    return view('calon_anggota.profile');
});

Route::get('/cek', function () {
    return view('cetak');
});
Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'recruitment',],  function () {

    Route::get('tes_tulis','tesTulisController@index')->name('tes_tulis');
    Route::get('cekTesTulis','tesTulisController@cekTesTulis')->name('cekTesTulis');
    Route::post('simpanJawaban','tesTulisController@simpanJawaban')->name('simpanJawaban');

    Route::get('tes_wawancara','wawancaraController@index')->name('tes_wawancara');
    Route::get('getDataWawancara','wawancaraController@getDataWawancara')->name('getDataWawancara');
    Route::post('konfirmasiSelesaiWawancara','wawancaraController@konfirmasiSelesaiWawancara')->name('konfirmasiSelesaiWawancara');

    Route::get('calon_anggota', 'recruitmentController@index')->name('CalonAnggota');
    Route::get('DTCalonAnggota', 'recruitmentController@datatableCalonAnggota')->name('DTCalonAnggota');

    Route::get('/daftar', 'PendaftaranController@Pendaftaran');
    Route::post('/daftar/tambah', 'PendaftaranController@simpanPendaftaran')->name('simpanPendaftaran');
    Route::get('/daftar/cetak_kartu/{id_pendaftar}', 'PendaftaranController@cetak_kertuTest');
    Route::get('/hasil_tes', 'PendaftaranController@showAnggotaDiterima')->name('showAnggotaDiterima');
    Route::get('/cetakan', 'PendaftaranController@pdfHasiltest')->name('cetak_hasil_test');

});

Route::get('/home', 'HomeController@index')->name('home');
// Route::post('/foto_kendaraan/add', 'KendaraanFotoController@action')->name('add_foto_kendaraan');
// Select two


Route::group(['prefix' => 'master',], function () {
    // =======================Agama===================
    Route::get('/agama', 'm_agamaControler@index')->name('showAgam');
    Route::get('/agama/delete/{id}', 'm_agamaControler@destroy')->name('deletAgm');
    Route::post('/agama/tambah', 'm_agamaControler@tambahAgama')->name('TamAgam');

    // ========================Prodi==============
    Route::get('/prodi', 'm_prodiControler@index')->name('showProd');
    Route::get('/prodi/delete/{id}', 'm_prodiControler@destroy')->name('deletProd');
    Route::post('/prodi/tambah', 'm_prodiControler@tambahProdi')->name('TamProd');
    Route::get('/prodi/select2', 'm_prodiControler@Prodi')->name('selPro');


    // ========================kelas==============
    Route::get('/kelas', 'm_kelasControler@index')->name('showKel');
    Route::get('/kelas/delete/{id}', 'm_kelasControler@destroy')->name('Keldel');
    Route::post('/kelas/tambah', 'm_kelasControler@tambahkelas')->name('TamKel');
    Route::get('/kelas/select2', 'm_kelasControler@Kelas')->name('selkel');


    // =========================Soal=================
    Route::get('/soal', 'm_soalControler@index')->name('showSoal');
    Route::get('/soal/delete/{id}', 'm_soalControler@destroy')->name('Soaldel');
    Route::post('/soal/tambah', 'm_soalControler@tambahsoal')->name('TamSoal');

    // =========================Alamat=================
    Route::get('/alamat', 'm_alamatControler@index')->name('showProv');
    Route::get('/alamat/kota', 'm_alamatControler@getkota')->name('showKot');
    Route::get('/alamat/kecamatan', 'm_alamatControler@getkecamatan')->name('showKec');
    Route::get('/alamat/desa', 'm_alamatControler@getdesa')->name('showDes');
    Route::get('/alamat/provinsi/delete/{id}', 'm_alamatControler@destroyProvinsi')->name('provdel');
    Route::post('/alamat/provinsi/tambah', 'm_alamatControler@tambahprovinsi')->name('TamProv');
    Route::get('/alamat/select2Provinsi', 'm_alamatControler@select2Provinsi')->name('select2Provinsi');
    Route::get('/alamat/select2Kota', 'm_alamatControler@select2Kota')->name('select2Kota');
    Route::get('/alamat/select2Kecamatan', 'm_alamatControler@select2Kecamatan')->name('select2Kecamatan');
    Route::get('/alamat/select2Desa', 'm_alamatControler@select2Desa')->name('select2Desa');

    // =========================Kepanitiaan=================
    Route::get('/kepanitiaan', 'm_sie_kepanitiaanController@index')->name('showKepan');
    Route::get('/kepanitiaan/delete/{id}', 'm_sie_kepanitiaanController@destroy')->name('Kepandel');
    Route::post('/kepanitiaan/tambah', 'm_sie_kepanitiaanController@tambahKepanitiaan')->name('TamKepan');
    Route::get('/kepanitiaan/select2', 'm_sie_kepanitiaanController@select2Kepanitiaan')->name('select2Kepanitiaan');

    // =========================Kriteria =================
    Route::get('/kriteria_penilaian_proposal', 'm_KriteriaPenilaianController@index')->name('showKriter');
    Route::get('/kriteria_penilaian_proposal/delete/{id}', 'm_KriteriaPenilaianController@destroy')->name('Kriterdel');
    Route::post('/kriteria_penilaian_proposal/tambah', 'm_KriteriaPenilaianController@tambahkriteria_penilaian')->name('TamKriter');
    Route::get('/kriteria_penilaian_proposal/get', 'm_KriteriaPenilaianController@getKriteriaPenilaian')->name('getKriteriaPenilaian');

    // =========================Surat =================
    Route::get('/Surat', 'SuratController@index')->name('showSurat');
    Route::get('/Suratkel', 'SuratController@index1')->name('showSuratKel');
    Route::post('/SuratMas/tambah', 'SuratController@tambahSurat')->name('TamSurMas');
    Route::post('/SuratKel/tambah', 'SuratController@tambahSuratKel')->name('TamSurKel');
    Route::get('/Surat/delete/{id}', 'SuratController@destroy')->name('Surdel');

    Route::get('/pagu', 'm_paguController@index')->name('showPagu');
    Route::get('/pagu/akun/dataTable', 'm_paguController@dataTable_akun')->name('dataTable_akun');
    Route::post('/pagu/akun/tambah', 'm_paguController@tambahAkun')->name('tambahAkun');
    Route::get('/pagu/akun/delete/{id}', 'm_paguController@destroyAkun')->name('destroyAkun');
    Route::get('/pagu/jenis_belanja/dataTable', 'm_paguController@dataTable_jenis_belanja')->name('dataTable_jenis_belanja');
    Route::post('/pagu/jenis_belanja/tambah', 'm_paguController@tambahJenisBelanja')->name('tambahJenisBelanja');
    Route::get('/pagu/jenis_belanja/delete/{id}', 'm_paguController@destroyJenisBelanja')->name('destroyJenisBelanja');
    Route::get('/pagu/kategori_belanja/dataTable', 'm_paguController@dataTable_kategori_belanja')->name('dataTable_kategori_belanja');
    Route::post('/pagu/kategori_belanja/tambah', 'm_paguController@tambahKategoriBelanja')->name('tambahKategoriBelanja');
    Route::get('/pagu/kategori_belanja/delete/{id}', 'm_paguController@destroyKategoriBelanja')->name('destroyKategoriBelanja');
    Route::get('/select2jenisBelanja', 'm_paguController@select2jenisBelanja')->name('select2jenisBelanja');
    Route::get('/select2akunBelanja', 'm_paguController@select2akunBelanja')->name('select2akunBelanja');

});

// Struktur Organisasi
Route::get('struktur/{tahun}', 'strukturController@index')->name('struktur');
Route::get('getStruktur', 'strukturController@getStruktur')->name('getStruktur');
Route::post('simpanRiwayatJabatan', 'strukturController@simpanRiwayatJabatan')->name('simpanRiwayatJabatan');


// --=============================================================== HAFIZH Territory --===============================================================
// !!! Keep Out !!! melanggar auto delete 🖕😎🖕

Route::get('event','prokerController@event')->name('event');
Route::get('proker','prokerController@index')->name('proker');
Route::get('proker/select2','prokerController@select2Proker')->name('select2Proker');
Route::get('proker/select2bulanProker','prokerController@select2bulanProker')->name('select2bulanProker');
Route::get('proker/select2tahunProker','prokerController@select2tahunProker')->name('select2tahunProker');
Route::get('DTproker', 'prokerController@dataTable_proker')->name('DTproker');
Route::post('proker/proker_diapain','prokerController@proker_diapain')->name('proker_diapain');
Route::get('proker/detail/kebutuhan_event/{id_proker}','prokerController@getDetailKeb')->name('getDetailKeb');
Route::get('proker/delete/{id_proker}','prokerController@deleteProker')->name('deleteProker');
Route::get('proker/recovery/{id_proker}','prokerController@recoveryProker')->name('recoveryProker');

Route::get('proker/kinerja_pj','prokerController@kinerja_pj')->name('kinerja_pj');
Route::get('getKinerjaPJ','prokerController@getKinerjaPJ')->name('getKinerjaPJ');

Route::get('getFormReview','prokerController@getFormReview')->name('getFormReview');

Route::get('proker/approval/{jenis}/{id_proker}', 'prokerController@approval')->name('approvalProker');
Route::post('proker/approval2/', 'prokerController@approval2')->name('approval2Proker');

Route::get('proker/deleted','prokerController@index_deleted')->name('proker_deleted');
Route::get('DTprokerDeleted', 'prokerController@dataTable_proker_deleted')->name('DTprokerDeleted');
Route::get('proker/true_delete/{id_proker}','prokerController@trueDeleteProker')->name('trueDeleteProker');

Route::get('proker/detail/{id_proker}', 'detailProkerController@detailProker')->name('detailProker');
Route::post('proker/detail/update', 'detailProkerController@updateDetail')->name('updateDetail');

Route::get('DTkebutuhan/{id_proker}', 'detailProkerController@dataTable_kebutuhan')->name('DTkebutuhan');
Route::post('proker/detail/kebutuhan/simpan', 'detailProkerController@simpanKebutuhan')->name('simpan_kebutuhan');
Route::get('proker/detail/kebutuhan/pdf/{id_proker}/{jenis}', 'detailProkerController@pdfKebutuhan')->name('pdfKebutuhan');

Route::get('DTpanitia/{id_proker}', 'detailProkerController@dataTable_panitia')->name('DTpanitia');
Route::post('proker/detail/panitia/simpan', 'detailProkerController@simpanPanitia')->name('simpan_panitia');
Route::get('proker/detail/panitia/pdf/{id_proker}', 'detailProkerController@pdfPanitia')->name('pdfPanitia');

Route::get('DTrapat', 'detailProkerController@dataTable_rapat')->name('DTrapat');
Route::post('proker/detail/rapat/simpan', 'detailProkerController@simpanRapat')->name('simpan_rapat');
Route::get('proker/detail/rapat/hapus/{id_rapat}', 'detailProkerController@delete_rapat')->name('delete_rapat');

Route::get('absensi', 'absensiController@index')->name('absensi');
Route::post('absensi/proker/simpan', 'absensiController@simpanAbsensifromProker')->name('simpanAbsensifromProker');
Route::get('absensi/proker/get/{id_proker}/{id_rapat}', 'absensiController@get_absen')->name('get_absen');
Route::post('absensi/proker/delete', 'absensiController@delete_absen')->name('delete_absen');

Route::get('proposal', 'proposalController@index')->name('proposal');
Route::get('DTproposal', 'proposalController@dataTable_proposal')->name('DTproposal');
Route::get('select2bulanProposal', 'proposalController@select2bulanProposal')->name('select2bulanProposal');
Route::get('select2tahunProposal', 'proposalController@select2tahunProposal')->name('select2tahunProposal');
Route::post('proposal/simpan', 'proposalController@simpanProposal')->name('simpanProposal');
Route::get('proposal/hapus/{id_proker}', 'proposalController@deleteProposal')->name('deleteProposal');
Route::post('proposal/review/simpan', 'proposalController@simpanReviewProposal')->name('simpanReviewProposal');
Route::get('proposal/review/get/{id_proposal}/{flag_lv}', 'proposalController@getReview')->name('getReviewProposal');
Route::get('getHasilReviewProposal', 'proposalController@getHasilReviewProposal')->name('getHasilReviewProposal');

Route::get('lpj', 'lpjController@index')->name('lpj');
Route::get('DTlpj', 'lpjController@dataTable_lpj')->name('DTlpj');
Route::get('select2bulanLPJ', 'lpjController@select2bulanLPJ')->name('select2bulanLPJ');
Route::get('select2tahunLPJ', 'lpjController@select2tahunLPJ')->name('select2tahunLPJ');
Route::post('lpj/simpan', 'lpjController@simpanLPJ')->name('simpanLPJ');
Route::get('lpj/hapus/{id_proker}', 'lpjController@deleteLPJ')->name('deleteLPJ');
Route::post('lpj/review/simpan', 'lpjController@simpanReviewLPJ')->name('simpanReviewLPJ');
Route::get('lpj/review/get/{id_lpj}/{flag_lv}', 'lpjController@getReview')->name('getReviewLPJ');
Route::get('getHasilReviewLPJ', 'lpjController@getHasilReviewLPJ')->name('getHasilReviewLPJ');

Route::post('simpan_proLPJ', 'proLPJController@simpan_proLPJ')->name('simpan_proLPJ');


Route::get('/anggota/select2', 'anggotaController@select2Anggota')->name('select2Anggota');
Route::get('/anggota/select2AnggotaPanitia', 'anggotaController@select2AnggotaPanitia')->name('select2AnggotaPanitia');
Route::get('/anggota/select2AnggotaRiwayat', 'anggotaController@select2AnggotaRiwayat')->name('select2AnggotaRiwayat');
Route::get('/anggota/select2AnggotaAll', 'anggotaController@select2AnggotaAll')->name('select2AnggotaAll');
Route::get('/anggota/getkelpro/{nim}', 'anggotaController@getkelpro')->name('getkelpro');
Route::get('/anggota/baru', 'anggotaController@index_new_anggota')->name('index_new_anggota');
Route::post('/anggota/baru/simpan', 'anggotaController@simpan_new_anggota')->name('simpan_new_anggota');
Route::get('/anggota/profil', 'anggotaController@profilAnggota')->name('profilAnggota');
Route::post('/anggota/profil/simpan', 'anggotaController@simpanProfilAnggota')->name('simpanProfilAnggota');
Route::get('/anggota/ganti_password', 'anggotaController@ganti_password')->name('ganti_password');
Route::post('/anggota/password/ganti', 'anggotaController@simpanPerubahanPass')->name('simpanPerubahanPass');
Route::get('/anggota/cetak_kartu', 'anggotaController@cetakKartuAnggota')->name('cetakKartuAnggota');

Route::get('/select2Pagu', 'anggaranController@select2Pagu')->name('select2Pagu');
Route::get('/getAnggaranDetail', 'anggaranController@getAnggaranDetail')->name('getAnggaranDetail');

// --=============================================================== HAFIZH Territory --===============================================================


Route::group(['prefix' => 'riwayat',], function () {
    Route::get('jabatan', 'riwayatController@index_riwayat_jabatan')->name('riwayat_jabatan');
    Route::get('DTjabatan', 'riwayatController@datatableRiwayatJabatan')->name('DTjabatan');
    Route::get('updateRiwayat', 'riwayatController@Update_Status_Keanggotaan')->name('updateRj');
    Route::get('kepanitiaan', 'riwayatController@index_riwayat_kepanitiaan')->name('riwayat_kepanitiaan');
    Route::get('DTkepanitiaan', 'riwayatController@datatableRiwayatKepanitiaan')->name('DTkepanitiaan');

});

Route::get('/setting', 'settingController@gotoSetting_pendaftaran')->name('gotoSetting_pendaftaran');
Route::get('/ubahSetting', 'settingController@ubahSetting')->name('ubahSetting');

Route::get('/settinghasil', 'settingController@gotoSetting_hasil')->name('gotoSetting_hasil');
Route::get('/ubahSettinghas', 'settingController@ubahSettinghasil')->name('ubahSettinghasil');

#=================================================== A D M I N ===================================================
// Route::group(['prefix' => 'verifikator', 'middleware' => ['verifikator']], function () {
//     Route::get('/service', 'ServiceController@index')->name('landing_verifikator');
//     Route::post('/service/antre', 'ServiceController@antre')->name('set_antre');
//     Route::post('/service/mulai', 'ServiceController@mulai')->name('set_mulai');
//     Route::post('/service/selesai', 'ServiceController@selesai')->name('set_selesai');
// });
