<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class rapat extends Model
{
    protected $table = 'kegiatan.rapat_event';
    protected $primaryKey ='id';

    protected $fillable = [
        'id_proker',
        'hasil_rapat',
        'moderator',
        'catatan',
        'tanggal_rapat',
        'jam',
        'created_by',
        'pokok_bahasan',
        'tempat',
        'tipe',
    ];
}
