<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class proposal extends Model
{
    protected $table = 'kegiatan.proposal';
    protected $primaryKey ='id';

    protected $fillable = [
        'proker_id',
        'nama_file',
    ];
}
