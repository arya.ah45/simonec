<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Panitia extends Model
{
    protected $table = 'kegiatan.kepanitiaan';
    protected $primaryKey ='id';

    protected $fillable = [
        'id_proker',
        'id_kepanitiaan',
        'nim'
    ];
}
