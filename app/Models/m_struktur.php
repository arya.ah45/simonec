<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class m_struktur extends Model
{
    protected $table = 'master.m_struktur';
    protected $primaryKey ='id';

    protected $fillable = [
        'kode_struktur',
        'nama',
        'deskripsi',      
    ];
}
