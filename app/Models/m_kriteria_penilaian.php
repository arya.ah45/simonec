<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class m_kriteria_penilaian extends Model
{
    protected $table = 'master.m_kriteria_penilaian';
    protected $primaryKey ='id';

      protected $fillable = [
        'nama',
        'is_lpj',
        'for_user'
    ];
}
