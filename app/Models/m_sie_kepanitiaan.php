<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class m_sie_kepanitiaan extends Model
{
    protected $table = 'master.m_sie_kepanitiaan';
    protected $primaryKey ='id';

      protected $fillable = [
        'nama'
    ];
}
