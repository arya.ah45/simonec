<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class m_prodi extends Model
{
    protected $table = 'master.m_prodi';
    protected $primaryKey ='id';

      protected $fillable = [
        'nama'
    ];
}
