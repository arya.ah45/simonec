<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class riwayat_jabatan extends Model
{
    protected $table = 'keanggotaan.riwayat_jabatan';
    protected $primaryKey ='id';

    protected $fillable = [
        'nim',
        'kode_struktur',
        'kode_struktur_detail',
        'no_sk',
        'tmt_sk',
    ];
}
