<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class proker extends Model
{
    protected $table = 'kegiatan.proker';
    protected $primaryKey ='id';

    protected $fillable = [
        'nama',
        'id_struktur',
        'nama',
        'tanggal_mulai',
        'tanggal_selesai',
        'dana_usulan',
        'dana_terpakai',
        'dana_turun',
        'ket_tambahan',
        'penanggungjawab',
        'event',
        'tujuan',
        'subjek_tujuan',
        'created_by',
        'is_final',
        'terlaksana',
        'dana_mandiri',
        'rating',
        'evaluasi'
    ];
}
