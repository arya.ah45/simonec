<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class m_struktur_detail extends Model
{
    protected $table = 'master.m_struktur_detail';
    protected $primaryKey ='id';

    protected $fillable = [
        'struktur_id',
        'nama',
    ];
}
