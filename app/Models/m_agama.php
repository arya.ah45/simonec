<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class m_agama extends Model
{
    protected $table = 'master.m_agama';
    protected $primaryKey ='id';

      protected $fillable = [
        'nama'
    ];
}
