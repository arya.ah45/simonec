<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class m_kelas extends Model
{
    protected $table = 'master.m_kelas';
    protected $primaryKey ='id';

      protected $fillable = [
        'nama',
        'id_prodi'
    ];
}
