<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class review_proposal extends Model
{
    protected $table = 'kegiatan.review_proposal';
    protected $primaryKey ='id';

    protected $fillable = [
        'proposal_id',
        'kriteria_id',
        'status',
        'catatan',
        'from_user',
    ];
}
