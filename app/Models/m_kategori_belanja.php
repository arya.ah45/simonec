<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class m_kategori_belanja extends Model
{
    protected $table = 'anggaran.kategori_belanja';
    protected $primaryKey ='id';

    protected $fillable = [
        'kategori_belanja',
        'minimal_anggaran',
        'maksimal_anggaran',
        'kode_akun',
        'id_jenis_belanja',
        'satuan'
    ];
}
