<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class m_provinsi extends Model
{
    protected $table = 'master.m_provinsi';
    protected $primaryKey ='id_prov';

      protected $fillable = [
        'nama',
    ];
}
