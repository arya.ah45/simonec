<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class kbt_proker extends Model
{
    protected $table = 'kegiatan.kebutuhan_event';
    protected $primaryKey ='id';

    protected $fillable = [
        'id_proker',
        'id_sie_kepanitiaan',
        'keterangan',
        'satuan',
        'harga_satuan',
        'jumlah',
        'satuan_real',
        'harga_satuan_real',
        'jumlah_real',
        'nota',
        'id_kabel'
    ];
}
