<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class m_soal extends Model
{
    protected $table = 'master.m_soal_tes';
    protected $primaryKey ='id';

      protected $fillable = [
        'nama',
    ];
}
