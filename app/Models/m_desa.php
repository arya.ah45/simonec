<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class m_desa extends Model
{
    protected $table = 'master.m_desa';
    protected $primaryKey ='id';

      protected $fillable = [
        'nama',
        'kecamatan_id'
    ];
}
