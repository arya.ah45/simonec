<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class anggota extends Model
{
    protected $table = 'keanggotaan.anggota';
    protected $primaryKey ='id';

    protected $fillable = [
        'nama',
        'nim',
        'id_desa',
        'id_kecamatan',
        'id_provinsi',
        'email',
        'tanggal_lahir',
        'jenis_kelamin',
        'no_hp',
        'foto',
        'alamat',
        'is_anggota',
        'created_at',
        'updated_at',
        'kode_struktur',
        'id_kota',
        'id_prodi',
        'id_kelas',
        'kode_struktur_detail',
        'id_agama',
        'angkatan',
        'tmt',
        'is_pengurus',
        'qr_code'
    ];
}
