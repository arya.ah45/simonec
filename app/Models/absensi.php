<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class absensi extends Model
{
    protected $table = 'kegiatan.absensi';
    protected $primaryKey ='id';

    protected $fillable = [
        'id_rapat',
        'nim',
        'id_proker'
    ];
}