<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class calon_anggota extends Model
{
    protected $table = 'keanggotaan.calon_anggota';
    protected $primaryKey ='id';

    protected $fillable = [
        'nama',
        'nim',
        'id_provinsi',
        'id_kota',
        'id_kecamatan',
        'id_desa',
        'email',
        'tanggal_lahir',
        'jenis_kelamin',
        'no_hp',
        'foto',
        'alamat',
        'is_anggota',
        'kode_struktur',
        'id_prodi',
        'id_kelas',
        'kode_struktur_detail',
        'id_agama',
        'angkatan',
        'tmt',
        'is_pengurus',
        'qr_code',
        'created_at',
    ];
}
