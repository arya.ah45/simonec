<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class m_jenis_belanja extends Model
{
    protected $table = 'anggaran.jenis_belanja';
    protected $primaryKey ='id';

    protected $fillable = [
        'nama'
    ];
}

