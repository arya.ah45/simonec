<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class review_lpj extends Model
{
    protected $table = 'kegiatan.review_lpj';
    protected $primaryKey ='id';

    protected $fillable = [
        'lpj_id',
        'kriteria_id',
        'status',
        'catatan',
        'from_user',        
    ];
}
