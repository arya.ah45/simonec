<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class lpj extends Model
{
    protected $table = 'kegiatan.lpj';
    protected $primaryKey ='id';

    protected $fillable = [
        'proker_id',
        'nama_file',
    ];
}

