<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Surat extends Model
{
    protected $table = 'keanggotaan.surat';
    protected $primaryKey ='id';

      protected $fillable = [
        'perihal',
        'no_surat',
        'asal_surat',
        'jenis_surat',
        'tgl_surat',
        'file'
    ];
}
