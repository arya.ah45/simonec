<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class m_akun extends Model
{
    protected $table = 'anggaran.akun';
    protected $primaryKey ='id';

    protected $fillable = [
        'kode_akun',
        'nama_akun'
    ];
}
