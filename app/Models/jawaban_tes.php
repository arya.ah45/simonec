<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class jawaban_tes extends Model
{
    protected $table = 'keanggotaan.jawaban_tes';
    protected $primaryKey ='id';

    protected $fillable = [
        'nim_calon',
        'id_tes',
        'jawaban',
    ];
}
