<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class m_kota extends Model
{
    protected $table = 'master.m_kota';
    protected $primaryKey ='id';

      protected $fillable = [
        'nama',
        'provinsi_id'
    ];
}
