<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class akun extends Model
{
    protected $table = 'keanggotaan.akun';
    protected $primaryKey ='id';

    protected $fillable = [
        'nim',
        'username',
        'password',        
    ];
    protected $hidden = [
        'password',
    ];    
}
