<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class notif extends Model
{
    protected $table = 'notif';
    protected $primaryKey ='id';

    protected $fillable = [
        'kode_subjek',
        'from',
        'to',
        'send_to',
        'status',
        'status_read',
        'id_subjek',
        'pesan',        
    ];
}
