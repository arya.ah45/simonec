<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class m_kecamatan extends Model
{
    protected $table = 'master.m_kecamatan';
    protected $primaryKey ='id';

      protected $fillable = [
        'nama',
        'kota_id'
    ];
}
