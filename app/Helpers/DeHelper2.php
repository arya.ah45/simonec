<?php
namespace App\Helpers;

use Illuminate\Http\Request;
use Session;
use DB;
use App\Models\proker;
use App\Models\kbt_proker;
use App\Models\Panitia;
use App\Models\rapat;
use App\Models\notif;
use DeHelper;

class DeHelper2
{
    
    public static function getRole($id)
    {
        $roles = [
            "a" => "DPK",
            "b" => "Ketua UKM",
            "c" => "Sekretaris UKM",
            "d" => "Anggota",
            "eb" => "Admin Kegiatan",
            "ec" => "Sekretaris UKM",
            "ed" => "Admin Kegiatan"
        ];
        return isset($roles[$id]) ? $roles[$id] : null;
        
    }

    public static function simpanNotif($notif)
    {

        // needed
        $action = $notif['action'];
        $id_subjek = $notif['id_subjek'];    
        $status = $notif['status'];        
        if (isset($notif['kode_subjek'])) {
            $urlSubjek = $notif['kode_subjek'];
        }else{
            $urlSubjek = DeHelper::getUrlSubjek();
        }

        $session = Session::get('data_user');
        $nim = $session['nim'];
        if (in_array($status, [3,4,5,8])) {
            $true_role = $session['flag_lv']; 
        }else{
            $true_role = $session['key_menu']; 
        }
            

        $dataSubjek = DB::table('kamus_url')->where('url_nama','=',$urlSubjek)->get()->first();
        $id_kamus_url = $dataSubjek->id;
        $nama_tabel = $dataSubjek->nama_tabel;
        $nama_subjek = $dataSubjek->nama;
        
        // $request->pesanNotif = DeHelper:::pesanNotif($action, $id_subjek);
        $pesan = DeHelper::pesanNotif($action, $id_subjek, $nim, $true_role, $urlSubjek, $nama_tabel, $nama_subjek);
        // return $pesan;

        $send_to_status = [
            0 => "b",
            1 => "c",
            2 => "b",
            3 => "a",
            4 => "c",
            5 => "c",
            6 => "c",
            7 => "a",
            8 => "c",
            9 => "c"
        ];

        $data_notif['kode_subjek'] = $id_kamus_url;
        $data_notif['from'] = $nim;
        $data_notif['to'] = null; //for pengembangan
        $data_notif['send_to'] = $send_to_status[$status];
        $data_notif['status'] = $status;
        $data_notif['status_read'] = 0;
        $data_notif['id_subjek'] = $id_subjek;
        $data_notif['pesan'] = $pesan;

        notif::create($data_notif);

    }

    public static function getNotif($menu_key,$nim)
    {
        // return in_array($menu_key, ['c','eb','ec','ed']);
        $dataNotifRaw = DB::table('notif as ntf')
            ->rightJoin(DB::raw('(
                SELECT A
                    .*
                    FROM
                        notif A
                        JOIN (
                        SELECT id_subjek, MAX ( ID ) AS idmax FROM notif GROUP BY id_subjek
                        ) B ON A.ID = B.idmax
                    AND A.id_subjek = B.id_subjek
            ) as notif'),'notif.id','=','ntf.id')
            ->leftjoin('kamus_url as ku','ku.id','=','ntf.kode_subjek')
            ->when($menu_key, function ($query) use ($menu_key,$nim) {
                if ( $menu_key == 'a') {
                    return $query->where('ntf.send_to','=','a');
                }elseif ( $menu_key == 'b' || $menu_key == 'eb') {
                    return $query->where('ntf.send_to','=','b');
                }elseif ( $menu_key == 'c' || $menu_key == 'ec') {
                    return $query->where('ntf.send_to','=','c');
                }elseif ( in_array($menu_key, ['eb','ec','ed']) ) {
                    $id_proker = DB::table('kegiatan.admin_kegiatan')->where('nim',$nim)->pluck('proker_id');
                    return $query->where('ntf.send_to','=','c')
                                ->whereIn('ntf.id_subjek',$id_proker);
                }{
                    return $query->where('ntf.send_to','=','d');
            }})
            ->selectRaw('ntf.id_subjek, ntf.kode_subjek, ntf.pesan, ntf.created_at as waktu, ku.url_nama, ntf.id, ntf.status')
            ->where('ntf.status_read','=',0)
            ->orderBy('ntf.created_at','desc');
        
        $dataNotif = $dataNotifRaw->get();
        // return $dataNotif;

        if(isset($dataNotif)){
            foreach ($dataNotif as $key => $value) {
                $datas[$key]['pesan'] = $value->pesan;
                $datas[$key]['id'] = $value->id;
                $datas[$key]['status'] = $value->status;
                $datas[$key]['lama'] = DeHelper::getLamaWaktu($value->waktu);
                if ($value->kode_subjek == 1) {
                    $datas[$key]['url'] = '/proker/detail/'.DeHelper::encrypt_($value->id_subjek);
                }else{
                    $datas[$key]['url'] = '/proker';
                }
            }
        }else {
        }

        $data_notif = (isset($datas))?$datas:[];        
        $countNotif = $dataNotifRaw->count();
        $data['data_notif'] = $data_notif;
        $data['count'] = $countNotif;

        return $data;
    }    

    public static function totalProker()
    {
        $tahun = date('Y');
        $data_proker = proker::where(DB::RAW('EXTRACT(YEAR FROM tanggal_mulai)'),$tahun)->count();
        return $data_proker;
    }

    public static function totalProkerTerlaksana()
    {
        $tahun = date('Y');
        $data_proker_raw = proker::where([[DB::RAW('EXTRACT(YEAR FROM tanggal_mulai)'),$tahun],['terlaksana',true]]);
        $data_proker['count'] = $data_proker_raw->count();
        $data_proker['data'] = $data_proker_raw->get();
        return $data_proker;
    }

    public static function totalProkerTakTerlaksana()
    {
        $tahun = date('Y');
        $data_proker_raw = proker::where([[DB::RAW('EXTRACT(YEAR FROM tanggal_mulai)'),$tahun],['terlaksana',false],['tanggal_selesai','<',DB::raw("'".date('Y-m-d')."'::date")]]);
        $data_proker['count'] = $data_proker_raw->count();
        $data_proker['data'] = $data_proker_raw->get();
        return $data_proker;
    }

    public static function totalAnggota()
    {
        $tahun = date('Y');
        $data_raw = DB::table('keanggotaan.riwayat_jabatan as rj')
                    ->join('master.m_struktur_detail as msd','msd.id','=','rj.kode_struktur_detail');

        $total_anggota_aktif = $data_raw->where(DB::RAW('EXTRACT(YEAR FROM rj.tmt_sk)'),'=',$tahun)
                                        ->count();

        $total_anggota = $data_raw->where([['msd.nama','ilike',"%anggota%"],[DB::RAW('EXTRACT(YEAR FROM rj.tmt_sk)'),'=',$tahun]])
                                    ->count();

        $data['total_anggota_aktif'] = $total_anggota_aktif;
        $data['total_pengurus'] = $total_anggota_aktif - $total_anggota;
        $data['total_anggota'] = $total_anggota;
        return $data;
    }

    public static function rekap_proker()
    {
        $data_raw = DB::table('kegiatan.v_rekap_proker')
                    ->get();
        foreach ($data_raw as $key => $value) {
            $data[$key]['nama_proker'] = $value->nama_proker;
            $data[$key]['tanggal_mulai'] = DeHelper::format_tanggal($value->tanggal_mulai);
            $data[$key]['terlaksana'] = $value->terlaksana;
            $data[$key]['status_proker'] = ($value->status_proker != '' || $value->status_proker != null ) ? DeHelper::presentase_proker($value->status_proker) : 'X';
            $data[$key]['status_proposal'] = ($value->status_proposal != '' || $value->status_proposal != null ) ? DeHelper::presentase_proker($value->status_proposal) : 'X';
            $data[$key]['status_lpj'] = ($value->status_lpj != '' || $value->status_lpj != null ) ? DeHelper::presentase_proker($value->status_lpj) : 'X';
        }
        return (isset($data)) ? $data : [] ;
    }

    public static function getPanitiaIfNormal()
    {
        $data = DB::table('master.m_sie_kepanitiaan')->whereRaw("( nama ilike '%pengurus%' )")->get()->first();
        return $data;
    }
}
