<?php
namespace App\Helpers;

use Illuminate\Http\Request;
use Session;
use DB;
use App\Models\proker;
use App\Models\kbt_proker;
use App\Models\Panitia;
use App\Models\rapat;
use App\Helpers\DeHelper2;
use Str;

class DeHelper
{

    public static function format_tanggal($tgl)
    {

        $bln = date("m", strtotime($tgl));
        $bln_h = array("01" => "Januari", "02" => "Februari", "03" => "Maret", "04" => "April", "05" => "Mei", "06" => "Juni", "07" => "Juli", "08" => "Agustus", "09" => "September", "10" => "Oktober", "11" => "November", "12" => "Desember");
        $bln = $bln_h[$bln];
        $tg = date("d", strtotime($tgl));
        $thn = date("Y", strtotime($tgl));
        $print = $tg.' '.$bln.' '.$thn;

        return $print;
    }

    public static function sideMenu($level)
    {
            $validasi = (request()->is('*validasi*')) ? 'active' : '';
            $master = (request()->is('*master/*')) ? 'active' : '';

            $activ_proker = (request()->is('*proker')) ? 'active' : '';
            $activ_krit_peni_prop = (request()->is('*kriteria_penilaian_proposal*')) ? 'active' : '';
            $activ_si_kpntia = (request()->is('*kepanitiaan*')) ? 'active' : '';
            $activ_m_agama = (request()->is('*agama*')) ? 'active' : '';
            $activ_m_alamat = (request()->is('*alamat*')) ? 'active' : '';
            $activ_m_kls = (request()->is('*kelas*')) ? 'active' : '';
            $activ_m_prodi = (request()->is('*prodi*')) ? 'active' : '';
            $activ_m_soal = (request()->is('*soal*')) ? 'active' : '';
            $activ_proposal = (request()->is('*proposal*')) ? 'active' : '';
            $activ_lpj = (request()->is('*lpj*')) ? 'active' : '';
            $activ_event = (request()->is('*event*')) ? 'active' : '';
            $activ_struktur = (request()->is('*struktur*')) ? 'active' : '';
            $activ_anggota_baru = (request()->is('*anggota/baru*')) ? 'active' : '';
            $activ_wawancara = (request()->is('*recruitment/tes_wawancara*')) ? 'active' : '';
            $activ_r_jabatan = (request()->is('*riwayat/jabatan*')) ? 'active' : '';
            $activ_r_kepanitiaan = (request()->is('*riwayat/kepanitiaan*')) ? 'active' : '';
            $activ_calon_anggota = (request()->is('*recruitment/calon_anggota*')) ? 'active' : '';
            $activ_m_pagu = (request()->is('*master/pagu*')) ? 'active' : '';
            $activ_pass = (request()->is('*gantipass*')) ? 'active' : '';
            $activ_profil_diri = (request()->is('*anggota/profil_diri*')) ? 'active' : '';
            $activ_setting = (request()->is('*setting*')) ? 'active' : '';

            $activ_master = '';
            $activ_anggota = '';
            $activ_riwayat = '';
            $activ_surat = '';
            $activ_recruitment = '';

            if ($activ_m_agama != '' || $activ_m_alamat != '' || $activ_m_kls != '' || $activ_m_prodi != '' || $activ_m_soal || $activ_krit_peni_prop != '' || $activ_si_kpntia != '' || $activ_m_pagu != '' || $activ_pass != '') {
                $activ_master = 'active' ;
            } else {
            }

            if ($activ_wawancara != '' || $activ_calon_anggota != '') {
                $activ_recruitment = 'active';
            } else {
            }

            if ($activ_anggota_baru != '' || $activ_profil_diri != '') {
                $activ_anggota = 'active';
            } else {
            }

            if ($activ_r_jabatan != '' || $activ_r_kepanitiaan != '') {
                $activ_riwayat = 'active';
            } else {
            }


            $tahun_now = date('Y');

            $goto_event = route('event');
            $goto_proker = route('proker');
            $goto_krit_peni_prop = route('showKriter');
            $goto_si_kpntia = route('showKepan');
            $goto_m_agama = route('showAgam');
            $goto_m_alamat = route('showProv');
            $goto_m_kls = route('showKel');
            $goto_m_prodi = route('showProd');
            $goto_m_soal = route('showSoal');
            $goto_proposal = route('proposal');
            $goto_lpj = route('lpj');
            $goto_m_surat = route('showSurat');
            $goto_struktur = route('struktur',$tahun_now);
            $goto_tes_wawancara = route('tes_wawancara');
            $goto_anggota_baru = route('index_new_anggota');
            $goto_riwayat_jabatan = route('riwayat_jabatan');
            $goto_riwayat_kepanitiaan = route('riwayat_kepanitiaan');
            $goto_calon_anggota = route('CalonAnggota');
            $goto_pagu = route('showPagu');
            $goto_profilAnggota = route('profilAnggota');
            $goto_setting_pendaftaran = route('gotoSetting_pendaftaran');
            $goto_gantipass = route('ganti_password');

            $rule = [
                'a' => [2,3], //DPK
                'b' => [2,3,10,11,15,12], //Ketua Wakil
                'c' => [2,3,8,9,10,11,15,13,12,6,7,16], //Sekretaris
                'd' => [14,11,12,15], //Anggota biasa
                'eb' => [0,1,2,3,10,15,11,12], //admin kegiatan sekaligus ketua / waka
                'ec' => [0,1,2,3,8,9,10,11,12,6,7,16], //Sekretaris
                'ed' => [0,1,14,11,12,15], //admin kegiatan sekalisgus anggota
            ];

            $user 		= Session::get("data_user");
            $flag_lv    = $user['flag_lv'];
            $nim        = $user['nim'];
            $key_menu   = $user['key_menu'];
            $menu15 = '';

            if ($flag_lv == 'c') {
                $menu15 = '<li class="site-menu-item has-sub '.$activ_anggota.'">
                        <a href="javascript:void(0)">
                            <i class="site-menu-icon fa-users" aria-hidden="true"></i>
                            <span class="site-menu-title">Anggota</span>
                            <span class="site-menu-arrow"></span>
                        </a>
                        <ul class="site-menu-sub">
                            <li class="site-menu-item">
                                <a class="animsition-link pl-20" href="'.$goto_anggota_baru.'">
                                    <i class="site-menu-icon fa-tasks" aria-hidden="true"></i>
                                    <span class="site-menu-title">Penerimaan Anggota</span>
                                </a>
                            </li>
                            <li class="site-menu-item">
                                <a class="animsition-link pl-20" href="'.$goto_profilAnggota.'">
                                    <i class="site-menu-icon fa-slideshare" aria-hidden="true"></i>
                                    <span class="site-menu-title">Profil Diri</span>
                                </a>
                            </li>
                        </ul>
                        </li>';
            }else{
                $menu15 = '<li class="site-menu-item has-sub '.$activ_anggota.'">
                            <a href="javascript:void(0)">
                                <i class="site-menu-icon fa-users" aria-hidden="true"></i>
                                <span class="site-menu-title">Anggota</span>
                                <span class="site-menu-arrow"></span>
                            </a>
                            <ul class="site-menu-sub">
                                <li class="site-menu-item">
                                    <a class="animsition-link pl-20" href="'.$goto_profilAnggota.'">
                                        <i class="site-menu-icon fa-slideshare" aria-hidden="true"></i>
                                        <span class="site-menu-title">Profil Diri</span>
                                    </a>
                                </li>
                            </ul>
                            </li>';
            }
            $sideMenu = array(
                '0' => '<li class="cat-side-menu">
                        Event
                        </li>',
                '1' => '<li class="site-menu-item '.$activ_event.'">
                        <a href="'.$goto_event.'">
                            <i class="site-menu-icon fa-star" aria-hidden="true"></i>
                            <span class="site-menu-title">Event</span>
                        </a>
                        </li>',
                '2' => '<li class="cat-side-menu">
                        Manajemen Kegiatan
                        </li>',
                '3' => '<li class="site-menu-item '.$activ_proker.'">
                        <a class="animsition-link" href="'.$goto_proker.'">
                            <i class="site-menu-icon fa-tasks" aria-hidden="true"></i>
                            <span class="site-menu-title">Proker</span>
                        </a>
                        </li>',
                '4' => '<li class="site-menu-item '.$activ_proposal.'">
                        <a class="animsition-link" href="'.$goto_proposal.'">
                            <i class="site-menu-icon fa-file-powerpoint-o" aria-hidden="true"></i>
                            <span class="site-menu-title">Proposal</span>
                        </a>
                        </li>',
                '5' => '<li class="site-menu-item '.$activ_lpj.'">
                        <a class="animsition-link" href="'.$goto_lpj.'">
                            <i class="site-menu-icon fa-file-text-o" aria-hidden="true"></i>
                            <span class="site-menu-title">LPJ</span>
                        </a>
                        </li>',
                '6' => '<li class="cat-side-menu">
                        Manajemen Master
                        </li>',
                '7' => '<li class="site-menu-item has-sub '.$activ_master.'">
                        <a href="javascript:void(0) pl-20">
                            <i class="site-menu-icon fa-gears" aria-hidden="true"></i>
                            <span class="site-menu-title">Master</span>
                            <span class="site-menu-arrow"></span>
                        </a>
                        <ul class="site-menu-sub">
                            <li class="site-menu-item '.$activ_krit_peni_prop.'">
                                <a class="animsition-link pl-20" href="'.$goto_krit_peni_prop.'">
                                    &nbsp;
                                    <i class="icon fa-edit" aria-hidden="true"></i>
                                    &nbsp;
                                    <span class="site-menu-title">Kriteria Penilaian</span>
                                </a>
                            </li>
                            <li class="site-menu-item '.$activ_si_kpntia.'">
                                <a class="animsition-link pl-20" href="'.$goto_si_kpntia.'">
                                    &nbsp;
                                    <i class="icon fa-bug" aria-hidden="true"></i>
                                    &nbsp;
                                    <span class="site-menu-title">Sie Kepanitiaan</span>
                                </a>
                            </li>
                            <li class="site-menu-item '.$activ_m_agama.'">
                                <a class="animsition-link pl-20" href="'.$goto_m_agama.'">
                                    &nbsp;
                                    <i class="icon fa-institution" aria-hidden="true"></i>
                                    &nbsp;
                                    <span class="site-menu-title">Agama</span>
                                </a>
                            </li>
                            <li class="site-menu-item '.$activ_m_alamat.'">
                                <a class="animsition-link pl-20" href="'.$goto_m_alamat.'">
                                    &nbsp;
                                    <i class="icon fa-map-signs" aria-hidden="true"></i>
                                    &nbsp;
                                    <span class="site-menu-title">Alamat</span>
                                </a>
                            </li>
                            <li class="site-menu-item '.$activ_m_kls.'">
                                <a class="animsition-link pl-20" href="'.$goto_m_kls.'">
                                    &nbsp;
                                    <i class="icon fa-line-chart" aria-hidden="true"></i>
                                    &nbsp;
                                    <span class="site-menu-title">Kelas</span>
                                </a>
                            </li>
                            <li class="site-menu-item '.$activ_m_prodi.'">
                                <a class="animsition-link pl-20" href="'.$goto_m_prodi.'">
                                    &nbsp;
                                    <i class="icon fa-street-view" aria-hidden="true"></i>
                                    &nbsp;
                                    <span class="site-menu-title">Prodi</span>
                                </a>
                            </li>

                            <li class="site-menu-item '.$activ_m_soal.'">
                                <a class="animsition-link pl-20" href="'.$goto_m_soal.'">
                                    &nbsp;
                                    <i class="icon fa-file-code-o" aria-hidden="true"></i>
                                    &nbsp;
                                    <span class="site-menu-title">Soal test</span>
                                </a>
                            </li>
                            <li class="site-menu-item '.$activ_pass.'">
                                <a class="animsition-link pl-20" href="'.$goto_gantipass.'">
                                    &nbsp;
                                    <i class="icon fa-user-circle-o" aria-hidden="true"></i>
                                    &nbsp;
                                    <span class="site-menu-title">Kelola Akun</span>
                                </a>
                            </li>
                            <li class="site-menu-item '.$activ_m_pagu.'">
                                <a class="animsition-link pl-20" href="'.$goto_pagu.'">
                                    &nbsp;
                                    <i class="icon fa-shopping-cart" aria-hidden="true"></i>
                                    &nbsp;
                                    <span class="site-menu-title">Kelola PAGU</span>
                                </a>
                            </li>
                        </ul>
                        </li>',
                '8' => '<li class="cat-side-menu">
                        Manajemen Surat
                        </li>',
                '9' => '<li class="site-menu-item '.$activ_surat.'">
                        <a class="animsition-link" href="'.$goto_m_surat.'">
                            <i class="site-menu-icon fa-envelope-o" aria-hidden="true"></i>
                            <span class="site-menu-title">Surat</span>
                        </a>
                        </li>',
                '10' => '<li class="cat-side-menu">
                        Manajemen Keanggotaan
                        </li>',
                '15' => $menu15,
                '11' => '<li class="site-menu-item '.$activ_struktur.'">
                        <a class="animsition-link" href="'.$goto_struktur.'">
                            <i class="site-menu-icon fa-sitemap" aria-hidden="true"></i>
                            <span class="site-menu-title">Struktur Organisasi</span>
                        </a>
                        </li>',
                '12' => '<li class="site-menu-item has-sub '.$activ_recruitment.'">
                        <a href="javascript:void(0)">
                            <i class="site-menu-icon fa-users" aria-hidden="true"></i>
                            <span class="site-menu-title">Recruitment</span>
                            <span class="site-menu-arrow"></span>
                        </a>
                        <ul class="site-menu-sub">
                            <li class="site-menu-item">
                                <a class="animsition-link pl-20" href="'.$goto_tes_wawancara.'">
                                    <i class="site-menu-icon fa-tasks" aria-hidden="true"></i>
                                    <span class="site-menu-title">Wawancara</span>
                                </a>
                            </li>
                            <li class="site-menu-item" hidden>
                                <a class="animsition-link pl-20" href="'.$goto_calon_anggota.'">
                                    <i class="site-menu-icon fa-slideshare" aria-hidden="true"></i>
                                    <span class="site-menu-title">Calon Anggota</span>
                                </a>
                            </li>
                        </ul>
                        </li>',
                '13' => '<li class="site-menu-item has-sub '.$activ_riwayat.'">
                        <a href="javascript:void(0)">
                            <i class="site-menu-icon fa-book" aria-hidden="true"></i>
                            <span class="site-menu-title">Kelola Riwayat</span>
                            <span class="site-menu-arrow"></span>
                        </a>
                        <ul class="site-menu-sub">
                            <li class="site-menu-item">
                                <a class="animsition-link pl-20 " href="'.$goto_riwayat_jabatan.'">
                                <i class="site-menu-icon fa-slideshare" aria-hidden="true"></i>

                                <span class="site-menu-title">Kelola Jabatan</span>
                                </a>
                            </li>
                            <li class="site-menu-item">
                                <a class="animsition-link pl-20 " href="'.$goto_riwayat_kepanitiaan.'">
                                <i class="site-menu-icon fa-slideshare" aria-hidden="true"></i>
                                <span class="site-menu-title">Kepanitiaan</span>
                                </a>
                            </li>
                        </ul>
                        </li>',
                '14' => '<li class="cat-side-menu">
                        Keanggotaan
                        </li>',
                '16' => '<li class="site-menu-item '.$activ_setting.'">
                        <a class="animsition-link" href="'.$goto_setting_pendaftaran.'">
                            <i class="site-menu-icon fa-gear" aria-hidden="true"></i>
                            <span class="site-menu-title">Setting</span>
                        </a>
                        </li>',
            );

            foreach ($rule[$level] as $key => $value) {
                $menu[$key] = $sideMenu[$value];
            }
            return $menu;
    }

    public static function status_pengajuan($ket)
    {

        $status = array(
            '0' =>'<span class="badge text-white bg-teal-600">Diperbaiki</span>&nbsp;',
            '1' =>'<span class="badge badge-secondary">Draf</span>&nbsp;',
            '2' =>'<span class="badge badge-primary">Dikirim ke Ketua UKM</span>&nbsp;',
            '3' =>'<span class="badge bg-indigo-900 text-white">Disetujui Ketua UKM</span>&nbsp;',
            '4' =>'<span class="badge badge-danger">Ditolak Ketua UKM</span>&nbsp;',
            '5' =>'<span class="badge badge-success">Disetujui DPK</span>&nbsp;',
            '6' =>'<span class="badge badge-info">Disetujui DPK dengan catatan</span>&nbsp;',
            '7' =>'<span class="badge badge-secondary">Telah disesuaikan</span>&nbsp;',
            '8' =>'<span class="badge badge-danger">Ditolak DPK</span>&nbsp;',
            '9' =>'<span class="badge badge-danger">Dihapus</span>&nbsp;',
        );
        if (is_numeric($ket)) {
            if (isset($status[$ket])) {
                $data = $status[$ket];
            } else {
                $data = $status[$ket];
            }
        }else{
            $data = '<span class="badge badge-secondary">Belum ada Pengajuan</span>&nbsp;';
        }
        return $data;
    }

    public static function singkatNama($str)
    {
        $namaDepan = array_slice(explode(' ', $str), 0, 1);

        $totalNama = count(explode(' ', $str));

        $namaBelakang = array_slice(explode(' ', $str), -($totalNama)+1);

        foreach ($namaBelakang as $key => $value) {
            $nama[$key] = strtoupper(substr($namaBelakang[$key],0,1)).'. ';
        }

        return implode(' ',array_merge($namaDepan,$nama));
    }

	public static function tanggal_indonesia($tgl, $tampil_hari=true){
		$nama_hari = array("Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jum'at", "Sabtu");
		$nama_bulan = array(1=>"Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
		$tahun = substr($tgl, 0, 4);
		$bulan = $nama_bulan[(int)substr($tgl,5,2)];
		$tanggal = substr($tgl, 8,2);

		$text="";
		if($tampil_hari){
			$urutan_hari = date('w', mktime(0,0,0, substr($tgl, 5,2), $tanggal, $tahun));
			$hari = $nama_hari[$urutan_hari];
			$text .= $hari.", ";
		}
		$text .= $tanggal ." ". $bulan ." ". $tahun;
		return $text;
    }

    public static function uang($nominal){
        if(is_numeric($nominal)){

            if(floor($nominal) != $nominal){
                $nominal = number_format($nominal, 2, ',', '.');
            }else{
                $nominal = number_format($nominal, 0, ',', '.');
            }

        }else{
            $nominal = 0;
        }

        return $nominal;
    }

    public static function encrypt_($id)
    {
        $data =base64_encode(base64_encode(base64_encode(base64_encode(base64_encode(base64_encode(base64_encode($id)))))));

        return $data;
    }

    public static function decrypt_($id)
    {
        $data =base64_decode(base64_decode(base64_decode(base64_decode(base64_decode(base64_decode(base64_decode($id)))))));

        return $data;
    }

    public static function time_remaining($tgl_mulai,$tgl_selesai)
    {
        $now = now()->toDateTimeString();
        $diff1 = strtotime($tgl_mulai) - strtotime($now);
        $diff2 = strtotime($tgl_selesai) - strtotime($now);
        if ($diff1>0) {
            $diff = $diff1;
            $data['realisasi'] = false;
        }elseif($diff1<=0 && $diff2>=0){
            $data['time'] = "Sedang berlangsung";
            $data['code'] = 'bg-teal-a400';
            $data['realisasi'] = true;
            return $data;exit();
        }else{
            if ($tgl_mulai == $tgl_selesai && $tgl_mulai == date('Y-m-d') && $tgl_selesai == date('Y-m-d')) {
                $data['time'] = "Sedang berlangsung";
                $data['code'] = 'bg-teal-a400';
                $data['realisasi'] = true;
                return $data;exit();
            }
            $data['time'] = "Selesai";
            $data['code'] = 'bg-grey-800 text-white';
            $data['realisasi'] = true;
            return $data;exit();
        }


        $years = round($diff / (365*60*60*24));
        $months = round(($diff - $years * 365*60*60*24) / (30*60*60*24));
        $days = round(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
        $hours = round($diff/(60*60));
        $minutes = round($diff/60);
        $second = $diff;

        if ($years != 0) {
            $data['time'] = "$years tahun lagi menuju hari-H";
            $data['code'] = 'bg-indigo-900';
        } elseif ($months != 0) {
            $data['time'] = "$months bulan lagi menuju hari-H";
            if ($months >= 6 && $months <=12) {
                $data['code'] = 'bg-light-blue-a400';
            }else{
                $data['code'] = 'bg-orange-700';
            }
        } elseif ($days != 0) {
            $data['time'] = "$days hari lagi menuju hari-H";
            $data['code'] = 'bg-deep-orange-a400';
        } elseif ($hours != 0) {
            $data['time'] = "$hours jam lagi";
            $data['code'] = 'bg-red-700';
        } elseif ($minutes != 0) {
            $data['time'] = "$minutes menit lagi";
            $data['code'] = "bg-red-800";
        } else {
            $data['time'] = "$second detik lagi";
            $data['code'] = "bg-red-900";
        }

        return $data;
        return $data['time'];
        return $data['code'];
    }

    public static function rupiah2number($uang)
    {
        $x = str_replace("Rp.","",$uang);
        $y = str_replace(".","",$x);
        $z = str_replace(",-","",$y);
        return $z;
    }

    public static function number2rupiah($uang)
    {
        $hasil_rupiah = "Rp. " . number_format($uang,0,',','.').',-';
        return $hasil_rupiah;
    }

    public static function time2jam($jam)
    {
        $jam_new = date('H:i',strtotime($jam));
        return $jam_new;
    }

    public static function jumlahPanitia($id_proker)
    {
        $jumlah_panitia = Panitia::where('id_proker','=',$id_proker)->get()->count();
        return $jumlah_panitia;
    }

    public static function getUrlSubjek()
    {
        $url = url()->current();
        $urlRiwayats = explode('/', $url);
        if ($urlRiwayats[4] == 'public') {
            $urlRiwayat = array_slice($urlRiwayats, 5);
        }else{
            $urlRiwayat = array_slice($urlRiwayats, 4);
        }
        $nmRiwayat = $urlRiwayat[0];
        return $nmRiwayat;
    }

    public static function pesanNotif($action, $idSubjek, $nim, $true_role, $urlSubjek, $nama_tabel, $nama_subjek)
    {
        // $session = Session::get('data_user');
        // $nim = $session['nim'];
        // $true_role = $session['key_menu'];

        // Simpan Notif
        // $urlSubjek = DeHelper::getUrlSubjek();
        // $idSubjek = $request->idSubjek;
        // $action = $request->action;

        // $action = 'tolak';
        // $idSubjek = 2;
        // golek jeneng
        try {
            $namaUser = DB::table('keanggotaan.anggota')->where('nim','=',$nim)->get()->first()->nama;
        } catch (\Throwable $th) {
            $namaUser = DB::table('master.m_dpk')->where('nidn','=',$nim)->get()->first()->nama;
        }
        // return $namaUser ;
        $role = DeHelper2::getRole($true_role);


        if ($urlSubjek == 'proposal' || $urlSubjek == 'lpj') {
            $proker_id = DB::table($nama_tabel)->where('id','=',$idSubjek)->get()->first()->proker_id;
            $nama = proker::find($proker_id)->nama;
            $nama_show = $nama_subjek.' dari proker '.$nama;
        }else{
            $nama = DB::table($nama_tabel)->where('id','=',$idSubjek)->get()->first()->nama;
            $nama_show = $nama_subjek.' '.$nama;
        }
        $pesan = [
            'perbaikan' => "$nama_show telah diperbaiki oleh $role",
            'tambah' => "Pengajuan $nama_show oleh $role",
            'kirim_ke_ketua' => "Pengajuan $nama_show oleh $role",
            'approve' => "Pengajuan $nama_show disetujui oleh $role",
            'tolak' => "Pengajuan $nama_show ditolak oleh $role",
            'approve_dc' => "Pengajuan $nama_show disetujui oleh $role, namun dengan catatan",
            'penyesuaian' => "$nama_show telah disesuaikan oleh $role",
        ];

        if (isset($pesan[$action])) {
            $data = $pesan[$action];
        } else {
            $data = $pesan[$action];
        }
        return $data;
    }

    public static function getLamaWaktu($created_at)
    {
        $now = now()->toDateTimeString();;
        $diff = abs(strtotime($now) - strtotime($created_at));

        $years = round($diff / (365*60*60*24));
        $months = round(($diff - $years * 365*60*60*24) / (30*60*60*24));
        $days = round(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
        $hours = round($diff/(60*60));
        $minutes = round($diff/60);
        $second = $diff;

        if ($years != 0) {
            $time = "$years tahun yang lalu";
        } elseif ($months != 0) {
            $time = "$months bulan yang lalu";
        } elseif ($days != 0) {
            $time = "$days hari yang lalu";
        } elseif ($hours != 0) {
            $time = "$hours jam yang lalu";
        } elseif ($minutes != 0) {
            $time = "$minutes menit yang lalu";
        } else {
            $time = "$second detik yang lalu";
        }

        return $time;
    }

    public static function getNotifIcon($status)
    {
        if ($status == '1' || $status == '2') {
            return '<i class="icon oi-file-text bg-purple-600 white icon-circle" aria-hidden="true"></i>';
        }elseif ($status == '3') { // disetujui ketua
            return '<i class="icon oi-file-text bg-teal-600 white icon-circle" aria-hidden="true"></i>';
        }elseif ($status == '4') { // ditolak ketua
            return '<i class="icon oi-file-text bg-deep-orange-600 white icon-circle" aria-hidden="true"></i>';
        }elseif ($status == '5') { // disetujui DPK
            return '<i class="icon oi-file-text bg-green-600 white icon-circle" aria-hidden="true"></i>';
        }elseif ($status == '8') { // ditolak ketua
            return '<i class="icon oi-file-text bg-red-600 white icon-circle" aria-hidden="true"></i>';
        }elseif ($status == '0') { // diperbaiki
            return '<i class="icon md-border-color bg-amber-600 white icon-circle" aria-hidden="true"></i>';
        }elseif ($status == '7') { // disesuaikan
            return '<i class="icon md-border-color bg-yellow-600 white icon-circle" aria-hidden="true"></i>';
        }elseif ($status == '6') { // disetujui dengan catatan
            return '<i class="icon oi-file-text bg-blue-600 white icon-circle" aria-hidden="true"></i>';
        }
    }

    public static function singkatJurusan($string){
        $text = Str::upper($string);
        if($text == "AKUNTANSI"){
            return 'AK';
        }
        $potongan = explode(' ', $string);
        foreach ($potongan as $key => $value) {
            $nama[$key] = strtoupper(substr($potongan[$key],0,1));
        }
        return implode('',$nama);
    }

    public static function getNamaBulan($angkaBulan)
    {
        $arrNamaBulan = array(
            1 => "Januari",
            2 => "Februari",
            3 => "Maret",
            4 => "April",
            5 => "Mei",
            6 => "Juni",
            7 => "Juli",
            8 => "Agustus",
            9 => "September",
            10 => "Oktober",
            11 => "November",
            12 => "Desember"
        );
        if (is_numeric($angkaBulan)) {
            if (isset($arrNamaBulan[$angkaBulan])) {
                $data = $arrNamaBulan[$angkaBulan];
            } else {
                $data = $arrNamaBulan[$angkaBulan];
            }
        }else{
            $data = $arrNamaBulan;
        }
        return $data;
    }

    public static function presentase_proker($ket)
    {

        $status = array(
            '0' =>'2/8',
            '1' =>'1/8',
            '2' =>'2/8',
            '3' =>'4/8',
            '4' =>'1/8',
            '5' =>'8/8',
            '6' =>'6/8',
            '7' =>'7/8',
            '8' =>'1/8',
            '9' =>'0/8',
        );
        if (is_numeric($ket)) {
            if (isset($status[$ket])) {
                $data = $status[$ket];
            } else {
                $data = $status[$ket];
            }
        }else{
            $data = $status;
        }
        return $data;
    }

    public static function presentase_prolpj($ket)
    {

        $status = array(
            '0' =>'2/8',
            '2' =>'2/8',
            '3' =>'4/8',
            '4' =>'1/8',
            '5' =>'8/8',
            '8' =>'1/8',
        );
        if (is_numeric($ket)) {
            if (isset($status[$ket])) {
                $data = $status[$ket];
            } else {
                $data = $status[$ket];
            }
        }else{
            $data = $status;
        }
        return $data;
    }

    public static function setting_pendaftaran()
    {
        $status_setting = DB::table('setting')->where('nama_settings','pendaftaran')->get()->first()->status;

        if ($status_setting == true) {
            return '';
        }else{
            return 'hidden';
        }
    }

    public static function setting_hasil_pendaftaran()
    {
        $status_setting = DB::table('setting')->where('nama_settings','hasil_recruitment')->get()->first()->status;

        if ($status_setting == true) {
            return '';
        }else{
            return 'hidden';
        }
    }



}
