<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\m_sie_kepanitiaan;
use DataTables;
use DB;
// use DeHelper;
use Session;

class m_sie_kepanitiaanController extends Controller
{
    public function index( Request $request){
        $query = DB::table('master.m_sie_kepanitiaan')->get();


        // return $query;
        $data = array();
        $no =1 ;

        foreach ($query as $key => $value) {
            $data[$key]['no'] = $no++;
            $data[$key]['id'] = $value->id;
            $data[$key]['nama'] = $value->nama;
        }

        //  return $data;

        if ($request->ajax()){
            return DataTables::of($data)
            ->addColumn('aksi', function($data){

                $buttonEdit ='<a  class="btn btn-sm btn-icon btn-pure btn-default  deleteData"
                        data-toggle="tooltip"
                        data-id="'.$data['id'].'"
                        data-nama="'.$data['nama'].'"
                        data-original-title="Edite">
                        <i class="icon wb-trash" aria-hidden="true"></i>
                        </a>';
               $buttonHapus='<a class="btn btn-sm btn-icon btn-pure btn-default editData"
                data-toggle="tooltip"
                data-id="'.$data['id'].'"
                data-nama="'.$data['nama'].'"
                data-original-title="Edite"> <i class="icon wb-edit"
                aria-hidden="true"></i></a>';

                return $buttonHapus.$buttonEdit;
            })
            ->rawColumns(['aksi'])
            ->make(true);
        }

        return view('master.m_sie_kepanitiaan');

    }

    public function tambahKepanitiaan(Request $request){

        $insert = [
            'id' => $request->id,
            'nama' => $request->nama
        ];

        $query = m_sie_kepanitiaan::updateOrCreate(['id' => $request->id], $insert);

        if (isset($request->id) || $request->id !='') {
            $data="Sukses Update data Kepanitiaan!";

            # code...
        } else {
            $data ="Sukses Tambah data Kepanitiaan!";
            # code...
        }
        return response()->json($data);

    }

    public function destroy(Request $request,$id)
    {
        //destroyyyyyyyy
        $query = DB::table('master.m_sie_kepanitiaan')
        ->where('id', $id)->delete();
        if ( $query == true ) {

            $data = "Sukses hapus data Kepanitiaan!";
        } else {

            $data ="Gagal hapus data Kepanitiaan!";
        }
        return response()->json($data);
    }

    public function select2Kepanitiaan(Request $request)
    {
        $search = $request->search;
        $tipe_event = (isset($request->tipe_event)) ? $request->tipe_event : 666;
        // return $tipe_event;
        $query = DB::table('master.m_sie_kepanitiaan')
                ->where([
                    ['nama','ilike','%'.$search.'%']
                ])
                ->when($tipe_event, function ($query) use ($tipe_event) {
                    if ($tipe_event == 1) {
                        return $query ->whereIn('id',[10]);
                    }elseif ($tipe_event == 2) {
                        return $query ->whereNotIn('id',[1,10]);
                    }elseif ($tipe_event == 666 || $tipe_event == 0) {
                        return $query ->where('id','=', 666);
                    }else{
                        return $query;
                    }
                })                
                ->orderBy('id','asc')
                ->get();   
                
        $data = array();
        foreach ($query as $key => $value) {
            $data[$key]['id']=$value->id;
            $data[$key]['nama']=$value->nama;
        }
        // $array[] = array('a'=>'a');
        return response()->json($data);
    }   

}
