<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Hash;
use App\Models\akun;
use App\Models\anggota;
use Session;
use App\Models\m_agama;
use App\Models\m_kelas;
use App\Models\m_prodi;
use File;
use DataTables;
use DeHelper;

class anggotaController extends Controller
{

    public function getkelpro(Request $request, $nim)
    {
        $query = DB::table('keanggotaan.anggota as ang')
                    ->leftjoin('master.m_kelas as mkls','mkls.id','=','ang.id_kelas')
                    ->leftjoin('master.m_prodi as mprd','mprd.id','=','ang.id_prodi')
                    ->selectRaw("ang.nama as nama_mhs, mkls.nama as nama_kelas, mprd.nama as nama_prodi")
                    ->where('ang.nim','=',$nim)
                    ->get()->first();
        return response()->json($query);
        return $query;
        # code...
    }

    public function select2Anggota(Request $request)
    {
        $search = $request->search;
        $query = DB::table('keanggotaan.anggota')
                ->where('is_anggota','=',3)
                ->whereRaw("( nim ilike '%".$search."%' or nama ilike '%".$search."%' )")
                ->orderBy('nama','asc')
                ->get();

        $data = array();
        foreach ($query as $key => $value) {
            $data[$key]['id']=$value->nim;
            $data[$key]['nama']='( '.$value->nim.' ) '.$value->nama;
        }
        // $array[] = array('a'=>'a');
        return response()->json($data);
    }

    public function select2AnggotaPanitia(Request $request)
    {
        $search = $request->search;
        $id_proker = $request->id_proker;
        $nim_panitia = DB::table('kegiatan.kepanitiaan')
                        ->where('id_proker','=',$id_proker)
                        ->select('nim')
                        ->pluck('nim')->toArray();

        // return $nim_panitia;
        $query = DB::table('keanggotaan.anggota')
                ->where('is_anggota','=',3)
                ->whereRaw("( nim ilike '%".$search."%' or nama ilike '%".$search."%' )")
                ->whereNotIn('nim', $nim_panitia)
                ->orderBy('nama','asc')
                ->get();

        $data = array();
        foreach ($query as $key => $value) {
            $data[$key]['id']=$value->nim;
            $data[$key]['nama']='( '.$value->nim.' ) '.$value->nama;
        }
        // $array[] = array('a'=>'a');
        return response()->json($data);
    }

    public function select2AnggotaRiwayat(Request $request)
    {
        $search = $request->search;
        $query = DB::table('keanggotaan.anggota')
                ->where('is_anggota','=',3)
                // ->where('is_pengurus','=',false)
                ->whereRaw("( nim ilike '%".$search."%' or nama ilike '%".$search."%' )")
                ->orderBy('nama','asc')
                ->get();

        $data = array();
        foreach ($query as $key => $value) {
            $data[$key]['id'] = $value->nim;
            $data[$key]['nama'] = $value->nama;
        }
        // $array[] = array('a'=>'a');
        return response()->json($data);
    }

    public function select2AnggotaAll(Request $request)
    {
        $search = $request->search;
        $query = DB::table('keanggotaan.anggota')
                ->whereIn('is_anggota',[3,4])
                ->whereRaw("( nim ilike '%".$search."%' or nama ilike '%".$search."%' )")
                ->orderBy('nama','asc')
                ->get();

        $data = array();
        foreach ($query as $key => $value) {
            $data[$key]['id']=$value->nim;
            $data[$key]['nama']='( '.$value->nim.' ) '.$value->nama;
        }
        // $array[] = array('a'=>'a');
        return response()->json($data);
    }

    public function index_new_anggota(Request $request)
    {
        $data_lulus_tetulis = DB::table('keanggotaan.calon_anggota as ang')
                                ->leftjoin('master.m_kelas as mkls','mkls.id','=','ang.id_kelas')
                                ->leftjoin('master.m_prodi as mprd','mprd.id','=','ang.id_prodi')
                                ->selectRaw("ang.*, mkls.nama as nama_kelas, mprd.nama as nama_prodi")
                                ->where('is_anggota','=',2)
                                ->get();
        $data['data_tes_tulis'] = $data_lulus_tetulis;
        return view('anggota.baru', $data);

    }

    public function simpan_new_anggota(Request $request)
    {
        // return $request->all();
        $data_calon_ang = DB::table('keanggotaan.calon_anggota')
                            ->whereIn('nim',$request->nim_anggota_baru)
                            ->orderBy('nim','asc')
                            ->get();

        $cek_nim = '';

        foreach ($data_calon_ang as $key => $value) {

            if ($cek_nim == $value->nim) {
            }else{
                $data_new['alamat'] = $value->alamat;
                $data_new['angkatan'] = $value->angkatan;
                $data_new['email'] = $value->email;
                $data_new['foto'] = $value->foto;
                $data_new['id_agama'] = $value->id_agama;
                $data_new['id_desa'] = $value->id_desa;
                $data_new['id_kecamatan'] = $value->id_kecamatan;
                $data_new['id_kelas'] = $value->id_kelas;
                $data_new['id_kota'] = $value->id_kota;
                $data_new['id_prodi'] = $value->id_prodi;
                $data_new['id_provinsi'] = $value->id_provinsi;
                $data_new['is_anggota'] = 3;
                $data_new['is_pengurus'] = false;
                $data_new['jenis_kelamin'] = $value->jenis_kelamin;
                $data_new['kode_struktur'] = $value->kode_struktur;
                $data_new['kode_struktur_detail'] = $value->kode_struktur_detail;
                $data_new['nama'] = $value->nama;
                $data_new['nim'] = $value->nim;
                $data_new['no_hp'] = $value->no_hp;
                $data_new['qr_code'] = $value->qr_code;
                $data_new['tanggal_lahir'] = $value->tanggal_lahir;
                $data_new['created_at'] = $value->created_at;
                $data_new['updated_at'] = $value->updated_at;
                $data_new['tmt'] = date('Y-m-d');
                $cek_nim = $value->nim;
            }
            DB::table('keanggotaan.anggota')->insert($data_new);


            $username = $value->nim;
            $password = hash::make($value->nim);


            $akun = new akun;
            $akun->nim = $username;
            $akun->username = $username;
            $akun->password = $password;
            $akun->save();

            DB::table('keanggotaan.calon_anggota')
            ->where('nim','=',$value->nim)
            ->update(['is_anggota' => 3]);

            DB::table('setting')->where('nama_settings','hasil_recruitment')->update(['status' =>true]);
            // return response()->json('sukses lur');

        }

        // return response()->json($data_new);

        // if (isset($request->nim_anggota_baru)) {
        //     foreach ($request->nim_anggota_baru as $key => $value) {
        //         DB::table('keanggotaan.anggota')
        //             ->where('keanggotaan.anggota.nim','=',$request->nim_anggota_baru[$key])
        //             ->update(['is_anggota' => 3]);
        //     }
        // }
        $data['code']="200";
        $data['message']="Data anggota baru berhasil disimpan";
        return response()->json($data);exit();
    }

    public function profilAnggota(Request $request)
    {
        $user 		= Session::get("data_user");
        $flag_lv = $user['flag_lv'];
        $nim = $user['nim'];
        $key_menu = $user['key_menu'];

        $data['data_ang'] = DB::table('keanggotaan.anggota as ang')
                        ->leftjoin('master.m_provinsi as prov','prov.id','=','ang.id_provinsi')
                        ->leftjoin('master.m_kota as kota','kota.id','=','ang.id_kota')
                        ->leftjoin('master.m_kecamatan as kec','kec.id','=','ang.id_kecamatan')
                        ->leftjoin('master.m_desa as desa','desa.id','=','ang.id_desa')
                        ->leftjoin('master.m_agama as ag','ag.id','=','ang.id_agama')
                        ->leftjoin('master.m_kelas as mkls','mkls.id','=','ang.id_kelas')
                        ->leftjoin('master.m_prodi as mprd','mprd.id','=','ang.id_prodi')
                        ->leftjoin(DB::raw("(SELECT A.* FROM keanggotaan.riwayat_jabatan
                            A JOIN ( SELECT nim, MAX(tmt_sk) AS iid FROM keanggotaan.riwayat_jabatan GROUP BY nim ) B ON A.tmt_sk = B.iid
                            AND A.nim = B.nim ) AS last_jabatan"),'last_jabatan.nim','=','ang.nim')
                        ->leftjoin('master.m_struktur_detail as sd','sd.id','=','last_jabatan.kode_struktur_detail')
                        ->selectRaw("ang.*, mkls.nama as nama_kelas, mprd.nama as nama_prodi,
                                    sd.nama as jabatan, prov.nama as nama_provinsi, kota.nama as nama_kota,
                                    kec.nama as nama_kecamatan, desa.nama as nama_desa, ag.nama as nama_agama")
                        ->where('ang.nim',$nim)
                        ->get()->first();
        // get Agm
        $data['agama'] = m_agama::all();
        // get kelas
        $data['kelas'] = m_kelas::all();
        //get prodi
        $data['prodi'] = m_prodi::all();

                        // return (array)$data_ang;
        return view('anggota.profil_anggota',$data);
    }

    public function simpanProfilAnggota(Request $request)
    {
        // return $request->all();
        $id = (isset($request->id)) ?  $request->id : null;
        $nama = (isset($request->nama)) ?  $request->nama : null;
        $email = (isset($request->email)) ?  $request->email : null;
        $id_agama = (isset($request->id_agama)) ?  $request->id_agama : null;
        $id_desa = (isset($request->id_desa)) ?  $request->id_desa : null;
        $id_kecamatan = (isset($request->id_kecamatan)) ?  $request->id_kecamatan : null;
        $id_kelas = (isset($request->id_kelas)) ?  $request->id_kelas : null;
        $id_kota = (isset($request->id_kota)) ?  $request->id_kota : null;
        $id_prodi = (isset($request->id_prodi)) ?  $request->id_prodi : null;
        $id_provinsi = (isset($request->id_provinsi)) ?  $request->id_provinsi : null;
        $jenis_kelamin = (isset($request->jenis_kelamin)) ?  $request->jenis_kelamin : null;
        $nim = (isset($request->nim)) ?  $request->nim : null;
        $no_hp = (isset($request->no_hp)) ?  $request->no_hp : null;
        $tanggal_lahir = (isset($request->tanggal_lahir)) ?  $request->tanggal_lahir : null;
        $foto_v = (isset($request->foto_v)) ?  $request->foto_v : null;
        $file = (isset($request->foto)) ? $request->file('foto') : null;

        $angkatan = DB::table('keanggotaan.anggota')->where('is_anggota',3)->orderBy('angkatan','desc')->get()->first()->angkatan + 1;

        $data = [
            'nama' => $nama,
            'email' => $email,
            'id_agama' => $id_agama,
            'id_desa' => $id_desa,
            'id_kecamatan' => $id_kecamatan,
            'id_kelas' => $id_kelas,
            'id_kota' => $id_kota,
            'id_prodi' => $id_prodi,
            'id_provinsi' => $id_provinsi,
            'jenis_kelamin' => $jenis_kelamin,
            'nim' => $nim,
            'no_hp' => $no_hp,
            'tanggal_lahir' => $tanggal_lahir,
        ];

        $data_additional = [];
        if($file != null){
            $nama_file = 'foto_'.$nim.'_'.str_replace(' ','_',$nama).'_'.$angkatan.'.'.$file->getClientOriginalExtension();
            File::delete(public_path().'/'.$nama_file);
            $file->move(public_path('foto_anggota'), $nama_file);
            $data_additional = ['foto' => $nama_file];
        }

        $data_pendaftar = array_merge($data+$data_additional);
            // return $data_pendaftar ;
        $prop = anggota::updateOrCreate(['id' => $id], $data_pendaftar);

        return response()->json('sukses lurr');exit();
    }

    public function ganti_password(Request $request)
    {
        $user 		= Session::get("data_user");
        $flag_lv = $user['flag_lv'];
        $nim = $user['nim'];
        $key_menu = $user['key_menu'];


        $query = akun::leftjoin('keanggotaan.anggota as ang','ang.nim','=','keanggotaan.akun.nim')
                    ->leftjoin('master.m_kelas as mkls','mkls.id','=','ang.id_kelas')
                    ->leftjoin('master.m_prodi as mprd','mprd.id','=','ang.id_prodi')
                    ->when($flag_lv, function ($query) use ($flag_lv,$nim) {
                    if($flag_lv == "c") {
                        return $query;
                    }else{
                        return $query->where('keanggotaan.akun.nim',$nim);
                    }})
                    ->selectRaw("keanggotaan.akun.*, ang.nama as nama_mhs, mkls.nama as nama_kelas, mprd.nama as nama_prodi")
                    ->get();
        // return $query;
        $data = array();
        $no =1 ;

        foreach ($query as $key => $value) {
            $data[$key]['no'] = $no++;
            $data[$key]['id'] = $value->id;
            $data[$key]['nim'] = $value->nim;
            $data[$key]['nama_mhs'] = $value->nama_mhs;
            $data[$key]['kelas_prodi'] = $value->nama_kelas.' / '.DeHelper::singkatJurusan($value->nama_prodi);
            $data[$key]['nama_prodi'] = $value->nama_prodi;
            $data[$key]['username'] = $value->username;
        }

        if ($request->ajax()){
            return DataTables::of($data)
            ->addColumn('aksi', function($data){

                // $buttonDelete ='<a  class="btn btn-sm btn-icon btn-pure btn-default deleteData"
                //         data-toggle="tooltip"
                //         data-id="'.$data['id'].'"
                //         data-nim="'.$data['nim'].'"
                //         data-nama_mhs="'.$data['nama_mhs'].'"
                //         data-nama_prodi="'.$data['nama_prodi'].'"
                //         data-username="'.$data['username'].'"
                //         >
                //         <i class="icon wb-trash" aria-hidden="true"></i></a>';
                $buttonEdit='<a class="btn btn-sm text-white btn-primary btnEdit"
                        data-toggle="tooltip"
                        data-id="'.$data['id'].'"
                        data-nim="'.$data['nim'].'"
                        data-nama_mhs="'.$data['nama_mhs'].'"

                        data-nama_prodi="'.$data['nama_prodi'].'"
                        data-username="'.$data['username'].'"
                        > <i class="icon wb-edit" aria-hidden="true"></i> Ubah Password</a>';

                return $buttonEdit;
            })
            ->rawColumns(['aksi'])
            ->make(true);
        };

        return view('anggota.ganti_password');
    }

    public function simpanPerubahanPass(Request $request)
    {
        // return $request->all();

        $nim = $request->nim;
        $username = $request->username;
        $password = hash::make($request->password);

        $akun = akun::where('nim', $nim)
                    ->update(['username' => $username, 'password' => $password]);

        return response()->json('data berhasil di refresh');
    }

    public function cetakKartuAnggota(Request $request)
    {
        $user 		= Session::get("data_user");
        $flag_lv = $user['flag_lv'];
        $nim = $user['nim'];
        $key_menu = $user['key_menu'];

        $property = anggota::leftJoin(DB::raw('(SELECT A.* FROM  keanggotaan.riwayat_jabatan A
                        JOIN ( SELECT nim, MAX ( tmt_sk ) AS maxTanggal FROM keanggotaan.riwayat_jabatan GROUP BY nim
                        ) B ON A.tmt_sk = B.maxTanggal AND A.nim = B.nim) as rijab'),'rijab.nim','=','keanggotaan.anggota.nim')
                        ->leftjoin('master.m_struktur_detail as sd','sd.id','=','rijab.kode_struktur_detail')
                        ->leftjoin('master.m_kelas as mkls','mkls.id','=','keanggotaan.anggota.id_kelas')
                        ->leftjoin('master.m_prodi as mprd','mprd.id','=','keanggotaan.anggota.id_prodi')
                        ->selectRaw("keanggotaan.anggota.*, mkls.nama as nama_kelas, mprd.nama as nama_prodi, sd.nama as nama_jabatan")
                        ->where('keanggotaan.anggota.nim',$nim)->get()->first();

        $send['data'] = $property;
        $send['link_qr'] = asset('qr_code_anggota/').'/'.$property->qr_code;
        // return $send;
        return view('cetak_kartu_anggota', $send);
    }
}
