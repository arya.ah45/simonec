<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\m_agama;
use DataTables;
use DB;
// use DeHelper;
use Session;

class m_agamaControler extends Controller
{
    public function index( Request $request){
        $query = DB::table('master.m_agama')->get();


        // return $query;
        $data = array();
        $no =1 ;

        foreach ($query as $key => $value) {
            $data[$key]['no'] = $no++;
            $data[$key]['id'] = $value->id;
            $data[$key]['nama'] = $value->nama;
        }

        //  return $data;

        if ($request->ajax()){
            return DataTables::of($data)
            ->addColumn('aksi', function($data){

                $buttonEdit ='<a  class="btn btn-sm btn-icon btn-pure btn-default  deleteData"
                        data-toggle="tooltip"
                        data-id="'.$data['id'].'"
                        data-nama="'.$data['nama'].'"
                        data-original-title="Edite">
                        <i class="icon wb-trash" aria-hidden="true"></i>
                        </a>';
               $buttonHapus='<a class="btn btn-sm btn-icon btn-pure btn-default editData"
                data-toggle="tooltip"
                data-id="'.$data['id'].'"
                data-nama="'.$data['nama'].'"
                data-original-title="Edite"> <i class="icon wb-edit"
                aria-hidden="true"></i></a>';

                return $buttonHapus.$buttonEdit;
            })
            ->rawColumns(['aksi'])
            ->make(true);
        }

        return view('master.agama');

    }

    public function tambahAgama(Request $request){

        $insert = [
            'id' => $request->id,
            'nama' => $request->nama
        ];

        $query = m_agama::updateOrCreate(['id' => $request->id], $insert);

        if (isset($request->id) || $request->id !='') {
            $data="Sukses Update data Agama!";

            # code...
        } else {
            $data ="Sukses Tambah data Agama!";
            # code...
        }
        return response()->json($data);

    }


    public function destroy(Request $request,$id)
    {
        //destroyyyyyyyy
        $query = DB::table('master.m_agama')
        ->where('id', $id)->delete();
    if ( $query == true ) {

        $data = "Sukses hapus data Agama!";
    } else {

        $data ="Gagal hapus data Agama!";
    }
    return response()->json($data);
    }


}
