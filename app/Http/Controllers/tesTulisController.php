<?php

namespace App\Http\Controllers;
use RealRashid\SweetAlert\Facades\Alert;

use Illuminate\Http\Request;
use App\Models\m_soal;
use App\Models\jawaban_tes;
use DB;

class tesTulisController extends Controller
{
    public function index(Request $request)
    {
        $send['soal'] = m_soal::all();
        $send['jumlah_soal'] = count($send['soal']);
        // return $send;
        return view('calon_anggota.tes_tulis',$send);
    }

    public function cekTesTulis(Request $request)
    {
        $nim = (isset($request->nim)) ? $request->nim : '';
        $cek_data = DB::table('keanggotaan.calon_anggota')
                        ->where([
                            ['nim','=',$nim],
                            ['is_anggota','=',0],
                        ])->get();

        if (count($cek_data) != 0) {
            $data_anggota = DB::table('keanggotaan.calon_anggota as ang')
                        ->leftjoin('master.m_kelas as mkls','mkls.id','=','ang.id_kelas')
                        ->leftjoin('master.m_prodi as mprd','mprd.id','=','ang.id_prodi')
                        ->selectRaw("ang.nim as nim_mhs, ang.nama as nama_mhs, mkls.nama as nama_kelas, mprd.nama as nama_prodi")
                        ->where('ang.nim','=',$nim)
                        ->get()->first();

            $data['code']="1";
            $data['message']="Data OK!";
            $data['data'] = $data_anggota;
            return response()->json($data);
        }else{
            $data['code']="0";
            $data['message']="QR Code tidak sesuai!";
            $data['data'] = [];
            Alert::error('Anda Tidak Terdaftar', 'QR Code tidak sesuai!');
            return response()->json($data);
        }
    }

    public function simpanJawaban(Request $request)
    {
        // return $request->all();
        $simpan = [];
        foreach ($request->id_soal as $key => $value) {
            $simpan['nim_calon'] = $request->nim;
            $simpan['id_tes'] = $request->id_soal[$key];
            $simpan['jawaban'] = $request->jawaban[$key];
            jawaban_tes::create($simpan);
        }

        DB::table('keanggotaan.calon_anggota as ang')
            ->where('nim','=',$request->nim)
            ->update([
                'is_anggota' => 1,
            ]);
        // return response()->json($simpan);

        return response()->json('ok');



    }
}
