<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use DB;
use DeHelper;

class recruitmentController extends Controller
{
    public function index(Request $request)
    {
       return view('recruitment\calon_anggota');
    }

    
    public function datatableCalonAnggota(Request $request)
    {
        $user = Session::get("data_user");
        $flag_lv = $user['flag_lv'];
        $nim = $user['nim'];
        $key_menu = $user['key_menu'];

        $searchs = $request->searchs;

        if (isset($request->nim)) {
            $nim = $request->nim;
        
        }
        $jenis_status = '';
        if (isset($request->jenis)) {
            $jenis_status = $request->jenis;
        }else{
            $jenis_status = 666;
        }

        $limit = $request->limit;
        $offset = $request->offset;
        $data=[];

        $tahun = date('Y');

        $data_Raw = DB::table('keanggotaan.calon_anggota as ang')
            ->leftjoin('master.m_kelas as kls','kls.id','=','ang.id_kelas')
            ->leftjoin('master.m_prodi as prod','prod.id','=','ang.id_prodi')
            ->where('ang.is_anggota','=',$jenis_status)   
            ->where(DB::raw('EXTRACT(YEAR FROM ang.created_at)'),'=',$tahun)                                
            ->selectRaw("ang.*, ang.nama as nama_anggota, kls.id as id_kelas, kls.nama as kelas, prod.id as id_prodi, prod.nama as prodi")
            ->orderBy('ang.created_at','asc')         
            ->limit($limit)
            ->offset($offset);
            
        $data_filtered = $data_Raw->when($searchs, function ($query) use ($searchs) {
                        if ($searchs) {
                            return $query->whereRaw("( ang.nama ilike '%".$searchs."%' || kls.nama ilike '%".$searchs."%' || prod.nama ilike '%".$searchs."%')");
                        }
                    });
        $data_count_Raw = $data_Raw->count();
        $data_count_filtered = $data_filtered->count();
        $data_isi = $data_filtered->get();
                
        // return $data_isi;

        $no = 1;
        foreach ($data_isi as $value) {
            $id_anggota = DeHelper::encrypt_($value->id);
            $nim = (isset($value->nim)) ? $value->nim : '';
            $nama = (isset($value->nama)) ? $value->nama : '';
            $kelas = (isset($value->kelas)) ? $value->kelas : '';
            $prodi = (isset($value->prodi)) ? $value->prodi : '';

            $dataTabel[] = array(
                'no' => $no++,
                'id_anggota' => $id_anggota,
                'nim' => $nim,
                'nama' => $nama,
                'kelas' => $kelas,
                'prodi' => $prodi,
            );
        }
        // return $dataTabel;

        $recordsTotal = is_null($data_count_Raw) ? 0 : $data_count_Raw;
        $recordsFiltered = is_null($data_count_filtered) ? 0 : $data_count_filtered;

        $data_ = (isset($dataTabel)?$dataTabel:[]);

        $data['recordsTotal'] = $recordsTotal;
        $data['recordsFiltered'] = $recordsFiltered;
        $data['data'] = $data_;

        return $data;
    }        
}
