<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\riwayat_jabatan;
use App\Models\Panitia;
use Session;
use DB;
use DeHelper;
use App\Helpers\DeHelper2;

class riwayatController extends Controller
{
    public function index_riwayat_jabatan(Request $request)
    {
        return view('riwayat.riwayat_jabatan');
    }

    public function datatableRiwayatJabatan(Request $request)
    {
        $user 		= Session::get("data_user");
        $flag_lv = $user['flag_lv'];
        $nim = $user['nim'];
        $key_menu = $user['key_menu'];

        $searchs = $request->searchs;
        if (isset($request->nim)) {
            $nim = $request->nim;
        }
        $limit = $request->limit;
        $offset = $request->offset;
        $data=[];

        $data_Raw = riwayat_jabatan::leftjoin('keanggotaan.anggota as ang','ang.nim','=','keanggotaan.riwayat_jabatan.nim')
            ->leftjoin('master.m_kelas as kls','kls.id','=','ang.id_kelas')
            ->leftjoin('master.m_prodi as prod','prod.id','=','ang.id_prodi')
            ->leftjoin('master.m_struktur_detail as sd','sd.id','=','keanggotaan.riwayat_jabatan.kode_struktur_detail')
            ->where('keanggotaan.riwayat_jabatan.nim','=',$nim)
            ->selectRaw("keanggotaan.riwayat_jabatan.*, sd.nama as nama_jabatan, ang.nama as nama_pejabat, kls.id as id_kelas, kls.nama as kelas, prod.id as id_prodi, prod.nama as prodi")
            ->orderBy('keanggotaan.riwayat_jabatan.tmt_sk','desc')
            ->limit($limit)
            ->offset($offset);

        $data_filtered = $data_Raw->when($searchs, function ($query) use ($searchs) {
                        if ($searchs) {
                            return $query->whereRaw("( sd.nama ilike '%".$searchs."%' )");
                        }
                    });
        $data_count_Raw = $data_Raw->count();
        $data_count_filtered = $data_filtered->count();
        $data_isi = $data_filtered->get();

        // return $data_isi;

        $no = 1;
        foreach ($data_isi as $value) {
            $id_proposal = DeHelper::encrypt_($value->id);
            $id = $value->id;
            $nim = (isset($value->nim)) ? $value->nim : '';
            $kode_struktur = (isset($value->kode_struktur)) ? $value->kode_struktur : '';
            $kode_struktur_detail = (isset($value->kode_struktur_detail)) ? $value->kode_struktur_detail : '';
            $no_sk = (isset($value->no_sk)) ? $value->no_sk : '';
            $tmt_sk = (isset($value->tmt_sk)) ? $value->tmt_sk : '';
            $nama_jabatan = (isset($value->nama_jabatan)) ? $value->nama_jabatan : '';
            $nama_pejabat = (isset($value->nama_pejabat)) ? $value->nama_pejabat : '';
            $id_kelas = (isset($value->id_kelas)) ? $value->id_kelas : '';
            $kelas = (isset($value->kelas)) ? $value->kelas : '';
            $id_prodi = (isset($value->id_prodi)) ? $value->id_prodi : '';
            $prodi = (isset($value->prodi)) ? $value->prodi : '';
            $status_keanggotaan = (isset($value->status_keanggotaan)) ? $value->status_keanggotaan : '';
            $ubah = (isset($value->status_keanggotaan)) ? $value->status_keanggotaan : '';
            $aa = 1 ;
            $ab  = 0;
            if (date('Y') > substr($tmt_sk,0,4)) {
                $stat = '<span class="badge badge-secondary">Pensiun</span>';
            }elseif (date('Y') == substr($tmt_sk,0,4)) {
                $stat = '<span class="badge badge-success">Berlangsung</span>';
            }else{
                $stat = '<span class="badge badge-primary">Kandidat</span>';
            }

            if ($status_keanggotaan == 1) {
                $status_keanggotaan = '<span class="badge badge-success">Aktif</span>';
            } else {
                $status_keanggotaan = '<span class="badge badge-danger">tidak Aktif</span>';
            }

            if ($ubah == 1) {
                $ubah = '<a type="button"  class="btn btn-xs bg-success text-white btnUbah"
                data-id="'.$id.'"
                data-nama="'.$nama_pejabat.'"
                data-stat="'.$ab.'"
                data-ubah="'.$ubah.'">
                <i class="icon fa-check "></i> Aktif </a>';
            } else {
                $ubah = '<a type="button"  class="btn btn-xs bg-danger text-white btnUbah"
                data-id="'.$id.'"
                data-nama="'.$nama_pejabat.'"
                data-stat="'.$aa.'"
                data-ubah="'.$ubah.'">
                <i class="icon fa-times "></i> Tidak Aktif </a>';
            }

            $dataTabel[] = array(
                'no' => $no++,
                'id_proposal' => $id_proposal,
                'nim' => $nim,
                'kode_struktur' => $kode_struktur,
                'kode_struktur_detail' => $kode_struktur_detail,
                'no_sk' => $no_sk,
                'tmt_sk' => $tmt_sk,
                'tgl_sk' => DeHelper::format_tanggal($tmt_sk),
                'nama_jabatan' => $nama_jabatan,
                'nama_pejabat' => $nama_pejabat,
                'id_kelas' => $id_kelas,
                'kelas' => $kelas,
                'id_prodi' => $id_prodi,
                'prodi' => $prodi,
                'status' => $stat,
                'status_keanggotaan' => $status_keanggotaan,
                'ubah' => $ubah
            );
        }
        // return $dataTabel;

        $recordsTotal = is_null($data_count_Raw) ? 0 : $data_count_Raw;
        $recordsFiltered = is_null($data_count_filtered) ? 0 : $data_count_filtered;

        $data_ = (isset($dataTabel)?$dataTabel:[]);

        $data['recordsTotal'] = $recordsTotal;
        $data['recordsFiltered'] = $recordsFiltered;
        $data['data'] = $data_;

        return $data;
    }

    public function Update_Status_Keanggotaan(Request $request)
    {
        $id = (isset($request->id)) ? $request->id : '';
        $stat = (isset($request->stat)) ? $request->stat : '';
        DB::table('keanggotaan.riwayat_jabatan')->where('id',$id)->update(['status_keanggotaan' =>$stat]);
        return response()->json('sukses lur');
    }

    public function index_riwayat_kepanitiaan(Request $request)
    {
        return view('riwayat.riwayat_kepanitiaan');
    }

    public function datatableRiwayatKepanitiaan(Request $request)
    {
        $user = Session::get("data_user");

        $flag_lv = $user['flag_lv'];
        $nim = $user['nim'];
        $key_menu = $user['key_menu'];

        $searchs = $request->searchs;
        if (isset($request->nim)) {
            $nim = $request->nim;
        }

        $limit = $request->limit;
        $offset = $request->offset;
        $data=[];

        $data_Raw = Panitia::leftjoin('keanggotaan.anggota as ang','ang.nim','=','kegiatan.kepanitiaan.nim')
            ->leftjoin('master.m_kelas as kls','kls.id','=','ang.id_kelas')
            ->leftjoin('master.m_prodi as prod','prod.id','=','ang.id_prodi')
            ->leftjoin('kegiatan.proker as pkr','pkr.id','=','kegiatan.kepanitiaan.id_proker')
            ->leftjoin('master.m_sie_kepanitiaan as msk','msk.id','=','kegiatan.kepanitiaan.id_kepanitiaan')
            ->where('kegiatan.kepanitiaan.nim','=',$nim)
            ->selectRaw("kegiatan.kepanitiaan.*, msk.nama as nama_kepanitiaan, pkr.nama as nama_proker, pkr.tanggal_mulai as tmt_proker, ang.nama as nama_pejabat, kls.id as id_kelas, kls.nama as kelas, prod.id as id_prodi, prod.nama as prodi")
            ->orderBy('pkr.tanggal_mulai','desc')
            ->limit($limit)
            ->offset($offset);

        $data_filtered = $data_Raw->when($searchs, function ($query) use ($searchs) {
                        if ($searchs) {
                            return $query->whereRaw("( pkr.nama ilike '%".$searchs."%' )");
                        }
                    });
        $data_count_Raw = $data_Raw->count();
        $data_count_filtered = $data_filtered->count();
        $data_isi = $data_filtered->get();

        // return $data_isi;

        $no = 1;
        foreach ($data_isi as $value) {
            $id_riwayat_panitia = DeHelper::encrypt_($value->id);
            $id_proker = (isset($value->id_proker)) ? $value->id_proker : '';
            $id_kepanitiaan = (isset($value->id_kepanitiaan)) ? $value->id_kepanitiaan : '';
            $nim = (isset($value->nim)) ? $value->nim : '';
            $created_at = (isset($value->created_at)) ? $value->created_at : '';
            $updated_at = (isset($value->updated_at)) ? $value->updated_at : '';
            $nama_kepanitiaan = (isset($value->nama_kepanitiaan)) ? $value->nama_kepanitiaan : '';
            $nama_proker = (isset($value->nama_proker)) ? $value->nama_proker : '';
            $tmt_proker = (isset($value->tmt_proker)) ? $value->tmt_proker : '';
            $nama_pejabat = (isset($value->nama_pejabat)) ? $value->nama_pejabat : '';
            $id_kelas = (isset($value->id_kelas)) ? $value->id_kelas : '';
            $kelas = (isset($value->kelas)) ? $value->kelas : '';
            $id_prodi = (isset($value->id_prodi)) ? $value->id_prodi : '';
            $prodi = (isset($value->prodi)) ? $value->prodi : '';

            $dataTabel[] = array(
                'no' => $no++,
                'id_proker' => $id_proker,
                'id_kepanitiaan' => $id_kepanitiaan,
                'nim' => $nim,
                'created_at' => $created_at,
                'updated_at' => $updated_at,
                'nama_kepanitiaan' => $nama_kepanitiaan,
                'nama_proker' => $nama_proker,
                'tmt_proker' => $tmt_proker,
                'tgl_proker' =>  DeHelper::format_tanggal($tmt_proker),
                'nama_pejabat' => $nama_pejabat,
                'id_kelas' => $id_kelas,
                'kelas' => $kelas,
                'id_prodi' => $id_prodi,
                'prodi' => $prodi,
            );
        }
        // return $dataTabel;

        $recordsTotal = is_null($data_count_Raw) ? 0 : $data_count_Raw;
        $recordsFiltered = is_null($data_count_filtered) ? 0 : $data_count_filtered;

        $data_ = (isset($dataTabel)?$dataTabel:[]);

        $data['recordsTotal'] = $recordsTotal;
        $data['recordsFiltered'] = $recordsFiltered;
        $data['data'] = $data_;

        return $data;
    }

}
