<?php

namespace App\Http\Controllers;
use RealRashid\SweetAlert\Facades\Alert;

use Illuminate\Http\Request;
use DB;
use App\Models\anggota;
use App\Models\calon_anggota;
use App\Models\m_agama;
use App\Models\m_kelas;
use App\Models\m_prodi;
use App\Models\m_provinsi;
use App\Models\m_kota;
use App\Models\m_kecamatan;
use App\Models\m_desa;
use PDF;
use QrCode;
use File;
use Session;
use Carbon\Carbon;

class PendaftaranController extends Controller
{
    public function Pendaftaran(Request $request){
        // get agama
        $agama = m_agama::all();
        // get kelas
        $kelas = m_kelas::all();
        //get prodi
        $prodi = m_prodi::all();
        // get provinsi
        $provinsi =m_provinsi::all();
        // get kota
        $kota =m_kota::all();
        // get kecamatan
        $kecamatan =m_kecamatan::all();
        // get desa
        $desa =m_desa::all();
        // return $agama;
        return view('calon_anggota.pendaftaran', compact('agama','kelas','prodi','provinsi','kota','kecamatan','desa'));
    }

    public function simpanPendaftaran(Request $request)
    {
        // return $request->all();

        $agama =  (isset($request->agama)) ? $request->agama : NULL;
        $alamat =  (isset($request->alamat)) ? $request->alamat : NULL;
        $email =  (isset($request->email)) ? $request->email : NULL;
        $id_desa =  (isset($request->id_desa)) ? $request->id_desa : NULL;
        $id_kecamatan =  (isset($request->id_kecamatan)) ? $request->id_kecamatan : NULL;
        $id_kelas =  (isset($request->id_kelas)) ? $request->id_kelas : NULL;
        $id_kota =  (isset($request->id_kota)) ? $request->id_kota : NULL;
        $id_prodi =  (isset($request->id_prodi)) ? $request->id_prodi : NULL;
        $id_provinsi =  (isset($request->id_provinsi)) ? $request->id_provinsi : NULL;
        $jenis_kelamin =  (isset($request->jenis_kelamin)) ? $request->jenis_kelamin : NULL;
        $nama =  (isset($request->nama)) ? $request->nama : NULL;
        $nim =  (isset($request->nim)) ? $request->nim : NULL;
        $no_hp =  (isset($request->no_hp)) ? $request->no_hp : NULL;
        $tanggal_lahir =  (isset($request->tanggal_lahir)) ? $request->tanggal_lahir : NULL;

        $file_foto =  (isset($request->foto)) ? $request->file('foto') : NULL;

        $angkatan = date('Y') - 2008;

        $angkatan = DB::table('keanggotaan.anggota')->where('is_anggota',3)->orderBy('angkatan','desc')->get()->first()->angkatan + 1;

        $nama_file = 'foto_'.$nim.'_'.str_replace(' ','_',$nama).'_'.$angkatan.'.'.$file_foto->getClientOriginalExtension();

        // nyimpen file
        $file_foto->move(public_path('foto_anggota'), $nama_file);

        $nama_qr = 'qr_'.$nim.'_'.str_replace(' ','_',$nama).'_'.$angkatan.'.png';
        QrCode::size(500)
                ->format('png')
                ->generate($nim, public_path('qr_code_anggota/'.$nama_qr));

        $data_pendaftar = [
            'nama' => $nama,
            'nim' => $nim,
            'id_provinsi' => $id_provinsi,
            'id_kota' => $id_kota,
            'id_kecamatan' => $id_kecamatan,
            'id_desa' => $id_desa,
            'id_prodi' => $id_prodi,
            'id_kelas' => $id_kelas,
            'email' => $email,
            'tanggal_lahir' => $tanggal_lahir,
            'jenis_kelamin' => $jenis_kelamin,
            'no_hp' => $no_hp,
            'id_agama' => $agama,
            'alamat' => $alamat,
            'is_anggota' => 0,
            'foto' => $nama_file,
            'angkatan' => $angkatan,
            'is_pengurus' => false,
            'qr_code' => $nama_qr,
        ];

        $prop = calon_anggota::create($data_pendaftar);

        // $link_qr = asset('qr_code_anggota/').'/'.$nama_qr;
        // $data['link_qr'] = $link_qr;
        // $data['data'] = $data_pendaftar;
        // // return $link_qr;
        // // return view('cetak',$data);
        // return PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true, 'isJavascriptEnabled' => true])
        //             ->loadView('cetak',$data)
        //             ->download('Kartu Test'.str_replace(' ','_',$nama).'.pdf');

        return response()->json($prop->id);
    }

    public function cetak_kertuTest(Request $request,$id_pendaftar)
    {
        $data_calon_ang = calon_anggota::leftjoin('master.m_kelas as mkls','mkls.id','=','keanggotaan.calon_anggota.id_kelas')
                    ->leftjoin('master.m_prodi as mprd','mprd.id','=','keanggotaan.calon_anggota.id_prodi')
                    ->where('keanggotaan.calon_anggota.id',$id_pendaftar)
                    ->selectRaw("keanggotaan.calon_anggota.*, mkls.nama as nama_kelas, mprd.nama as nama_prodi")
                    ->get()->first();

        // return $data_calon_ang;
        $link_qr = asset('qr_code_anggota/').'/'.$data_calon_ang->qr_code;
        $data['link_qr'] = $link_qr;
        $data['data'] = $data_calon_ang;

        // return $link_qr;
        // return view('cetak',$data);
        return PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true, 'isJavascriptEnabled' => true])
                    ->loadView('cetak',$data)
                    ->download('Kartu_Test_'.str_replace(' ','_',$data_calon_ang->nama).'.pdf');
    }

    public function showAnggotaDiterima(Request $request)
    {
        $searchs = $request->search['value'];
        $limit = $request->limit;
        $offset = $request->offset;
        $data=[];

        // otomatis berdasarkan tahun terkini
        $tahun = date('Y');
        $data_Raw = DB::table('keanggotaan.calon_anggota as ang')
                                ->leftjoin('master.m_kelas as mkls','mkls.id','=','ang.id_kelas')
                                ->leftjoin('master.m_prodi as mprd','mprd.id','=','ang.id_prodi')
                                ->selectRaw("ang.*, mkls.nama as nama_kelas, mprd.nama as nama_prodi")
                            // ->where(DB::RAW('EXTRACT(YEAR FROM ang.created_at)'),$tahun, )
                                ->where([
                                    [DB::RAW('EXTRACT(YEAR FROM ang.created_at)'),$tahun,],
                                    ['is_anggota', '=', '3'],
                                ])
                            ->limit($limit)
                            ->offset($offset);
        // $data['ang_baru'] = $data_ang_baru;
        // return $data;
        // return view('calon_anggota.pengumuman_hasil_tes',$data);

        $data_filtered = $data_Raw->when($searchs, function ($query) use ($searchs) {
                            if ($searchs) {
                                return $query->whereRaw("( ang.nama ilike '%".$searchs."%' or ang.nim ilike '%".$searchs."%'  or mkls.nama ilike '%".$searchs."%' or mprd.nama ilike '%".$searchs."%')");
                            }
                        });

        $data_count_Raw = $data_Raw->count();
        $data_count_filtered = $data_filtered->count();
        $data_isi = $data_filtered->get();

        // return $data_isi;

        $no = 1;
        foreach ($data_isi as $value) {
            $nim = (isset($value->nim)) ? $value->nim : '';
            $nama = (isset($value->nama)) ? $value->nama : '';
            $nama_kelas = (isset($value->nama_kelas)) ? $value->nama_kelas : '';
            $nama_prodi = (isset($value->nama_prodi)) ? $value->nama_prodi : '';
            $is_anggota = (isset($value->is_anggota)) ? $value->is_anggota : '';

            if ($is_anggota == 3) {
                $is_anggota = '<span class="label label-pill label-inline label-success mr-2">diterima</span>';
            } else {
                $is_anggota = '<span class="label label-pill label-inline label-danger  mr-2">tidak diterima</span>';
            }

            $dataTabel[] = array(
                'no' => $no++,
                'nim' => $nim,
                'nama' => $nama,
                'nama_kelas' => $nama_kelas,
                'nama_prodi' => $nama_prodi,
                'is_anggota' => $is_anggota
            );

        }


        $recordsTotal = is_null($data_count_Raw) ? 0 : $data_count_Raw;
        $recordsFiltered = is_null($data_count_filtered) ? 0 : $data_count_filtered;

        $data_ = (isset($dataTabel)?$dataTabel:[]);

        $data['recordsTotal'] = $recordsTotal;
        $data['recordsFiltered'] = $recordsFiltered;
        $data['data'] = $data_;

        return $data;
    }


    public function pdfHasiltest(Request $request)
    {

        $tahun = date('Y');
        $data_Raw = DB::table('keanggotaan.calon_anggota as ang')
                                ->leftjoin('master.m_kelas as mkls','mkls.id','=','ang.id_kelas')
                                ->leftjoin('master.m_prodi as mprd','mprd.id','=','ang.id_prodi')
                                ->selectRaw("ang.*, mkls.nama as nama_kelas, mprd.nama as nama_prodi")
                                ->where(DB::RAW('EXTRACT(YEAR FROM ang.created_at)'),$tahun, )
                                ->get();
        //  return $data_Raw;
        foreach ($data_Raw as $key => $value) {
            $data_hasil_test[$key]["nim"] = ( $value->nim != null ) ? $value->nim : '';
            $data_hasil_test[$key]["nama"] = ( $value->nama != null ) ? $value->nama : '';
            $data_hasil_test[$key]["nama_kelas"] = ( $value->nama_kelas != null ) ? $value->nama_kelas : '';
            $data_hasil_test[$key]["nama_prodi"] = ( $value->nama_prodi != null ) ? $value->nama_prodi : '';
            $data_hasil_test[$key]["is_anggota"] = ( $value->is_anggota != null ) ? $value->is_anggota : '-';

        }
        //  return $data_hasil_test;

        $data['data_hasil_test'] = $data_hasil_test;

        // return view('proker.detail.pdf_pengurus',$data);
        // $pdf = PDF::loadView('proker.detail.pdf_pengurus', $data);
        // If you want to store the generated pdf to the server then you can use the store function
        // $pdf->save(storage_path().'_filename.pdf');
        return PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true, 'isJavascriptEnabled' => true])->loadView('calon_anggota.pdf_hasil_test', $data)->download('hasil_penerimaan_EC_'.$tahun.'.pdf');
        return $pdf->download('hasil_penerimaan_EC_'.$tahun.'.pdf');
    }
}
