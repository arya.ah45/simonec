<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\m_soal;
use DataTables;
use DB;
// use DeHelper;
use Session;

class m_soalControler extends Controller
{
    public function index( Request $request){
        $query = m_soal::all();
        //  return $query;
        $data = array();
        $no =1 ;
        foreach ($query as $key => $value) {
            $data[$key]['no'] = $no++;
            $data[$key]['id'] = $value->id;
            $data[$key]['nama'] = $value->nama;
        }
        //  return $data;
        if ($request->ajax()){
            return DataTables::of($data)
            ->addColumn('aksi', function($data){

                $buttonEdit ='<a  class="btn btn-sm btn-icon btn-pure btn-default  deleteData"
                        data-toggle="tooltip"
                        data-id="'.$data['id'].'"
                        data-nama="'.$data['nama'].'"
                        data-original-title="Edite">
                        <i class="icon wb-trash" aria-hidden="true"></i>
                        </a>';
               $buttonHapus='<a class="btn btn-sm btn-icon btn-pure btn-default editData"
                data-toggle="tooltip"
                data-id="'.$data['id'].'"
                data-nama="'.$data['nama'].'"
                data-original-title="Edite"> <i class="icon wb-edit"
                aria-hidden="true"></i></a>';

                return $buttonHapus.$buttonEdit;
            })
            ->rawColumns(['aksi'])
            ->make(true);
        }

        return view('master.soal');

    }

    public function tambahsoal(Request $request){

        $insert = [
            'id' => $request->id,
            'nama' => $request->nama
        ];

        $query = m_soal::updateOrCreate(['id' => $request->id], $insert);

        if (isset($request->id) || $request->id !='') {
            $data="Sukses Update data soal!";

            # code...
        } else {
            $data ="Sukses Tambah data soal!";
            # code...
        }
        return response()->json($data);

    }


    public function destroy(Request $request,$id)
    {
        //destroyyyyyyyy
        $query = DB::table('master.m_soal_tes')
        ->where('id', $id)->delete();
    if ( $query == true ) {

        $data = "Sukses hapus data soal!";
    } else {

        $data ="Gagal hapus data soal!";
    }
    return response()->json($data);
    }


}
