<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\m_provinsi;
use App\Models\m_kota;
use App\Models\m_kecamatan;
use App\Models\m_desa;
use DataTables;
use DB;
use Session;

class m_alamatControler extends Controller
{
    public function index( Request $request){

        // $query = kendaraan::leftjoin('m_jenis_kendaraans as mjk','mjk.id','=','kendaraans.id_jenis')
        // ->leftjoin('m_merek_kendaraans as mmk','mmk.id','=','kendaraans.id_merek')
        // ->leftjoin('pegawais as p','p.nip','=','kendaraans.penanggungjawab')
        // ->orderBy('nama_kendaraan','ASC')
        // ->selectRaw("kendaraans.*,nama_merek,nama_jenis,nama_pegawai,jabatan,bagian")
        // ->get();
        $query  = m_provinsi::all();
        $query1 = m_kota::all();
        $query2 = m_kecamatan::all();
        $query3 = m_desa::all();


        // $query1 = m_kelas::leftjoin(DB::raw("master.m_prodi as pro"),'pro.id','=','master.m_kelas.id_prodi')
        //   ->selectRaw("m_kelas.*, pro.nama as nampro, pro.id as id_prodi")
        //  ->toSql();
        //  ->get();
        // $query = m_kelas::all();
        //  return $query;
        $data = array();
        $no =1 ;

        foreach ($query as $key => $value) {
            $data[$key]['no'] = $no++;
            $data[$key]['id'] = $value->id;
            $data[$key]['nama'] = $value->nama;
            $data[$key]['id_prov'] = $value->id_prov;
            // $data[$key]['nampro'] = $value->nampro;
            // $data[$key]['id_prodi'] = $value->id_prodi;
        }


        // return $query ;
        // $query2 = m_prodi::all();

        //   return $data;

        if ($request->ajax()){
            return DataTables::of($data)
            ->addColumn('aksi', function($data){

                $buttonEdit ='<a  class="btn btn-sm btn-icon btn-pure btn-default  deleteData"
                        data-toggle="tooltip"
                        data-id="'.$data['id'].'"
                        data-id_prov="'.$data['id_prov'].'"
                        data-nama="'.$data['nama'].'"
                        data-original-title="Edite">
                        <i class="icon wb-trash" aria-hidden="true"></i>
                        </a>';
               $buttonHapus='<a class="btn btn-sm btn-icon btn-pure btn-default editData"
                data-toggle="tooltip"
                data-id="'.$data['id'].'"
                data-nama="'.$data['nama'].'"
                data-original-title="Edite"> <i class="icon wb-edit"
                aria-hidden="true"></i></a>';

                return $buttonHapus.$buttonEdit;
            })
            ->rawColumns(['aksi'])
            ->make(true);
        }

        return view('master.alamat',compact('query','query1','query2','query3'));

    }

    public function getkota( Request $request){
        $query1 = m_kota::leftjoin(DB::raw("master.m_provinsi as prov"),'prov.id','=','master.m_kota.provinsi_id')
          ->selectRaw("m_kota.*, prov.nama as namprov, prov.id as id_provinsi")
        //  ->toSql();
         ->get();
        // $query = m_kelas::all();
        //  return $query1;
        $data = array();
        $no =1 ;

        foreach ($query1 as $key => $value) {
            $data[$key]['no'] = $no++;
            $data[$key]['id'] = $value->id;
            $data[$key]['nama'] = $value->nama;
            $data[$key]['namprov'] = $value->namprov;
            $data[$key]['id_provinsi'] = $value->id_provinsi;
            // $data[$key]['nampro'] = $value->nampro;
            // $data[$key]['id_prodi'] = $value->id_prodi;
        }


        // return $data ;
        // $query2 = m_prodi::all();

        //   return $query1;

        if ($request->ajax()){
            return DataTables::of($data)
            ->addColumn('aksi', function($data){

                $buttonEdit ='<a  class="btn btn-sm btn-icon btn-pure btn-default  deleteDatakota"
                        data-toggle="tooltip"
                        data-id="'.$data['id'].'"
                        data-id_prov="'.$data['id_provinsi'].'"
                        data-nama="'.$data['nama'].'"
                        data-namprov="'.$data['namprov'].'"
                        data-original-title="Edite">
                        <i class="icon wb-trash" aria-hidden="true"></i>
                        </a>';
               $buttonHapus='<a class="btn btn-sm btn-icon btn-pure btn-default editDatakota"
                data-toggle="tooltip"
                data-id="'.$data['id'].'"
                data-id_prov="'.$data['id_provinsi'].'"
                data-nama="'.$data['nama'].'"
                data-namprov="'.$data['namprov'].'"
                data-original-title="Edite"> <i class="icon wb-edit"
                aria-hidden="true"></i></a>';

                return $buttonHapus.$buttonEdit;
            })
            ->rawColumns(['aksi'])
            ->make(true);
        }

        //  return view('master.alamat',compact('query','query1','query2','query3'));

    }


    public function getkecamatan( Request $request){
        $query1 = m_kecamatan::leftjoin(DB::raw("master.m_kota as kot"),'kot.id','=','master.m_kecamatan.kota_id')
          ->selectRaw("m_kecamatan.*, kot.nama as namakota, kot.id as id_kota")
         ->get();
        // $query = m_kelas::all();
        //  return $query1;
        $data = array();
        $no =1 ;

        foreach ($query1 as $key => $value) {
            $data[$key]['no'] = $no++;
            $data[$key]['id'] = $value->id;
            $data[$key]['nama'] = $value->nama;
            $data[$key]['namakota'] = $value->namakota;
            $data[$key]['id_kota'] = $value->id_kota;
            // $data[$key]['nampro'] = $value->nampro;
            // $data[$key]['id_prodi'] = $value->id_prodi;
        }

        // return $query1;

        if ($request->ajax()){
            return DataTables::of($data)
            ->addColumn('aksi', function($data){

                $buttonEdit ='<a  class="btn btn-sm btn-icon btn-pure btn-default  deleteDatakota"
                        data-toggle="tooltip"
                        data-id="'.$data['id'].'"
                        data-id_kota="'.$data['id_kota'].'"
                        data-nama="'.$data['nama'].'"
                        data-namakota="'.$data['namakota'].'"
                        data-original-title="Edite">
                        <i class="icon wb-trash" aria-hidden="true"></i>
                        </a>';
               $buttonHapus='<a class="btn btn-sm btn-icon btn-pure btn-default editDatakota"
                data-toggle="tooltip"
                data-id="'.$data['id'].'"
                data-id_kota="'.$data['id_kota'].'"
                data-nama="'.$data['nama'].'"
                data-namakota="'.$data['namakota'].'"
                data-original-title="Edite"> <i class="icon wb-edit"
                aria-hidden="true"></i></a>';

                return $buttonHapus.$buttonEdit;
            })
            ->rawColumns(['aksi'])
            ->make(true);
        }

        //  return view('master.alamat',compact('query','query1','query2','query3'));

    }

    public function getdesa( Request $request){
        $query1 = m_desa::leftjoin(DB::raw("master.m_kecamatan as kec"),'kec.id','=','master.m_desa.kecamatan_id')
          ->selectRaw("m_desa.*, kec.nama as namakecamatan, kec.id as id_kecamatan")
         ->get();
        // $query = m_kelas::all();
        //  return $query1;
        $data = array();
        $no =1 ;

        foreach ($query1 as $key => $value) {
            $data[$key]['no'] = $no++;
            $data[$key]['id'] = $value->id;
            $data[$key]['nama'] = $value->nama;
            $data[$key]['namakecamatan'] = $value->namakecamatan;
            $data[$key]['id_kecamatan'] = $value->id_kecamatan;
            // $data[$key]['nampro'] = $value->nampro;
            // $data[$key]['id_prodi'] = $value->id_prodi;
        }

        // return $data;

        if ($request->ajax()){
            return DataTables::of($data)
            ->addColumn('aksi', function($data){

                $buttonEdit ='<a  class="btn btn-sm btn-icon btn-pure btn-default  deleteDatakecamatan"
                        data-toggle="tooltip"
                        data-id="'.$data['id'].'"
                        data-id_kecamatan="'.$data['id_kecamatan'].'"
                        data-nama="'.$data['nama'].'"
                        data-namakecamatan="'.$data['namakecamatan'].'"
                        data-original-title="Edite">
                        <i class="icon wb-trash" aria-hidden="true"></i>
                        </a>';
               $buttonHapus='<a class="btn btn-sm btn-icon btn-pure btn-default editDatakecamatan"
                data-toggle="tooltip"
                data-id="'.$data['id'].'"
                data-id_kecamatan="'.$data['id_kecamatan'].'"
                data-nama="'.$data['nama'].'"
                data-namakecamatan="'.$data['namakecamatan'].'"
                data-original-title="Edite"> <i class="icon wb-edit"
                aria-hidden="true"></i></a>';

                return $buttonHapus.$buttonEdit;
            })
            ->rawColumns(['aksi'])
            ->make(true);
        }

        //  return view('master.alamat',compact('query','query1','query2','query3'));

    }


    public function tambahprovinsi(Request $request){

        $insert = [
            'id' => $request->id,
            'nama' => $request->nama,
            // 'id_prodi' => $request->id_prodi,
        ];

        $query = m_provinsi::updateOrCreate(['id' => $request->id], $insert);

        if (isset($request->id) || $request->id !='') {
            $data="Sukses Update data provinsi!";
            # code...
        } else {
            $data ="Sukses Tambah data provinsi!";
            # code...
        }
        return response()->json($data);
    }


    public function destroyProvinsi(Request $request,$id)
    {
        $query = DB::table('master.m_provinsi')
        ->where('id_prov', $id)->delete();
    if ( $query == true ) {

        $data = "Sukses hapus data provinsi!";
    } else {

        $data ="Gagal hapus data provinsi!";
    }
    return response()->json($data);
    }

    public function select2Provinsi(Request $request)
    {
        $search = $request->search;
        $query = m_provinsi::whereRaw("( nama ilike '%".$search."%' )")
                ->orderBy('nama','asc')
                ->get();

        $data = array();
        foreach ($query as $key => $value) {
            $data[$key]['id'] = $value->id;
            $data[$key]['nama'] = $value->nama;
        }
        // $array[] = array('a'=>'a');
        return response()->json($data);
    }

    public function select2Kota(Request $request)
    {
        $search = $request->search; 
        $id_provinsi = $request->id_provinsi; 
        $query = m_kota::where('provinsi_id','=',$id_provinsi)
                ->whereRaw("( nama ilike '%".$search."%' )")
                ->orderBy('nama','asc')
                ->get();

        $data = array();
        foreach ($query as $key => $value) {
            $data[$key]['id'] = $value->id;
            $data[$key]['nama'] = $value->nama;
        }
        // $array[] = array('a'=>'a');
        return response()->json($data);
    }

    public function select2Kecamatan(Request $request)
    {
        $search = $request->search;
        $id_kota = $request->id_kota;         
        $query = m_kecamatan::where('kota_id','=',$id_kota)
                ->whereRaw("( nama ilike '%".$search."%' )")
                ->orderBy('nama','asc')
                ->get();

        $data = array();
        foreach ($query as $key => $value) {
            $data[$key]['id'] = $value->id;
            $data[$key]['nama'] = $value->nama;
        }
        // $array[] = array('a'=>'a');
        return response()->json($data);
    }

    public function select2Desa(Request $request)
    {
        $search = $request->search;
        $id_kecamatan = $request->id_kecamatan;         
        $query = m_desa::where('kecamatan_id','=',$id_kecamatan)
                ->whereRaw("( nama ilike '%".$search."%' )")
                ->orderBy('nama','asc')
                ->get();

        $data = array();
        foreach ($query as $key => $value) {
            $data[$key]['id'] = $value->id;
            $data[$key]['nama'] = $value->nama;
        }
        // $array[] = array('a'=>'a');
        return response()->json($data);
    }
}
