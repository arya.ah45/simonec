<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\m_prodi;
use DataTables;
use DB;
use Session;

class m_prodiControler extends Controller
{
    public function index( Request $request){
        $query = DB::table('master.m_prodi')->get();


        // return $query;
        $data = array();
        $no =1 ;

        foreach ($query as $key => $value) {
            $data[$key]['no'] = $no++;
            $data[$key]['id'] = $value->id;
            $data[$key]['nama'] = $value->nama;
        }

        //  return $data;

        if ($request->ajax()){
            return DataTables::of($data)
            ->addColumn('aksi', function($data){

                $buttonEdit ='<a  class="btn btn-sm btn-icon btn-pure btn-default  deleteData"
                        data-toggle="tooltip"
                        data-id="'.$data['id'].'"
                        data-nama="'.$data['nama'].'"
                        data-original-title="Edite">
                        <i class="icon wb-trash" aria-hidden="true"></i>
                        </a>';
               $buttonHapus='<a class="btn btn-sm btn-icon btn-pure btn-default editData"
                data-toggle="tooltip"
                data-id="'.$data['id'].'"
                data-nama="'.$data['nama'].'"
                data-original-title="Edite"> <i class="icon wb-edit"
                aria-hidden="true"></i></a>';

                return $buttonHapus.$buttonEdit;
            })
            ->rawColumns(['aksi'])
            ->make(true);
        }

        return view('master.prodi');

    }

    public function tambahProdi(Request $request){

        $insert = [
            'id' => $request->id,
            'nama' => $request->nama
        ];

        $query = m_prodi::updateOrCreate(['id' => $request->id], $insert);

        if (isset($request->id) || $request->id !='') {
            $data="Sukses Update data prodi!";
            # code...
        } else {
            $data ="Sukses Tambah data Prodi!";
            # code...
        }
        return response()->json($data);
    }


    public function destroy(Request $request,$id)
    {
        $query = DB::table('master.m_prodi')
        ->where('id', $id)->delete();
    if ( $query == true ) {
        $data = "Sukses hapus data Prodi!";
    } else {
        $data ="Sukses hapus data Prodi!";
    }
    return response()->json($data);
    }

    public function Prodi(Request $request)
    {
        $search = $request->search;
        $query = DB::table('master.m_prodi')
                // ->where('is_anggota','=',3)
                // ->where('is_pengurus','=',false)
                ->whereRaw("( nama ilike '%".$search."%')")
                ->orderBy('nama','asc')
                ->get();

        $data = array();
        foreach ($query as $key => $value) {
            $data[$key]['id'] = $value->id;
            $data[$key]['nama'] = $value->nama;
        }
        // $array[] = array('a'=>'a');
        return response()->json($data);
    }

 

}
