<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\m_kriteria_penilaian;
use DataTables;
use DB;
// use DeHelper;
use Session;

class m_KriteriaPenilaianController extends Controller
{
    public function index( Request $request){
        $query = DB::table('master.m_kriteria_penilaian')->orderBy('for_user','asc')->orderBy('is_lpj','asc')->orderBy('created_at','desc')->get();


        // return $query;
        $data = array();
        $no =1 ;

        foreach ($query as $key => $value) {
            $data[$key]['no'] = $no++;
            $data[$key]['id'] = $value->id;
            $data[$key]['nama'] = $value->nama;
            $data[$key]['is_lpj'] = $value->is_lpj;
            $data[$key]['jenis'] = ($value->is_lpj == 0) ? 'Proposal' : 'LPJ';
            $data[$key]['for_user'] = ($value->for_user == 'a') ? 'DPK' : 'Ketua UKM';
            $data[$key]['for_user_raw'] = $value->for_user;
        }

        //  return $data;

        if ($request->ajax()){
            return DataTables::of($data)
            ->addColumn('aksi', function($data){

                $buttonEdit ='<a  class="btn btn-sm btn-icon btn-pure btn-default  deleteData"
                        data-toggle="tooltip"
                        data-id="'.$data['id'].'"
                        data-nama="'.$data['nama'].'"
                        data-for_user="'.$data['for_user_raw'].'"
                        data-original-title="Edite">
                        <i class="icon wb-trash" aria-hidden="true"></i>
                        </a>';
                $buttonHapus='<a class="btn btn-sm btn-icon btn-pure btn-default editData"
                        data-toggle="tooltip"
                        data-id="'.$data['id'].'"
                        data-nama="'.$data['nama'].'"
                        data-is_lpj="'.$data['is_lpj'].'"
                        data-jenis="'.$data['jenis'].'"          
                        data-for_user="'.$data['for_user_raw'].'"
                        data-original-title="Edite"> <i class="icon wb-edit"
                        aria-hidden="true"></i></a>';

                return $buttonHapus.$buttonEdit;
            })
            ->rawColumns(['aksi'])
            ->make(true);
        }

        return view('master.kriteria_penilaian');

    }

    public function tambahkriteria_penilaian(Request $request){

        $insert = [
            'id' => $request->id,
            'nama' => $request->nama,
            'is_lpj' => $request->jenis,
            'for_user' => $request->for_user
        ];

        $query = m_kriteria_penilaian::updateOrCreate(['id' => $request->id], $insert);

        if (isset($request->id) || $request->id !='') {
            $data="Sukses Update data kriteria penilaian!";

            # code...
        } else {
            $data ="Sukses Tambah data kriteria penilaian!";
            # code...
        }
        return response()->json($data);

    }


    public function destroy(Request $request,$id)
    {
        //destroyyyyyyyy
        $query = DB::table('master.m_kriteria_penilaian')
        ->where('id', $id)->delete();
        if ( $query == true ) {

            $data = "Sukses hapus data kriteria penilaian!";
        } else {

            $data ="Gagal hapus data kriteria penilaian!";
        }
        return response()->json($data);
    }

    public function getKriteriaPenilaian()
    {
        $data = m_kriteria_penilaian::orderBy('id','asc')->get();
        return response()->json($data);
    }

}
