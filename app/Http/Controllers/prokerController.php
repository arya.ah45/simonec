<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use DB;
use DeHelper;
use App\Models\proker; 
use App\Models\kbt_proker;
use App\Models\Panitia;   
use App\Models\m_kriteria_penilaian;
use App\Models\review_proposal;
use App\Models\review_lpj;
use App\Helpers\DeHelper2;
use DataTables;

class prokerController extends Controller
{
    public function index()
    {
        $data['now_year'] = date('Y');
        $data['now_month'] = date('m');
        $data['cek_page'] = 'normal';
        
        session::put('is_proker', true);

        return view('proker.proker_h', $data);
    }

    public function select2bulanProker(Request $request)
    {
        $tahun = (isset($request->tahun)) ? $request->tahun : 0;

        $data_bulan = DB::table('kegiatan.proker')
                        ->selectRaw("EXTRACT(MONTH FROM tanggal_mulai) as bulan")
                        ->when($tahun, function ($query) use ($tahun) {
                            if ($tahun != 0 ) {
                                return $query->where([
                                        [DB::raw('EXTRACT(YEAR FROM tanggal_mulai)'),'=',$tahun],
                                    ]);
                            }else{
                                return $query;
                            }
                        })                        
                        ->groupByRaw("EXTRACT(MONTH FROM tanggal_mulai)")
                        ->orderBy(DB::RAW('EXTRACT(MONTH FROM tanggal_mulai)'),'asc')
                        ->get();
        // return $data_bulan;

        $data = array();
        foreach ($data_bulan as $key => $value) {
            $data[$key]['id'] = $value->bulan;
            $data[$key]['nama'] = DeHelper::getNamaBulan($value->bulan);
        }
        // $array[] = array('a'=>'a');
        return response()->json($data);        
    }

    public function select2tahunProker(Request $request)
    {
        $bulan = (isset($request->bulan)) ? $request->bulan : 0;

        $data_tahun = DB::table('kegiatan.proker')
                        ->selectRaw("EXTRACT(YEAR FROM tanggal_mulai) as tahun")
                        ->when($bulan, function ($query) use ($bulan) {
                            if ($bulan != 0 ) {
                                return $query->where([
                                        [DB::raw('EXTRACT(MONTH FROM tanggal_mulai)'),'=',$bulan],
                                    ]);
                            }else{
                                return $query;
                            }
                        })                        
                        ->groupByRaw("EXTRACT(YEAR FROM tanggal_mulai)")
                        ->orderBy(DB::RAW('EXTRACT(YEAR FROM tanggal_mulai)'),'desc')
                        ->get();
        // return $data_tahun;

        $data = array();
        foreach ($data_tahun as $key => $value) {
            $data[$key]['id'] = $value->tahun;
            $data[$key]['nama'] = $value->tahun;
        }
        // $array[] = array('a'=>'a');
        return response()->json($data);  
    }

    public function event()
    {
        $user 		= Session::get("data_user");
        // $flag_lv = $user['flag_lv'];
        $data['nim'] = $user['nim'];
        // $key_menu = $user['key_menu'];
        $data['cek_page'] = 'normal';

        Session::forget('is_proker');        
        return view('proker.proker_h', $data);
    }

    public function dataTable_proker(Request $request)
    {
        // return $request->all();
        $nim_admin = (isset($request->nim)) ? $request->nim : '';

        $tahun = (isset($request->tahun)) ? $request->tahun : 0;
        $bulan = (isset($request->bulan)) ? $request->bulan : 0;

        $user 		= Session::get("data_user");
        $flag_lv = $user['flag_lv'];
        $nim = $user['nim'];
        $key_menu = $user['key_menu'];
        $id_gets = [];

        $data_isi = DB::table('kegiatan.proker as proker')
                ->leftJoin(DB::raw('(SELECT A.* FROM  approve.approv_proker A
                    JOIN ( SELECT proker_id, MAX ( ID ) AS iid FROM approve.approv_proker GROUP BY proker_id ) B
                    ON A.ID = B.iid AND A.proker_id = B.proker_id) as aprov'),'aprov.proker_id','=','proker.id')
                ->leftjoin('keanggotaan.anggota as ang','ang.nim','=','proker.penanggungjawab')                
                ->leftJoin('master.m_struktur as ms','ms.id','=','proker.id_struktur')
                ->leftJoin('kegiatan.proposal as prop','prop.proker_id','=','proker.id')
                ->leftJoin(DB::raw('(SELECT A.* FROM  approve.approv_proposal A
                    JOIN ( SELECT proposal_id, MAX ( ID ) AS iid FROM approve.approv_proposal GROUP BY proposal_id ) B
                    ON A.ID = B.iid AND A.proposal_id = B.proposal_id) as aprov_prop'),'aprov_prop.proposal_id','=','prop.id')                
                ->leftJoin('kegiatan.lpj as lpj','lpj.proker_id','=','proker.id')
                ->leftJoin(DB::raw('(SELECT A.* FROM  approve.approv_lpj A
                    JOIN ( SELECT lpj_id, MAX ( ID ) AS iid FROM approve.approv_lpj GROUP BY lpj_id ) B
                    ON A.ID = B.iid AND A.lpj_id = B.lpj_id) as aprov_lpj'),'aprov_lpj.lpj_id','=','lpj.id')
                // ->get();
                // return $total_data;     

                ->when($nim_admin, function ($query) use ($nim_admin,$id_gets) {
                    if ($nim_admin != "") {
                        $id_gets = DB::table('kegiatan.admin_kegiatan as ak')
                                ->where('nim','=',$nim_admin)
                                ->select('proker_id')
                                ->pluck('proker_id');

                        return $query ->whereIn('proker.id', $id_gets);
                    }
                })                    

                ->when($key_menu, function ($query) use ($key_menu,$nim_admin,$flag_lv) {
                    if ($nim_admin == "") {
                        $key_menu = $flag_lv;
                    }

                    if ($key_menu === "c" || substr($key_menu, 0, 1)  == 'e') {
                        return $query->whereIn('aprov.status',array(0,1,2,3,4,5,6,7,8));                  
                    }elseif ($key_menu == "b") {
                        return  $query  ->where(function($q) use ($key_menu) {
                                            $q ->whereIn('aprov.status',array(0,2,3,4,5,6));
                                        });
                    }elseif ($key_menu == "a") {
                        return  $query  ->where(function($q) use ($key_menu) {
                                            $q ->whereIn('aprov.status',array(3,5,6,7,8));
                                        });
                    }elseif ($key_menu == "d") {
                        return $query->whereIn('aprov.status',array(5));
                    }else{
                        return $query;
                }})

                    // ->whereNotIn('aprov.status',array(6))
                ->when($tahun, function ($query) use ($tahun) {
                    if ($tahun != 0) {
                        return $query->where([
                                [DB::raw('EXTRACT(YEAR FROM tanggal_mulai)'),'=',$tahun],
                            ]);
                    }else{
                        return $query;
                    }
                })    

                ->when($bulan, function ($query) use ($bulan) {
                    if ($bulan != 0 ) {
                        return $query->where([
                                [DB::raw('EXTRACT(MONTH FROM tanggal_mulai)'),'=',$bulan],
                            ]);
                    }else{
                        return $query;
                    }
                })            
                ->selectRaw("proker.id, proker.*, aprov.status, aprov.catatan, ms.nama as nama_struktur,ang.nama as nama_pj, 
                    prop.id as id_proposal, prop.nama_file as nama_file_proposal, aprov_prop.status as status_proposal, aprov_prop.catatan as catatan_proposal, 
                    lpj.id as id_lpj, lpj.nama_file as nama_file_lpj,aprov_lpj.status as status_lpj, aprov_lpj.catatan as catatan_lpj")
                ->orderBy('proker.tanggal_mulai','desc')
                ->get();
        
        // return $data_count_Raw;
        // return $data_isi;

        $no = 1;
        foreach ($data_isi as $value) {
            $id_proker = DeHelper::encrypt_($value->id);
            $id_struktur = (isset($value->id_struktur)) ? $value->id_struktur : NULL;
            $nama=(isset($value->nama)) ? $value->nama : NULL;
            $tanggal_mulai =(isset($value->tanggal_mulai)) ? $value->tanggal_mulai : NULL;
            $tanggal_selesai =(isset($value->tanggal_selesai)) ? $value->tanggal_selesai : NULL;
            $dana_usulan =(isset($value->dana_usulan)) ? $value->dana_usulan : NULL;
            $dana_terpakai=(isset($value->dana_terpakai)) ? $value->dana_terpakai : NULL;
            $ket_tambahan=(isset($value->ket_tambahan)) ? $value->ket_tambahan : NULL;
            $penanggungjawab=(isset($value->penanggungjawab)) ? $value->penanggungjawab : NULL;
            $subjek_tujuan=(isset($value->subjek_tujuan)) ? $value->subjek_tujuan : NULL;
            $dana_turun=(isset($value->dana_turun)) ? $value->dana_turun : NULL;
            $dana_mandiri=(isset($value->dana_mandiri)) ? $value->dana_mandiri : NULL;
            $event=(isset($value->event)) ? $value->event : NULL;
            $tujuan=(isset($value->tujuan)) ? $value->tujuan : NULL;;
            $subjek_tujuan=(isset($value->subjek_tujuan)) ? $value->subjek_tujuan : NULL;
            $status=(isset($value->status)) ? $value->status : NULL;
            $catatan=(isset($value->catatan)) ? $value->catatan : '-';
            $nama_struktur=(isset($value->nama_struktur)) ? $value->nama_struktur : NULL;
            $nama_pj=(isset($value->nama_pj)) ? $value->nama_pj : '-';
            $terlaksana=(isset($value->terlaksana)) ? $value->terlaksana : NULL;
            $rating=(isset($value->rating)) ? $value->rating : NULL;
            $evaluasi=(isset($value->evaluasi)) ? $value->evaluasi : NULL;

            if ($terlaksana == true) {
                $terlaksana = '<span class="badge badge-success">Terlaksana</span>';
            }else{
                if (strtotime($tanggal_mulai) >= strtotime(date('d-m-Y'))) {
                    $terlaksana = '<span class="badge badge-secondary">Belum Terlaksana</span>';
                }elseif(strtotime($tanggal_mulai) <= strtotime(date('d-m-Y')) && strtotime($tanggal_selesai) >= strtotime(date('d-m-Y'))){
                    $terlaksana = '<span class="badge badge-info">Sedang berlangsung</span>';
                }else{
                    $terlaksana = '<span class="badge badge-danger">Tidak Terlaksana</span>';
                }
            }

            // $link_hapus = url('surat_keterangan/hapus_penghargaan/'.$id_riwayat);
            $aprove = '<a style="margin-left:5px;" data-id="'.$id_proker.'"  href="javascript:void(0)" class="dropdown-item btn btn-xs btn-success btnAprove" role="menuitem" ><i class="icon fa-check"></i> Aprove </a>';
            $aprove_dg_catatan = '<a style="margin-left:5px;" data-id="'.$id_proker.'"  href="javascript:void(0)" class="dropdown-item btn btn-xs bg-light-green-700 text-white btnAproveCt"  role="menuitem"><i class="icon fa-check"></i> Aprove dengan catatan </a>';
            $kirim_ke_ketua = '<a style="margin-left:5px;" data-id="'.$id_proker.'"  href="javascript:void(0)" class="dropdown-item btn btn-xs btn-primary btnKirimKt"  role="menuitem"><i class="icon fa-check"></i> Kirim ke ketua </a>';
            $penyesuaian_selesai = '<a style="margin-left:5px;" data-id="'.$id_proker.'"  href="javascript:void(0)" class="dropdown-item btn btn-xs btn-primary btnPenyesuaian" role="menuitem" ><i class="icon fa-check"></i> Aprove dengan catatan </a>';
            $review_selesai = '<a style="margin-left:5px;" data-id="'.$id_proker.'"  href="javascript:void(0)" class="dropdown-item btn btn-xs btn-primary btnReview"  role="menuitem"><i class="icon fa-check"></i>Review catatan selesai</a>';
            $ditolak = '<a style="margin-left:5px;"  data-id="'.$id_proker.'" class="dropdown-item btn btn-xs btn-danger show_modal_tolak" href="javascript:void(0)" role="menuitem"><i class="icon fa-remove"></i> Ditolak </a>';
            $perbaikan = '<a  data-id="'.$id_proker.'" href="javascript:void(0)" class="dropdown-item btn btn-xs bg-amber-700 text-white btn_perbaikan btnRevisi"
                data-id_struktur="'.$id_struktur.'"
                data-nama="'.$nama.'"
                data-tanggal_mulai="'.$tanggal_mulai.'"
                data-tanggal_selesai="'.$tanggal_selesai.'"
                data-dana_usulan="'.$dana_usulan.'"
                data-dana_terpakai="'.$dana_terpakai.'"
                data-dana_turun="'.$dana_turun.'"
                data-ket_tambahan="'.$ket_tambahan.'"
                data-penanggungjawab="'.$penanggungjawab.'"
                data-event="'.$event.'"
                data-tujuan="'.$tujuan.'"
                data-subjek_tujuan="'.$subjek_tujuan.'"
                data-status="'.$status.'"
                data-catatan="'.$catatan.'"
                data-nama_pj="'.$nama_pj.'"
                data-subjek_tujuan="'.$subjek_tujuan.'"
                data-dana_mandiri="'.$dana_mandiri.'"
                data-nama_struktur="'.$nama_struktur.'" role="menuitem">
            <i class="icon fa-wrench"></i> Perbaikan </a>';
            $cek = '<a href="javascript:void(0)" class="dropdown-item btn btn-xs btn-secondary btnDetail" style="margin-left:5px;"
                data-id="'.$id_proker.'"
                data-id_struktur="'.$id_struktur.'"
                data-nama="'.$nama.'"
                data-tanggal_mulai="'.$tanggal_mulai.'"
                data-tanggal_selesai="'.$tanggal_selesai.'"
                data-dana_usulan="'.$dana_usulan.'"
                data-dana_terpakai="'.$dana_terpakai.'"
                data-dana_turun="'.$dana_turun.'"
                data-ket_tambahan="'.$ket_tambahan.'"
                data-penanggungjawab="'.$penanggungjawab.'"
                data-event="'.$event.'"
                data-tujuan="'.$tujuan.'"
                data-subjek_tujuan="'.$subjek_tujuan.'"
                data-status="'.$status.'"
                data-catatan="'.$catatan.'"
                data-nama_pj="'.$nama_pj.'"        
                data-subjek_tujuan="'.$subjek_tujuan.'"
                data-dana_mandiri="'.$dana_mandiri.'"                        
                data-nama_struktur="'.$nama_struktur.'" role="menuitem">
            <i class="icon fa-eye"></i> Cek </a>';
            $edit = '<a href="javascript:void(0)" class="dropdown-item btn btn-xs text-white bg-purple-600 btnEdit" style="margin-left:5px;"
                data-id="'.$id_proker.'"
                data-id_struktur="'.$id_struktur.'"
                data-nama="'.$nama.'"
                data-tanggal_mulai="'.$tanggal_mulai.'"
                data-tanggal_selesai="'.$tanggal_selesai.'"
                data-dana_usulan="'.$dana_usulan.'"
                data-dana_terpakai="'.$dana_terpakai.'"
                data-dana_turun="'.$dana_turun.'"
                data-ket_tambahan="'.$ket_tambahan.'"
                data-penanggungjawab="'.$penanggungjawab.'"
                data-event="'.$event.'"
                data-tujuan="'.$tujuan.'"
                data-subjek_tujuan="'.$subjek_tujuan.'"
                data-status="'.$status.'"
                data-catatan="'.$catatan.'"
                data-nama_pj="'.$nama_pj.'"            
                data-subjek_tujuan="'.$subjek_tujuan.'"
                data-dana_mandiri="'.$dana_mandiri.'"                    
                data-nama_struktur="'.$nama_struktur.'" role="menuitem">
            <i class="icon fa-pencil"></i> Edit </a>';
            $hapus = '<a style="margin-left:5px;" data-id="'.$id_proker.'"  class="dropdown-item btn btn-xs btn-danger bg-pink-600 btnDelete" href="javascript:void(0)" role="menuitem"><i class="icon fa-trash"></i> Delete </a>';
            $url_detail = route('detailProker', ['id_proker' => $id_proker]);
            $detail = '<a style="margin-left:5px;" data-id="'.$id_proker.'"  class="dropdown-item btn btn-xs bg-light-blue-a400 text-white" href="'.$url_detail.'" role="menuitem"><i class="icon fa-search-plus"></i> Detail </a>';
            
            $stat_event = '';
            if($event == 1)
            {
                $stat_event = "<span class='badge bg-blue-600 text-white'>Normal</span>";
            } else if($event == 2) {
                $stat_event = "<span class='badge bg-purple-600 text-white'>Event</span>";
            }else{
                $stat_event = "<span class='badge badge-secondary'>belum dipilih</span>";
            }
            if ($flag_lv=="c" || substr($key_menu,0,1) == 'e') {
                // proker
                if ($status == 1 ) {
                    $aksi = $detail.' '.$hapus;
                }elseif($status  == 4 || $status  == 8){
                    $aksi = $detail;
                }elseif($status  == 6){
                    $aksi = $detail;
                }elseif($flag_lv=="b"){
                    if ($status  ==  2 || $status == 0) {
                        $aksi = $detail;
                    }else{
                        $aksi = $detail;
                    }                         
                }else{
                    $aksi = $detail;
                }
            }else if($flag_lv=="b"){
                if ($status  ==  2 || $status == 0) {
                    $aksi = $detail;
                }else{
                    $aksi = $detail;
                }           
            }elseif ($flag_lv=="a") {
                if ($status  ==  3) {
                    $aksi = $detail;
                }elseif($status  == 7){
                    $aksi = $detail;                    
                }else{
                    $aksi = $detail;
                }              
            } else{
                $aksi= $cek;
            }

            $aksi = '<div class="btn-group">
                        <button type="button" class="btn btn-icon btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false" aria-hidden="true">
                            <i class="icon fa-gear fa-spin" aria-hidden="true"></i> Aksi
                        </button>
                        <div class="dropdown-menu" role="menu">
                            '.$aksi.'
                        </div>
                    </div>';

            // ------------------------------------------------- Proposal LPJ --------------------------------------------------- 
            // ------------------------------------------------- Proposal LPJ --------------------------------------------------- 
            // ------------------------------------------------- Proposal LPJ ---------------------------------------------------
        


            $id_proposal =(isset($value->id_proposal)) ? DeHelper::encrypt_($value->id_proposal) : '';            
            $nama_file_proposal=(isset($value->nama_file_proposal)) ? $value->nama_file_proposal : '';
            $status_proposal =(isset($value->status_proposal)) ? $value->status_proposal : NULL;
            $catatan_proposal =(isset($value->catatan_proposal)) ? $value->catatan_proposal : '';            
            $file_path_proposal = asset('proposal_file').'/'.$nama_file_proposal;

            $flag_level_proposal = '';
            if ($flag_lv=="c") {
                if ($status_proposal == 4) {
                    $flag_level_proposal = 'b';
                }elseif($status_proposal  == 8){
                    $flag_level_proposal = 'a';
                }else{
                }
            }else if($flag_lv=="b"){
                if ($status_proposal  ==  2 || $status_proposal == 0) {
                    $flag_level_proposal = 'b';
                }else{
                }
            }elseif ($flag_lv=="a") {
                if ($status_proposal  ==  3) {
                    $flag_level_proposal = 'a';
                }else{
                }
            } else{
            }

            $review_proposal = '<a type="button" href="javascript:void(0)" class="dropdown-item btn btn-xs bg-pink-700 text-white btnReview" role="menuitem" style="margin-left:5px;"
                data-id="'.$id_proposal.'" 
                data-proker_id="'.$id_proker.'" 
                data-status="'.$status_proposal.'" 
                data-catatan="'.$catatan_proposal.'" 
                data-nama_proker="'.$nama.'"
                data-tipe_proker="'.$stat_event.'"
                data-penanggungjawab="'.$penanggungjawab.'"
                data-tanggal_mulai="'.$tanggal_mulai.'"
                data-tanggal_selesai="'.$tanggal_selesai.'"
                data-tujuan="'.$tujuan.'"
                data-dana_turun="'.$dana_turun.'"
                data-dana_mandiri="'.$dana_mandiri.'"
                data-dana_usulan="'.$dana_usulan.'"
                data-flag_lv="'.$flag_level_proposal.'"
                data-jenis="proposal"
                data-nama_file="'.$nama_file_proposal.'">
                <i class="icon fa-search"></i> Review </a>';
            $cek_review_proposal = '<a type="button" href="javascript:void(0)" class="dropdown-item btn btn-xs bg-grey-700 text-white btnCekReview" role="menuitem" style="margin-left:5px;"
                data-id="'.$id_proposal.'" 
                data-proker_id="'.$id_proker.'"
                data-status="'.$status_proposal.'" 
                data-catatan="'.$catatan.'"
                data-nama_proker="'.$nama.'"                      
                data-tipe_proker="'.$stat_event.'"
                data-penanggungjawab="'.$penanggungjawab.'"
                data-tanggal_mulai="'.$tanggal_mulai.'"
                data-tanggal_selesai="'.$tanggal_selesai.'"
                data-tujuan="'.$tujuan.'"
                data-dana_turun="'.$dana_turun.'"
                data-dana_mandiri="'.$dana_mandiri.'"
                data-dana_usulan="'.$dana_usulan.'"
                data-flag_lv="'.$flag_level_proposal.'"        
                data-jenis="proposal"
                data-nama_file="'.$nama_file_proposal.'">
                <i class="icon fa-search"></i> Cek Review </a>';
            $perbaikan_proposal = '<a type="button"  data-id="'.$id_proposal.'" href="javascript:void(0)" class="dropdown-item btn btn-xs bg-amber-700 text-white btn_perbaikan_propLPJ" role="menuitem" style="margin-left:5px;"
                data-proker_id="'.$id_proker.'"
                data-nama_file="'.$nama_file_proposal.'"
                data-status="'.$status_proposal.'"
                data-jenis="proposal"
                data-nama_proker="'.$nama.'">
                <i class="icon fa-wrench"></i> Perbaikan </a>';
            $edit_proposal = '<a type="button"  data-id="'.$id_proposal.'" href="javascript:void(0)" class="dropdown-item btn btn-xs bg-amber-700 text-white btn_edit_propLPJ" role="menuitem" style="margin-left:5px;"
                data-proker_id="'.$id_proker.'"
                data-nama_file="'.$nama_file_proposal.'"
                data-status="'.$status_proposal.'"
                data-jenis="proposal"
                data-nama_proker="'.$nama.'">
                <i class="icon fa-edit"></i> Edit </a>';
            $cek_proposal = '<a href="javascript:void(0)" type="button" class="dropdown-item btn btn-xs btn-secondary btn_detail_propLPJ" role="menuitem" style="margin-left:5px;"
                data-proker_id="'.$id_proker.'"
                data-nama_file="'.$nama_file_proposal.'"
                data-status="'.$status_proposal.'"
                data-jenis="proposal"
                data-nama_proker="'.$nama.'">
                <i class="icon fa-eye"></i> Cek </a>';
            $add_proposal = '<a type="button" data-proker_id="'.$id_proker.'" data-nama_proker="'.$nama.'" data-jenis="proposal" class="dropdown-item btn btn-xs btn-success btn_tambah_propLPJ" href="javascript:void(0)" role="menuitem"><i class="icon fa-plus"></i> Proposal </a>';          
            $hapus_proposal = '<a type="button" style="margin-left:5px;" data-id="'.$id_proposal.'" data-jenis="proposal" class="dropdown-item btn btn-xs btn-danger bg-pink-600 btn_delete_propLPJ" href="javascript:void(0)" role="menuitem"><i class="icon fa-trash"></i> Delete </a>';          
            $file_path_proposal = asset('proposal_file').'/'.$nama_file_proposal;
            $download_file_proposal = '<a href="'.$file_path_proposal.'" download class="dropdown-item btn btn-xs btn-round btn-success" role="menuitem">Download file proposal</a>';
            
            if ($flag_lv=="c") {
                if ($status_proposal === NULL ) {
                    if ($nim_admin == '') {
                        $aksi_proposal = $add_proposal;
                    }else{
                        $aksi_proposal = '';
                    }
                }elseif($status_proposal == 2 ) {
                    $aksi_proposal = $download_file_proposal.' '.$edit_proposal.' '.$hapus_proposal;
                }elseif($status_proposal  == 4 || $status_proposal  == 8){
                    $aksi_proposal = $download_file_proposal.' '.$cek_review_proposal.' '.$perbaikan_proposal;
                }else{
                    $aksi_proposal = $download_file_proposal;
                }
            }else if($flag_lv=="b"){
                if ($status_proposal  ==  2 || $status_proposal === 0) {
                    $aksi_proposal = $download_file_proposal.' '.$review_proposal;
                }else{
                    $aksi_proposal = '';
                }    
            }elseif ($flag_lv=="a") {
                if ($status_proposal  ==  3) {
                    $aksi_proposal = $download_file_proposal.' '.$review_proposal;                  
                }else{
                    $aksi_proposal = '';
                }                                               
            }else{
                $aksi_proposal = '';
            }
                                
            if ($aksi_proposal != '') {
                $aksi_proposal_grub = '<div class="btn-group">
                            <button type="button" class="btn btn-icon btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false" aria-hidden="true">
                                <i class="icon fa-gear fa-spin" aria-hidden="true"></i> Aksi
                            </button>
                            <div class="dropdown-menu" role="menu">
                                '.$aksi_proposal.'
                            </div>
                        </div>';
            }else{
                $aksi_proposal_grub = '-';
            }
            
            $id_lpj =(isset($value->id_lpj)) ? DeHelper::encrypt_($value->id_lpj) : '';            
            $nama_file_lpj = (isset($value->nama_file_lpj)) ? $value->nama_file_lpj : '';
            $status_lpj =(isset($value->status_lpj)) ? $value->status_lpj : NULL;
            $catatan_lpj =(isset($value->catatan_lpj)) ? $value->catatan_lpj : '';            
            $file_path_lpj = asset('lpj_file').'/'.$nama_file_lpj;

            $flag_level_lpj = '';
            if ($flag_lv=="c") {
                if ($status_lpj == 4) {
                    $flag_level_lpj = 'b';
                }elseif($status_lpj  == 8){
                    $flag_level_lpj = 'a';
                }else{
                }
            }else if($flag_lv=="b"){
                if ($status_lpj  ==  2 || $status_lpj == 0) {
                    $flag_level_lpj = 'b';
                }else{
                }
            }elseif ($flag_lv=="a") {
                if ($status_lpj  ==  3) {
                    $flag_level_lpj = 'a';
                }else{
                }
            } else{
            }

            $review_lpj = '<a type="button" href="javascript:void(0)" class="dropdown-item btn btn-xs bg-pink-700 text-white btnReview" role="menuitem" style="margin-left:5px;"
                data-id="'.$id_lpj.'" 
                data-proker_id="'.$id_proker.'" 
                data-status="'.$status_lpj.'" 
                data-catatan="'.$catatan_lpj.'" 
                data-nama_proker="'.$nama.'"
                data-tipe_proker="'.$stat_event.'"
                data-penanggungjawab="'.$penanggungjawab.'"
                data-tanggal_mulai="'.$tanggal_mulai.'"
                data-tanggal_selesai="'.$tanggal_selesai.'"
                data-tujuan="'.$tujuan.'"
                data-dana_turun="'.$dana_turun.'"
                data-dana_mandiri="'.$dana_mandiri.'"
                data-dana_usulan="'.$dana_usulan.'"
                data-flag_lv="'.$flag_level_lpj.'"
                data-jenis="LPJ"
                data-nama_file="'.$nama_file_lpj.'">
                <i class="icon fa-search"></i> Review </a>';
            $cek_review_lpj = '<a type="button" href="javascript:void(0)" class="dropdown-item btn btn-xs bg-grey-700 text-white btnCekReview" role="menuitem" style="margin-left:5px;"
                data-id="'.$id_lpj.'" 
                data-proker_id="'.$id_proker.'"
                data-status="'.$status_lpj.'" 
                data-catatan="'.$catatan.'"
                data-nama_proker="'.$nama.'"                      
                data-tipe_proker="'.$stat_event.'"
                data-penanggungjawab="'.$penanggungjawab.'"
                data-tanggal_mulai="'.$tanggal_mulai.'"
                data-tanggal_selesai="'.$tanggal_selesai.'"
                data-tujuan="'.$tujuan.'"
                data-dana_turun="'.$dana_turun.'"
                data-dana_mandiri="'.$dana_mandiri.'"
                data-dana_usulan="'.$dana_usulan.'"
                data-flag_lv="'.$flag_level_lpj.'"        
                data-jenis="LPJ"
                data-nama_file="'.$nama_file_lpj.'">
                <i class="icon fa-search"></i> Cek Review </a>';
            $perbaikan_lpj = '<a type="button"  data-id="'.$id_lpj.'" href="javascript:void(0)" class="dropdown-item btn btn-xs bg-amber-700 text-white btn_perbaikan_propLPJ" role="menuitem" style="margin-left:5px;"
                data-proker_id="'.$id_proker.'"
                data-nama_file="'.$nama_file_lpj.'"
                data-status="'.$status_lpj.'"
                data-jenis="LPJ"
                data-nama_proker="'.$nama.'">
                <i class="icon fa-wrench"></i> Perbaikan </a>';
            $edit_lpj = '<a type="button"  data-id="'.$id_lpj.'" href="javascript:void(0)" class="dropdown-item btn btn-xs bg-amber-700 text-white btn_edit_propLPJ" role="menuitem" style="margin-left:5px;"
                data-proker_id="'.$id_proker.'"
                data-nama_file="'.$nama_file_lpj.'"
                data-status="'.$status_lpj.'"
                data-jenis="LPJ"
                data-nama_proker="'.$nama.'">
                <i class="icon fa-edit"></i> Edit </a>';
            $cek_lpj = '<a href="javascript:void(0)" type="button" class="dropdown-item btn btn-xs btn-secondary btn_detail_propLPJ" role="menuitem" style="margin-left:5px;"
                data-proker_id="'.$id_proker.'"
                data-nama_file="'.$nama_file_lpj.'"
                data-status="'.$status_lpj.'"
                data-jenis="LPJ"
                data-nama_proker="'.$nama.'">
                <i class="icon fa-eye"></i> Cek </a>';
            $add_lpj = '<a type="button"  data-proker_id="'.$id_proker.'" data-nama_proker="'.$nama.'" data-jenis="LPJ" class="dropdown-item btn btn-xs btn-success btn_tambah_propLPJ" href="javascript:void(0)" role="menuitem"><i class="icon fa-plus"></i> LPJ </a>';          
            $hapus_lpj = '<a type="button" style="margin-left:5px;" data-id="'.$id_lpj.'" data-jenis="LPJ" class="dropdown-item btn btn-xs btn-danger bg-pink-600 btn_delete_propLPJ" href="javascript:void(0)" role="menuitem"><i class="icon fa-trash"></i> Delete </a>';          
            $file_path_lpj = asset('lpj_file').'/'.$nama_file_lpj;
            $download_file_lpj = '<a href="'.$file_path_lpj.'" download class="dropdown-item btn btn-xs btn-round btn-success" role="menuitem">Download file LPJ</a>';
                                    
            if ($flag_lv=="c") {
                if ($status_lpj === NULL ) {
                    if ($nim_admin == '') {
                        $aksi_lpj = $add_lpj;
                    }else{
                        $aksi_lpj = '';
                    }                    
                }elseif($status_lpj == 2 ) {
                    $aksi_lpj = $download_file_lpj.' '.$edit_lpj.' '.$hapus_lpj;
                }elseif($status_lpj  == 4 || $status_lpj  == 8){
                    $aksi_lpj = $download_file_lpj.' '.$cek_review_lpj.' '.$perbaikan_lpj;
                }else{
                    $aksi_lpj = $download_file_lpj;
                }
            }else if($flag_lv=="b"){
                if ($status_lpj  ==  2 || $status_lpj === 0) {
                    $aksi_lpj = $download_file_lpj.' '.$review_lpj;
                }else{
                    $aksi_lpj = '';
                }    
            }elseif ($flag_lv=="a") {
                if ($status_lpj  ==  3) {
                    $aksi_lpj = $download_file_lpj.' '.$review_lpj;                  
                }else{
                    $aksi_lpj = '';
                }                                               
            }else{
                $aksi_lpj = '';
            }
                                
            if ($aksi_lpj != '') {
                $aksi_lpj_grub = '<div class="btn-group">
                            <button type="button" class="btn btn-icon btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false" aria-hidden="true">
                                <i class="icon fa-gear fa-spin" aria-hidden="true"></i> Aksi
                            </button>
                            <div class="dropdown-menu" role="menu">
                                '.$aksi_lpj.'
                            </div>
                        </div>';
            }else{
                $aksi_lpj_grub = '-';
            }

            if($status === NULL)
            {
                $stat = '<span class="badge badge-secondary">Telah disetujui</span>';
                $aksi = $cek;                    
            } else {
                $info = '';
                if ($status == 4 || $status == 8) {
                    $info = '<span class="red-700"><i class="icon md-info-outline st_pengajuan" data-catatan="'.$catatan.'" aria-hidden="true"></i></span>';
                }else if ($status == 6) {
                    $info = '<span class="blue-700"><i class="icon md-info-outline st_pengajuan" data-catatan="'.$catatan.'" aria-hidden="true"></i></span>';
                }else{
                    $info = '';
                }

                $stat = DeHelper::status_pengajuan($status).$info;
                $stat_proposal = DeHelper::status_pengajuan($status_proposal);
                $stat_lpj = DeHelper::status_pengajuan($status_lpj);
            }

            $tanggal = DeHelper::format_tanggal($value->tanggal_mulai).' s/d '.DeHelper::format_tanggal($value->tanggal_selesai);
            $nama_ = $nama.' '.'<span class="blue-400"><i class="icon md-info-outline st_nama" data-tanggal="'.$tanggal.'" data-pj="'.$nama_pj.'" aria-hidden="true"></i></span>';
            $rating_ = '<div class="rating" data-rating="'.$rating.'" data-evaluasi="'.$evaluasi.'"></div>';
            $dataTabel[] = array(
                'no' => $no++,
                'id_proker' => $id_proker,
                'id_struktur' => $id_struktur,
                'nama' => $nama_,
                'tanggal' => $tanggal,
                'dana_usulan' => $dana_usulan,
                'dana_terpakai' => $dana_terpakai,
                'dana_turun' => $dana_turun,
                'ket_tambahan' => $ket_tambahan,
                'penanggungjawab' => $penanggungjawab,                
                'nama_pj' => $nama_pj,                
                'event' => $event,
                'stat_event' => $stat_event,
                'tujuan' => $tujuan,
                'subjek_tujuan' => $subjek_tujuan,
                'status' => $status,
                'catatan' => $catatan,
                'nama_struktur' => $nama_struktur,
                'terlaksana' => $terlaksana,
                'status' => $stat,
                'status_prop' => $status_proposal,
                'status_proposal' => $stat_proposal,
                'aksi_prop' => $aksi_proposal,
                'aksi_proposal' => $aksi_proposal_grub,
                'stat_lpj' => $status_lpj,
                'status_lpj' => $stat_lpj,
                'act_lpj' => $aksi_lpj,
                'aksi_lpj' => $aksi_lpj_grub,
                'flag_level_lpj' => $flag_level_lpj,
                'flag_level_proposal' => $flag_level_proposal,
                'rating' => $rating_,
                'evaluasi' => $evaluasi,
                'aksi'=>$aksi
            );                 
        }
        // return $dataTabel;

        $data_ = (isset($dataTabel)?$dataTabel:[]);

        return DataTables::of($data_)
            ->addColumn('nama', function($data_){
                return $data_['nama'];
            })
            ->addColumn('stat_event', function($data_){
                return $data_['stat_event'];
            })
            ->addColumn('status', function($data_){
                return $data_['status'];
            })
            ->addColumn('terlaksana', function($data_){
                return $data_['terlaksana'];
            })
            ->addColumn('aksi', function($data_){
                return $data_['aksi'];
            })
            ->addColumn('status_proposal', function($data_){
                return $data_['status_proposal'];
            })
            ->addColumn('aksi_proposal', function($data_){
                return $data_['aksi_proposal'];
            })
            ->addColumn('status_lpj', function($data_){
                return $data_['status_lpj'];
            })
            ->addColumn('aksi_lpj', function($data_){
                return $data_['aksi_lpj'];
            })
            ->addColumn('rating', function($data_){
                return $data_['rating'];
            })
            ->rawColumns(['stat_event','status','terlaksana','aksi','nama','status_proposal','aksi_proposal','status_lpj','aksi_lpj','rating'])
            ->make(true);

        // return $data;
    }

    public function index_deleted()
    {
        $data['cek_page'] = 'deleted';
        return view('proker.proker_h', $data);
    }

    public function dataTable_proker_deleted(Request $request)
    {
        $user 		= Session::get("data_user");
        
        $flag_lv = $user['flag_lv'];
        $nim = $user['nim'];
        $key_menu = $user['key_menu'];

        $data_isi = DB::table('kegiatan.proker as proker')
                ->leftJoin(DB::raw('(SELECT A.* FROM  approve.approv_proker A
                    JOIN ( SELECT proker_id, MAX ( ID ) AS iid FROM approve.approv_proker GROUP BY proker_id ) B
                    ON A.ID = B.iid AND A.proker_id = B.proker_id) as aprov'),'aprov.proker_id','=','proker.id')
                ->leftjoin('keanggotaan.anggota as ang','ang.nim','=','proker.penanggungjawab')                                
                ->leftJoin('master.m_struktur as ms','ms.id','=','proker.id_struktur')
                // ->get();
                // return $total_data;     
                ->when($key_menu, function ($query) use ($key_menu) {
                    if ($key_menu == "c" || substr($key_menu, 0, 1)  == 'e') {
                        return $query->whereIn('aprov.status',array(9));                  
                    }else{
                        return $query->whereIn('aprov.status',array(9));                           
                    }})                
                ->selectRaw("proker.id, proker.*, aprov.status, aprov.catatan, ms.nama as nama_struktur, ang.nama as nama_pj")
                ->orderBy('proker.tanggal_mulai','desc')
                ->get();
        // return $data_isi;

        $no = 1;
        foreach ($data_isi as $value) {
            $id_proker = DeHelper::encrypt_($value->id);
            $id_struktur = (isset($value->id_struktur)) ? $value->id_struktur : NULL;
            $nama=(isset($value->nama)) ? $value->nama : NULL;
            $tanggal_mulai =(isset($value->tanggal_mulai)) ? $value->tanggal_mulai : NULL;
            $tanggal_selesai =(isset($value->tanggal_selesai)) ? $value->tanggal_selesai : NULL;
            $dana_usulan =(isset($value->dana_usulan)) ? $value->dana_usulan : NULL;
            $dana_terpakai=(isset($value->dana_terpakai)) ? $value->dana_terpakai : NULL;
            $dana_turun=(isset($value->dana_turun)) ? $value->dana_turun : NULL;
            $ket_tambahan=(isset($value->ket_tambahan)) ? $value->ket_tambahan : NULL;
            $penanggungjawab=(isset($value->penanggungjawab)) ? $value->penanggungjawab : NULL;

            $event=(isset($value->event)) ? $value->event : NULL;
            $event = ($event == false) ? 0 : 1;

            $tujuan=(isset($value->tujuan)) ? $value->tujuan : NULL;;
            $subjek_tujuan=(isset($value->subjek_tujuan)) ? $value->subjek_tujuan : NULL;
            $status=(isset($value->status)) ? $value->status : NULL;
            $catatan=(isset($value->catatan)) ? $value->catatan : '-';
            $nama_struktur=(isset($value->nama_struktur)) ? $value->nama_struktur : NULL;
            $nama_pj=(isset($value->nama_pj)) ? $value->nama_pj : '-';
            $terlaksana=(isset($value->terlaksana)) ? $value->terlaksana : NULL;
                        
            if ($terlaksana == true) {
                $terlaksana = '<span class="badge badge-success">Terlaksana</span>';
            }else{
                if (strtotime($tanggal_mulai) >= strtotime(date('d-m-Y'))) {
                    $terlaksana = '<span class="badge badge-secondary">Belum Terlaksana</span>';
                }elseif(strtotime($tanggal_mulai) <= strtotime(date('d-m-Y')) && strtotime($tanggal_selesai) >= strtotime(date('d-m-Y'))){
                    $terlaksana = '<span class="badge badge-info">Sedang berlangsung</span>';
                }else{
                    $terlaksana = '<span class="badge badge-danger">Tidak Terlaksana</span>';
                }
            }
            // $link_hapus = url('surat_keterangan/hapus_penghargaan/'.$id_riwayat);
            $aprove = '<a type="button" data-id="'.$id_proker.'"  href="javascript:void(0)" class="btn btn-xs btn-success btnAprove" ><i class="icon glyphicon-ok"></i> Aprove </a>';
            $ditolak = '<a type="button"  data-id="'.$id_proker.'" class="btn btn-xs btn-danger show_modal_tolak" href=javascript:void(0)><i class="icon glyphicon-remove"></i> Ditolak </a>';
            $perbaikan = '<a type="button"  data-id="'.$id_proker.'" href="javascript:void(0)" class="btn btn-xs btn-dark btn_perbaikan btnRevisi"
                data-id_struktur="'.$id_struktur.'"
                data-nama="'.$nama.'"
                data-tanggal_mulai="'.$tanggal_mulai.'"
                data-tanggal_selesai="'.$tanggal_selesai.'"
                data-dana_usulan="'.$dana_usulan.'"
                data-dana_terpakai="'.$dana_terpakai.'"
                data-dana_turun="'.$dana_turun.'"
                data-ket_tambahan="'.$ket_tambahan.'"
                data-penanggungjawab="'.$penanggungjawab.'"
                data-event="'.$event.'"
                data-tujuan="'.$tujuan.'"
                data-subjek_tujuan="'.$subjek_tujuan.'"
                data-status="'.$status.'"
                data-catatan="'.$catatan.'"
                data-nama_pj="'.$nama_pj.'"                
                data-nama_struktur="'.$nama_struktur.'">
            <i class="icon fa-wrench"></i> perbaikan </a>';
            $detail = '<a href="javascript:void(0)" type="button" class="btn btn-xs btn-info btnDetail"
                data-id="'.$id_proker.'"
                data-id_struktur="'.$id_struktur.'"
                data-nama="'.$nama.'"
                data-tanggal_mulai="'.$tanggal_mulai.'"
                data-tanggal_selesai="'.$tanggal_selesai.'"
                data-dana_usulan="'.$dana_usulan.'"
                data-dana_terpakai="'.$dana_terpakai.'"
                data-dana_turun="'.$dana_turun.'"
                data-ket_tambahan="'.$ket_tambahan.'"
                data-penanggungjawab="'.$penanggungjawab.'"
                data-event="'.$event.'"
                data-tujuan="'.$tujuan.'"
                data-subjek_tujuan="'.$subjek_tujuan.'"
                data-status="'.$status.'"
                data-catatan="'.$catatan.'"
                data-nama_pj="'.$nama_pj.'"                
                data-nama_struktur="'.$nama_struktur.'">
            <i class="icon glyphicon-eye-open"></i> Detail </a>';
            $edit = '<a type="button" href="javascript:void(0)" class="btn btn-xs btn-primary bg-purple-600 btnEdit"
                data-id="'.$id_proker.'"
                data-id_struktur="'.$id_struktur.'"
                data-nama="'.$nama.'"
                data-tanggal_mulai="'.$tanggal_mulai.'"
                data-tanggal_selesai="'.$tanggal_selesai.'"
                data-dana_usulan="'.$dana_usulan.'"
                data-dana_terpakai="'.$dana_terpakai.'"
                data-dana_turun="'.$dana_turun.'"
                data-ket_tambahan="'.$ket_tambahan.'"
                data-penanggungjawab="'.$penanggungjawab.'"
                data-event="'.$event.'"
                data-tujuan="'.$tujuan.'"
                data-subjek_tujuan="'.$subjek_tujuan.'"
                data-status="'.$status.'"
                data-catatan="'.$catatan.'"
                data-nama_pj="'.$nama_pj.'"                
                data-nama_struktur="'.$nama_struktur.'">
            <i class="icon glyphicon-pencil"></i> Edit </a>';
            $hapus = '<a type="button"  data-id="'.$id_proker.'"  class="btn btn-xs btn-danger bg-pink-600 btnDelete" href=javascript:void(0)><i class="icon fa-trash"></i> Delete </a>';
            $recover = '<a type="button"  data-id="'.$id_proker.'"  class="btn btn-xs bg-light-blue-600 text-white btnRecover" href=javascript:void(0)><i class="icon fa-recycle"></i> Recover </a>';
            $true_hapus = '<a type="button"  data-id="'.$id_proker.'"  class="btn btn-xs btn-danger bg-pink-600 btnTDelete" href=javascript:void(0)><i class="icon fa-trash"></i> Delete </a>';

            if ($flag_lv=="c") {
                if ($status  == 9){
                    $aksi= $detail.' '.$recover.' '.$true_hapus;
                }else{
                    $aksi= $detail;
                }
            }else if($flag_lv=="b"){
                if ($status  == 9) {
                    $aksi= $detail;
                }else{
                    $aksi= $detail;
                }
            }elseif ($flag_lv=="a") {
                if ($status  == 9) {
                    $aksi= $detail;
                }else{
                    $aksi= $detail;
                }
            } else{
                $aksi= $detail;
            }

            if($status === NULL)
            {
                $stat = '<span class="badge badge-secondary">Telah disetujui</span>';
                $aksi = $detail;                    
            } else {
                $info = '';
                if ($status == 4 || $status == 8) {
                    $info = '<span class="red-700"><i class="icon md-info-outline st_pengajuan" data-catatan="'.$catatan.'" aria-hidden="true"></i></span>';
                }else{
                    $info = '';
                }
                $stat = DeHelper::status_pengajuan($status).$info;
            }

            $stat_event = '';
            if($event == 1)
            {
                $stat_event = '<span class="badge bg-blue-600 text-white">Normal</span>';
            } else if($event == 2) {
                $stat_event = '<span class="badge bg-purple-600 text-white">Event</span>';
            }else{
                $stat_event = '<span class="badge badge-secondary">belum dipilih</span>';
            }

            $dataTabel[] = array(
                'no' => $no++,
                'id_proker' => $id_proker,
                'id_struktur' => $id_struktur,
                'nama' => $nama,
                'tanggal' => DeHelper::format_tanggal($value->tanggal_mulai).' s/d '.DeHelper::format_tanggal($value->tanggal_selesai),
                'dana_usulan' => $dana_usulan,
                'dana_terpakai' => $dana_terpakai,
                'dana_turun' => $dana_turun,
                'ket_tambahan' => $ket_tambahan,
                'penanggungjawab' => $penanggungjawab,                
                'nama_pj' => $nama_pj,                
                'event' => $event,
                'stat_event' => $stat_event,
                'tujuan' => $tujuan,
                'subjek_tujuan' => $subjek_tujuan,
                'status' => $status,
                'catatan' => $catatan,
                'nama_struktur' => $nama_struktur,
                'terlaksana' => $terlaksana,
                'status' => $stat,
                'aksi'=>$aksi
            );

            // if($flag_lv == "A") {
            // }else{
            //     if($status == 12) {
            //         $dataNotif = DB::table('notif.notifikasi as ntf')
            //                         ->where('ntf.id_riwayat','=',$value->id)
            //                         ->update([
            //                             'status_read' => 1
            //                         ]);
            //     }
            // }                     
        }
        // return $dataTabel;

        $data_ = (isset($dataTabel)?$dataTabel:[]);

        return DataTables::of($data_)
            ->addColumn('stat_event', function($data_){
                return $data_['stat_event'];
            })
            ->addColumn('status', function($data_){
                return $data_['status'];
            })
            ->addColumn('terlaksana', function($data_){
                return $data_['terlaksana'];
            })
            ->addColumn('aksi', function($data_){
                return $data_['aksi'];
            })
            ->rawColumns(['stat_event','status','terlaksana','aksi'])
            ->make(true);
    }
    
    public function proker_diapain(Request $request)
    {
        if ($request->action == 'tambah') {
            $this->tambahProker($request);
            $data['code']="200";
            $data['message']="Data proker berhasil ditambahkan!";
            return response()->json($data);            
            try {
            } catch (\Throwable $th) {
                $data['code']="500";
                $data['message']=$th;
                return response()->json($data);
            }
        }elseif ($request->action == 'edit') {
            try {
                $this->updateProker($request);
                $data['code']="200";
                $data['message']="Data proker berhasil diupdate!";
                return response()->json($data);exit();  
            } catch (\Throwable $th) {
                $data['code']="500";
                $data['message']=$th;
                return response()->json($data);
            }            
        }elseif ($request->action == 'perbaikan') {
            try {
                $this->updateProker($request);
                $data['code']="200";
                $data['message']="Data proker berhasil diperbaiki!";
                return response()->json($data);exit();  
            } catch (\Throwable $th) {
                $data['code']="500";
                $data['message']=$th;
                return response()->json($data);
            }    
        }else{
                $data['code']="404";
                $data['message']='action tidak diketahui';
                return response()->json($data);
        }
    }

    public function tambahProker($request)
    {
        $session = Session::get('data_user');

        $flag_lv = $session['flag_lv'];
        $nim = $session['nim'];
        // return $session;
        // return $request->all();
        // return date("Y-m-d H:i:s");

        if ($flag_lv =='a') {
            $is_final = true;             
        } else {
            $is_final = false;             
        }

        $dana_turun = DeHelper::rupiah2number($request->dana_turun);
        $dana_mandiri = DeHelper::rupiah2number($request->dana_mandiri);

        $insert = array(
            'nama' => $request->nama,
            'event' => $request->tipe_proker,
            'penanggungjawab' => $request->penanggungjawab,
            'tanggal_mulai' => $request->tgl_mulai,
            'tanggal_selesai' => $request->tgl_selesai,
            'tujuan' => $request->tujuan,
            'ket_tambahan' => $request->ket_tambahan,
            'dana_turun' => $dana_turun,
            'dana_mandiri' => $dana_mandiri,
            'dana_usulan' => $request->total_biaya, 
            'subjek_tujuan' => $request->subjek_tujuan, 
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),
            'is_final' => $is_final,            
            'created_by' => $nim
        );

        $id = DB::table('kegiatan.proker')->insertGetId($insert);

        $admin =[
            'proker_id' => $id,
            'nim' => $request->penanggungjawab,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")            
        ];
        DB::table('kegiatan.admin_kegiatan')->insert($admin);

        $pj =[
            'id_proker' => $id,
            'id_kepanitiaan' => '1',
            'nim' => $request->penanggungjawab,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")            
        ];        
        DB::table('kegiatan.kepanitiaan')->insert($pj);

        if ($id!=null) {
            $data['code']="100";
            $data['message']="Data proker berhasil ditambahkan!";
        }else{
            $data['code']="500";
            $data['message']="Data proker gagal ditambahkan!";
            return response()->json($data);
        } 

        $sie_panitia = (isset($request->sie_panitia))?$request->sie_panitia:[];
        $kebutuhan = $request->kebutuhan;
        $kabel = $request->kabel;
        $jumlah = $request->jumlah;
        $satuan = $request->satuan;
        $harga_satuan = $request->harga_satuan;
        
        foreach ($kebutuhan as $key => $value) {

            if(
                $kebutuhan[$key] != null &&
                $jumlah[$key] != null &&
                $harga_satuan[$key] != null &&
                $satuan[$key] != null
            ){
                $data_keb[$key]['id_proker'] = $id;
                $data_keb[$key]['id_sie_kepanitiaan'] = (isset($sie_panitia[$key])) ? $sie_panitia[$key] : null;
                $data_keb[$key]['id_kabel'] = (isset($kebutuhan[$key])) ? $kebutuhan[$key] : null;
                $data_keb[$key]['keterangan'] = (isset($kabel[$key])) ? $kabel[$key] : null;
                $data_keb[$key]['jumlah'] = (isset($jumlah[$key])) ? $jumlah[$key] : null;
                $data_keb[$key]['harga_satuan'] = (isset($harga_satuan[$key])) ? str_replace(".","",str_replace("Rp.","",$harga_satuan[$key])) : null;
                $data_keb[$key]['satuan'] = (isset($satuan[$key])) ? $satuan[$key] : null;
                $data_keb[$key]['created_at'] = date("Y-m-d H:i:s");
            }
        }
    
        if(!empty($data_keb)){
            DB::table('kegiatan.kebutuhan_event')->insert($data_keb);
        }
        
        try {
        } catch (\Throwable $th) {
            $data['code']="404";
            $data['message']=$th;
            return response()->json($data);
        }

        // inserting status Approval
        if ($flag_lv == "c") {
            $data_approv['status'] = 1; 
            $data_approv['send_to'] = 'b'; 
        } 
        elseif ($flag_lv =='b') {
            $data_approv['status'] = 2; 
            $data_approv['send_to'] = 'a'; 
        } 
        elseif ($flag_lv =='a') {
            $data_approv['status'] = 4; 
            $data_approv['send_to'] = 'c'; 
        }            
        else{
            $data_approv['status'] = 1; 
            $data_approv['send_to'] = 'b'; 
        }

        $data_approv['proker_id'] = $id;
        $data_approv['created_at'] = date("Y-m-d H:i:s");
        $data_approv['created_by'] = $nim;
        DB::table('approve.approv_proker')->insert($data_approv);      

            $notif['action'] = $request->action;
            $notif['id_subjek'] = $data_approv['proker_id'];
            $notif['status'] = $data_approv['status'];
            DeHelper2::simpanNotif($notif);   
    }

    public function getDetailKeb($id_proker)
    {
        $id = DeHelper::decrypt_($id_proker);
        $datas = DB::table('kegiatan.kebutuhan_event as ke')
                    ->leftjoin('master.m_sie_kepanitiaan as msk','msk.id','=','ke.id_sie_kepanitiaan')
                    ->where('id_proker','=',$id)
                    ->selectRaw('ke.*, msk.nama as nama_kepanitiaan')
                    ->get();
        return response()->json($datas);
    }

    public function query_update($request)
    {
        $dana_turun = DeHelper::rupiah2number($request->dana_turun);
        $dana_mandiri = DeHelper::rupiah2number($request->dana_mandiri);

        $update = array(
            'nama' => $request->nama,
            'event' => $request->tipe_proker,
            'penanggungjawab' => $request->penanggungjawab,
            'tanggal_mulai' => $request->tgl_mulai,
            'tanggal_selesai' => $request->tgl_selesai,
            'tujuan' => $request->tujuan,
            'ket_tambahan' => $request->ket_tambahan,
            'dana_turun' => $dana_turun,
            'dana_mandiri' => $dana_mandiri,
            'dana_usulan' => $request->total_biaya, 
            'subjek_tujuan' => $request->subjek_tujuan, 
            'updated_at' => date("Y-m-d H:i:s"),
            'created_by' => $request->nim_session
        );

        $query = DB::table('kegiatan.proker')->where([['id', '=', $request->id_decripted]])->update($update);
        
        $admin = array(
            'nim' => $request->penanggungjawab,
        );
        
        DB::table('kegiatan.admin_kegiatan')->where([['proker_id', '=', $request->id_decripted]])->update($admin);
        
        $pj =[
            'nim' => $request->penanggungjawab,
            'updated_at' => date("Y-m-d H:i:s")            
        ];        

        DB::table('kegiatan.kepanitiaan')->where([['id_proker', '=', $request->id_decripted],['id_kepanitiaan', '=', '1']])->update($pj);
        
        $sie_panitia = (isset($request->sie_panitia))?$request->sie_panitia:[];
        $kebutuhan = $request->kebutuhan;
        $kabel = $request->kabel;
        $jumlah = $request->jumlah;
        $satuan = $request->satuan;
        $harga_satuan = $request->harga_satuan;
        
        foreach ($kebutuhan as $key => $value) {
            $data_keb[$key]['id_proker'] = $request->id_decripted;
            $data_keb[$key]['id_sie_kepanitiaan'] = (isset($sie_panitia[$key])) ? $sie_panitia[$key] : null;
            $data_keb[$key]['id_kabel'] = (isset($kebutuhan[$key])) ? $kebutuhan[$key] : null;
            $data_keb[$key]['keterangan'] = (isset($kabel[$key])) ? $kabel[$key] : null;
            $data_keb[$key]['jumlah'] = (isset($jumlah[$key])) ? $jumlah[$key] : null;
            $data_keb[$key]['harga_satuan'] = (isset($harga_satuan[$key])) ? DeHelper::rupiah2number($harga_satuan[$key]): null;
            $data_keb[$key]['satuan'] = (isset($satuan[$key])) ? $satuan[$key] : null;
            $data_keb[$key]['created_at'] = date("Y-m-d H:i:s");
        }

        DB::table('kegiatan.kebutuhan_event')->where([['id_proker', '=', $request->id_decripted]])->delete();
        DB::table('kegiatan.kebutuhan_event')->insert($data_keb);
        try {
        } catch (\Throwable $th) {
            $data['code']="404";
            $data['message']=$th;
            return response()->json($data);
        }
    }

    public function updateProker($request)
    {
        $session = Session::get('data_user');
        $id = DeHelper::decrypt_($request->id);       
        $request->id_decripted = $id;
        
        $nim = $session['nim'];
        $request->nim_session = $nim;

        $flag_lv = $session['flag_lv'];

        // Update
        if ($request->action == 'edit') {
            $this->query_update($request);

        // Perbaikan
        }elseif($request->action == 'perbaikan') {
            $this->query_update($request);
            // inserting status Approval
            if ($flag_lv == "c") {
                $data_approv['status'] = 0; 
                $data_approv['send_to'] = 'b'; 
            }else{
                $data_approv['status'] = 0; 
                $data_approv['send_to'] = 'b'; 
            }
            $data_approv['proker_id'] = $id;
            $data_approv['created_at'] = date("Y-m-d H:i:s");
            $data_approv['created_by'] = $nim;
            DB::table('approve.approv_proker')->insert($data_approv);                             
            
            $notif['action'] = $request->action;
            $notif['id_subjek'] = $data_approv['proker_id'];
            $notif['status'] = $data_approv['status'];
            DeHelper2::simpanNotif($notif);               
        }
    }

    public function deleteProker($id_proker)
    {
        $session = Session::get('data_user');
        $id = DeHelper::decrypt_($id_proker);       
        $nim = $session['nim'];
        $flag_lv = $session['flag_lv']; 
        
        if ($flag_lv == "c") {
            $data_approv['status'] = 9; 
            $data_approv['send_to'] = 'b'; 
        }else{
            $data_approv['status'] = 9; 
            $data_approv['send_to'] = 'b'; 
        }
        $data_approv['proker_id'] = $id;
        $data_approv['created_at'] = date("Y-m-d H:i:s");
        $data_approv['created_by'] = $nim;
        
        DB::table('approve.approv_proker')->insert($data_approv);
        
        $data['code']="200";
        $data['message']="Data proker berhasil dihapus!";
        return response()->json($data);              
    }

    public function trueDeleteProker($id_proker)
    {
        $id = DeHelper::decrypt_($id_proker);  
        $datas = DB::table('kegiatan.proker')->where('id', '=', $id)->delete();

        $data['code']="200";
        $data['message']="Data proker berhasil dihapus permanen!";
        return response()->json($data);   
        // DB::table('kegiatan.kebutuhan_event')->where([['id_proker', '=', $request->id_decripted]])->delete();
    }

    public function recoveryProker($id_proker)
    {
        $id = DeHelper::decrypt_($id_proker);  
        $id_approv = DB::table('approve.approv_proker')->where('proker_id', '=', $id)->max('id');
        try {
            DB::table('approve.approv_proker')->where('id', '=', $id_approv)->delete();

            $data['code']="200";
            $data['message']="Data proker berhasil direcovey!";            
            return response()->json($data);   
        } catch (\Throwable $th) {
            return response()->json($th);   
        }

    }

    public function approval($jenis,$id_proker)
    {
        // return '';
        $session = Session::get('data_user');
        $id = DeHelper::decrypt_($id_proker);       
        $nim = $session['nim'];
        $flag_lv = $session['flag_lv']; 
        $key_menu = $session['key_menu'];
        $kode_admin = substr($key_menu,0,1);

        if ($jenis == 'approve') {
            $action = 'approve';           
            if ($flag_lv == "a") {
                $data_approv['status'] = 5; 
                $data_approv['send_to'] = 'c'; 
                $data['message']="Data proker disetujui DPK!";
            }elseif ($flag_lv == "b") {
                $data_approv['status'] = 3; 
                $data_approv['send_to'] = 'a'; 
                $data['message']="Data proker disetujui Ketua UKM!";
            }else{
                $data['code']="200";
                $data['message']="user tidak berhak atas aksi ini!";
                return response()->json($data);exit();
            }
        } elseif ($jenis == 'kirim_ke_ketua') {
            $action = 'kirim_ke_ketua';              
            if ($flag_lv == "c" || $kode_admin == 'e') {
                $data_approv['status'] = 2; 
                $data_approv['send_to'] = 'b';
                $data['message']="Data proker berhasil dikirim ke Ketua UKM!";
            }else{
                $data['code']="200";
                $data['message']="user tidak berhak atas aksi ini!";
                return response()->json($data);exit();
            }
        } elseif ($jenis == 'telah_disesuaikan') {
            $action = 'penyesuaian';               
            if ($flag_lv == "c" || $kode_admin == 'e') {
                $data_approv['status'] = 7; 
                $data_approv['send_to'] = 'a';
                $data['message']="Pengesuaian data proker berhasil!";
            }else{
                $data['code']="200";
                $data['message']="user tidak berhak atas aksi ini!";
                return response()->json($data);exit();
            }
        } elseif ($jenis == 'review_ok') {
            $action = 'approve';               
            if ($flag_lv == "a") {
                $data_approv['status'] = 5; 
                $data_approv['send_to'] = 'b';
                $data['message']="Data proker telah direview oleh DPK!";
            }else{
                $data['code']="200";
                $data['message']="user tidak berhak atas aksi ini!";
                return response()->json($data);exit();
            }
        } elseif ($jenis == 'telah_diperbaiki') {
            $action = 'perbaikan';               
            if ($flag_lv == "c" || $kode_admin == 'e') {
                $data_approv['status'] = 0; 
                $data_approv['send_to'] = 'b'; 
                $data['message']="Data proker berhasil diperbaiki!";                
            }else{
                $data['code']="200";
                $data['message']="user tidak berhak atas aksi ini!";
                return response()->json($data);exit();
            }
        } else {
            $data['code']="200";
            $data['message']="jenis tidak diketahui!";
            return response()->json($data);exit();
        }

        $data_approv['proker_id'] = $id;
        $data_approv['created_at'] = date("Y-m-d H:i:s");
        $data_approv['created_by'] = $nim;
        DB::table('approve.approv_proker')->insert($data_approv);
        
        $notif['action'] = $action;
        $notif['id_subjek'] = $data_approv['proker_id'];
        $notif['status'] = $data_approv['status'];
        DeHelper2::simpanNotif($notif);     

        $data['code']="200";
        return response()->json($data);    
    }

    public function approval2(Request $request)
    {
        $session = Session::get('data_user');
        $id = DeHelper::decrypt_($request->id_proker);       
        $nim = $session['nim'];
        $flag_lv = $session['flag_lv']; 
        $jenis = $request->jenis;
        
        if ($jenis == 'ditolak') {
            $action = 'tolak';               
            if ($flag_lv == "a") {
                $data_approv['status'] = 8; 
                $data_approv['send_to'] = 'c'; 
                $data['message']="Data proker ditolak DPK!";                
            }elseif ($flag_lv == "b") {
                $data_approv['status'] = 4; 
                $data_approv['send_to'] = 'c'; 
                $data['message']="Data proker ditolak Ketua UKM!";                
            }else{
                $data['code']="200";
                $data['message']="user tidak berhak atas aksi ini!";
                return response()->json($data);exit();
            }
        } elseif ($jenis == 'approve_ct') {
            $action = 'approve_dc';               
            if ($flag_lv == "a") {
                $data_approv['status'] = 6; 
                $data_approv['send_to'] = 'c'; 
                $data['message']="Data proker disetujui dengan catatan!";                            
            }else{
                $data['code']="200";
                $data['message']="user tidak berhak atas aksi ini!";
                return response()->json($data);exit();
            }
        } else {
            $data['code']="200";
            $data['message']="user tidak berhak atas aksi ini!";
            return response()->json($data);exit();
        }

        $data_approv['proker_id'] = $id;
        $data_approv['catatan'] = $request->catatan;
        $data_approv['created_at'] = date("Y-m-d H:i:s");
        $data_approv['created_by'] = $nim;
        DB::table('approve.approv_proker')->insert($data_approv);
        
        $notif['action'] = $action;
        $notif['id_subjek'] = $data_approv['proker_id'];
        $notif['status'] = $data_approv['status'];
        DeHelper2::simpanNotif($notif);     

        $data['code']="200";
        return response()->json($data);   
    }

    public function approveDC(Request $request)
    {
        $session = Session::get('data_user');
        $id = DeHelper::decrypt_($request->id_proker);       
        $nim = $session['nim'];
        $flag_lv = $session['flag_lv']; 
        
        if ($flag_lv == "a") {
            $data_approv['status'] = 5; 
            $data_approv['send_to'] = 'c'; 
        }elseif ($flag_lv == "b") {
            $data_approv['status'] = 3; 
            $data_approv['send_to'] = 'a'; 
        }else{
            $data['code']="200";
            $data['message']="user tidak berhak atas aksi ini!";
            return response()->json($data);exit();
        }

        $data_approv['proker_id'] = $id;
        $data_approv['catatan'] = $request->catatan;
        $data_approv['created_at'] = date("Y-m-d H:i:s");
        $data_approv['created_by'] = $nim;
        
        DB::table('approve.approv_proker')->insert($data_approv);
        
        $data['code']="200";
        $data['message']="Data proker berhasil ditolak!";
        return response()->json($data);   
    }

    public function select2Proker(Request $request)
    {
        $search = $request->search;
        $jenis = $request->jenis;
        // $id_proker = $request->id_proker;

        if ($jenis == 'proposal') {
            $id_get = DB::table('kegiatan.proposal')
                            ->select('proker_id')
                            ->pluck('proker_id')->toArray();
        }elseif ($jenis == 'lpj') {
            $id_get = DB::table('kegiatan.lpj')
                            ->select('proker_id')
                            ->pluck('proker_id')->toArray();                       
        }

        $query = DB::table('kegiatan.proker as proker')
                ->leftJoin(DB::raw('(SELECT A.* FROM  approve.approv_proker A
                    JOIN ( SELECT proker_id, MAX ( ID ) AS iid FROM approve.approv_proker GROUP BY proker_id ) B
                    ON A.ID = B.iid AND A.proker_id = B.proker_id) as aprov'),'aprov.proker_id','=','proker.id')  
                ->when($jenis, function ($query) use ($jenis) {
                    if ($jenis == "proposal") {
                        return $query->whereIn('aprov.status',array(1,3,4,5,6,8));
                    }else{
                        return $query;
                    }
                })                        
                ->when($jenis, function ($query) use ($jenis) {
                    if ($jenis == "lpj") {
                        return $query->whereIn('aprov.status',array(3,5))  
                                    ->where('proker.tanggal_mulai','<=',DB::RAW("'".date('Y-m-d')."'::date"));
                    }else{
                        return $query;
                    }
                })                   
                ->where(DB::raw('EXTRACT(YEAR FROM proker.tanggal_mulai)'),'>=',date('Y')-1)                 
                ->whereRaw("( nama ilike '%".$search."%' or penanggungjawab ilike '%".$search."%' )")
                ->whereNotIn('proker.id', $id_get)
                ->selectRaw("proker.id as id_proker, proker.nama as nama_proker")
                ->orderBy('tanggal_mulai','desc')
                ->get();                
        // return $query;
                
        $data = array();
        foreach ($query as $key => $value) {
            $data[$key]['id']=$value->id_proker;
            $data[$key]['nama']=$value->nama_proker;
        }
        // $array[] = array('a'=>'a');
        return response()->json($data);
    }    

    public function getFormReview(Request $request)
    {
        $jenis = (isset($request->jenis)) ? $request->jenis : '';
        $proLPJ_id = (isset($request->proLPJ_id)) ? DeHelper::decrypt_($request->proLPJ_id) : '';

        $session = Session::get('data_user');
        $nim = $session['nim'];
        $flag_lv = $session['flag_lv']; 

        $dataKriteria = m_kriteria_penilaian::when($jenis, function ($query) use ($jenis,$flag_lv) {
                                    if ($jenis == "LPJ") {
                                        return $query   ->where([['is_lpj','=','1'],['for_user','=',$flag_lv]]);
                                    }else{
                                        return $query   ->where([['is_lpj','=','0'],['for_user','=',$flag_lv]]);
                                    }
                                })
                                ->orderBy('master.m_kriteria_penilaian.id','asc')->get();  
        // return $dataKriteria;
        $content = '';
        foreach ($dataKriteria as $key => $value) {
            if ($jenis == "LPJ") {
                $dataReview = review_lpj::where([['lpj_id','=',$proLPJ_id],['kriteria_id','=',$value->id]])->get()->first();
            }else{
                $dataReview = review_proposal::where([['proposal_id','=',$proLPJ_id],['kriteria_id','=',$value->id]])->get()->first();
            }
            $status = (isset($dataReview->status)) ? $dataReview->status : '';
            $catatan = (isset($dataReview->catatan)) ? $dataReview->catatan : '';
            $status = ($status == 1) ? 'checked' : '';

            $content .= '<div class="card border-primary w-p100 mb-10 kolomPenilaian">
                <div class="card-body pb-0">
                    <div class="form-group form-material row" data-plugin="formMaterial">
                        <div class="col-md-10">
                            <label class="form-control-label" for="nama">'.++$key.'. '.$value->nama.'</label>
                        </div>
                        <div class="col-md-2 text-right">
                            <input type="checkbox" class="status" id="status'.$value->id.'" '.$status.' name="status[]" value="1" data-id="'.$value->id.'"/>
                        </div>
                        <div class="col-md-12">
                            <input hidden type="text" id="kriteria_id'.$value->id.'" name="kriteria_id[]" value="'.$value->id.'" data-id="'.$value->id.'">
                            <textarea class="form-control catatan" id="catatan'.$value->id.'" name="catatan[]" placeholder="catatan" data-hint="">'.$catatan.'</textarea>
                        </div>
                    </div>   
                </div>
            </div>';

        }

        return $content;
    }

    public function kinerja_pj(Request $request)
    {
        $data['now_year'] = date('Y');
        $data['now_month'] = date('m');

        return view('proker.kinerja_pj', $data);
    }

    public function getKinerjaPJ(Request $request)
    {
        $tahun = (isset($request->tahun)) ? $request->tahun : 0;
        $bulan = (isset($request->bulan)) ? $request->bulan : 0;

        $user 		= Session::get("data_user");
        $flag_lv = $user['flag_lv'];
        $nim = $user['nim'];
        $key_menu = $user['key_menu'];
        $id_gets = [];

        $data_isi = DB::table('kegiatan.proker as proker')
                ->leftJoin(DB::raw('(SELECT A.* FROM  approve.approv_proker A
                    JOIN ( SELECT proker_id, MAX ( ID ) AS iid FROM approve.approv_proker GROUP BY proker_id ) B
                    ON A.ID = B.iid AND A.proker_id = B.proker_id) as aprov'),'aprov.proker_id','=','proker.id')
                ->leftjoin('keanggotaan.anggota as ang','ang.nim','=','proker.penanggungjawab')                
                ->leftJoin('master.m_struktur as ms','ms.id','=','proker.id_struktur')          
                ->leftJoin('kegiatan.lpj as lpj','lpj.proker_id','=','proker.id')
                ->leftJoin(DB::raw('(SELECT A.* FROM  approve.approv_lpj A
                    JOIN ( SELECT lpj_id, MAX ( ID ) AS iid FROM approve.approv_lpj GROUP BY lpj_id ) B
                    ON A.ID = B.iid AND A.lpj_id = B.lpj_id) as aprov_lpj'),'aprov_lpj.lpj_id','=','lpj.id')                   

                    // ->whereNotIn('aprov.status',array(6))
                ->when($tahun, function ($query) use ($tahun) {
                    if ($tahun != 0) {
                        return $query->where([
                                [DB::raw('EXTRACT(YEAR FROM tanggal_mulai)'),'=',$tahun],
                            ]);
                    }else{
                        return $query;
                    }
                })    

                ->when($bulan, function ($query) use ($bulan) {
                    if ($bulan != 0 ) {
                        return $query->where([
                                [DB::raw('EXTRACT(MONTH FROM tanggal_mulai)'),'=',$bulan],
                            ]);
                    }else{
                        return $query;
                    }
                })            
                ->selectRaw("proker.id, proker.*, aprov.status, aprov.catatan, ms.nama as nama_struktur,ang.nama as nama_pj,
                    lpj.id as id_lpj, lpj.nama_file as nama_file_lpj,aprov_lpj.status as status_lpj, aprov_lpj.catatan as catatan_lpj")
                    ->orderBy('proker.tanggal_mulai','desc')
                ->get();
        
        // return $data_count_Raw;
        // return $data_isi;
        $no = 1;
        foreach ($data_isi as $value) {
            $id_proker = DeHelper::encrypt_($value->id);
            $id_struktur = (isset($value->id_struktur)) ? $value->id_struktur : NULL;
            $nama=(isset($value->nama)) ? $value->nama : NULL;
            $tanggal_mulai =(isset($value->tanggal_mulai)) ? $value->tanggal_mulai : NULL;
            $tanggal_selesai =(isset($value->tanggal_selesai)) ? $value->tanggal_selesai : NULL;
            $dana_usulan =(isset($value->dana_usulan)) ? $value->dana_usulan : NULL;
            $dana_terpakai=(isset($value->dana_terpakai)) ? $value->dana_terpakai : NULL;
            $ket_tambahan=(isset($value->ket_tambahan)) ? $value->ket_tambahan : NULL;
            $penanggungjawab=(isset($value->penanggungjawab)) ? $value->penanggungjawab : NULL;
            $subjek_tujuan=(isset($value->subjek_tujuan)) ? $value->subjek_tujuan : NULL;
            $dana_turun=(isset($value->dana_turun)) ? $value->dana_turun : NULL;
            $dana_mandiri=(isset($value->dana_mandiri)) ? $value->dana_mandiri : NULL;
            $event=(isset($value->event)) ? $value->event : NULL;
            $tujuan=(isset($value->tujuan)) ? $value->tujuan : NULL;;
            $subjek_tujuan=(isset($value->subjek_tujuan)) ? $value->subjek_tujuan : NULL;
            $status=(isset($value->status)) ? $value->status : NULL;
            $catatan=(isset($value->catatan)) ? $value->catatan : '-';
            $nama_struktur=(isset($value->nama_struktur)) ? $value->nama_struktur : NULL;
            $nama_pj=(isset($value->nama_pj)) ? $value->nama_pj : '-';
            $terlaksana=(isset($value->terlaksana)) ? $value->terlaksana : NULL;
            $rating=(isset($value->rating)) ? $value->rating : NULL;
            $evaluasi=(isset($value->evaluasi)) ? $value->evaluasi : NULL;

            if ($terlaksana == true) {
                $terlaksana = '<span class="badge badge-success">Terlaksana</span>'.' <span class="blue-400"><i class="icon md-info-outline st_eval" data-evaluasi="'.$evaluasi.'" aria-hidden="true"></i></span>';
            }else{
                if (strtotime($tanggal_mulai) >= strtotime(date('d-m-Y'))) {
                    $terlaksana = '<span class="badge badge-secondary">Belum Terlaksana</span>';
                }elseif(strtotime($tanggal_mulai) <= strtotime(date('d-m-Y')) && strtotime($tanggal_selesai) >= strtotime(date('d-m-Y'))){
                    $terlaksana = '<span class="badge badge-info">Sedang berlangsung</span>';
                }else{
                    $terlaksana = '<span class="badge badge-danger">Tidak Terlaksana</span>'.' <span class="red-400"><i class="icon md-info-outline st_eval" data-evaluasi="'.$evaluasi.'" aria-hidden="true"></i></span>';
                }
            }

            // $link_hapus = url('surat_keterangan/hapus_penghargaan/'.$id_riwayat);
            $aprove = '<a style="margin-left:5px;" data-id="'.$id_proker.'"  href="javascript:void(0)" class="dropdown-item btn btn-xs btn-success btnAprove" role="menuitem" ><i class="icon fa-check"></i> Aprove </a>';
            $aprove_dg_catatan = '<a style="margin-left:5px;" data-id="'.$id_proker.'"  href="javascript:void(0)" class="dropdown-item btn btn-xs bg-light-green-700 text-white btnAproveCt"  role="menuitem"><i class="icon fa-check"></i> Aprove dengan catatan </a>';
            $kirim_ke_ketua = '<a style="margin-left:5px;" data-id="'.$id_proker.'"  href="javascript:void(0)" class="dropdown-item btn btn-xs btn-primary btnKirimKt"  role="menuitem"><i class="icon fa-check"></i> Kirim ke ketua </a>';
            $penyesuaian_selesai = '<a style="margin-left:5px;" data-id="'.$id_proker.'"  href="javascript:void(0)" class="dropdown-item btn btn-xs btn-primary btnPenyesuaian" role="menuitem" ><i class="icon fa-check"></i> Aprove dengan catatan </a>';
            $review_selesai = '<a style="margin-left:5px;" data-id="'.$id_proker.'"  href="javascript:void(0)" class="dropdown-item btn btn-xs btn-primary btnReview"  role="menuitem"><i class="icon fa-check"></i>Review catatan selesai</a>';
            $ditolak = '<a style="margin-left:5px;"  data-id="'.$id_proker.'" class="dropdown-item btn btn-xs btn-danger show_modal_tolak" href="javascript:void(0)" role="menuitem"><i class="icon fa-remove"></i> Ditolak </a>';
            $perbaikan = '<a  data-id="'.$id_proker.'" href="javascript:void(0)" class="dropdown-item btn btn-xs bg-amber-700 text-white btn_perbaikan btnRevisi"
                data-id_struktur="'.$id_struktur.'"
                data-nama="'.$nama.'"
                data-tanggal_mulai="'.$tanggal_mulai.'"
                data-tanggal_selesai="'.$tanggal_selesai.'"
                data-dana_usulan="'.$dana_usulan.'"
                data-dana_terpakai="'.$dana_terpakai.'"
                data-dana_turun="'.$dana_turun.'"
                data-ket_tambahan="'.$ket_tambahan.'"
                data-penanggungjawab="'.$penanggungjawab.'"
                data-event="'.$event.'"
                data-tujuan="'.$tujuan.'"
                data-subjek_tujuan="'.$subjek_tujuan.'"
                data-status="'.$status.'"
                data-catatan="'.$catatan.'"
                data-nama_pj="'.$nama_pj.'"
                data-subjek_tujuan="'.$subjek_tujuan.'"
                data-dana_mandiri="'.$dana_mandiri.'"
                data-nama_struktur="'.$nama_struktur.'" role="menuitem">
            <i class="icon fa-wrench"></i> Perbaikan </a>';
            $cek = '<a href="javascript:void(0)" class="dropdown-item btn btn-xs btn-secondary btnDetail" style="margin-left:5px;"
                data-id="'.$id_proker.'"
                data-id_struktur="'.$id_struktur.'"
                data-nama="'.$nama.'"
                data-tanggal_mulai="'.$tanggal_mulai.'"
                data-tanggal_selesai="'.$tanggal_selesai.'"
                data-dana_usulan="'.$dana_usulan.'"
                data-dana_terpakai="'.$dana_terpakai.'"
                data-dana_turun="'.$dana_turun.'"
                data-ket_tambahan="'.$ket_tambahan.'"
                data-penanggungjawab="'.$penanggungjawab.'"
                data-event="'.$event.'"
                data-tujuan="'.$tujuan.'"
                data-subjek_tujuan="'.$subjek_tujuan.'"
                data-status="'.$status.'"
                data-catatan="'.$catatan.'"
                data-nama_pj="'.$nama_pj.'"        
                data-subjek_tujuan="'.$subjek_tujuan.'"
                data-dana_mandiri="'.$dana_mandiri.'"                        
                data-nama_struktur="'.$nama_struktur.'" role="menuitem">
            <i class="icon fa-eye"></i> Cek </a>';
            $edit = '<a href="javascript:void(0)" class="dropdown-item btn btn-xs text-white bg-purple-600 btnEdit" style="margin-left:5px;"
                data-id="'.$id_proker.'"
                data-id_struktur="'.$id_struktur.'"
                data-nama="'.$nama.'"
                data-tanggal_mulai="'.$tanggal_mulai.'"
                data-tanggal_selesai="'.$tanggal_selesai.'"
                data-dana_usulan="'.$dana_usulan.'"
                data-dana_terpakai="'.$dana_terpakai.'"
                data-dana_turun="'.$dana_turun.'"
                data-ket_tambahan="'.$ket_tambahan.'"
                data-penanggungjawab="'.$penanggungjawab.'"
                data-event="'.$event.'"
                data-tujuan="'.$tujuan.'"
                data-subjek_tujuan="'.$subjek_tujuan.'"
                data-status="'.$status.'"
                data-catatan="'.$catatan.'"
                data-nama_pj="'.$nama_pj.'"            
                data-subjek_tujuan="'.$subjek_tujuan.'"
                data-dana_mandiri="'.$dana_mandiri.'"                    
                data-nama_struktur="'.$nama_struktur.'" role="menuitem">
            <i class="icon fa-pencil"></i> Edit </a>';
            $hapus = '<a style="margin-left:5px;" data-id="'.$id_proker.'"  class="dropdown-item btn btn-xs btn-danger bg-pink-600 btnDelete" href="javascript:void(0)" role="menuitem"><i class="icon fa-trash"></i> Delete </a>';
            $url_detail = route('detailProker', ['id_proker' => $id_proker]);
            $detail = '<a style="margin-left:5px;" data-id="'.$id_proker.'"  class="dropdown-item btn btn-xs bg-light-blue-a400 text-white" href="'.$url_detail.'" role="menuitem"><i class="icon fa-search-plus"></i> Detail </a>';
            
            $stat_event = '';
            if($event == 1)
            {
                $stat_event = "<span class='badge bg-blue-600 text-white'>Normal</span>";
            } else if($event == 2) {
                $stat_event = "<span class='badge bg-purple-600 text-white'>Event</span>";
            }else{
                $stat_event = "<span class='badge badge-secondary'>belum dipilih</span>";
            }
            if ($flag_lv=="c" || substr($key_menu,0,1) == 'e') {
                // proker
                if ($status == 1 ) {
                    $aksi = $detail.' '.$edit.' '.$hapus;
                }elseif($status  == 4 || $status  == 8){
                    $aksi = $detail;
                }elseif($status  == 6){
                    $aksi = $detail;
                }elseif($flag_lv=="b"){
                    if ($status  ==  2 || $status == 0) {
                        $aksi = $detail;
                    }else{
                        $aksi = $detail;
                    }                         
                }else{
                    $aksi = $detail;
                }
            }else if($flag_lv=="b"){
                if ($status  ==  2 || $status == 0) {
                    $aksi = $detail;
                }else{
                    $aksi = $detail;
                }           
            }elseif ($flag_lv=="a") {
                if ($status  ==  3) {
                    $aksi = $detail;
                }elseif($status  == 7){
                    $aksi = $detail;                    
                }else{
                    $aksi = $detail;
                }              
            } else{
                $aksi= $cek;
            }

            $aksi = '<div class="btn-group">
                        <button type="button" class="btn btn-icon btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false" aria-hidden="true">
                            <i class="icon fa-gear fa-spin" aria-hidden="true"></i> Aksi
                        </button>
                        <div class="dropdown-menu" role="menu">
                            '.$aksi.'
                        </div>
                    </div>';

            // ------------------------------------------------- Proposal LPJ --------------------------------------------------- 
            // ------------------------------------------------- Proposal LPJ --------------------------------------------------- 
            // ------------------------------------------------- Proposal LPJ ---------------------------------------------------
        
            
            $id_lpj =(isset($value->id_lpj)) ? DeHelper::encrypt_($value->id_lpj) : '';            
            $nama_file_lpj = (isset($value->nama_file_lpj)) ? $value->nama_file_lpj : '';
            $status_lpj =(isset($value->status_lpj)) ? $value->status_lpj : NULL;
            $catatan_lpj =(isset($value->catatan_lpj)) ? $value->catatan_lpj : '';            
            $file_path_lpj = asset('lpj_file').'/'.$nama_file_lpj;

            $flag_level_lpj = '';
            if ($flag_lv=="c") {
                if ($status_lpj == 4) {
                    $flag_level_lpj = 'b';
                }elseif($status_lpj  == 8){
                    $flag_level_lpj = 'a';
                }else{
                }
            }else if($flag_lv=="b"){
                if ($status_lpj  ==  2 || $status_lpj == 0) {
                    $flag_level_lpj = 'b';
                }else{
                }
            }elseif ($flag_lv=="a") {
                if ($status_lpj  ==  3) {
                    $flag_level_lpj = 'a';
                }else{
                }
            } else{
            }

            $review_lpj = '<a type="button" href="javascript:void(0)" class="dropdown-item btn btn-xs bg-pink-700 text-white btnReview" role="menuitem" style="margin-left:5px;"
                data-id="'.$id_lpj.'" 
                data-proker_id="'.$id_proker.'" 
                data-status="'.$status_lpj.'" 
                data-catatan="'.$catatan_lpj.'" 
                data-nama_proker="'.$nama.'"
                data-tipe_proker="'.$stat_event.'"
                data-penanggungjawab="'.$penanggungjawab.'"
                data-tanggal_mulai="'.$tanggal_mulai.'"
                data-tanggal_selesai="'.$tanggal_selesai.'"
                data-tujuan="'.$tujuan.'"
                data-dana_turun="'.$dana_turun.'"
                data-dana_mandiri="'.$dana_mandiri.'"
                data-dana_usulan="'.$dana_usulan.'"
                data-flag_lv="'.$flag_level_lpj.'"
                data-jenis="LPJ"
                data-nama_file="'.$nama_file_lpj.'">
                <i class="icon fa-search"></i> Review </a>';
            $cek_review_lpj = '<a type="button" href="javascript:void(0)" class="dropdown-item btn btn-xs bg-grey-700 text-white btnCekReview" role="menuitem" style="margin-left:5px;"
                data-id="'.$id_lpj.'" 
                data-proker_id="'.$id_proker.'"
                data-status="'.$status_lpj.'" 
                data-catatan="'.$catatan.'"
                data-nama_proker="'.$nama.'"                      
                data-tipe_proker="'.$stat_event.'"
                data-penanggungjawab="'.$penanggungjawab.'"
                data-tanggal_mulai="'.$tanggal_mulai.'"
                data-tanggal_selesai="'.$tanggal_selesai.'"
                data-tujuan="'.$tujuan.'"
                data-dana_turun="'.$dana_turun.'"
                data-dana_mandiri="'.$dana_mandiri.'"
                data-dana_usulan="'.$dana_usulan.'"
                data-flag_lv="'.$flag_level_lpj.'"        
                data-jenis="LPJ"
                data-nama_file="'.$nama_file_lpj.'">
                <i class="icon fa-search"></i> Cek Review </a>';
            $perbaikan_lpj = '<a type="button"  data-id="'.$id_lpj.'" href="javascript:void(0)" class="dropdown-item btn btn-xs bg-amber-700 text-white btn_perbaikan_propLPJ" role="menuitem" style="margin-left:5px;"
                data-proker_id="'.$id_proker.'"
                data-nama_file="'.$nama_file_lpj.'"
                data-status="'.$status_lpj.'"
                data-jenis="LPJ"
                data-nama_proker="'.$nama.'">
                <i class="icon fa-wrench"></i> Perbaikan </a>';
            $edit_lpj = '<a type="button"  data-id="'.$id_lpj.'" href="javascript:void(0)" class="dropdown-item btn btn-xs bg-amber-700 text-white btn_edit_propLPJ" role="menuitem" style="margin-left:5px;"
                data-proker_id="'.$id_proker.'"
                data-nama_file="'.$nama_file_lpj.'"
                data-status="'.$status_lpj.'"
                data-jenis="LPJ"
                data-nama_proker="'.$nama.'">
                <i class="icon fa-edit"></i> Edit </a>';
            $cek_lpj = '<a href="javascript:void(0)" type="button" class="dropdown-item btn btn-xs btn-secondary btn_detail_propLPJ" role="menuitem" style="margin-left:5px;"
                data-proker_id="'.$id_proker.'"
                data-nama_file="'.$nama_file_lpj.'"
                data-status="'.$status_lpj.'"
                data-jenis="LPJ"
                data-nama_proker="'.$nama.'">
                <i class="icon fa-eye"></i> Cek </a>';
            $add_lpj = '<a type="button"  data-proker_id="'.$id_proker.'" data-nama_proker="'.$nama.'" data-jenis="LPJ" class="dropdown-item btn btn-xs btn-success btn_tambah_propLPJ" href="javascript:void(0)" role="menuitem"><i class="icon fa-plus"></i> LPJ </a>';          
            $hapus_lpj = '<a type="button" style="margin-left:5px;" data-id="'.$id_lpj.'" data-jenis="LPJ" class="dropdown-item btn btn-xs btn-danger bg-pink-600 btn_delete_propLPJ" href="javascript:void(0)" role="menuitem"><i class="icon fa-trash"></i> Delete </a>';          
            $file_path_lpj = asset('lpj_file').'/'.$nama_file_lpj;
            $download_file_lpj = '<a href="'.$file_path_lpj.'" download class="dropdown-item btn btn-xs btn-round btn-success" role="menuitem">Download file LPJ</a>';
            if($status === NULL)
            {
                $stat = '<span class="badge badge-secondary">Telah disetujui</span>';
                $aksi = $cek;                    
            } else {
                $info = '';
                if ($status == 4 || $status == 8) {
                    $info = '<span class="red-700"><i class="icon md-info-outline st_pengajuan" data-catatan="'.$catatan.'" aria-hidden="true"></i></span>';
                }else if ($status == 6) {
                    $info = '<span class="blue-700"><i class="icon md-info-outline st_pengajuan" data-catatan="'.$catatan.'" aria-hidden="true"></i></span>';
                }else{
                    $info = '';
                }

                $stat = DeHelper::status_pengajuan($status).$info;
                $stat_lpj = DeHelper::status_pengajuan($status_lpj);
            }

            $tanggal = DeHelper::format_tanggal($value->tanggal_mulai).' s/d '.DeHelper::format_tanggal($value->tanggal_selesai);
            $nama_ = $nama;
            $rating_ = '<div class="rating" data-rating="'.$rating.'" data-evaluasi="'.$evaluasi.'"></div>';
            $dataTabel[] = array(
                'no' => $no++,
                'id_proker' => $id_proker,
                'id_struktur' => $id_struktur,
                'nama' => $nama_,
                'tanggal' => $tanggal,
                'dana_usulan' => $dana_usulan,
                'dana_terpakai' => $dana_terpakai,
                'dana_turun' => $dana_turun,
                'ket_tambahan' => $ket_tambahan,
                'penanggungjawab' => $penanggungjawab,                
                'nama_pj' => $nama_pj,                
                'event' => $event,
                'stat_event' => $stat_event,
                'tujuan' => $tujuan,
                'subjek_tujuan' => $subjek_tujuan,
                'status' => $status,
                'catatan' => $catatan,
                'nama_struktur' => $nama_struktur,
                'terlaksana' => $terlaksana,
                'status' => $stat,
                'stat_lpj' => $status_lpj,
                'status_lpj' => $stat_lpj,
                // 'act_lpj' => $aksi_lpj,
                // 'aksi_lpj' => $aksi_lpj_grub,
                'flag_level_lpj' => $flag_level_lpj,
                'rating' => $rating_,
                'evaluasi' => $evaluasi,
                'aksi'=>$aksi
            );                 
        }
        // return $dataTabel;

        $data_ = (isset($dataTabel)?$dataTabel:[]);

        return DataTables::of($data_)
            ->addColumn('nama', function($data_){
                return $data_['nama'];
            })
            ->addColumn('stat_event', function($data_){
                return $data_['stat_event'];
            })
            ->addColumn('status', function($data_){
                return $data_['status'];
            })
            ->addColumn('terlaksana', function($data_){
                return $data_['terlaksana'];
            })
            ->addColumn('aksi', function($data_){
                return $data_['aksi'];
            })
            ->addColumn('status_lpj', function($data_){
                return $data_['status_lpj'];
            })
            ->addColumn('rating', function($data_){
                return $data_['rating'];
            })
            ->rawColumns(['stat_event','status','terlaksana','aksi','nama','status_lpj','rating'])
            ->make(true);

        // return $data;        
    }
}
