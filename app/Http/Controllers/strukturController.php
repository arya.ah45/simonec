<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\m_struktur;
use App\Models\m_struktur_detail;
use App\Models\riwayat_jabatan;
use Session;
use DB;
use DeHelper;

class strukturController extends Controller
{
    public function index(Request $request,$tahun)
    {
        // =========== ngatasi option select tahun -==============
        $list_tahun_raw = DB::select("SELECT EXTRACT(YEAR FROM tmt_sk) as tahun FROM keanggotaan.riwayat_jabatan WHERE tmt_sk IS NOT NULL GROUP BY EXTRACT(YEAR FROM tmt_sk) ORDER BY EXTRACT(YEAR FROM tmt_sk) ASC");
        // return $list_tahun_raw;
        foreach ($list_tahun_raw as $key => $value) {
            $list_tahun[$key] = $value->tahun;
        }
        // return $list_tahun;

        $total_tahun = count($list_tahun);
        $tahun1 = $list_tahun[$total_tahun - 1] + 1;
        $tahun2 = $tahun1 + 1;

        $list_tahun[$total_tahun] = strval($tahun1);
        $list_tahun[$total_tahun+1] = strval($tahun2);
        // return $list_tahun;
        $send['list_tahun'] = $list_tahun;

        // =========== ngatasi tahun -==============
        $tahun = (isset($request->tahun)) ? $request->tahun : date("Y");
        $send['tahun'] = $tahun;

        $data_struktur =  m_struktur::rightjoin('master.m_struktur_detail as sd','sd.struktur_id','=','master.m_struktur.id')
                                    ->selectRaw("master.m_struktur.*, sd.id as id_jabatan, sd.nama as nama_jabatan")
                                    ->orderBy('sd.id','asc')
                                    ->whereNotIn('kode_struktur',['0','010103'])
                                    ->get();

        $kode_struktur_cek = '';
        $key_hold = $key_new = $cek_anggota = 0;

        foreach ($data_struktur as $key => $value) {

            $data_riwayat = riwayat_jabatan::leftjoin('keanggotaan.anggota as ang','ang.nim','=','keanggotaan.riwayat_jabatan.nim')
                                            ->leftjoin('master.m_kelas as kls','kls.id','=','ang.id_kelas')
                                            ->leftjoin('master.m_prodi as prod','prod.id','=','ang.id_prodi')
                                            ->selectRaw("keanggotaan.riwayat_jabatan.*, ang.nama as nama_pejabat, kls.id as id_kelas, kls.nama as kelas, prod.id as id_prodi, prod.nama as prodi")
                                            ->where([
                                                ['keanggotaan.riwayat_jabatan.kode_struktur','=',$value->kode_struktur],
                                                ['keanggotaan.riwayat_jabatan.kode_struktur_detail','=',$value->id_jabatan],
                                                [DB::raw('EXTRACT(YEAR FROM tmt_sk)'),'=',$tahun],
                                            ])
                                            ->orderBy('keanggotaan.riwayat_jabatan.id','asc')                                            
                                            ->get();
            // return $data_riwayat;

            if ($kode_struktur_cek != $value->kode_struktur) {
                $data[$key]['kode_struktur'] = $value->kode_struktur;
                $data[$key]['struktur'] = $value->nama;
                $data[$key]['isi'] = [];
                $data[$key]['total_anggota'] = 0;
                $key_hold = $key;
                $key_new = 0;
                $datas = [];
                // return $kode_struktur_cek;
            }else{

            }

            $keys = 0;
            $cek_anggota = 0;
            // $total_anggota = 0;

            // return count($data_riwayat);
            if (count($data_riwayat) > 0 ) {

                foreach ($data_riwayat as $keys => $values) {
                    if (strlen($value->kode_struktur) <= 4 ) {
                        $datas['id_rj'] = $values->id;
                        $datas['nama_jabatan'] = $value->nama_jabatan;
                        $datas['kode_struktur_detail'] = $value->id_jabatan;
                        $datas['pejabat'] = $values->nama_pejabat;
                        $datas['nim'] = $values->nim;
                        $datas['kelas'] = $values->kelas;
                        $datas['prodi'] = DeHelper::singkatJurusan($values->prodi);
                    }else {
                        if(substr($value->nama_jabatan,0,7) == 'Anggota'){
                            $cek_anggota++;
                        }else{
                        }
                        $datas[$key_new]['id_rj'] = $values->id;
                        $datas[$key_new]['nama_jabatan'] = $value->nama_jabatan;
                        $datas[$key_new]['kode_struktur_detail'] = $value->id_jabatan;
                        $datas[$key_new]['pejabat'] = $values->nama_pejabat;
                        $datas[$key_new]['nim'] = $values->nim;
                        $datas[$key_new]['kelas'] = $values->kelas;
                        $datas[$key_new]['prodi'] = DeHelper::singkatJurusan($values->prodi);
                    }
                $key_new++;

                }
            } else {
                    if (strlen($value->kode_struktur) <= 4 ) {
                        $datas['id_rj'] = '';
                        $datas['nama_jabatan'] = $value->nama_jabatan;
                        $datas['kode_struktur_detail'] = $value->id_jabatan;
                        $datas['pejabat'] = '';
                        $datas['nim'] = '';
                        $datas['kelas'] = '';
                        $datas['prodi'] = '';
                    }else {
                        $datas[$key_new]['id_rj'] = '';
                        $datas[$key_new]['nama_jabatan'] = $value->nama_jabatan;
                        $datas[$key_new]['kode_struktur_detail'] = $value->id_jabatan;
                        $datas[$key_new]['pejabat'] = '';
                        $datas[$key_new]['nim'] = '';
                        $datas[$key_new]['kelas'] = '';
                        $datas[$key_new]['prodi'] = '';
                    }
                $key_new++;
            }

            // return $kode_struktur_cek;
            $data[$key_hold]['isi'] = $datas;
            $data[$key_hold]['total_anggota'] = $cek_anggota;
            $kode_struktur_cek = $value->kode_struktur;
        }
        // return $data;
        // return $values;

        $send['nomor_sk'] = (isset($values->no_sk)) ? $values->no_sk : '';
        $send['tanggal_sk'] = (isset($values->tmt_sk)) ? $values->tmt_sk : '';
        $send['data'] = $data;
        return view('struktur.struktur_h', $send);
    }

    public function getStruktur(Request $request)
    {
        $data_struktur =  m_struktur::rightjoin('master.m_struktur_detail as sd','sd.struktur_id','=','master.m_struktur.id')
                                    ->selectRaw("master.m_struktur.*, sd.id as id_jabatan, sd.nama as nama_jabatan")
                                    ->orderBy('sd.id','asc')
                                    ->whereNotIn('kode_struktur',['0','010103'])
                                    ->get();

        // =========== ngatasi tahun -==============
        $tahun = (isset($request->tahun)) ? $request->tahun : date("Y");

        $data_struktur =  m_struktur::rightjoin('master.m_struktur_detail as sd','sd.struktur_id','=','master.m_struktur.id')
                                    ->selectRaw("master.m_struktur.*, sd.id as id_jabatan, sd.nama as nama_jabatan")
                                    ->orderBy('sd.id','asc')
                                    ->whereNotIn('kode_struktur',['0','010103'])
                                    ->get();

        $kode_struktur_cek = '';
        $key_hold = $key_new = $cek_anggota = 0;

        foreach ($data_struktur as $key => $value) {

            $data_riwayat = riwayat_jabatan::leftjoin('keanggotaan.anggota as ang','ang.nim','=','keanggotaan.riwayat_jabatan.nim')
                                            ->leftjoin('master.m_kelas as kls','kls.id','=','ang.id_kelas')
                                            ->leftjoin('master.m_prodi as prod','prod.id','=','ang.id_prodi')
                                            ->selectRaw("keanggotaan.riwayat_jabatan.*, ang.nama as nama_pejabat, kls.id as id_kelas, kls.nama as kelas, prod.id as id_prodi, prod.nama as prodi")
                                            ->where([
                                                ['keanggotaan.riwayat_jabatan.kode_struktur','=',$value->kode_struktur],
                                                ['keanggotaan.riwayat_jabatan.kode_struktur_detail','=',$value->id_jabatan],
                                                [DB::raw('EXTRACT(YEAR FROM tmt_sk)'),'=',$tahun],
                                            ])->get();
            // return $data_riwayat;

            if ($kode_struktur_cek != $value->kode_struktur) {
                $data[$key]['kode_struktur'] = $value->kode_struktur;
                $data[$key]['struktur'] = $value->nama;
                $data[$key]['isi'] = [];
                $data[$key]['total_anggota'] = 0;
                $key_hold = $key;
                $key_new = 0;
                $datas = [];
                // return $kode_struktur_cek;
            }else{

            }

            $keys = 0;
            $cek_anggota = 0;
            // $total_anggota = 0;

            // return count($data_riwayat);
            if (count($data_riwayat) > 0 ) {

                foreach ($data_riwayat as $keys => $values) {
                    if (strlen($value->kode_struktur) <= 4 ) {
                        $datas['nama_jabatan'] = $value->nama_jabatan;
                        $datas['kode_struktur_detail'] = $value->id_jabatan;
                        $datas['pejabat'] = $values->nama_pejabat;
                        $datas['nim'] = $values->nim;
                        $datas['kelas'] = $values->kelas;
                        $datas['prodi'] = DeHelper::singkatJurusan($values->prodi);
                    }else {
                        if(substr($value->nama_jabatan,0,7) == 'Anggota'){
                            $cek_anggota++;
                        }else{
                        }
                        $datas[$key_new]['nama_jabatan'] = $value->nama_jabatan;
                        $datas[$key_new]['kode_struktur_detail'] = $value->id_jabatan;
                        $datas[$key_new]['pejabat'] = $values->nama_pejabat;
                        $datas[$key_new]['nim'] = $values->nim;
                        $datas[$key_new]['kelas'] = $values->kelas;
                        $datas[$key_new]['prodi'] = DeHelper::singkatJurusan($values->prodi);
                    }
                $key_new++;

                }
            } else {
                    if (strlen($value->kode_struktur) <= 4 ) {
                        $datas['nama_jabatan'] = $value->nama_jabatan;
                        $datas['kode_struktur_detail'] = $value->id_jabatan;
                        $datas['pejabat'] = '-';
                        $datas['nim'] = '-';
                        $datas['kelas'] = '-';
                        $datas['prodi'] = '-';
                    }else {
                        $datas[$key_new]['nama_jabatan'] = $value->nama_jabatan;
                        $datas[$key_new]['kode_struktur_detail'] = $value->id_jabatan;
                        $datas[$key_new]['pejabat'] = '-';
                        $datas[$key_new]['nim'] = '-';
                        $datas[$key_new]['kelas'] = '-';
                        $datas[$key_new]['prodi'] = '-';
                    }
                $key_new++;
            }

            // return $kode_struktur_cek;
            $data[$key_hold]['isi'] = $datas;
            $data[$key_hold]['total_anggota'] = $cek_anggota;
            $kode_struktur_cek = $value->kode_struktur;
        }
        return response()->json($data);

    }

    public function simpanRiwayatJabatan(Request $request)
    {
        // return response()->json($request->all());
        
        $tahun_sk = substr($request->tanggal_sk,0,4); 
        $tahun_rj = $request->tahun_rj; 

        if(isset($request->deleted_id)){
            $deleted_id = explode(',', substr($request->deleted_id,0,strlen($request->deleted_id)-1));
            // return $deleted_id;
            foreach ($deleted_id as $key => $value) {
                if ($deleted_id[$key] != 'new') {
                    $rj = riwayat_jabatan::where('id','=',$deleted_id[$key]);
                    $rj->delete();
                }
            }
        }

        // cek tahun spsksh sync;
        if($tahun_rj != $tahun_sk){
            $data['code']="265";
            $data['message']="tahun terpilih dan tahun di tanggal SK tidak singkron!";
            return response()->json($data);exit();  
        }

        // lek pomo tahun terkini lakukan peng pensiunan
        $rj_tahun_terkini = riwayat_jabatan::selectRaw("max(EXTRACT(YEAR FROM tmt_sk)) as tahun")->get()->first()->tahun;
        if ($tahun_rj >= $rj_tahun_terkini) {
            // return ($tahun_rj >= $rj_tahun_terkini);
            DB::table('keanggotaan.anggota')
                ->where('keanggotaan.anggota.is_pengurus','=',true)
                ->update(['is_anggota' => 4,'is_pengurus' => false]);
        }
            
        // return $rj_tahun_terkini;

        $key=0;
        foreach ($request->kode_struktur as $key => $value) {

            if ($request->nim[$key] != null &&
                $request->kode_struktur[$key] != null &&
                $request->kode_struktur_detail[$key] != null ||
                $request->kode_struktur_detail[$key] !='' &&
                $request->kode_struktur[$key] !='' &&
                $request->nim[$key] !='') {

                    $data_simpan['nim'] = $request->nim[$key];
                    $data_simpan['kode_struktur'] = $request->kode_struktur[$key];
                    $data_simpan['kode_struktur_detail'] = $request->kode_struktur_detail[$key];
                    $data_simpan['no_sk'] = $request->nomor_sk;
                    $data_simpan['tmt_sk'] = $request->tanggal_sk;

                    if($tahun_rj >= $rj_tahun_terkini) {
                        DB::table('keanggotaan.anggota')
                            ->where('keanggotaan.anggota.nim','=',$request->nim[$key])
                            ->update(['is_anggota' => 3,'is_pengurus' => true]);
                    }

                    riwayat_jabatan::updateOrCreate(['id' => $request->id_rj[$key]], $data_simpan);
            }

        }
        // return $data_simpan;
    
        $data['code']="200";
        $data['message']="Data struktur organisasi berhasil disimpan";
        return response()->json($data);exit();  

    }
}
