<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Hash;
use DB;
use App\Models\akun;
use Illuminate\Http\Request;
use Validator;
use Redirect;
use Session;
use App\Models\calon_anggota;


class LoginController extends Controller
{

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function renew_pass()
    {
        $datas = DB::table('keanggotaan.anggota')->get();
        // return $data;

        akun::truncate();

        foreach ($datas as $key => $value) {
            # code...
            $username = $value->nim;
            $password = hash::make($value->nim);

            $akun = new akun;
            $akun->nim = $username;
            $akun->username = $username;
            $akun->password = $password;
            $akun->save();
        }
        return 'data berhasil di refresh';
    }

    public function renew_pass_dosen()
    {
        $datas = DB::table('master.m_dpk')->get();
        // return $data;

        foreach ($datas as $key => $value) {
            # code...
            $username = $value->nidn;
            $password = hash::make($value->nidn);

            DB::table('master.m_dpk')
                ->where('id','=', $value->id)
                ->update([
                    'username' => $username,
                    'password' => $password,
                ]);
        }
        return 'data berhasil di refresh';
    }

    public function cek_login(Request $request)
    {
        // return $request->all();
        // validate the info, create rules for the inputs
        $rules = array(
            'username'    => 'required|min:3', // make sure the email is an actual email
            'password' => 'required|min:3' // password can only be alphanumeric and has to be greater than 3 characters
        );

        $messages = [
            'required' => ':attribute WAJIB di ini',
            'min' => ':attribute minimal harus berisi :min karakter',
        ];
        $data_user = [];
        // Cek kondisi LOGIN menggunakan QR Code / Login biasa
        if (isset($request->qr)) { //Menggunakan QR
                # code...

            $cek_kode = DB::table('v_login')->where('username','=',$request->username)->first();
            if (!empty($cek_kode)) {
                $data_user = $cek_kode;
                // return $data_user;
            } else {
                return Redirect::to('/log')
                    ->with('QR','QR Code tidak Sesuai');
            }
        }else{ //tanpa menggunakan QR
            // run the validation rules on the inputs from the form
            $validator = Validator::make($request->all(), $rules, $messages);
            // if the validator fails, redirect back to the form
            if ($validator->fails()) {
                return Redirect::to('/log')
                    ->withErrors($validator) // send back all errors to the login form
                    ->withInput($request->except('password')); // send back the input (not the password) so that we can repopulate the form
            } else {

                $get_avl = DB::table('v_login')->where('username','=',$request->username)->first();
                if (empty($get_avl)) {
                        return Redirect::to('/log')
                        ->with('QR','Username tidak terdaftar');
                } else {
                    if (Hash::check($request->password, $get_avl->password)) {
                        $data_user = $get_avl;
                    }else{
                        return Redirect::to('/log')
                        ->with('QR','Password salah !');
                    }
                }
            }
        }
        $flag_lv = $data_user->flag_lv;
        $key_menu = '';
        $is_admin = DB::table('kegiatan.admin_kegiatan')->where('nim','=',$data_user->nim)->first();
        if (empty($is_admin)) {
            $key_menu = $flag_lv;
        } else {
            $key_menu = 'e'.$flag_lv;
        }
        $foto_anggotas = DB::table('keanggotaan.anggota')->where('nim',$data_user->nim)->get()->first();
        $foto_anggota = (isset($foto_anggotas->foto)) ? $foto_anggotas->foto: '';
        $array_user = [];
        $array_user['nim']      		= $data_user->nim;
        $array_user['flag_lv']    	    = $flag_lv;
        $array_user['kode_struktur']    = $data_user->kode_struktur;
        $array_user['nama']    	        = $data_user->nama;
        $array_user['key_menu']    	    = $key_menu;
        $array_user['foto']    	        = ($foto_anggota != '') ? $foto_anggota : '12.jpg';
        session::put('data_user', $array_user);
        // dd($array_user);
        $landing = '';
        $status_landing = array(
            'a' =>'landing DPK',
            'b' =>'landing Ketua UKM',
            'c' =>'landing Sekretaris UKM',
            'd' =>'landing Anggota UKM',
            'eb' =>'landing Ketua UKM (Panitia Kegiatan)',
            'ec' =>'landing Sekretaris UKM (Panitia Kegiatan)',
            'ed' =>'landing Anggota UKM (Panitia Kegiatan)',
        );
        // Checking landing
        if (isset($status_landing[$key_menu])) {
            $landing = $status_landing[$key_menu];
        } else {
            return Redirect::to('/log')
            ->with('QR','Landing tidak ditemukan!');
        }
        // return view('landing',(array)$data_ang);


        return redirect()->route('beranda');
        return Redirect::to('/beranda');
    }

    public function beranda(Request $request)
    {
        // DB::raw('EXTRACT(YEAR FROM tanggal_mulai)')
        $users['data'] = calon_anggota::selectRaw("count(*) as jumlah,EXTRACT(YEAR FROM  created_at)")
        ->groupByRaw('EXTRACT(YEAR FROM  created_at)')
        ->orderBy('date_part','ASC')
        ->pluck('jumlah');

        $users['dataLos'] = calon_anggota::selectRaw("count(*) as jumlah,EXTRACT(YEAR FROM  created_at)")
        ->where([
            ['is_anggota', '=', '3'],
        ])
        ->groupByRaw('EXTRACT(YEAR FROM  created_at)')
        ->orderBy('date_part','ASC')
        ->pluck('jumlah');

        $users['dataGal'] = calon_anggota::selectRaw("count(*) as jumlah,EXTRACT(YEAR FROM  created_at)")
        ->where([
            ['is_anggota', '!=', '3'],
        ])
        ->groupByRaw('EXTRACT(YEAR FROM  created_at)')
        ->orderBy('date_part','ASC')
        ->pluck('jumlah');
        // return $users;
        return view('landing',$users);

    }
}
