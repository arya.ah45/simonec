<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Surat;
use DataTables;
use Validator;
use DB;
use DeHelper;
use Session;

class SuratController extends Controller
{
    public function index( Request $request){
        // $query = Surat::all();
        //  return $query;
        $query = DB::table('keanggotaan.surat as surat')
             ->select(DB::raw(' id, perihal, no_surat, asal_surat, tgl_surat , file , jenis_surat,created_at'))
             ->where('jenis_surat', '=', 1)
             ->orderBy('created_at','DESC')
             ->get();
        // return $query;

        $data = array();
        $no =1 ;

        foreach ($query as $key => $value) {
            $data[$key]['no'] = $no++;
            $data[$key]['id'] = $value->id;
            $data[$key]['perihal'] = $value->perihal;
            $data[$key]['no_surat'] = $value->no_surat;
            $data[$key]['asal_surat'] = $value->asal_surat;
            $data[$key]['tgl_surat'] = DeHelper::format_tanggal($value->tgl_surat) ;
            $data[$key]['created_at'] = DeHelper::format_tanggal($value->created_at) ;
            $data[$key]['tgl_surat1'] = $value->tgl_surat ;
            $data[$key]['file'] = $value->file;
            $data[$key]['jenis_surat'] = $value->jenis_surat;
        }

        //  return $data;

        if ($request->ajax()){
            return DataTables::of($data)
            ->addColumn('aksi', function($data){

                $buttonHapus ='<a  class="btn btn-sm btn-icon btn-pure btn-default  deleteData"
                data-toggle="tooltip"
                data-id="'.$data['id'].'"

                data-original-title="Edite">
                <i class="icon wb-trash" aria-hidden="true"></i>
                </a>';

               $buttonEdit='<a class="btn btn-sm btn-icon btn-pure btn-default editData"
                data-toggle="tooltip"
                data-id="'.$data['id'].'"
                data-perihal="'.$data['perihal'].'"
                data-no_surat="'.$data['no_surat'].'"
                data-asal_surat="'.$data['asal_surat'].'"
                data-tgl_surat="'.$data['tgl_surat1'].'"
                data-file="'.$data['file'].'"
                data-original-title="Edite"> <i class="icon wb-edit"
                aria-hidden="true"></i></a>';



                $buttonLihat='<a class="btn btn-sm btn-icon btn-pure btn-default detail"
                data-toggle="tooltip"
                data-perihal="'.$data['perihal'].'"
                data-no_surat="'.$data['no_surat'].'"
                data-asal_surat="'.$data['asal_surat'].'"
                data-tgl_surat="'.$data['tgl_surat1'].'"
                data-file="'.$data['file'].'"
                data-file="'.$data['created_at'].'"
                data-original-title="Edite"> <i class="icon wb-eye"
                aria-hidden="true"></i></a>';

                return $buttonHapus.$buttonEdit.$buttonLihat;
            })
            ->rawColumns(['aksi'])
            ->make(true);
        }

        return view('surat.surat');

    }


    public function index1( Request $request){
        // $query = Surat::all();
        //  return $query;
        $query = DB::table('keanggotaan.surat as surat')
             ->select(DB::raw(' id, perihal, no_surat, asal_surat, tgl_surat , file , jenis_surat,created_at'))
             ->where('jenis_surat', '=', 2)
             ->orderBy('created_at','DESC')
             ->get();
        // return $query;

        $data = array();
        $no =1 ;

        foreach ($query as $key => $value) {
            $data[$key]['no'] = $no++;
            $data[$key]['id'] = $value->id;
            $data[$key]['perihal'] = $value->perihal;
            $data[$key]['no_surat'] = $value->no_surat;
            $data[$key]['asal_surat'] = $value->asal_surat;
            $data[$key]['tgl_surat'] = DeHelper::format_tanggal($value->tgl_surat) ;
            $data[$key]['tgl_surat1'] = $value->tgl_surat ;
            $data[$key]['file'] = $value->file;
            $data[$key]['jenis_surat'] = $value->jenis_surat;
        }

        //  return $data;

        if ($request->ajax()){
            return DataTables::of($data)
            ->addColumn('aksi', function($data){

                $buttonHapus ='<a  class="btn btn-sm btn-icon btn-pure btn-default  deleteData"
                data-toggle="tooltip"
                data-id="'.$data['id'].'"

                data-original-title="Edite">
                <i class="icon wb-trash" aria-hidden="true"></i>
                </a>';

               $buttonEdit='<a class="btn btn-sm btn-icon btn-pure btn-default editData"
                data-toggle="tooltip"
                data-id="'.$data['id'].'"
                data-perihal="'.$data['perihal'].'"
                data-no_surat="'.$data['no_surat'].'"
                data-asal_surat="'.$data['asal_surat'].'"
                data-tgl_surat="'.$data['tgl_surat1'].'"
                data-file="'.$data['file'].'"
                data-original-title="Edite"> <i class="icon wb-edit"
                aria-hidden="true"></i></a>';



                $buttonLihat='<a class="btn btn-sm btn-icon btn-pure btn-default detail1"
                data-toggle="tooltip"
                data-perihal="'.$data['perihal'].'"
                data-no_surat="'.$data['no_surat'].'"
                data-asal_surat="'.$data['asal_surat'].'"
                data-tgl_surat="'.$data['tgl_surat1'].'"
                data-file="'.$data['file'].'"
                data-original-title="Edite"> <i class="icon wb-eye"
                aria-hidden="true"></i></a>';

                return $buttonHapus.$buttonEdit.$buttonLihat;
            })
            ->rawColumns(['aksi'])
            ->make(true);
        }

        return view('surat.surat');

    }

    public function tambahSurat(Request $request){

        // dd($file2);
        if( $request->hasFile('file1')){
            $file2 = $request->file('file1');
            $nama_file2 = 'Surat Masuk'.'_'.date("YmdHis"). '.' . $file2->getClientOriginalExtension();
            $tujuan_upload = 'file_surat';
            $file2->move($tujuan_upload, $nama_file2);
            // $file = $request->file('file');
            // $new_name = 'Surat Masuk'.'_'.date("YmdHis"). '.' . $file->getClientOriginalExtension();
            // $file->move(public_path(''), $new_name);

            $insert = [
                'id' => $request->id,
                'perihal' => $request->perihal,
                'no_surat' => $request->no_surat,
                'asal_surat' => $request->asal_surat,
                'tgl_surat' => $request->tgl_surat,
                'file' => $nama_file2,
                'jenis_surat' => $request->jenis_surat,
            ];

            $query = Surat::updateOrCreate(['id' => $request->id], $insert);

            if (isset($request->id) || $request->id !='') {
                $data="Sukses Update data surat masuk!";

                # code...
            } else {
                $data ="Sukses Tambah data surat masuk!";
                # code...
            }
        }else{

            $insert = [
                'id' => $request->id,
                'perihal' => $request->perihal,
                'no_surat' => $request->no_surat,
                'asal_surat' => $request->asal_surat,
                'tgl_surat' => $request->tgl_surat,
                'file' => $request->file1,
                'jenis_surat' => $request->jenis_surat,
            ];

            $query = Surat::updateOrCreate(['id' => $request->id], $insert);

            if (isset($request->id) || $request->id !='') {
                $data="Sukses Update data surat masuk!";

                # code...
            } else {
                $data ="Sukses Tambah data surat masuk!";
                # code...
            }
        }


        return redirect()->back();

    }

    public function tambahSuratKel(Request $request){

        // dd($file2);
        if( $request->hasFile('file1')){
            $file2 = $request->file('file1');
            $nama_file2 = 'Surat Keluar'.'_'.date("YmdHis"). '.' . $file2->getClientOriginalExtension();
            $tujuan_upload = 'file_surat';
            $file2->move($tujuan_upload, $nama_file2);
            // $file = $request->file('file');
            // $new_name = 'Surat Masuk'.'_'.date("YmdHis"). '.' . $file->getClientOriginalExtension();
            // $file->move(public_path(''), $new_name);

            $insert = [
                'id' => $request->id,
                'perihal' => $request->perihal,
                'no_surat' => $request->no_surat,
                'asal_surat' => $request->asal_surat,
                'tgl_surat' => $request->tgl_surat,
                'file' => $nama_file2,
                'jenis_surat' => $request->jenis_surat,
            ];

            $query = Surat::updateOrCreate(['id' => $request->id], $insert);

            if (isset($request->id) || $request->id !='') {
                $data="Sukses Update data surat masuk!";

                # code...
            } else {
                $data ="Sukses Tambah data surat masuk!";
                # code...
            }
        }else{

            $insert = [
                'id' => $request->id,
                'perihal' => $request->perihal,
                'no_surat' => $request->no_surat,
                'asal_surat' => $request->asal_surat,
                'tgl_surat' => $request->tgl_surat,
                'file' => $request->file1,
                'jenis_surat' => $request->jenis_surat,
            ];

            $query = Surat::updateOrCreate(['id' => $request->id], $insert);

            if (isset($request->id) || $request->id !='') {
                $data="Sukses Update data surat masuk!";

                # code...
            } else {
                $data ="Sukses Tambah data surat masuk!";
                # code...
            }
        }


        return redirect()->back();

    }


    public function destroy(Request $request,$id)
    {
        //destroyyyyyyyy
        $query = DB::table('keanggotaan.surat')
        ->where('id', $id)->delete();
        if ( $query == true ) {

            $data = "Sukses hapus data surat!";
        } else {

            $data ="Gagal hapus data surat!";
        }
        return response()->json($data);
    }


}
