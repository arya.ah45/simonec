<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\m_kriteria_penilaian;
use App\Models\proker;
use App\Models\proposal;
use App\Models\review_proposal;
use App\Models\lpj;
use App\Models\review_lpj;
use App\Models\notif;
use Session;
use DB;
use DeHelper;
use App\Helpers\DeHelper2;
use File;
class proLPJController extends Controller
{
    public function simpan_proLPJ(Request $request)
    {
        return $request->all();
        $id = (isset($request->id)) ? $request->id : null;
        $proker_id = (isset($request->proker_id)) ? DeHelper::decrypt_($request->proker_id) : null;
        $action = (isset($request->action)) ? $request->action : null;
        $file = $request->file('nama_file');

        if ($action != 'perbaikan') {
            $id_proker = $proker_id;
        }else{
        }

        $session = Session::get('data_user');
        $nim = $session['nim'];
        $flag_lv = $session['flag_lv']; 

        $data_proker = proker::find($id_proker);
        $nama_file = 'proposal_'.str_replace(' ','_',$data_proker->nama).'_'.$data_proker->tanggal_mulai.'.'.$file->getClientOriginalExtension();

        if ($action == 'perbaikan') {
            $data_prop =  proposal::find($id);
            // return $_SERVER['DOCUMENT_ROOT'].'/simonec/public/proposal_file/'.$data_prop->nama_file;
            // return asset('proposal_file').'/'.$data_prop->nama_file;
            // unlink($_SERVER['DOCUMENT_ROOT'].'/simonec/public/proposal_file/'.$data_prop->nama_file);
            File::delete(public_path().'/'.$data_prop->nama_file);            
        }else{}

        // nyimpen file
        $file->move(public_path('proposal_file'), $nama_file);    

        $data = [
            'proker_id' => $id_proker,
            'nama_file' => $nama_file,
        ];

        $prop = proposal::updateOrCreate(['id' => $id], $data);
        $proposal_id = $prop->id;

        if ($action == 'perbaikan') {
            
            if ($flag_lv == "c") {
                $data_approv['status'] = 0; 
                $data_approv['send_to'] = 'b'; 
            }else{
                $data['code']="200";
                $data['message']="user tidak memiliki akses!";
                return response()->json($data);   
            }

            $data_approv['proposal_id'] = $id;
            $data_approv['proker_id'] = $id_proker;
            $data_approv['created_at'] = date("Y-m-d H:i:s");
            $data_approv['created_by'] = $nim;
            DB::table('approve.approv_proposal')->insert($data_approv);      

            $notif['action'] = $action;
            $notif['id_subjek'] = $data_approv['proposal_id'];
            $notif['status'] = $data_approv['status'];
            DeHelper2::simpanNotif($notif);        

            $data['code']="200";
            $data['message']="Data proposal berhasil diperbaiki!";
            return response()->json($data);                        
        }else{
            // inserting status Approval
            if ($flag_lv == "c") {
                $data_approv['status'] = 2; 
                $data_approv['send_to'] = 'b'; 
            }else{
                $data_approv['status'] = 1; 
                $data_approv['send_to'] = 'b'; 
            }
    
            $data_approv['proposal_id'] = $proposal_id;
            $data_approv['proker_id'] = $id_proker;
            $data_approv['created_at'] = date("Y-m-d H:i:s");
            $data_approv['created_by'] = $nim;
            DB::table('approve.approv_proposal')->insert($data_approv);    
        
            $notif['action'] = $action;
            $notif['id_subjek'] = $data_approv['proposal_id'];
            $notif['status'] = $data_approv['status'];
            DeHelper2::simpanNotif($notif);        

            $data['code']="200";
            $data['message']="Data proposal berhasil disimpan!";
            return response()->json($data);              
        }                
    }
}
