<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\m_kelas;
use App\Models\m_prodi;
use DataTables;
use DB;
use Session;

class m_kelasControler extends Controller
{
    public function index( Request $request){

        // $query = kendaraan::leftjoin('m_jenis_kendaraans as mjk','mjk.id','=','kendaraans.id_jenis')
        // ->leftjoin('m_merek_kendaraans as mmk','mmk.id','=','kendaraans.id_merek')
        // ->leftjoin('pegawais as p','p.nip','=','kendaraans.penanggungjawab')
        // ->orderBy('nama_kendaraan','ASC')
        // ->selectRaw("kendaraans.*,nama_merek,nama_jenis,nama_pegawai,jabatan,bagian")
        // ->get();

        $query = m_kelas::leftjoin(DB::raw("master.m_prodi as pro"),'pro.id','=','master.m_kelas.id_prodi')
          ->selectRaw("m_kelas.*, pro.nama as nampro, pro.id as id_prodi")
        //  ->toSql();
         ->get();
        // $query = m_kelas::all();
        //  return $query;
        $data = array();
        $no =1 ;

        foreach ($query as $key => $value) {
            $data[$key]['no'] = $no++;
            $data[$key]['id'] = $value->id;
            $data[$key]['nama'] = $value->nama;
            $data[$key]['nampro'] = $value->nampro;
            $data[$key]['id_prodi'] = $value->id_prodi;
        }


        // return $query ;
        $query2 = m_prodi::all();

        //  return $data;

        if ($request->ajax()){
            return DataTables::of($data)
            ->addColumn('aksi', function($data){

                $buttonEdit ='<a  class="btn btn-sm btn-icon btn-pure btn-default  deleteData"
                        data-toggle="tooltip"
                        data-id="'.$data['id'].'"
                        data-nama="'.$data['nama'].'"
                        data-id_prodi="'.$data['id_prodi'].'"
                        data-original-title="Edite">
                        <i class="icon wb-trash" aria-hidden="true"></i>
                        </a>';
               $buttonHapus='<a class="btn btn-sm btn-icon btn-pure btn-default editData"
                data-toggle="tooltip"
                data-id="'.$data['id'].'"
                data-nama="'.$data['nama'].'"
                data-nampro="'.$data['nampro'].'"
                data-id_prodi="'.$data['id_prodi'].'"
                data-original-title="Edite"> <i class="icon wb-edit"
                aria-hidden="true"></i></a>';

                return $buttonHapus.$buttonEdit;
            })
            ->rawColumns(['aksi'])
            ->make(true);
        }

        return view('master.kelas',compact('query2'));

    }

    public function tambahkelas(Request $request){

        $insert = [
            'id' => $request->id,
            'nama' => $request->nama,
            'id_prodi' => $request->id_prodi,
        ];

        $query = m_kelas::updateOrCreate(['id' => $request->id], $insert);

        if (isset($request->id) || $request->id !='') {
            $data="Sukses Update data kelas!";
            # code...
        } else {
            $data ="Sukses Tambah data kelas!";
            # code...
        }
        return response()->json($data);
    }


    public function destroy(Request $request,$id)
    {
        $query = DB::table('master.m_kelas')
        ->where('id', $id)->delete();
    if ( $query == true ) {

        $data = "Sukses hapus data kelas!";
    } else {

        $data ="Gagal hapus data kelas!";
    }
    return response()->json($data);
    }


    public function Kelas(Request $request)
    {
        $search = $request->search;
        $query = DB::table('master.m_kelas')
                // ->where('is_anggota','=',3)
                // ->where('is_pengurus','=',false)
                ->whereRaw("( nama ilike '%".$search."%')")
                ->orderBy('nama','asc')
                ->get();

        $data = array();
        foreach ($query as $key => $value) {
            $data[$key]['id'] = $value->id;
            $data[$key]['nama'] = $value->nama;
        }
        // $array[] = array('a'=>'a');
        return response()->json($data);
    }

   

}
