<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;

class settingController extends Controller
{
    public function gotoSetting_pendaftaran()
    {
        // seting recruitmen
        $status_setting = DB::table('setting')->where('nama_settings','pendaftaran')->get()->first()->status;
        $data['status_setting'] = $status_setting;

        //seting hsil
        $status_hasil = DB::table('setting')->where('nama_settings','hasil_recruitment')->get()->first()->status;
        $data['status_hasil'] = $status_hasil;
        // return $data;
        return view('setting',$data);
    }

    public function ubahSetting(Request $request)
    {
        $stat = (isset($request->status)) ? $request->status : false;
        DB::table('setting')->where('nama_settings','pendaftaran')->update(['status' =>$stat]);
        return response()->json('sukses lur');

    }

    public function gotoSetting_hasil()
    {
        $status_setting = DB::table('setting')->where('nama_settings','hasil_recruitment')->get()->first()->status;
        $data['status_setting'] = $status_setting;
        // return $data;
        return view('setting',$data);
    }

    public function ubahSettinghasil(Request $request)
    {
        $stat = (isset($request->statushas)) ? $request->statushas : false;
        DB::table('setting')->where('nama_settings','hasil_recruitment')->update(['status' =>$stat]);
        return response()->json('sukses lur');

    }
}
