<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\m_akun;
use App\Models\m_jenis_belanja;
use App\Models\m_kategori_belanja;
use DataTables;
use DB;
use Session;

class m_paguController extends Controller
{
    public function index( Request $request){
        return view('master.pagu.pagu_h');
    }

    public function dataTable_akun(Request $request)
    {
        $query = m_akun::all();

        // return $query;
        $data = array();
        $no =1 ;

        foreach ($query as $key => $value) {
            $data[$key]['no'] = $no++;
            $data[$key]['id'] = $value->id;
            $data[$key]['kode_akun'] = $value->kode_akun;
            $data[$key]['nama_akun'] = $value->nama_akun;
            $data[$key]['created_at'] = $value->created_at;
        }

            return DataTables::of($data)
            ->addColumn('aksi', function($data){

                $buttonEdit ='<a  class="btn btn-sm btn-icon btn-pure btn-default deleteData"
                        data-toggle="tooltip"
                        data-id="'.$data['id'].'"
                        data-kode_akun="'.$data['kode_akun'].'"
                        data-nama_akun="'.$data['nama_akun'].'"
                        data-original-title="Edite">
                        <i class="icon wb-trash" aria-hidden="true"></i>
                        </a>';
                $buttonHapus='<a class="btn btn-sm btn-icon btn-pure btn-default editData_akun"
                        data-toggle="tooltip"
                        data-id="'.$data['id'].'"
                        data-kode_akun="'.$data['kode_akun'].'"
                        data-nama_akun="'.$data['nama_akun'].'"
                        data-original-title="Edite"> <i class="icon wb-edit"
                        aria-hidden="true"></i></a>';

                return $buttonHapus.$buttonEdit;
            })
            ->rawColumns(['aksi'])
            ->make(true);
    }

    public function tambahAkun(Request $request){

        $insert = [
            'kode_akun' => $request->kode_akun,
            'nama_akun' => $request->nama_akun            
        ];

        // khusus akun ada unique key
        if (isset($request->id)) {
            # code...
            $query = m_akun::find($request->id)->delete();
        }

        $query = m_akun::updateOrCreate(['id' => $request->id], $insert);

        if (isset($request->id) || $request->id !='') {
            $data="Sukses Update data Akun!";
            # code...
        } else {
            $data ="Sukses Tambah data Akun!";
            # code...
        }
        return response()->json($data);
    }


    public function destroyAkun(Request $request,$id)
    {
        $query = m_akun::find($request->id)->delete();
        if ( $query == true ) {
            $data = "Sukses hapus data Akun!";
        } else {
            $data ="Sukses hapus data Akun!";
        }
        return response()->json($data);
    }

    public function dataTable_jenis_belanja(Request $request)
    {
        $query = m_jenis_belanja::all();

        // return $query;
        $data = array();
        $no =1 ;

        foreach ($query as $key => $value) {
            $data[$key]['no'] = $no++;
            $data[$key]['id'] = $value->id;
            $data[$key]['nama'] = $value->nama;
            $data[$key]['created_at'] = $value->created_at;
        }

            return DataTables::of($data)
            ->addColumn('aksi', function($data){

                $buttonEdit ='<a  class="btn btn-sm btn-icon btn-pure btn-default deleteData"
                        data-toggle="tooltip"
                        data-id="'.$data['id'].'"
                        data-nama="'.$data['nama'].'"
                        data-original-title="Edite">
                        <i class="icon wb-trash" aria-hidden="true"></i>
                        </a>';
                $buttonHapus='<a class="btn btn-sm btn-icon btn-pure btn-default editData_jb"
                        data-toggle="tooltip"
                        data-id="'.$data['id'].'"
                        data-nama="'.$data['nama'].'"
                        data-original-title="Edite"> <i class="icon wb-edit"
                        aria-hidden="true"></i></a>';

                return $buttonHapus.$buttonEdit;
            })
            ->rawColumns(['aksi'])
            ->make(true);
    }

    public function tambahJenisBelanja(Request $request){

        $insert = [
            'nama' => $request->nama,
        ];

        $query = m_jenis_belanja::updateOrCreate(['id' => $request->id], $insert);

        if (isset($request->id) || $request->id !='') {
            $data="Sukses Update data Jenis Belanja!";
            # code...
        } else {
            $data ="Sukses Tambah data Jenis Belanja!";
            # code...
        }
        return response()->json($data);
    }


    public function destroyJenisBelanja(Request $request,$id)
    {
        $query = m_jenis_belanja::find($request->id)->delete();
        if ( $query == true ) {
            $data = "Sukses hapus data Jenis Belanja!";
        } else {
            $data ="Sukses hapus data Jenis Belanja!";
        }
        return response()->json($data);
    }

    public function dataTable_kategori_belanja(Request $request)
    {
        $query = m_kategori_belanja::join('anggaran.akun as a','a.kode_akun','=','anggaran.kategori_belanja.kode_akun')
                                    ->join('anggaran.jenis_belanja as jb','jb.id','=','anggaran.kategori_belanja.id_jenis_belanja')
                                    ->selectRaw("anggaran.kategori_belanja.*, a.nama_akun, jb.nama as jenis_belanja")->get();

        // return $query;
        $data = array();
        $no =1 ;

        foreach ($query as $key => $value) {
            $data[$key]['no'] = $no++;
            $data[$key]['id'] = $value->id;
            $data[$key]['kategori_belanja'] = $value->kategori_belanja;
            $data[$key]['minimal_anggaran'] = ($value->minimal_anggaran != '' || $value->minimal_anggaran != null) ? $value->minimal_anggaran : 'belum ditentukan';
            $data[$key]['maksimal_anggaran'] = ($value->maksimal_anggaran != '' || $value->maksimal_anggaran != null) ? $value->maksimal_anggaran : 'belum ditentukan';
            $data[$key]['kode_akun'] = $value->kode_akun;
            $data[$key]['id_jenis_belanja'] = $value->id_jenis_belanja;
            $data[$key]['satuan'] = $value->satuan;
            $data[$key]['created_at'] = $value->created_at;
            $data[$key]['nama_akun'] = $value->nama_akun;
            $data[$key]['jenis_belanja'] = $value->jenis_belanja;
        }
        
            return DataTables::of($data)
            ->addColumn('aksi', function($data){

                $buttonEdit ='<a  class="btn btn-sm btn-icon btn-pure btn-default deleteData"
                        data-toggle="tooltip"
                        data-id="'.$data['id'].'"
                        data-kategori_belanja="'.$data['kategori_belanja'].'"
                        data-minimal_anggaran="'.$data['minimal_anggaran'].'"
                        data-maksimal_anggaran="'.$data['maksimal_anggaran'].'"
                        data-kode_akun="'.$data['kode_akun'].'"
                        data-id_jenis_belanja="'.$data['id_jenis_belanja'].'"
                        data-satuan="'.$data['satuan'].'"
                        data-nama_akun="'.$data['nama_akun'].'"
                        data-jenis_belanja="'.$data['jenis_belanja'].'"
                        data-original-title="Edite">
                        <i class="icon wb-trash" aria-hidden="true"></i>
                        </a>';
                $buttonHapus='<a class="btn btn-sm btn-icon btn-pure btn-default editData_kb"
                        data-toggle="tooltip"
                        data-id="'.$data['id'].'"
                        data-kategori_belanja="'.$data['kategori_belanja'].'"
                        data-minimal_anggaran="'.$data['minimal_anggaran'].'"
                        data-maksimal_anggaran="'.$data['maksimal_anggaran'].'"
                        data-kode_akun="'.$data['kode_akun'].'"
                        data-id_jenis_belanja="'.$data['id_jenis_belanja'].'"
                        data-satuan="'.$data['satuan'].'"
                        data-nama_akun="'.$data['nama_akun'].'"
                        data-jenis_belanja="'.$data['jenis_belanja'].'"                        
                        data-original-title="Edite"> 
                        <i class="icon wb-edit" aria-hidden="true"></i></a>';

                return $buttonHapus.$buttonEdit;
            })
            ->rawColumns(['aksi'])
            ->make(true);
    }

    public function tambahKategoriBelanja(Request $request){

        $insert = [
            'kategori_belanja' => $request->kategori_belanja,
            'minimal_anggaran' => $request->minimal_anggaran,
            'maksimal_anggaran' => $request->maksimal_anggaran,
            'kode_akun' => $request->kode_akun,
            'id_jenis_belanja' => $request->id_jenis_belanja,
            'satuan' => $request->satuan,            
        ];

        $query = m_kategori_belanja::updateOrCreate(['id' => $request->id], $insert);

        if (isset($request->id) || $request->id !='') {
            $data="Sukses Update data Kategori Belanja!";
            # code...
        } else {
            $data ="Sukses Tambah data Kategori Belanja!";
            # code...
        }
        return response()->json($data);
    }

    public function destroyKategoriBelanja(Request $request,$id)
    {
        $query = m_kategori_belanja::find($request->id)->delete();
        if ( $query == true ) {
            $data = "Sukses hapus data Kategori Belanja!";
        } else {
            $data ="Sukses hapus data Kategori Belanja!";
        }
        return response()->json($data);
    }

    public function select2jenisBelanja(Request $request)
    {
        $search = (isset($request->search)) ? $request->search : '';

        $query = m_jenis_belanja::whereRaw("( nama ilike '%".$search."%' )")
                ->orderBy('nama','asc')
                ->get();

        $data = array();
        foreach ($query as $key => $value) {
            $data[$key]['id']=$value->id;
            $data[$key]['nama']=$value->nama;
        }
        // $array[] = array('a'=>'a');
        return response()->json($data);
    }

    public function select2akunBelanja(Request $request)
    {
        $search = (isset($request->search)) ? $request->search : '';

        $query = m_akun::whereRaw("( kode_akun::varchar ilike '%".$search."%' or nama_akun ilike '%".$search."%' )")
                ->orderBy('kode_akun','asc')
                ->get();

        $data = array();
        foreach ($query as $key => $value) {
            $data[$key]['id']=$value->kode_akun;
            $data[$key]['nama']='( '.$value->kode_akun.' ) '.$value->nama_akun;
        }
        // $array[] = array('a'=>'a');
        return response()->json($data);
    }
}
