<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class anggaranController extends Controller
{
    public function select2Pagu(Request $request)
    {
        $search = $request->search;
        $query = DB::table('anggaran.v_anggaran as va')
                ->whereRaw("( kategori_belanja ilike '%".$search."%' or kode_akun::text ilike '%".$search."%' )")        
                ->get();

        $data = array();
        foreach ($query as $key => $value) {
            $data[$key]['id'] = $value->id;
            $data[$key]['nama'] = $value->kategori_belanja;
            $data[$key]['satuan'] = $value->satuan;
            $data[$key]['maksimal_anggaran'] = $value->maksimal_anggaran;
        }
        // $array[] = array('a'=>'a');
        return response()->json($data);
    }

    public function getAnggaranDetail(Request $request)
    {
        $id_kategori_belanja = (isset($request->id_kabel)) ? $request->id_kabel : '';
        $kabel_data = DB::table('anggaran.kategori_belanja as kabel')
                    ->where("id","=",$id_kategori_belanja)
                    ->get()->first();

        return response()->json($kabel_data);
    }
}
