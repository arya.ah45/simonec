<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\rapat;
use App\Models\absensi;
use DeHelper;
use DB;

class absensiController extends Controller
{
    public function index()
    {
        return view('absensi');
    }

    public function simpanAbsensifromProker(Request $request)
    {
        // return $request->all();
        $id_proker = $request->id_proker;
        $id_rapat = DeHelper::decrypt_($request->id_rapat);
        $nim = '';

        if ($request->tipe_absensi == 0) { //online
            
            foreach ($request->nim as $key => $value) {
                $absen = new absensi;
                
                $absen->id_proker = $id_proker;
                $absen->id_rapat = $id_rapat;
                $absen->nim = $request->nim[$key];
                $absen->save();
            }
            
            return response()->json(['code' => 1, 'message' => 'Data absen berhasil disimpan!']);

        } else { //offline

            if ($request->nim_absen != null) {
                $nim = $request->nim_absen;
            }else{
                $nim = $request->nim;
            }

            $cek = DB::table('keanggotaan.anggota')
                    ->where('nim','=',$nim)
                    ->get()->first();
            
            if (empty($cek) == true) {
                return response()->json(['code' => 0, 'message' => 'QR Code tidak sesuai!']);
            }else{
                $cek_absen = absensi::where([['id_rapat',$id_rapat],['id_proker',$id_proker],['nim',$nim]])->get()->first();
                if (empty($cek_absen) == false) {
                    return response()->json(['code' => 0, 'message' => 'Anggota telah absen!']);
                }else{
                    $absen = new absensi;
                    $absen->id_proker = $id_proker;
                    $absen->id_rapat = $id_rapat;
                    $absen->nim = $nim;
                    $absen->save();
                    return response()->json(['code' => 1, 'message' => 'Data absen berhasil disimpan!']);
                }
            }
        }
    }

    public function get_absen($id_proker,$id_rapat)
    {
        $id_rapat = DeHelper::decrypt_($id_rapat);

        $data_raw = absensi::leftjoin('keanggotaan.anggota as ang','ang.nim','=','kegiatan.absensi.nim')
                            ->selectRaw("kegiatan.absensi.*, ang.nama as nama")
                            ->where([['id_proker','=',$id_proker],['id_rapat','=',$id_rapat]])
                            ->get();
        
        return $data_raw;
    }

    public function delete_absen(Request $request)
    {
        // return $request->all();

        $id_deleted_raw = substr($request->deleted_id,1,strlen($request->deleted_id));
        $id_deleted = explode(",", $id_deleted_raw);
        // return $pieces;

        $datadelete = absensi::whereIn('id',$id_deleted)->delete();
        return response()->json(['code' => 1, 'message' => 'Data absen berhasil dihapus!']);
    }
}
