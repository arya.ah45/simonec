<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\m_soal;
use App\Models\jawaban_tes;
use App\Models\calon_anggota;
use DB;
use Session;

class wawancaraController extends Controller
{
    public function index(Request $request)
    {
        return view('recruitment.wawancara');
    }

    public function getDataWawancara(Request $request)
    {
        $nim_calon = (isset($request->nim)) ? $request->nim : 'bukan_nim';
        $content = '';

        if ($nim_calon == 'bukan_nim') {
            $data['message'] = "Kode QR tidak terdaftar";
            $data['code'] = 0;
            return response()->json($data);
        }else{
            $check_data_raw = jawaban_tes::where('nim_calon','=',$nim_calon)->get();
            // return count($check_data_raw);
            if (count($check_data_raw) == 0) {
                $data['message'] = "Kode QR tidak terdaftar";
                $data['code'] = 0;
                return response()->json($data);
            }
        }

        $data_raw = m_soal::leftjoin('keanggotaan.jawaban_tes as jt','jt.id_tes','=','master.m_soal_tes.id')
                            ->leftjoin('keanggotaan.calon_anggota as ang','ang.nim','=','jt.nim_calon')
                            ->leftjoin('master.m_kelas as mkls','mkls.id','=','ang.id_kelas')
                            ->leftjoin('master.m_prodi as mprd','mprd.id','=','ang.id_prodi')
                            ->selectRaw("master.m_soal_tes.nama as soal, jt.nim_calon, jt.jawaban, ang.nama as nama_mhs,ang.is_anggota as kondis, mkls.nama as nama_kelas, mprd.nama as nama_prodi")
                            // ->where('jt.nim_calon','=',$nim_calon)
                            ->where([
                                ['jt.nim_calon','=',$nim_calon],
                                ['ang.is_anggota', '=', 1],
                            ])
                            ->orderBy('jt.id_tes', 'asc')
                            ->get();
        // return $data_raw;

        if (count($data_raw) > 0 ) {
            foreach ($data_raw as $key => $value) {
                $no = $key + 1;

                $content .= '<div class="col-xxl-12 col-lg-12 panel_isi soal_jawaban"><div class="panel shadow panel-bordered">';
                $content .= '<div class="panel-heading"><h3 class="panel-title text-dark display-5">Soal nomor '.$no.'</h3><div class="panel-actions ">';
                $content .= '<a class="panel-action icon wb-minus" aria-expanded="true" data-toggle="panel-collapse" aria-hidden="true"></a>';
                $content .= '<a class="panel-action icon wb-expand" data-toggle="panel-fullscreen" aria-hidden="true"></a></div></div>';
                $content .= '<div class="panel-body pb-15 pt-10 "><div class="row"><div class="col-md-12">';
                $content .= '<blockquote class="blockquote P-0 pt-4"><p class="mb-0 ">'.$value->soal.'</p></blockquote>';
                $content .= '<h4>Jawaban</h4>';
                $content .= '<p>'.$value->jawaban.'</p>';
                $content .= '</div></div></div>';
                // if (count($data_raw) == $key+1) {
                // $content .= '<div class="panel-footer bg-gray-100 pb-15 pt-10">';
                // $content .= '<button class="btn btn-outline-info btn-block btn_selesai">SELESAI</button></div>';
                // }
                $content .= '</div></div>';
            }
        }else{
            $data['message'] = "Anda Sudah Melakukan Test Wawancara";
            $data['code'] = 0;
            return response()->json($data);
        }

        $data['content'] = $content;
        $data['nim_calon'] = (isset($value->nim_calon)) ? $value->nim_calon : '';
        $data['nama_mhs'] = (isset($value->nama_mhs)) ? $value->nama_mhs : '';
        $data['nama_kelas'] = (isset($value->nama_kelas)) ? $value->nama_kelas : '';
        $data['nama_prodi'] = (isset($value->nama_prodi)) ? $value->nama_prodi : '';

        return response()->json($data);
    }

    public function konfirmasiSelesaiWawancara(Request $request)
    {
        // return $request->all();
        $nim = (isset($request->nim)) ? $request->nim : '' ;

        if ($nim != '') {
            DB::table('keanggotaan.calon_anggota as ang')
                ->where('ang.nim','=',$nim)
                ->update(['is_anggota' => 2]);
        }
        $data['code']="200";
        $data['message']="Data terkonfirmasi!";
        return response()->json($data);exit();
    }
}
