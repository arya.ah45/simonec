<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\proposal;
use App\Models\lpj;
use App\Models\proker;
use App\Models\m_kriteria_penilaian;
use App\Models\review_lpj;
use App\Models\notif;
use Session;
use DB;
use DeHelper;
use App\Helpers\DeHelper2;
use File;
use DataTables;

class lpjController extends Controller
{
    public function index()
    {
        $user 		= Session::get("data_user");
        
        $flag_lv = $user['flag_lv'];
        $nim = $user['nim'];
        $key_menu = $user['key_menu'];

        $data['kriteria_pen'] = m_kriteria_penilaian::where([['is_lpj','=','1'],['for_user','=',$flag_lv]])
                                ->orderBy('id','asc')->get();  
        return view('lpj.lpj_h',$data);
    }

    public function getHasilReviewLPJ(Request $request)
    {
        $id_lpj = (isset($request->id_lpj)) ? DeHelper::decrypt_($request->id_lpj) : '';
        $flag_lv = (isset($request->flag_lv)) ? $request->flag_lv : '';

        $data_review = review_lpj::leftjoin("master.m_kriteria_penilaian as mkp","mkp.id","=","kegiatan.review_lpj.kriteria_id")
                        ->where([['lpj_id','=',$id_lpj],['from_user','=',$flag_lv]])->get();
        // return $data_review ;
        $htmle ='';
        foreach ($data_review as $key => $value) {
            $judul = $value->nama;
            $judul_true = substr($judul,6,strlen($judul)-20);
            $catatan = $value->catatan;
            // return $judul_true;
            $status_badge = '';

            if ($catatan == '' || $catatan == null) {
            } else {
                if ($value->status == 0) {
                    $status_badge = '<span class="badge badge-danger">salah</span>';
                } else {
                    $status_badge = '<span class="badge badge-info">sudah benar dengan catatan</span>';
                }
                
                $htmle .= '<dt>'.++$key.'. '.$judul_true.' '.$status_badge.'</dt>';
                $htmle .= '<dl class="dl-horizontal row"><dt class="col-sm-2">&emsp;Catatan: </dt><dd class="col-sm-10">'.$catatan.'</dd></dl> ';

            }
            
        }
        return response()->json($htmle);
    }

    public function select2bulanLPJ(Request $request)
    {
        $tahun = (isset($request->tahun)) ? $request->tahun : 0;

        $data_bulan = DB::table('kegiatan.lpj')
                        ->selectRaw("EXTRACT(MONTH FROM created_at) as bulan")
                        ->when($tahun, function ($query) use ($tahun) {
                            if ($tahun != 0 ) {
                                return $query->where([
                                        [DB::raw('EXTRACT(YEAR FROM created_at)'),'=',$tahun],
                                    ]);
                            }else{
                                return $query;
                            }
                        })                        
                        ->groupByRaw("EXTRACT(MONTH FROM created_at)")
                        ->orderBy(DB::RAW('EXTRACT(MONTH FROM created_at)'),'asc')                        
                        ->get();
        // return $data_bulan;

        $data = array();
        foreach ($data_bulan as $key => $value) {
            $data[$key]['id'] = $value->bulan;
            $data[$key]['nama'] = DeHelper::getNamaBulan($value->bulan);
        }
        // $array[] = array('a'=>'a');
        return response()->json($data);        
    }

    public function select2tahunLPJ(Request $request)
    {
        $bulan = (isset($request->bulan)) ? $request->bulan : 0;

        $data_tahun = DB::table('kegiatan.lpj')
                        ->selectRaw("EXTRACT(YEAR FROM created_at) as tahun")
                        ->when($bulan, function ($query) use ($bulan) {
                            if ($bulan != 0 ) {
                                return $query->where([
                                        [DB::raw('EXTRACT(MONTH FROM created_at)'),'=',$bulan],
                                    ]);
                            }else{
                                return $query;
                            }
                        })                        
                        ->groupByRaw("EXTRACT(YEAR FROM created_at)")
                        ->orderBy(DB::RAW('EXTRACT(YEAR FROM created_at)'),'desc')                          
                        ->get();
        // return $data_tahun;

        $data = array();
        foreach ($data_tahun as $key => $value) {
            $data[$key]['id'] = $value->tahun;
            $data[$key]['nama'] = $value->tahun;
        }
        // $array[] = array('a'=>'a');
        return response()->json($data);  
    }

    public function dataTable_lpj(Request $request)
    {
        $tahun = (isset($request->tahun)) ? $request->tahun : 0;
        $bulan = (isset($request->bulan)) ? $request->bulan : 0;
        
        $user 		= Session::get("data_user");
        
        $flag_lv = $user['flag_lv'];
        $nim = $user['nim'];
        $key_menu = $user['key_menu'];

        $data_isi = lpj::leftJoin(DB::raw('(SELECT A.* FROM  approve.approv_lpj A
                    JOIN ( SELECT lpj_id, MAX ( ID ) AS iid FROM approve.approv_lpj GROUP BY lpj_id ) B
                    ON A.ID = B.iid AND A.lpj_id = B.lpj_id) as aprov'),'aprov.lpj_id','=','kegiatan.lpj.id')
                    ->leftjoin('kegiatan.proker as pkr','pkr.id','=','kegiatan.lpj.proker_id')                 
                    ->when($flag_lv, function ($query) use ($flag_lv) {
                        if ($flag_lv == "d") {
                            return $query->whereIn('aprov.status',array(5));
                        }elseif ($flag_lv == "c") {
                            return $query->whereIn('aprov.status',array(0,1,2,3,4,5,6,7,8));                  
                        }elseif ($flag_lv == "b") {
                            return  $query  ->where(function($q) use ($flag_lv) {
                                                $q ->whereIn('aprov.status',array(0,2,3,4,5));
                                            });
                        }elseif ($flag_lv == "a") {
                            return  $query  ->where(function($q) use ($flag_lv) {
                                                $q ->whereIn('aprov.status',array(3,5,6,7,8));
                                            });
                        }else{
                        return $query;
                    }})
                    ->when($tahun, function ($query) use ($tahun) {
                        if ($tahun != 0) {
                            return $query->where([
                                    [DB::raw('EXTRACT(YEAR FROM kegiatan.lpj.created_at)'),'=',$tahun],
                                ]);
                        }else{
                            return $query;
                        }
                    })    
                    ->when($bulan, function ($query) use ($bulan) {
                        if ($bulan != 0 ) {
                            return $query->where([
                                    [DB::raw('EXTRACT(MONTH FROM kegiatan.lpj.created_at)'),'=',$bulan],
                                ]);
                        }else{
                            return $query;
                        }
                    })                   
                    ->selectRaw("kegiatan.lpj.*, aprov.status, aprov.catatan, pkr.nama as nama_proker, pkr.event as tipe_proker,
                                pkr.penanggungjawab, pkr.tanggal_mulai, pkr.tanggal_selesai, pkr.tujuan, pkr.dana_turun, pkr.dana_usulan, pkr.dana_mandiri")
                    ->orderBy('pkr.tanggal_mulai','desc')
                    ->get();
        // return $data_isi;

        $no = 1;
        foreach ($data_isi as $value) {
            $id_lpj = DeHelper::encrypt_($value->id);
            $proker_id = (isset($value->proker_id)) ? $value->proker_id : '';
            $nama_file=(isset($value->nama_file)) ? $value->nama_file : '';
            $status =(isset($value->status)) ? $value->status : NULL;
            $nama_proker =(isset($value->nama_proker)) ? $value->nama_proker : '';
            $catatan =(isset($value->catatan)) ? $value->catatan : '';
            $file_path = asset('lpj_file').'/'.$nama_file;
            $tipe_proker = (isset($value->tipe_proker)) ? $value->tipe_proker : '';
            $penanggungjawab = (isset($value->penanggungjawab)) ? $value->penanggungjawab : '';
            $tanggal_mulai = (isset($value->tanggal_mulai)) ? DeHelper::tanggal_indonesia($value->tanggal_mulai) : '';
            $tanggal_selesai = (isset($value->tanggal_selesai)) ? DeHelper::tanggal_indonesia($value->tanggal_selesai) : '';
            $tujuan = (isset($value->tujuan)) ? $value->tujuan : '';
            $dana_turun = (isset($value->dana_turun)) ? DeHelper::number2rupiah($value->dana_turun) : '';
            $dana_mandiri = (isset($value->dana_mandiri)) ? DeHelper::number2rupiah($value->dana_mandiri) : '';
            $dana_usulan = (isset($value->dana_usulan)) ? DeHelper::number2rupiah($value->dana_usulan) : '';

            if ($tipe_proker == '1') {
                $tipe_proker = 'Proker non Event';
            } elseif ($tipe_proker == '2') {
                $tipe_proker = 'Proker Event';
            } else {
                $tipe_proker = 'belum dipilih';
            }
            
            $flag_level = '';
            if ($flag_lv=="c") {
                if ($status == 4) {
                    $flag_level = 'b';
                }elseif($status  == 8){
                    $flag_level = 'a';
                }else{
                }
            }else if($flag_lv=="b"){
                if ($status  ==  2 || $status == 0) {
                    $flag_level = 'b';
                }else{
                }
            }elseif ($flag_lv=="a") {
                if ($status  ==  3) {
                    $flag_level = 'a';
                }else{
                }
            } else{
            }

            // $link_hapus = url('surat_keterangan/hapus_penghargaan/'.$id_riwayat);
            $review = '<a type="button" href="javascript:void(0)" class="btn btn-xs bg-pink-700 text-white btnReview" 
                data-id="'.$id_lpj.'" 
                data-proker_id="'.$proker_id.'" 
                data-status="'.$status.'"                
                data-catatan="'.$catatan.'"     
                data-nama_proker="'.$nama_proker.'"
                data-tipe_proker="'.$tipe_proker.'"
                data-penanggungjawab="'.$penanggungjawab.'"
                data-tanggal_mulai="'.$tanggal_mulai.'"
                data-tanggal_selesai="'.$tanggal_selesai.'"
                data-tujuan="'.$tujuan.'"
                data-dana_turun="'.$dana_turun.'"
                data-dana_mandiri="'.$dana_mandiri.'"
                data-dana_usulan="'.$dana_usulan.'"
                data-flag_lv="'.$flag_level.'"                           
                data-nama_file="'.$nama_file.'">
                <i class="icon fa-search"></i> Review </a>';
            $cek_review = '<a type="button" href="javascript:void(0)" class="btn btn-xs btn-secondary btnCekReview" 
                data-id="'.$id_lpj.'" 
                data-proker_id="'.$proker_id.'"
                data-status="'.$status.'"                 
                data-catatan="'.$catatan.'"     
                data-nama_proker="'.$nama_proker.'"
                data-tipe_proker="'.$tipe_proker.'"
                data-penanggungjawab="'.$penanggungjawab.'"
                data-tanggal_mulai="'.$tanggal_mulai.'"
                data-tanggal_selesai="'.$tanggal_selesai.'"
                data-tujuan="'.$tujuan.'"
                data-dana_turun="'.$dana_turun.'"
                data-dana_mandiri="'.$dana_mandiri.'"
                data-dana_usulan="'.$dana_usulan.'"
                data-flag_lv="'.$flag_level.'"                            
                data-nama_file="'.$nama_file.'">
                <i class="icon fa-search"></i> Cek Review </a>';
            $perbaikan = '<a type="button"  data-id="'.$id_lpj.'" href="javascript:void(0)" class="btn btn-xs bg-amber-700 text-white btn_perbaikan"
                data-proker_id="'.$proker_id.'"
                data-nama_file="'.$nama_file.'"
                data-status="'.$status.'"
                data-nama_proker="'.$nama_proker.'">
                <i class="icon fa-wrench"></i> Perbaikan </a>';
            $cek = '<a href="javascript:void(0)" type="button" class="btn btn-xs btn-secondary btnDetail"
                data-proker_id="'.$proker_id.'"
                data-nama_file="'.$nama_file.'"
                data-status="'.$status.'"
                data-nama_proker="'.$nama_proker.'">
            <i class="icon fa-eye"></i> Cek </a>';
            $hapus = '<a type="button"  data-id="'.$id_lpj.'"  class="btn btn-xs btn-danger bg-pink-600 btnDelete" href="javascript:void(0)"><i class="icon fa-trash"></i> Delete </a>';
            $download_file = '<a href="'.$file_path.'" download class="btn btn-xs btn-round btn-outline-success btnAddAbsensi">Download file LPJ</a>';

            if ($flag_lv=="c") {
                if ($status == 2 ) {
                    $aksi = $hapus;
                }elseif($status  == 4 || $status  == 8){
                    $aksi = $cek_review.' '.$perbaikan;
                }else{
                    $aksi = '-';
                }
            }else if($flag_lv=="b"){
                if ($status  ==  2 || $status == 0) {
                    $aksi = $review;
                }else{
                    $aksi = '-';
                }
            }elseif ($flag_lv=="a") {
                if ($status  ==  3) {
                    $aksi = $review;                  
                }else{
                    $aksi = '-';
                }
            } else{
                $aksi= '-';
            }

            if($status === NULL)
            {
                $stat = '<span class="badge badge-secondary">Telah disetujui</span>';
                $aksi = $cek;                    
            } else {
                $stat = DeHelper::status_pengajuan($status);
            }

            $dataTabel[] = array(
                'no' => $no++,
                'id_lpj' => $id_lpj,
                'proker_id' => $proker_id,
                'nama_file' => $nama_file,
                'status' => $status,
                'nama_proker' => $nama_proker,
                'download_file' => $download_file,
                'tanggal_upload' => DeHelper::format_tanggal($value->created_at),
                'status' => $stat,
                'aksi'=>$aksi
            );           
        }
        // return $dataTabel;

        $data_ = (isset($dataTabel)?$dataTabel:[]);

        return DataTables::of($data_)
            ->addColumn('status', function($data_){
                return $data_['status'];
            })
            ->addColumn('download_file', function($data_){
                return $data_['download_file'];
            })
            ->addColumn('aksi', function($data_){
                return $data_['aksi'];
            })
            ->rawColumns(['status','download_file','aksi'])
            ->make(true);
    }

    public function simpanLPJ(Request $request)
    {
        // return '';
        // return $request->all();
        $id = (isset($request->id)) ? DeHelper::decrypt_($request->id) : null;
        $proker_id = (isset($request->proker_id)) ? $request->proker_id : null;
        $id_proker = (isset($request->proker)) ? $request->proker : null;
        $action = (isset($request->action)) ? $request->action : null;
        $file = $request->file('file_proposal');

        if ($action != 'perbaikan') {
            $id_proker = $proker_id;
        }else{
        }

        if ($action == 'perbaikan') {
            $data_prop = lpj::find($id);
            // return $_SERVER['DOCUMENT_ROOT'].'/simonec/public/lpj_file/'.$data_prop->nama_file;
            // return asset('lpj_file').'/'.$data_prop->nama_file;
            // unlink($_SERVER['DOCUMENT_ROOT'].'/simonec/public/lpj_file/'.$data_prop->nama_file);
            File::delete(public_path().'/'.$data_prop->nama_file);            
        }else{}

        $session = Session::get('data_user');
        $nim = $session['nim'];
        $flag_lv = $session['flag_lv']; 

        // menyimpan file
        $data_proker = proker::find($id_proker);
        $nama_file = 'LPJ_'.str_replace(' ','_',$data_proker->nama).'_'.$data_proker->tanggal_mulai.'.'.$file->getClientOriginalExtension();
        $file->move(public_path('lpj_file'), $nama_file);    

        $data = [
            'proker_id' => $id_proker,
            'nama_file' => $nama_file,
        ];

        $prop = lpj::updateOrCreate(['id' => $id], $data);
        $lpj_id = $prop->id;

        if ($action == 'perbaikan') {
            
            if ($flag_lv == "c") {
                $data_approv['status'] = 0; 
                $data_approv['send_to'] = 'b'; 
            }else{
                $data['code']="200";
                $data['message']="user tidak memiliki akses!";
                return response()->json($data);   
            }

            $data_approv['lpj_id'] = $id;
            $data_approv['proker_id'] = $id_proker;
            $data_approv['created_at'] = date("Y-m-d H:i:s");
            $data_approv['created_by'] = $nim;
            DB::table('approve.approv_lpj')->insert($data_approv);      
            
            $notif['action'] = $action;
            $notif['id_subjek'] = $data_approv['lpj_id'];    
            $notif['status'] = $data_approv['status'];
            DeHelper2::simpanNotif($notif);
            
            $data['code']="200";
            $data['message']="Data LPJ berhasil diperbaiki!";
            return response()->json($data);    

        }else{
            // inserting status Approval
            if ($flag_lv == "c") {
                $data_approv['status'] = 2; 
                $data_approv['send_to'] = 'b'; 
            }else{
                $data_approv['status'] = 1; 
                $data_approv['send_to'] = 'b'; 
            }
    
            $data_approv['lpj_id'] = $lpj_id;
            $data_approv['proker_id'] = $id_proker;
            $data_approv['created_at'] = date("Y-m-d H:i:s");
            $data_approv['created_by'] = $nim;
            DB::table('approve.approv_lpj')->insert($data_approv);    
        
            $notif['action'] = $action;
            $notif['id_subjek'] = $data_approv['lpj_id'];    
            $notif['status'] = $data_approv['status'];
            DeHelper2::simpanNotif($notif);

            $data['code']="200";
            $data['message']="Data LPJ berhasil disimpan!";
            return response()->json($data);              
        }                
    }

    public function deleteLPJ($id_lpj)
    {
        $id = DeHelper::decrypt_($id_lpj);  
        $data_lpj = DB::table('kegiatan.lpj')->where('id', '=', $id)->get()->first();
        File::delete(public_path().'/lpj_file/'.$data_lpj->nama_file);            

        $datas = DB::table('kegiatan.lpj')->where('id', '=', $id)->delete();

        $data['code']="200";
        $data['message']="Data LPJ berhasil dihapus permanen!";
        return response()->json($data);   
    }

    public function simpanReviewLPJ(Request $request)
    {
        // return '';
        // return $request->all();
        $session = Session::get('data_user');
        $nim = $session['nim'];
        $flag_lv = $session['flag_lv'];           
        $stat = [];
        $lpj_id = DeHelper::decrypt_($request->lpj_id);  
        $id_proker = (isset($request->proker_id)) ? $request->proker_id : null;
        $catatan_tambahan = (isset($request->catatan_tambahan)) ? $request->catatan_tambahan : null;

        // karena saat tidak dicentang status tidak dikirim, maka butuh helper dibawah ini
        $stat = explode(',',substr($request->status_ok,1,strlen($request->status_ok)));
        
        foreach ($request->kriteria_id as $key => $value) {
            $note = (isset($request->catatan[$key])) ? $request->catatan[$key] : ''; 
            
            try {
                $review_lpj_raw = review_lpj::where([
                    ['lpj_id','=',$lpj_id],
                    ['kriteria_id','=',$request->kriteria_id[$key]],
                    ['from_user',"=",$flag_lv]])->delete();                   
            } catch (\Throwable $th) {
            }

            $data_insert = [
                'kriteria_id' => $request->kriteria_id[$key],
                'status' => $stat[$key],
                'catatan' => $note,
                'from_user' => $flag_lv,
                'lpj_id' => $lpj_id,
            ];
            $insert = review_lpj::create($data_insert);
        }

        $session = Session::get('data_user');
        $nim = $session['nim'];
        $flag_lv = $session['flag_lv']; 
        // inserting status Approval
        if ($flag_lv == "a") {
            if(in_array(0, $stat)){
                $data_approv['status'] = 8; 
                $action = 'tolak';                
            }else{
                $data_approv['status'] = 5; 
                $action = 'approve';                
            }            
            $data_approv['send_to'] = 'c'; 
        }elseif ($flag_lv == "b") {
            if(in_array(0, $stat)){
                $data_approv['status'] = 4;
                $action = 'tolak';                   
            }else{
                $data_approv['status'] = 3;
                $action = 'approve';                
            }            
            $data_approv['send_to'] = 'a'; 
        }else{
            $data['code']="200";
            $data['message']="user tidak memiliki hak akses!";
            return response()->json($data);  
        }

        $data_approv['lpj_id'] = $lpj_id;
        $data_approv['catatan'] = $catatan_tambahan;
        $data_approv['proker_id'] = $id_proker;
        $data_approv['created_at'] = date("Y-m-d H:i:s");
        $data_approv['created_by'] = $nim;
        DB::table('approve.approv_lpj')->insert($data_approv);    

            $notif['action'] = $action;
            $notif['id_subjek'] = $data_approv['lpj_id'];
            $notif['status'] = $data_approv['status'];
            DeHelper2::simpanNotif($notif);        

        $data['code']="200";
        $data['message']="Data review LPJ berhasil disimpan!";
        return response()->json($data);        
    }

    public function getReview($id_lpj,$flag_lv)
    {
        $id_lpj = DeHelper::decrypt_($id_lpj);        
        $data_review = review_lpj::where([['lpj_id','=',$id_lpj],['from_user','=',$flag_lv]])->get();
        return response()->json($data_review);        
    }    
}
