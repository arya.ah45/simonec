<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use DB;
use DeHelper;
use App\Models\proker;
use App\Models\kbt_proker;
use App\Models\Panitia;
use App\Models\rapat;
use PDF;

class detailProkerController extends Controller
{
    public function detailProker(Request $request,$id_proker)
    {
        $id_proker = DeHelper::decrypt_($id_proker);

        $data_proker = proker::where('kegiatan.proker.id','=',$id_proker)
                            ->leftjoin('keanggotaan.anggota as ang','ang.nim','=','kegiatan.proker.penanggungjawab')
                            ->leftJoin(DB::raw('(SELECT A.* FROM  approve.approv_proker A
                                JOIN ( SELECT proker_id, MAX ( ID ) AS iid FROM approve.approv_proker GROUP BY proker_id ) B
                                ON A.ID = B.iid AND A.proker_id = B.proker_id) as aprov'),'aprov.proker_id','=','kegiatan.proker.id')                            
                            ->selectRaw("kegiatan.proker.*, ang.nama as nama_pj, aprov.status, aprov.catatan")
                            ->get()->first();
        // return $data_proker;

        $data_proker->subjek_tujuan = ($data_proker->subjek_tujuan != null) ? $data_proker->subjek_tujuan : '-';
        $data_proker->dana_usulan = ($data_proker->dana_usulan != null) ? $data_proker->dana_usulan : 0;
        $data_proker->dana_terpakai = ($data_proker->dana_terpakai != null) ? $data_proker->dana_terpakai : 0;
        $data_proker->dana_turun = ($data_proker->dana_turun != null) ? $data_proker->dana_turun : 0;
        $data_proker->dana_mandiri = ($data_proker->dana_mandiri != null) ? $data_proker->dana_mandiri : 0;
        $data_proker->tujuan = ($data_proker->tujuan != null) ? $data_proker->tujuan : '-';
        // return $data_proker;
        $data['proker'] = $data_proker;
        
        $data_kebutuhans = kbt_proker::where('id_proker','=',$id_proker);
        $data_kebutuhan = $data_kebutuhans->get();
        $data['kebutuhan'] = $data_kebutuhan;
        
        // $dana_usulan = $data_kebutuhans->selectRaw("sum(harga_satuan*jumlah) as total_usulan")->first()->total_usulan;
        // $data['dana_usulan'] = $data_kebutuhan;
        
        // $dana_terpakai = (isset($data_kebutuhans->selectRaw("sum(harga_satuan_real*jumlah_real) as total_terpakai")->first()->total_terpakai)) ? $data_kebutuhans->selectRaw("sum(harga_satuan_real*jumlah_real) as total_terpakai")->first()->total_terpakai : 0;
        $time_tersisa = DeHelper::time_remaining($data_proker->tanggal_mulai,$data_proker->tanggal_selesai);
        // return $time_tersisa;
        $data['waktu'] = $time_tersisa;
        $data['id_proker'] = $id_proker;
        $id_proker_encrypted = DeHelper::encrypt_($id_proker);

        $tolak2 = '<button class="btn btn-round w-100 btn-danger btn_tolak" style="
            position: fixed;
            bottom: 50px;
            right: 305px;
            height: 2.5em;
            z-index:900;" data-id="'.$id_proker_encrypted.'" data-jenis="ditolak" data-q="Apakah anda yakin menolak proker ini?">Tolak</button>';     

        $approv_dc = '<button class="btn btn-round btn-info btn_approve_dc" style="
            position: fixed;
            bottom: 50px;
            right: 125px;
            height: 2.5em;
            z-index:800;" data-id="'.$id_proker_encrypted.'" data-jenis="approve_ct">Setujui dengan catatan</button>'; 

        $tolak = '<button class="btn btn-round w-100 btn-danger btn_tolak" style="
            position: fixed;
            bottom: 50px;
            right: 125px;
            height: 2.5em;
            z-index:700;" data-id="'.$id_proker_encrypted.'" data-jenis="ditolak" data-q="Apakah anda yakin menolak proker ini?">Tolak</button>';  

        $approve = '<button class="btn btn-round w-100 btn-success btn_approve" style="
            position: fixed;
            bottom: 50px;
            right: 20px;
            height: 2.5em;
            z-index:600;" data-id="'.$id_proker_encrypted.'" data-jenis="approve" data-q="Apakah anda yakin menyetujui proker ini?">Setujui</button>';     

        $perbaikan = '<button class="btn btn-round bg-amber-700 text-white btn_perbaikan" style="
            position: fixed;
            bottom: 50px;
            right: 20px;
            height: 2.5em;
            z-index:550;" data-id="'.$id_proker_encrypted.'" data-jenis="telah_diperbaiki" data-q="Apakah anda yakin menyelesaikan perbaikan proker ini?">Perbaikan selesai</button>';     

        $penyesuaian = '<button class="btn btn-round btn-primary btn_penyesuaian" style="
            position: fixed;
            bottom: 50px;
            right: 20px;
            height: 2.5em;
            z-index:540;" data-id="'.$id_proker_encrypted.'" data-jenis="telah_disesuaikan" data-q="Apakah anda yakin menyelesaikan penyesuaian proker ini?">Penyesuaian selesai</button>';     

        $review = '<button class="btn btn-round btn-primary btn_review_ok" style="
            position: fixed;
            bottom: 50px;
            right: 20px;
            height: 2.5em;
            z-index:530;" data-id="'.$id_proker_encrypted.'" data-jenis="review_ok" data-q="Apakah anda yakin menyelesaikan review proker ini?">Review selesai</button>';     

        $kirim_ket = '<button class="btn btn-round btn-primary btn_kirim_kt" style="
            position: fixed;
            bottom: 50px;
            right: 20px;
            height: 2.5em;
            z-index:500;" data-id="'.$id_proker_encrypted.'" data-jenis="kirim_ke_ketua" data-q="Apakah anda yakin mengirim proker ini ke ketua UKM?">kirim ke Ketua</button>';           

        $data['btn_edit_home'] = '';
        $data['btn_edit_keb'] = '';
        $data['btn_edit_pengurus'] = '';
        $data['btn_add_rapat'] = '';

        $user= Session::get("data_user");
        $flag_lv = $user['flag_lv'];
        $key_menu = $user['key_menu'];
        $nim = $user['nim'];
        $status = $data_proker->status;
        // return $data_proker->penanggungjawab;
        if ($data_proker->penanggungjawab == $nim ) {
                $data['btn_add_rapat'] = '<button class="btn btn-xs btn-round w-100 btn-info showModalRapat">+ tambah data</button>';                
            if ($status == 1 ) {
                $aksi = $kirim_ket;
                $data['btn_edit_home'] = '<button class="btn btn-sm btn-round w-100 btn-warning" id="btn_edit_home">Edit</button>';
                $data['btn_edit_keb'] = '<button class="btn btn-xs btn-round w-60 btn-warning" id="btn_edit_keb">Edit</button>';
                $data['btn_edit_pengurus'] = '<button class="btn btn-xs btn-round w-60 btn-warning" id="btn_edit_pan">edit</button>';                
            }elseif($status  == 4 || $status  == 8){
                $aksi = $perbaikan;
                $data['btn_edit_home'] = '<button class="btn btn-sm btn-round w-100 btn-warning" id="btn_edit_home">Edit</button>';
                $data['btn_edit_keb'] = '<button class="btn btn-xs btn-round w-60 btn-warning" id="btn_edit_keb">Edit</button>';
                $data['btn_edit_pengurus'] = '<button class="btn btn-xs btn-round w-60 btn-warning" id="btn_edit_pan">edit</button>';
            }elseif($status  == 6){
                $aksi = $penyesuaian;
                $data['btn_edit_home'] = '<button class="btn btn-sm btn-round w-100 btn-warning" id="btn_edit_home">Edit</button>';
                $data['btn_edit_keb'] = '<button class="btn btn-xs btn-round w-60 btn-warning" id="btn_edit_keb">Edit</button>';
                $data['btn_edit_pengurus'] = '<button class="btn btn-xs btn-round w-60 btn-warning" id="btn_edit_pan">edit</button>';
            }elseif($flag_lv=="b"){
                if ($status  ==  2 || $status == 0) {
                    $aksi = $tolak.' '.$approve;
                }else{
                    $aksi = '';
                }                
            }else{
                $aksi = '';
            }
        }elseif($flag_lv=="b"){
            if ($status  ==  2 || $status == 0) {
                $aksi = $tolak.' '.$approve;
            }else{
                $aksi = '';
            }
        }elseif ($flag_lv=="a") {
            if ($status  ==  3) {
                $aksi = $tolak2.' '.$approv_dc.' '.$approve;
            }elseif($status  == 7){
                $aksi = $review;                    
            }else{
                $aksi = ' ';
            }
        } else{
            $aksi= '';
        }
        $data['aksi'] = $aksi;
        // Session::put('id_proker',$id_proker);
        // return $data;
        return view('proker.detail.proker_detail',$data);
    }

    public function updateDetail(Request $request)
    {
        // return $request->all();
        
        $dana_kampus = DeHelper::rupiah2number((isset($request->dana_kampus)) ? $request->dana_kampus : 0);
        $dana_mandiri = DeHelper::rupiah2number((isset($request->dana_mandiri)) ? $request->dana_mandiri : 0);
        $dana_terpakai = DeHelper::rupiah2number((isset($request->dana_terpakai)) ? $request->dana_terpakai : 0);
        $dana_usulan = DeHelper::rupiah2number((isset($request->dana_usulan)) ? $request->dana_usulan : 0);
        $rating = (isset($request->rating)) ? $request->rating : 0;
        $evaluasi = (isset($request->evaluasi)) ? $request->evaluasi : null;

        if (isset($request->nama)) {
            $data_proker = [
                'id_struktur' => $request->id_struktur,
                'nama' => $request->nama,
                'tanggal_mulai' => $request->tgl_mulai,
                'tanggal_selesai' => $request->tgl_selesai,
                'dana_turun' => $dana_kampus,
                'dana_mandiri' => $dana_mandiri,
                'dana_terpakai' => $dana_terpakai,
                'dana_usulan' => $dana_usulan,
                'ket_tambahan' => $request->ket_tambahan,
                'penanggungjawab' => $request->penanggungjawab,
                'event' => $request->tipe_proker,
                'tujuan' => $request->tujuan,
                'subjek_tujuan' => $request->subjek_tujuan,
                'terlaksana' => $request->terlaksana,
                'updated_at' => date("Y-m-d H:i:s")
            ];

            $admin = array(
                'nim' => $request->penanggungjawab,
            );
            DB::table('kegiatan.admin_kegiatan')->where([['proker_id', '=', $request->id_proker]])->update($admin);
            
            $pj =[
                'nim' => $request->penanggungjawab,
                'updated_at' => date("Y-m-d H:i:s")            
            ];
            DB::table('kegiatan.kepanitiaan')->where([['id_proker', '=', $request->id_proker],['id_kepanitiaan', '=', '1']])->update($pj); 
            
        }else{
            $data_proker = [
                'terlaksana' => $request->terlaksana,
                'rating' => $rating,
                'evaluasi' => $evaluasi,                
                'updated_at' => date("Y-m-d H:i:s")
            ];
        }

        $query = DB::table('kegiatan.proker')->where([
            ['id', '=', $request->id_proker]
        ])->update($data_proker);        
        
        return response()->json("sukses lur");        
    }

    public function dataTable_kebutuhan(Request $request,$id_proker)
    {
        $searchs = $request->searchs;
        $limit = $request->limit;
        $offset = $request->offset;
        $data=[];

        $data_Raw = kbt_proker::where('id_proker','=',$id_proker)
                ->leftjoin('master.m_sie_kepanitiaan as mp','mp.id','=','kegiatan.kebutuhan_event.id_sie_kepanitiaan')
                ->selectRaw("kegiatan.kebutuhan_event.*,(harga_satuan*jumlah) as sub_total_p, (harga_satuan_real*jumlah_real) as sub_total_r,mp.nama as nama_kepanitiaan")
                ->orderBy('kegiatan.kebutuhan_event.created_at','asc')
                ->limit($limit)
                ->offset($offset);
                
        $data_filtered = $data_Raw->when($searchs, function ($query) use ($searchs) {
                        if ($searchs) {
                            return $query->whereRaw("( mp.nama ilike '%".$searchs."%' or keterangan ilike '%".$searchs."%' )");
                        }
                    });
        $data_count_Raw = $data_Raw->count();
        $data_count_filtered = $data_filtered->count();
        $data_isi = $data_filtered->get();
        
        // return $data_isi;
        foreach ($data_isi as $key => $value) {
            $data_keb[$key]["id"] = ($value->id != null) ? $value->id : '';
            $data_keb[$key]["id_proker"] = ($value->id_proker != null) ? $value->id_proker : '';
            $data_keb[$key]["id_sie_kepanitiaan"] = ($value->id_sie_kepanitiaan != null) ? $value->id_sie_kepanitiaan : '';
            $data_keb[$key]["keterangan"] = ($value->keterangan != null) ? $value->keterangan : '';
            $data_keb[$key]["satuan"] = ($value->satuan != null) ? $value->satuan : '';
            $data_keb[$key]["harga_satuan"] = ($value->harga_satuan != null) ? $value->harga_satuan : '';
            $data_keb[$key]["jumlah"] = ($value->jumlah != null) ? $value->jumlah : '';
            $data_keb[$key]["satuan_real"] = ($value->satuan_real != null) ? $value->satuan_real : '';
            $data_keb[$key]["harga_satuan_real"] = ($value->harga_satuan_real != null) ? $value->harga_satuan_real : '';
            $data_keb[$key]["jumlah_real"] = ($value->jumlah_real != null) ? $value->jumlah_real : '';
            $data_keb[$key]["nota"] = ($value->nota != null) ? $value->nota : '';
            $data_keb[$key]["sub_total_p"] = ($value->sub_total_p != null) ? $value->sub_total_p : '';
            $data_keb[$key]["sub_total_r"] = ($value->sub_total_r != null) ? $value->sub_total_r : '';
            $data_keb[$key]["nama_kepanitiaan"] = ($value->nama_kepanitiaan != null) ? $value->nama_kepanitiaan : '';
            $data_keb[$key]["id_kabel"] = ($value->id_kabel != null) ? $value->id_kabel : '';
        }

        return $data_keb;
                
    }

    public function simpanKebutuhan(Request $request)
    {
        // return $request->all();

        $dana_turun = DeHelper::rupiah2number((isset($request->dana_turun))?$request->dana_turun:0);
        $dana_turun_real = DeHelper::rupiah2number((isset($request->dana_turun_real))?$request->dana_turun_real:0);
        $dana_mandiri = DeHelper::rupiah2number((isset($request->dana_mandiri))?$request->dana_mandiri:0);
        $dana_mandiri_real = DeHelper::rupiah2number((isset($request->dana_mandiri_real))?$request->dana_mandiri_real:0);
        $dana_terpakai = DeHelper::rupiah2number((isset($request->dana_terpakai))?$request->dana_terpakai:0);
        $dana_usulan = DeHelper::rupiah2number((isset($request->dana_usulan))?$request->dana_usulan:0);

        if (isset($request->dana_mandiri)) {
            $data_proker = [
                    'dana_usulan' => $dana_usulan,
                    // 'dana_terpakai' => $dana_terpakai,
                    'dana_turun' => $dana_turun,
                    'dana_mandiri' => $dana_mandiri,
                    // 'dana_mandiri_real' => $dana_mandiri_real,
                    // 'dana_turun_real' => $dana_turun_real,
                    'updated_at' => date("Y-m-d H:i:s")
            ];
        }else{
            $data_proker = [
                    // 'dana_usulan' => $dana_usulan,
                    'dana_terpakai' => $dana_terpakai,
                    // 'dana_turun' => $dana_turun,
                    // 'dana_mandiri' => $dana_mandiri,
                    'dana_mandiri_real' => $dana_mandiri_real,
                    'dana_turun_real' => $dana_turun_real,
                    'updated_at' => date("Y-m-d H:i:s")
            ];
        }
        // return $data_proker;

        $query = DB::table('kegiatan.proker')->where([
            ['id', '=', $request->id_proker]
        ])->update($data_proker);

        if(isset($request->deleted_id)){
            foreach ($request->deleted_id as $key => $value) {
                $kbt = kbt_proker::where('id','=',$request->deleted_id[$key]);
                $kbt->delete();
                $data[] = $request->deleted_id[$key];
            }
        }

        $key=0;
        foreach ($request->uraian as $key => $value) {

            $harga_satuan_p = (isset($request->harga_satuan_p[$key]))?$request->harga_satuan_p[$key]:null;
            $harga_satuan_r = (isset($request->harga_satuan_r[$key]))?$request->harga_satuan_r[$key]:null;
            $jumlah_p = (isset($request->jumlah_p[$key]))?$request->jumlah_p[$key]:null;
            $jumlah_r = (isset($request->jumlah_r[$key]))?$request->jumlah_r[$key]:null;
            $satuan_p = (isset($request->satuan_p[$key]))?$request->satuan_p[$key]:null;
            $satuan_r = (isset($request->satuan_r[$key]))?$request->satuan_r[$key]:null;
            $sie_panitia = (isset($request->sie_panitia[$key]))?$request->sie_panitia[$key]:null;
            $kabel_id = (isset($request->kabel_id[$key]))?$request->kabel_id[$key]:null;

            $harga_satuan_p = ( DeHelper::rupiah2number($harga_satuan_p) != "" ) ? DeHelper::rupiah2number($harga_satuan_p) : 0;
            $harga_satuan_r = ( DeHelper::rupiah2number($harga_satuan_r) != "" ) ? DeHelper::rupiah2number($harga_satuan_r) : 0;
            $jumlah_p = ($jumlah_p != '' || $jumlah_p != null) ? $jumlah_p : 0; 
            $jumlah_r = ($jumlah_r != '' || $jumlah_r != null) ? $jumlah_r : 0; 
            // return response()->json($jumlah_r);

            if ($kabel_id != null) {
                $data = [
                    'id_proker' => $request->id_proker,
                    'id_sie_kepanitiaan' => $sie_panitia,
                    'keterangan' => $request->uraian[$key],
                    'satuan' => $satuan_p,
                    'harga_satuan' => $harga_satuan_p,
                    'jumlah' => $jumlah_p,
                    'satuan_real' => $satuan_r,
                    'harga_satuan_real' => $harga_satuan_r,
                    'jumlah_real' => $jumlah_r,
                    'id_kabel' => $kabel_id,
                ];
            }else{
                $data = [
                    'satuan_real' => $satuan_r,
                    'harga_satuan_real' => $harga_satuan_r,
                    'jumlah_real' => $jumlah_r,
                ];
            }
            // return $data;
            kbt_proker::updateOrCreate(['id' => $request->id[$key]], $data);
        }


        // return response()->json($data);
        return response()->json("sukses lur");

    }

    public function pdfKebutuhan($id_proker, $jenis)
    {
        $data_proker = proker::find($id_proker);
        
        $t_debit = $data_proker->dana_mandiri + $data_proker->dana_turun;
        $t_debit_r = $data_proker->dana_mandiri_real + $data_proker->dana_turun_real;
        $selisih = $t_debit - $data_proker->dana_usulan;
        $selisih_r = $t_debit_r - $data_proker->dana_terpakai;

        $data_proker->t_debit = DeHelper::number2rupiah($t_debit);
        $data_proker->t_debit_r = DeHelper::number2rupiah($t_debit_r);
        $data_proker->selisih = DeHelper::number2rupiah($selisih);
        $data_proker->selisih_r = DeHelper::number2rupiah($selisih_r);
        
        $data_proker->dana_usulan = DeHelper::number2rupiah($data_proker->dana_usulan);
        $data_proker->dana_terpakai = DeHelper::number2rupiah($data_proker->dana_terpakai);
        $data_proker->dana_turun = DeHelper::number2rupiah($data_proker->dana_turun);
        $data_proker->dana_mandiri = DeHelper::number2rupiah($data_proker->dana_mandiri);
        $data_proker->dana_mandiri_real = DeHelper::number2rupiah($data_proker->dana_mandiri_real);
        $data_proker->dana_turun_real = DeHelper::number2rupiah($data_proker->dana_turun_real);
        // return $data_proker;
        $data['data_proker'] =$data_proker;

        $data_keb_raw = kbt_proker::where('id_proker','=',$id_proker)
                    ->leftjoin('master.m_sie_kepanitiaan as mp','mp.id','=','kegiatan.kebutuhan_event.id_sie_kepanitiaan')
                    ->selectRaw("kegiatan.kebutuhan_event.*,(harga_satuan*jumlah) as sub_total_p, (harga_satuan_real*jumlah_real) as sub_total_r,mp.nama as nama_kepanitiaan")
                    ->orderBy('id_sie_kepanitiaan','desc')
                    ->get();   
        $total_p = 0;
        $total_r = 0;
        foreach ($data_keb_raw as $key => $value) {

            $total_p += ($value->sub_total_p != null) ? $value->sub_total_p : 0;
            $total_r += ($value->sub_total_r != null) ? $value->sub_total_r : 0;            

            $data_keb[$key]["id"] = ($value->id != null) ? $value->id : '';
            $data_keb[$key]["id_proker"] = ($value->id_proker != null) ? $value->id_proker : '';
            $data_keb[$key]["id_sie_kepanitiaan"] = ($value->id_sie_kepanitiaan != null) ? $value->id_sie_kepanitiaan : '';
            $data_keb[$key]["keterangan"] = ($value->keterangan != null) ? $value->keterangan : '';
            $data_keb[$key]["satuan"] = ($value->satuan != null) ? $value->satuan : '';
            $data_keb[$key]["harga_satuan"] = DeHelper::number2rupiah(($value->harga_satuan != null) ? $value->harga_satuan : 0);
            $data_keb[$key]["jumlah"] = ($value->jumlah != null) ? $value->jumlah : 0;            
            $data_keb[$key]["satuan_real"] = ($value->satuan_real != null) ? $value->satuan_real : '';
            $data_keb[$key]["harga_satuan_real"] = DeHelper::number2rupiah(($value->harga_satuan_real != null) ? $value->harga_satuan_real : 0);
            $data_keb[$key]["jumlah_real"] = ($value->jumlah_real != null) ? $value->jumlah_real : 0;            
            $data_keb[$key]["nota"] = ($value->nota != null) ? $value->nota : '';
            $data_keb[$key]["sub_total_p"] = DeHelper::number2rupiah(($value->sub_total_p != null) ? $value->sub_total_p : 0);
            $data_keb[$key]["sub_total_r"] = DeHelper::number2rupiah(($value->sub_total_r != null) ? $value->sub_total_r : 0);
            $data_keb[$key]["nama_kepanitiaan"] = ($value->nama_kepanitiaan != null) ? $value->nama_kepanitiaan : '';
        }
        $data['total_p'] = DeHelper::number2rupiah($total_p);
        $data['total_r'] = DeHelper::number2rupiah($total_r);
        $data['data_keb'] =$data_keb;
        if ($jenis == 0) {
            $data['hidden2'] = 'style=display:none;';
            $data['colspan'] = 2;
        }else{
            $data['hidden2'] = '';
            $data['colspan'] = 3;
        }

        
        // return $data_keb;
        // return view('proker.detail.pdf_kebutuhan',$data);
        $data_proker = proker::find($id_proker);
        return PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true, 'isJavascriptEnabled' => true])->loadView('proker.detail.pdf_kebutuhan',$data)->download('data_kebutuhan_proker_'.$data_proker->nama.'.pdf');
        return $pdf->download('data_panitia_proker_'.$data_proker->nama.'.pdf');  
    }

    public function dataTable_panitia(Request $request, $id_proker)
    {
        $searchs = $request->searchs;
        $limit = $request->limit;
        $offset = $request->offset;
        $data=[];

        $data_Raw = Panitia::where('id_proker','=',$id_proker)
                ->leftjoin('master.m_sie_kepanitiaan as mp','mp.id','=','kegiatan.kepanitiaan.id_kepanitiaan')
                ->leftjoin('keanggotaan.anggota as ang','ang.nim','=','kegiatan.kepanitiaan.nim')
                ->leftjoin('master.m_kelas as mkls','mkls.id','=','ang.id_kelas')
                ->leftjoin('master.m_prodi as mprd','mprd.id','=','ang.id_prodi')
                ->selectRaw("kegiatan.kepanitiaan.*, mp.nama as nama_kepanitiaan, ang.nama as nama_mahasiswa, mkls.nama as nama_kelas, mprd.nama as nama_prodi")
                ->orderBy('id_kepanitiaan','asc')
                ->limit($limit)
                ->offset($offset);
                
        $data_filtered = $data_Raw->when($searchs, function ($query) use ($searchs) {
                        if ($searchs) {
                            return $query->whereRaw("( ang.nim ilike '%".$searchs."%'
                                                    or ang.nama ilike '%".$searchs."%'
                                                    or mkls.nama ilike '%".$searchs."%'
                                                    or mprd.nama ilike '%".$searchs."%' )");
                        }
                    });
        $data_count_Raw = $data_Raw->count();
        $data_count_filtered = $data_filtered->count();
        $data_isi = $data_filtered->get();

        foreach ($data_isi as $key => $value) {
            $data_panitia[$key]["id"] = ( $value->id != null ) ? $value->id : '';
            $data_panitia[$key]["id_proker"] = ( $value->id_proker != null ) ? $value->id_proker : '';
            $data_panitia[$key]["id_kepanitiaan"] = ( $value->id_kepanitiaan != null ) ? $value->id_kepanitiaan : '';
            $data_panitia[$key]["nim"] = ( $value->nim != null ) ? $value->nim : '';
            $data_panitia[$key]["created_at"] = ( $value->created_at != null ) ? $value->created_at : '';
            $data_panitia[$key]["updated_at"] = ( $value->updated_at != null ) ? $value->updated_at : '';
            $data_panitia[$key]["nama_kepanitiaan"] = ( $value->nama_kepanitiaan != null ) ? $value->nama_kepanitiaan : '';
            $data_panitia[$key]["nama_mahasiswa"] = ( $value->nama_mahasiswa != null ) ? $value->nama_mahasiswa : '';
            $data_panitia[$key]["nama_kelas"] = ( $value->nama_kelas != null ) ? $value->nama_kelas : '-';
            $data_panitia[$key]["nama_prodi"] = ( $value->nama_prodi != null ) ? $value->nama_prodi : '-';
            $data_panitia[$key]["nama_prodi_s"] = ( $value->nama_prodi != null ) ? DeHelper::singkatJurusan($value->nama_prodi) : '-';
        }

        return $data_panitia;
                
        // return $data_isi;
    }   
    
    public function simpanPanitia(Request $request)
    {
        // return $request->all();

        if(isset($request->deleted_id)){
            foreach ($request->deleted_id as $key => $value) {
                if ($request->deleted_id[$key] == "undefined" || $request->deleted_id[$key] == '') {
                    // return $request->deleted_id[$key];
                }else{
                    $pan = Panitia::where('id','=',$request->deleted_id[$key]);
                    $pan->delete();
                }
            }
        }
        $key=0;
        foreach ($request->pan_nim as $key => $value) {

            $data = [
                'id_proker' => $request->id_proker,
                'id_kepanitiaan' => $request->pan_sie_panitia[$key],
                'nim' => $request->pan_nim[$key]
            ];

            Panitia::updateOrCreate(['id' => $request->pan_id[$key]], $data);
        }


        // return response()->json($data);
        return response()->json("sukses lur");

    }    

    public function pdfPanitia(Request $request,$id_proker)
    {
        // return $request->all();
        
        $data_proker = proker::find($id_proker);
        
        $data_raw = Panitia::where('id_proker','=',$id_proker)
                ->leftjoin('master.m_sie_kepanitiaan as mp','mp.id','=','kegiatan.kepanitiaan.id_kepanitiaan')
                ->leftjoin('keanggotaan.anggota as ang','ang.nim','=','kegiatan.kepanitiaan.nim')
                ->leftjoin('master.m_kelas as mkls','mkls.id','=','ang.id_kelas')
                ->leftjoin('master.m_prodi as mprd','mprd.id','=','ang.id_prodi')
                ->selectRaw("kegiatan.kepanitiaan.*, mp.nama as nama_kepanitiaan, ang.nama as nama_mahasiswa, mkls.nama as nama_kelas, mprd.nama as nama_prodi")
                ->orderBy('id_kepanitiaan','asc')
                ->get();

        foreach ($data_raw as $key => $value) {
            $data_panitia[$key]["id"] = ( $value->id != null ) ? $value->id : '';
            $data_panitia[$key]["id_proker"] = ( $value->id_proker != null ) ? $value->id_proker : '';
            $data_panitia[$key]["id_kepanitiaan"] = ( $value->id_kepanitiaan != null ) ? $value->id_kepanitiaan : '';
            $data_panitia[$key]["nim"] = ( $value->nim != null ) ? $value->nim : '';
            $data_panitia[$key]["created_at"] = ( $value->created_at != null ) ? $value->created_at : '';
            $data_panitia[$key]["updated_at"] = ( $value->updated_at != null ) ? $value->updated_at : '';
            $data_panitia[$key]["nama_kepanitiaan"] = ( $value->nama_kepanitiaan != null ) ? $value->nama_kepanitiaan : '';
            $data_panitia[$key]["nama_mahasiswa"] = ( $value->nama_mahasiswa != null ) ? $value->nama_mahasiswa : '';
            $data_panitia[$key]["nama_kelas"] = ( $value->nama_kelas != null ) ? $value->nama_kelas : '-';
            $data_panitia[$key]["nama_prodi"] = ( $value->nama_prodi != null ) ? $value->nama_prodi : '-';
        }
        // return $data_panitia;

        $data['data_panitia'] = $data_panitia;
        $data['id_proker'] = $id_proker;

        // return view('proker.detail.pdf_pengurus',$data);
        // $pdf = PDF::loadView('proker.detail.pdf_pengurus', $data);
        // If you want to store the generated pdf to the server then you can use the store function
        // $pdf->save(storage_path().'_filename.pdf');
        return PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true, 'isJavascriptEnabled' => true])->loadView('proker.detail.pdf_pengurus', $data)->download('data_panitia_proker_'.$data_proker->nama.'.pdf');
        return $pdf->download('data_panitia_proker_'.$data_proker->nama.'.pdf');                
    }

    public function dataTable_rapat(Request $request)
    {
        $id_proker = $request->id_proker;
        $searchs = $request->searchs;
        $limit = $request->limit;
        $offset = $request->offset;
        $data=[];

        $rekap_absen = DB::table('kegiatan.absensi')
                            ->selectRaw("id_rapat, count(nim) as absen")
                            ->groupBy("id_rapat")
                            ->toSql();

        $data_Raw = rapat::where('id_proker','=',$id_proker)
                ->leftjoin('keanggotaan.anggota as ang','ang.nim','=','kegiatan.rapat_event.moderator')
                ->leftjoin(DB::raw('('.$rekap_absen.') as rkp_absen'),'rkp_absen.id_rapat','=','kegiatan.rapat_event.id')
                ->selectRaw("kegiatan.rapat_event.*, ang.nama as nama_mahasiswa, rkp_absen.absen as jumlah_peserta")
                ->orderBy('tanggal_rapat','asc')
                ->limit($limit)
                ->offset($offset);
        
        $data_filtered = $data_Raw->when($searchs, function ($query) use ($searchs) {
                        if ($searchs) {
                            return $query->whereRaw("( tanggal_rapat ilike '%".$searchs."%'
                                                    or moderator ilike '%".$searchs."%' )");
                        }
                    });

        $data_count_Raw = $data_Raw->count();
        $data_count_filtered = $data_filtered->count();
        $data_isi = $data_filtered->get();
        // return $data_isi;


        $no = 1;
        foreach ($data_isi as $value) {
            $id_rapat = DeHelper::encrypt_($value->id);
            $id_proker = (isset($value->id_proker)) ? $value->id_proker : NULL;
            $hasil_rapat=(isset($value->hasil_rapat)) ? $value->hasil_rapat : NULL;
            $moderator =(isset($value->moderator)) ? $value->moderator : NULL;
            $catatan =(isset($value->catatan)) ? $value->catatan : NULL;
            $tanggal_rapat =(isset($value->tanggal_rapat)) ? $value->tanggal_rapat : NULL;
            $tanggal_rapat_v =(isset($value->tanggal_rapat)) ? DeHelper::format_tanggal($value->tanggal_rapat) : NULL;
            $pokok_bahasan=(isset($value->pokok_bahasan)) ? $value->pokok_bahasan : '-';
            $jam=(isset($value->jam)) ? $value->jam : NULL;
            $tempat=(isset($value->tempat)) ? $value->tempat : NULL;
            $penanggungjawab=(isset($value->penanggungjawab)) ? $value->penanggungjawab : NULL;
            $tipe=(isset($value->tipe)) ? $value->tipe : NULL;
            $tipe_v=(isset($value->tipe)) ? ($value->tipe == 1)?'OFFLINE':'ONLINE' : NULL;
            $nama_mahasiswa=(isset($value->nama_mahasiswa)) ? $value->nama_mahasiswa : NULL;
            $jumlah_peserta=(isset($value->jumlah_peserta)) ? $value->jumlah_peserta : 0;
            
            
            // $link_hapus = url('surat_keterangan/hapus_penghargaan/'.$id_riwayat);
            $cek = '<a href="javascript:void(0)" type="button" class="btn btn-xs btn-secondary btnDetail"
                data-id_rapat="'.$id_rapat.'"
                data-id_proker="'.$id_proker.'"
                data-hasil_rapat="'.$hasil_rapat.'"
                data-moderator="'.$moderator.'"
                data-catatan="'.$catatan.'"
                data-tanggal_rapat="'.$tanggal_rapat.'"
                data-pokok_bahasan="'.$pokok_bahasan.'"
                data-jam="'.$jam.'"
                data-tempat="'.$tempat.'"
                data-penanggungjawab="'.$penanggungjawab.'"
                data-tipe="'.$tipe.'"
                data-nama_mahasiswa="'.$nama_mahasiswa.'"
                data-jumlah_peserta="'.$jumlah_peserta.'">
            <i class="icon fa-eye"></i> Detail </a>';
            $edit = '<a type="button" href="javascript:void(0)" class="btn btn-xs btn-primary bg-purple-600 btnEdit"
                data-id_rapat="'.$id_rapat.'"
                data-id_proker="'.$id_proker.'"
                data-hasil_rapat="'.$hasil_rapat.'"
                data-moderator="'.$moderator.'"
                data-catatan="'.$catatan.'"
                data-tanggal_rapat="'.$tanggal_rapat.'"
                data-pokok_bahasan="'.$pokok_bahasan.'"
                data-jam="'.$jam.'"
                data-tempat="'.$tempat.'"
                data-penanggungjawab="'.$penanggungjawab.'"
                data-tipe="'.$tipe.'"
                data-nama_mahasiswa="'.$nama_mahasiswa.'"
                data-jumlah_peserta="'.$jumlah_peserta.'">
            <i class="icon fa-pencil"></i> Edit </a>';
            $hapus = '<a type="button"  data-id="'.$id_rapat.'"  class="btn btn-xs btn-danger bg-pink-600 btnDelete" href="javascript:void(0)"><i class="icon fa-trash"></i> Delete </a>';

            $aksi=$cek.' '.$edit.' '.$hapus;

            $jam_new = DeHelper::time2jam($jam);

            $dataTabel[] = array(
                'no' => $no++,
                'id_rapat' => $id_rapat,
                'id_proker' => $id_proker,
                'hasil_rapat' => $hasil_rapat,
                'moderator' => $moderator,
                'catatan' => $catatan,
                'jam_tanggal' => $jam_new.'<br>'.DeHelper::format_tanggal($tanggal_rapat),
                'pokok_bahasan' => $pokok_bahasan,
                'jam' => $jam,
                'tempat' => $tempat,
                'penanggungjawab' => $penanggungjawab,
                'tipe' => $tipe_v,
                'nama_mahasiswa' => $nama_mahasiswa,
                'jumlah_peserta' => $jumlah_peserta,
                'aksi'=>$aksi,
                'absensi'=> '<button class="btn btn-xs btn-round w-100 btn-outline-success btnAddAbsensi" data-tipe="'.$tipe.'" data-id_rapat="'.$id_rapat.'">+ absensi</button> <button class="btn btn-xs btn-round w-100 btn-outline-info btnShowAbsensi" data-tipe="'.$tipe.'" data-id_rapat="'.$id_rapat.'">cek absensi</button>'
            );
        }
        // return $dataTabel;

        $recordsTotal = is_null($data_count_Raw) ? 0 : $data_count_Raw;
        $recordsFiltered = is_null($data_count_filtered) ? 0 : $data_count_filtered;

        $data_ = (isset($dataTabel)?$dataTabel:[]);

        $data['recordsTotal'] = $recordsTotal;
        $data['recordsFiltered'] = $recordsFiltered;
        $data['data'] = $data_;

        return $data;        
    }   
    
    public function simpanRapat(Request $request)
    {
        // return $request->all();
        $id = ($request->id_rapat != null) ? DeHelper::decrypt_($request->id_rapat) : null;
        // return $id;
        if($id == null){
            $out = 1;
        }else{
            $out = 0;
        }
        $key=0;

        $data = [
            'id_proker' => $request->id_proker,
            'pokok_bahasan' => $request->pokok_bahasan,
            'moderator' => $request->moderator,
            'catatan' => $request->catatan,
            'hasil_rapat' => $request->hasil_rapat,
            'jam' => $request->jam,
            'tanggal_rapat' => $request->tanggal_rapat,
            'tempat' => $request->tempat,
            'tipe' => $request->tipe,
        ];
        // return response()->json($id);
        
        // return DeHelper::decrypt_($request->id);

        rapat::updateOrCreate(['id' => $id], $data);
        
        // return response()->json($data);
        return response()->json($out);
    }    

    public function delete_rapat($id_rapat)
    {
        $rapat = rapat::where('id','=',DeHelper::decrypt_($id_rapat));
        $rapat->delete();
        return response()->json('berhasil menghapus data');
    }
}
