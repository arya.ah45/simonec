<head>
<style type="text/css">
    * {
        margin:0;
        padding:0;

        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
    }


    body {
        background:#ddd;
    }

    .page {
        position:relative;
        width:21cm;
        min-height:29.7cm;
        page-break-after: always;
        margin:0.5cm auto;
        background:#FFF;
        padding:1.5cm;
        box-shadow:0 2px 10px rgba(0,0,0,0.3);
        -webkit-box-sizing: none;
        -moz-box-sizing: none;
        box-sizing: none;

        page-break-after: always;
    }
    .page-landscape {
        position:relative;
        width:29.7cm;
        min-height:19cm;
        page-break-after: always;
        margin:1.5cm;
        background:#FFF;
        padding:1.5cm;
        box-shadow:0 2px 10px rgba(0,0,0,0.3);
        -webkit-box-sizing: none;
        -moz-box-sizing: none;
        box-sizing: none;

        page-break-after: always;
    }
    .footer {
        position:absolute;
        bottom:1.5cm;
        left:1.5cm;
        right:1.5cm;
        width:auto;
        height:30px;
    }
    .kanan {
        float:right;
    }
    .page *, .page-landscape * {
        font-family:arial;
        font-size:11px;
    }
    .it-grid {
        background:#FFF;
        border-collapse:collapse;
        border:1px solid #000;
    }
    .seri {
        font-family:'Lucida Handwriting';
    }
    .it-grid th {
        color:#000;
        border:1px solid #000;
        border-top:1px solid #000;
        background:#C4BC96;
    }
    .it-grid tr:nth-child(even) { background:#f8f8f8; }
    .it-grid td, .it-grid th {
        padding:3px;
        border:1px solid #000;
    }
    .it-cetak td {
        padding:5px 5px;
    }
    h1, h2, h3, h4, h5, h6 {
        font-weight:normal;
    }

    table {
        border-collapse:collapse;
    }

    td{
        padding:1px;
    }

    .f14 {
        font-size:14pt;
    }
    .f12 {
        font-size:12pt;
    }
    .line-bottom{
        border-bottom:1px solid black;
    }
    .detail {
        margin-top:10px;
        margin-bottom:10px;
    }
    .detail td{
        padding:5px;
        font-size:12px;
    }
    .detail span{
        border-bottom:1px solid black;
        display:inline-block;
        font-size:12px;
    }

    .cetakan{
        font-size:14px;
        line-height:1.5em;
    }
    .cetakan *{
        font-size:14px;
        line-height:1.5em;
    }
    .cetakan span {
        border-bottom:1px solid black;
        display:inline-block;
    }
    .full {
        width:100%;
    }
    nip {
        display:inline-block;
        width:130px;
    }
    a {
        text-decoration:none;
        color:#006600;
    }
    ol {
        margin-left:30px;
    }

    ol > li {
        padding:10px;
    }
    table { page-break-inside:auto }
    tr    { page-break-inside:avoid; page-break-after:auto }
    thead { display:table-header-group }
    tfoot { display:table-footer-group }


    @media print {
        body {
            background:#ddd;
        }

        .page {
            height:10cm;
            padding:0.7cm;
            box-shadow:none;
            margin:0;
        }
        @page {
            size: A4;
            margin: 0;
            -webkit-print-color-adjust: exact;
        }

        .page-landscape {
            height:5cm;
            padding:0.5cm;
            box-shadow:none;
            margin:0;
        }

        .footer {
            bottom:0.7cm;
            left:0.7cm;
            right:0.7cm;
        }
        thead {
            display: table-header-group;
        }
    }
    </style>
</head>

<body >
    <div class="page">
        <center>
            <table align="center">
                        <tbody>
                            <tr>
                                <td style="padding:8px;">
                                    <table style="width:8.5cm;border:1px solid black; background-image: url({{asset('assets')}}/images/kartu.png); background-repeat: no-repeat;" class="kartu">
                                        <tbody>
                                            <tr>
                                                <td colspan="4" style="border-bottom:1px solid black">
                                                    <table width="100%" class="kartu">
                                                        <tbody>
                                                            <tr>
                                                                <td style="padding: 7px"><img src="{{asset('assets/images/logo.png')}}" height="40"></td>
                                                                <td colspan="2" align="center" style="font-weight:bold">
                                                                    KARTU ANGGOTA<br> ENGLISH CLUB POLINEMA PSDKU KEDIRI
                                                                </td>
                                                                <td style="padding: 7px"><img src="{{asset('assets/images/LOGO-simonec.png')}}" height="40"></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td rowspan="5" style="padding: 7px 5px 0px 5px;" width="200px">
                                                    <center>
                                                        <div style="">
                                                            <img src="{{asset('foto_anggota').'/'.$data->foto}}" 
                                                            style="
                                                            height:90px;width:60px;
                                                            object-fit: cover; /* Do not scale the image */
                                                            object-position: center; /* Center the image within the element */
                                                            ">
                                                        </div>
                                                    </center>
                                                </td>
                                                <td style="padding: 5px 5px 0px 5px" width="160px">NIM</td>
                                                <td style="font-size:12px;" width="350px">: <b>{{$data->nim}}</b></td>
                                                <td></td>
                                            </tr>

                                            <tr>
                                                <td style="padding: 5px 5px 0px 5px">Nama Lengkap</td>
                                                <td style="font-size:12px;">: <b>{{$data->nama}}</b></td>
                                                <td></td>
                                            </tr>

                                            <tr>
                                                <td style="padding: 5px 5px 0px 5px" >Prodi</td>
                                                <td>: {{$data->nama_prodi}}</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td style="padding: 5px 5px 0px 5px" >Kelas</td>
                                                <td>: {{$data->nama_kelas}}</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td style="padding: 5px 5px 0px 5px" >Jabatan</td>
                                                <td>: {{$data->nama_jabatan}}</td>
                                                <td></td>
                                            </tr>

                                            <br>

                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td colspan="2">
                                                    &nbsp;
                                                </td>                                                
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td colspan="2">
                                                    <font size="1">TTD,
                                                    </font>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td><img src="{{$link_qr}}" height="45" style="margin-top:5px;"></td>
                                                <td></td>
                                            </tr>

                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td>
                                                    <font size="1">{{$data->nama}}
                                                    </font>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td style="font-size: 8px; color:red">            
                                                </td>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                        </tbody>
                    </table>
                </td>
                    </tr>
                    </tbody>
                    </table>
                    </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                </tbody>
                </table>
        </center>
    </div>
</body>
