@extends('layouts.app')

@php
$urlEdit = url()->current();
@endphp
@section('content')
<div class="page-content container-fluid">
    <div class="row" data-plugin="matchHeight" data-by-row="true">
        <div class="col-xxl-12 col-lg-12">
          <div class="panel shadow panel-bordered">
                  <div class="panel-heading">
                      <h3 class="panel-title">Master Kriteria Penilaian</h3>
                      <div class="panel-actions">
                          <a class="panel-action icon wb-minus" aria-expanded="true" data-toggle="panel-collapse"
                          aria-hidden="true"></a>
                          <a class="panel-action icon wb-expand" data-toggle="panel-fullscreen" aria-hidden="true"></a>
                      </div>
                  </div>
                <div class="panel-body pb-25 pt-15 ">
                      <div class="row pt-3">
                          <div class="col-xxl-12 col-lg-12">
                            <a href="" class="btn mb-10 btn-xs btn-success"><i class="fa fa-reply" aria-hidden="true"></i> Kembali</a>
                            <button id="showKriteria_Penilaian" class="btn btn-xs mb-10 btn-outline bg-teal-700 grey-100 "  data-toggle="modal"
                            data-target="#">
                                <i class="icon wb-plus" aria-hidden="true"></i> Tambah Data
                            </button>
                            <table class="table table-bordered table-hover table-striped" cellspacing="0" id="tbKriteria_Penilaian">
                                <thead class="text-center">
                                    <tr>
                                        <th data-priority="1" width="5%" ><b>No.</b></th>
                                        <th data-priority="2" width="10%" ><b>User</b></th>
                                        <th data-priority="3" width="10%" ><b>Jenis</b></th>
                                        <th data-priority="4" width="55%" ><b>Kriteria Penilaian</b></th>
                                        <th width="20%" ><b>AKSI</b></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


  <!-- Modal -->
<div class="modal h-p80 fade modal-slide-from-bottom" id="ModalKriteria_Penilaian" aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-simple modal-center">
        <div class="modal-content">

            <div class="modal-header">
                <div class="ribbon ribbon-clip px-10 w-300 h-25" style="z-index:100">
                    <span class="ribbon-inner">
                        <h4 id="JudulModal" class="modal-title text-white"></h4>
                    </span>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                {{-- <h4 id="JudulModal" class="modal-title"></h4> --}}
            </div>

            <div class="modal-body" data-keyboard="false" data-backdrop="static" style="overflow-y: auto;">
                <form id="formKriteria_Penilaian" name="formKriteria_Penilaian">
                    <input type="text" class="form-control empty" hidden id="id" name="id" placeholder="Islam">
                    @csrf
                    <div class="form-group form-material floating row mb-0 px-15" data-plugin="formMaterial">
                        <div class="col-md-12">
                            <label class="form-control-label mb-0" for="nama">Kriteria Penilaian</label>
                            <textarea rows="2" class="form-control empty" id="nama" name="nama" placeholder="contoh : apakah cover Proposal telah sesuai?">
                            </textarea>
                            <div class="invalid-feedback">*wajib diisi</div>
                        </div>
                    </div>

                    <div class="form-group form-material floating row mt-15 mb-5 px-15" data-plugin="formMaterial">
                        <div class="col-md-12">
                            <label class="form-control-label mb-0" for="nama">Jenis Kriteria</label>
                            <select class="form-control-plaintext" id="jenis" name="jenis">
                                <option value="99" selected disabled>Pilih Kriteria</option>
                                <option value="0">Proposal</option>
                                <option value="1">LPJ</option>
                            </select>
                            <div class="red-700 warn_select2 font-size-10" hidden> *wajib diisi, tidak boleh kosong</div> 
                        </div>
                    </div>

                    <div class="form-group form-material floating row mt-15 mb-5 px-15" data-plugin="formMaterial">
                        <div class="col-md-12">
                            <label class="form-control-label mb-0" for="for_user">User</label>
                            <select class="form-control-plaintext" id="for_user" name="for_user">
                                <option value="99" selected disabled>Pilih User</option>
                                <option value="a">DPK</option>
                                <option value="b">Ketua UKM</option>
                            </select>
                            <div class="red-700 warn_select2 font-size-10" hidden> *wajib diisi, tidak boleh kosong</div> 
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="tutup">Tutup</button>
                <button type="button" id="btnSave" class="btn btn-primary">Simpan</button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
    var cek = true;

    $(document).ready(function () {
        function vld_text(id_input,col){
            if ($(id_input).val()=='') {
                $(id_input).addClass('is-invalid').closest('.col-md-'+col).find('.form-control-label').addClass('text-danger');
                $(id_input).closest('.form-material').addClass('has-danger');
                $(id_input).focus(function () {
                    $(this).removeClass('is-invalid').closest('.form-material').removeClass('has-danger');
                });
                $(id_input).keyup(function () {
                    $(this).closest('.col-md-'+col).find('.form-control-label').removeClass('text-danger');
                });
                cek = false;
            }else{
                cek = true;
            }
        }

        function vld_select2(id_input,col){
            if ($(id_input).val()==null) {
                $(id_input).addClass('is-invalid').closest('.col-md-'+col).find('.form-control-label').addClass('text-danger');
                $(id_input).closest('.form-material').addClass('has-danger').find('.red-700').attr('hidden',false);                 

                $(document).on('change',id_input, function () {
                    $(id_input).removeClass('is-invalid').closest('.col-md-'+col).find('.form-control-label').removeClass('text-danger');
                    $(this).removeClass('is-invalid').closest('.form-material').removeClass('has-danger').find('.red-700').attr('hidden',true);
                });
                cek = false;
            }else{
                cek = true;                    
            }
        }

        // data table server side configurasi
        var table = $('#tbKriteria_Penilaian').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            "scrollY": "250px",
            "scrollx": "1050px",
            "scrollCollapse": true,                                
            ajax: "{{ route('showKriter') }}",
            language: {
                            processing: "Sedang diproses...<img height=50 src='{{asset('assets/images/load3.gif')}}'>"
                        },
            columns: [
                {
                    data: 'no',
                    name: 'no',
                    searchable : false,
                    orderable:true
                }, {
                    data: 'for_user',
                    name: 'for_user',
                    orderable:true
                }, {
                    data: 'jenis',
                    name: 'jenis',
                    orderable:true
                }, {
                    data: 'nama',
                    name: 'nama',
                    orderable:true
                },{
                    data: 'aksi',
                    name: 'aksi',
                    orderable: false,
                    searchable: false
                },
            ],
            columnDefs: [
                { className: 'text-center', targets: [0,1,3,4] },
            ]
        });

        //  get url terkini untuk hapus data
        function url_wilayah() {
            // Get Option Wilayah
            var url_current = '{{url()->current()}}';
            var start = url_current.indexOf("/kriteria_penilaian_proposal");
            var url_true = url_current.substring(0,start)+"/kriteria_penilaian_proposal/delete/";
            // console.log(url_true);
            return url_true;
        }

        // aksi ketika save data dan update
        $('#btnSave').click(function (e) {
            e.preventDefault();
            cek = true;

            vld_text('#nama',12);
            vld_select2('#jenis',12);
            vld_select2('#for_user',12);
            // console.log(cek);

            if (cek == true) {
                $(this).html('Sending..');
                $.ajax({
                    data: $('#formKriteria_Penilaian').serialize(),
                    url: "{{route('TamKriter')}}",
                    type: "POST",
                    dataType: 'json',
                    success: function (data) {
                        $('#formKriteria_Penilaian').trigger('reset');
                        $('#btnSave').html('Simpan');
                        $('#ModalKriteria_Penilaian').modal('hide');
                        swal({
                            title: "Sukses!",
                            text: data,
                            type: "success",
                            showCancelButton: false,
                            confirmButtonClass: "btn-success",
                            confirmButtonText: 'OK',
                            closeOnConfirm: false
                        });                          
                        table.ajax.reload();
                    },
                    error: function (data) {
                        console.log('Error:', data);
                        $('#btnSave').html('Simpan');
                        toastr.error('Kelola Data Gagal');

                    }
                })
            }
        });


        // aksi button tambah di klik
        $(document).on('click', '#showKriteria_Penilaian', function () {
            $('#jenis').select2();
            $('#for_user').select2();
            $('#JudulModal').html('Tambah Data Kriteria Penilaian');
            $('#id').val('');
            $('#nama').val('');

                $('#ModalKriteria_Penilaian').modal({
                    backdrop: 'static',
                    keyboard: false,
                    show: true
                })
        });

        $(document).on('click', '.editData', function () {
            // console.log('sjfhdav');
            $('#jenis').select2();
            $('#for_user').select2();
            $('#JudulModal').html('Edit data Kriteria Penilaian');
            $('#nama').val($(this).data('nama'));
            $('#id').val($(this).data('id'));
            $('#jenis').val($(this).data('is_lpj')).trigger('change');
            $('#for_user').val($(this).data('for_user')).trigger('change');
            $('#ModalKriteria_Penilaian').modal({
                backdrop: 'static',
                keyboard: false, // to prevent closing with Esc button (if you want this too)
                show: true
            })
        });

        $(document).on('click', '.deleteData', function () {
            // console.log('deleted');
            var url = url_wilayah()+$(this).data('id');
            // console.log(url);
            swal({
            title: "Apakah anda yakin menghapus data ini?",
            text: "Peringatan!! Data yang telah dihapus tidak bisa dikembalikan.",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-warning",
            confirmButtonText: 'Iya',
            cancelButtonText: "Tidak",
            closeOnConfirm: true,
            closeOnCancel: true
            }, function (isConfirm) {
                if (isConfirm) {
                    cek='update';
                    // console.log(cek);
                        $.ajax({
                            url: url,
                            type: "GET",
                            dataType: 'json',
                            success: function (data) {
                                // console.log(data);
                                swal({
                                    title: "Sukses!",
                                    text: data,
                                    type: "success",
                                    showCancelButton: false,
                                    confirmButtonClass: "btn-success",
                                    confirmButtonText: 'OK',
                                    closeOnConfirm: false
                                });                                   
                                table.ajax.reload();
                                },
                            error: function (data) {
                                console.log('Error:', data);
                                toastr.error(data);
                                //$('#modalOrg').modal('show');
                                }
                        });
                }else {
                }
            });
        });

        $("#ModalKriteria_Penilaian").on("hidden.bs.modal", function () {
            $('.form-control').removeAttr('disabled', true).removeClass('is-invalid');
            $('#formKriteria_Penilaian').trigger('reset');
            $('.form-control-label').removeClass('text-danger')
            $('.form-group').removeClass('has-danger')
            $('.warn_select2').attr('hidden',true);  
        });
    });
</script>
@endpush