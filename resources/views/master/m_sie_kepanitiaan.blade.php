@extends('layouts.app')

@php
$urlEdit = url()->current();
@endphp
@section('content')
<div class="page-content container-fluid">
    <div class="row" data-plugin="matchHeight" data-by-row="true">
        <div class="col-xxl-12 col-lg-12">
          <div class="panel shadow panel-bordered">
                  <div class="panel-heading">
                      <h3 class="panel-title">Master Kepanitiaan</h3>
                      <div class="panel-actions">
                          <a class="panel-action icon wb-minus" aria-expanded="true" data-toggle="panel-collapse"
                          aria-hidden="true"></a>
                          <a class="panel-action icon wb-expand" data-toggle="panel-fullscreen" aria-hidden="true"></a>
                      </div>
                  </div>
                  <div class="panel-body pb-25 pt-15 ">
                      <div class="row pt-3">
                          <div class="col-xxl-12 col-lg-12">
                            <a href="" class="btn mb-10 btn-xs btn-success"><i class="fa fa-reply" aria-hidden="true"></i> Kembali</a>
                            <button id="showKepanitiaan" class="btn btn-xs mb-10 btn-outline bg-teal-700 grey-100 "  data-toggle="modal"
                            data-target="#">
                                <i class="icon wb-plus" aria-hidden="true"></i> Tambah Data
                            </button>
                            <table class="table table-bordered table-hover table-striped" cellspacing="0" id="tbKepanitiaan">
                                <thead class="text-center">
                                    <tr>
                                        <th width="10%" ><b>No.</b></th>
                                        <th width="45%" ><b>Nama Kepanitiaan</b></th>
                                        <th width="30%" ><b>AKSI</b></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


  <!-- Modal -->
<div class="modal h-p80 fade modal-slide-from-bottom" id="ModalKepanitiaan" aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-simple modal-center">
        <div class="modal-content">

            <div class="modal-header">
                <div class="ribbon ribbon-clip px-10 w-300 h-25" style="z-index:100">
                    <span class="ribbon-inner">
                        <h4 id="JudulModal" class="modal-title text-white"></h4>
                    </span>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                {{-- <h4 id="JudulModal" class="modal-title"></h4> --}}
            </div>

            <div class="modal-body pb-0" data-keyboard="false" data-backdrop="static" style="overflow-y: auto;">
                <form id="formKepanitiaan" name="formKepanitiaan">
                    @csrf
                    <div class="form-group form-material floating row pt-10" data-plugin="formMaterial">
                        <input type="text" class="form-control empty" hidden id="id" name="id">

                        <div class="col-md-12">
                            <label class="form-control-label mb-0" for="nama">Nama Kepanitiaan</label>
                            <input type="text" class="form-control empty" id="nama" name="nama" placeholder="Contoh : Ketua Panitia ">
                            <div class="invalid-feedback">*wajib diisi</div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-pure" data-dismiss="modal" id="tutup"Tutup</button>
                <button type="button" id="btnSave" class="btn btn-primary">Simpan</button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
    var cek = true;
        
    $(document).ready(function () {
        function vld_text(id_input,col){
            if ($(id_input).val()=='') {
                $(id_input).addClass('is-invalid').closest('.col-md-'+col).find('.form-control-label').addClass('text-danger');
                $(id_input).closest('.form-material').addClass('has-danger');
                $(id_input).focus(function () {
                    $(this).removeClass('is-invalid').closest('.form-material').removeClass('has-danger');
                });
                $(id_input).keyup(function () {
                    $(this).closest('.col-md-'+col).find('.form-control-label').removeClass('text-danger');
                });
                cek = false;
                // console.log(cek);
            }else{
                cek = true;
            }
        }
                
        // data table server side configurasi
        var table = $('#tbKepanitiaan').DataTable({
            processing: true,
            serverSide: true,
            "scrollY": "250px",
            "scrollx": "1050px",
            "scrollCollapse": true,
            ajax: "{{ route('showKepan') }}",
            language: {
                            processing: "Sedang diproses...<img height=50 src='{{asset('assets/images/load3.gif')}}'>"
                        },
            columns: [
                {
                    data: 'no',
                    name: 'no',
                    searchable : false,
                    orderable:true
                }, {
                    data: 'nama',
                    name: 'nama',
                    orderable:true
                },{
                    data: 'aksi',
                    name: 'aksi',
                    orderable: false,
                    searchable: false
                },
            ],
            columnDefs: [
                { className: 'text-center', targets: [0,1,2] },
            ]
        });

        // aksi button tambah di klik
        $(document).on('click', '#showKepanitiaan', function () {
            $('#JudulModal').html('Tambah Data Kepanitiaan');
            $('#id').val('');
            $('#nama').val('');

                $('#ModalKepanitiaan').modal({
                    backdrop: 'static',
                    keyboard: false,
                    show: true
                })
        });

        //  get url terkini untuk hapus data
        function url_wilayah() {
            // Get Option Wilayah
            var url_current = '{{url()->current()}}';
            var start = url_current.indexOf("/kepanitiaan");
            var url_true = url_current.substring(0,start)+"/kepanitiaan/delete/";
            // console.log(url_true);
            return url_true;
        }

        // aksi ketika save data dan update
        $('#btnSave').click(function (e) {
            e.preventDefault();
            
            vld_text('#nama',12);
            console.log(cek);
            if (cek == true) {
                $(this).html('Sending..');
                $.ajax({
                    data: $('#formKepanitiaan').serialize(),
                    url: "{{route('TamKepan')}}",
                    type: "POST",
                    dataType: 'json',
                    success: function (data) {
                        $('#formKepanitiaan').trigger('reset');
                        $('#btnSave').html('Simpan');
                        $('#ModalKepanitiaan').modal('hide');
                        swal({
                            title: "Sukses!",
                            text: data,
                            type: "success",
                            showCancelButton: false,
                            confirmButtonClass: "btn-success",
                            confirmButtonText: 'OK',
                            closeOnConfirm: false
                        });                  
                        table.ajax.reload();
                    },
                    error: function (data) {
                        console.log('Error:', data);
                        $('#btnSave').html('Simpan');
                        toastr.error('Kelola Data Gagal');
                    }
                })
            }
            
        });


        $(document).on('click', '.editData', function () {
            // console.log('sjfhdav');
            $('#JudulModal').html('Edit data Kepanitiaan');
            $('#nama').val($(this).data('nama'));
            $('#id').val($(this).data('id'));

            $('#ModalKepanitiaan').modal({
                backdrop: 'static',
                keyboard: false, // to prevent closing with Esc button (if you want this too)
                show: true
            })
        });

        $(document).on('click', '.deleteData', function () {
            console.log('deleted');
            var url = url_wilayah()+$(this).data('id');
            console.log(url);
            swal({
            title: "Peringatan!!",
            text: "Apakah anda yakin menghapus data ini?",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-warning",
            confirmButtonText: 'Iya',
            cancelButtonText: "Tidak",
            closeOnConfirm: true,
            closeOnCancel: true
            }, function (isConfirm) {
                if (isConfirm) {
                    cek='update';
                    console.log(cek);

                        $.ajax({
                            url: url,
                            type: "GET",
                            dataType: 'json',
                            success: function (data) {
                                console.log(data);
                                swal({
                                    title: "Sukses!",
                                    text: data,
                                    type: "success",
                                    showCancelButton: false,
                                    confirmButtonClass: "btn-success",
                                    confirmButtonText: 'OK',
                                    closeOnConfirm: false
                                });    
                                table.ajax.reload();
                                },
                            error: function (data) {
                                console.log('Error:', data);
                                toastr.error(data);
                                //$('#modalOrg').modal('show');
                                }
                        });
                }else {
                }
            });
        });

        $("#ModalKepanitiaan").on("hidden.bs.modal", function () {
            $('.form-control').removeAttr('disabled', true).removeClass('is-invalid');
            $('#formKriteria_Penilaian').trigger('reset');
            $('.form-control-label').removeClass('text-danger')
            $('.form-group').removeClass('has-danger')
            $('.warn_select2').attr('hidden',true);  
        });
    });
</script>
@endpush
