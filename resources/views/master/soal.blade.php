@extends('layouts.app')

@section('content')
<div class="page-content container-fluid">
    <div class="row" data-plugin="matchHeight" data-by-row="true">
        <div class="col-xxl-12 col-lg-12">
          <div class="panel shadow panel-bordered">
                  <div class="panel-heading">
                      <h3 class="panel-title">Master Soal</h3>
                      <div class="panel-actions">
                          <a class="panel-action icon wb-minus" aria-expanded="true" data-toggle="panel-collapse"
                          aria-hidden="true"></a>
                          <a class="panel-action icon wb-expand" data-toggle="panel-fullscreen" aria-hidden="true"></a>
                      </div>
                  </div>
                  <div class="panel-body pb-0 pt-15 ">
                      <div class="row pt-3">
                          <div class="col-xxl-12 col-lg-12">
                            <a href="" class="btn mb-10 btn-xs btn-success"><i class="fa fa-reply" aria-hidden="true"></i> Kembali</a>
                            <button id="showSoal" class="btn btn-xs mb-10 btn-outline bg-teal-700 grey-100">
                                <i class="icon wb-plus" aria-hidden="true"></i> Tambah Data
                            </button>
                            <table class="table table-bordered table-hover table-striped" cellspacing="0" id="tbSoal">
                                <thead class="text-center">
                                    <tr>
                                        <th width="10%" ><b>No.</b></th>
                                        <th width="45%" ><b>Nama Soal</b></th>
                                        <th width="30%" ><b>AKSI</b></th>
                                    </tr>
                                </thead>
                                </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


  <!-- Modal -->
  <div class="modal h-p80 fade modal-slide-from-bottom" id="ModalSoal" aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-simple">
        <div class="modal-content">

            <div class="modal-header">
                <div class="ribbon ribbon-clip px-10 w-300 h-25" style="z-index:100">
                    <span class="ribbon-inner">
                        <h4 id="judulModal" class="modal-title text-white"></h4>
                    </span>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                {{-- <h4 id="judulModal" class="modal-title"></h4> --}}
            </div>

            <div class="modal-body" data-keyboard="false" data-backdrop="static" style="height: 200px;overflow-y: auto;">
                <form id="formSoal" name="formSoal">
                    @csrf

                    <input type="text" class="form-control empty" hidden id="id" name="id" placeholder="">
                    <div class="form-group form-material floating row" data-plugin="formMaterial">
                        <div class="col-md-12">
                        <textarea data-provide="markdown" id="nama" name="nama" data-iconlibrary="fa" data-width="550"></textarea>

                        {{-- <div data-plugin="summernote" name="nama" id="nama" data-plugin-options='{"toolbar":[["style", ["bold", "italic", "underline", "clear"]],["color", ["color"]],["para", ["ul", "ol", "paragraph"]]]}'> --}}
                        {{-- <textarea class="form-control" id="nama" name="nama" ></textarea> --}}

                        {{-- </div> --}}
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-pure" data-dismiss="modal" id="tutup"Tutup</button>
                <button type="button" id="btnSave" class="btn btn-primary">Simpan</button>
            </div>
        </div>
    </div>

</div>
<script>

    $(document).ready(function () {


    (function(document, window, $){
        'use strict';

        var Site = window.Site;
        $(document).ready(function(){
          Site.run();
        });
      })(document, window, jQuery);
        // data table server side configurasi
        var table = $('#tbSoal').DataTable({
        processing: true,
        serverSide: true,
        "scrollY": "250px",
        "scrollx": "1050px",
        "scrollCollapse": true,
        ajax: "{{ route('showSoal') }}",
        language: {
                        processing: "Sedang diproses...<img height=50 src='{{asset('assets/images/load3.gif')}}'>"
                    },
        columns: [
            {
                data: 'no',
                name: 'no',
                searchable : false,
                orderable:true
            }, {
                data: 'nama',
                name: 'nama',
                orderable:true
            },{
                data: 'aksi',
                name: 'aksi',
                orderable: false,
                searchable: false
            },
        ],
        columnDefs: [
            { className: 'text-center', targets: [0,1,2] },
        ]
    });

        // aksi button tambah di klik
        $(document).on('click', '#showSoal', function () {
            $('#judulModal').html('Tambah Data Soal');
            $('#id').val('');
            $('#nama').val('');
            // $('#id_prodi').val('');

                $('#ModalSoal').modal({
                    backdrop: 'static',
                    keyboard: false,
                    show: true
                })
        });

    //  get url terkini untuk hapus data
        function url_wilayah() {
            // Get Option Wilayah
            var url_current = '{{url()->current()}}';
            var start = url_current.indexOf("/soal");
            var url_true = url_current.substring(0,start)+"/soal/delete/";
            // console.log(url_true);
            return url_true;
        }

        // aksi ketika save data dan update
        $('#btnSave').click(function (e) {
        e.preventDefault();
        $(this).html('Sending..');
            $.ajax({
                data: $('#formSoal').serialize(),
                url: "{{route('TamSoal')}}",
                type: "POST",
                dataType: 'json',
                success: function (data) {
                    $('#formSoal').trigger('reset');
                    $('#btnSave').html('Simpan');
                    $('#ModalSoal').modal('hide');
                    table.ajax.reload();
                    toastr.success(data);
                },
                error: function (data) {
                    console.log('Error:', data);
                    $('#btnSave').html('Simpan');
                    toastr.error('Kelola Data Gagal');

                }
            })
        });


        $(document).on('click', '.editData', function () {
            // console.log('sjfhdav');
            $('#judulModal').html('Edit data Soal');
            $('#nama').val($(this).data('nama'));
            $('#id').val($(this).data('id'));



            $('#ModalSoal').modal({
                backdrop: 'static',
                keyboard: false, // to prevent closing with Esc button (if you want this too)
                show: true
            })
        });

        $(document).on('click', '.deleteData', function () {
            console.log('deleted');
            var url = url_wilayah()+$(this).data('id');
            console.log(url);
            swal({
            title: "Apakah anda yakin menghapus data ini?",
            text: "Peringatan!! Data yang telah dihapus tidak bisa dikembalikan.",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-warning",
            confirmButtonText: 'Iya',
            cancelButtonText: "Tidak",
            closeOnConfirm: true,
            closeOnCancel: true
            }, function (isConfirm) {
                if (isConfirm) {
                    cek='update';
                    console.log(cek);

                        $.ajax({
                            url: url,
                            type: "GET",
                            dataType: 'json',
                            success: function (data) {
                                console.log(data);
                                toastr.success(data);
                                table.ajax.reload();
                                },
                            error: function (data) {
                                console.log('Error:', data);
                                toastr.success(data);
                                //$('#modalOrg').modal('show');
                                }
                        });
                }else {
                }
            });
        });
    });

    </script>
@endsection
