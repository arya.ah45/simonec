@extends('layouts.app')

@section('content')

<div class="row">

    <div class="col-md-12">
        <div class="panel">         
            <div class="panel-body nav-tabs-animate nav-tabs-horizontal px-10 py-15" data-plugin="tabs">
            <ul class="nav nav-tabs nav-tabs-line" role="tablist">
                <li class="nav-item" role="presentation"><a class="active nav-link tap_akun" data-toggle="tab" aria-controls="akun" href="#akun" role="tab">Akun</a></li>
                <li class="nav-item" role="presentation"><a class="nav-link tap_jenis_blj" data-toggle="tab" aria-controls="jenis_blj" href="#jenis_blj" role="tab">Jenis Belanja</a></li>
                <li class="nav-item" role="presentation"><a class="nav-link tap_kat_blj" data-toggle="tab" aria-controls="kat_blj" href="#kat_blj" role="tab">Kategori Belanja</a></li>
                <li class="nav-item dropdown">
                <a class="dropdown-toggle nav-link" data-toggle="dropdown" href="#" aria-expanded="false">Menu </a>
                <div class="dropdown-menu" role="menu">
                    <a class="active dropdown-item tap_akun" data-toggle="tab" aria-controls="akun" href="#akun" role="tab">Akun</a>
                    <a class="dropdown-item tap_jenis_blj" data-toggle="tab" aria-controls="jenis_blj" href="#jenis_blj" role="tab">Jenis Belanja</a>
                    <a class="dropdown-item tap_kat_blj" data-toggle="tab" aria-controls="kat_blj" href="#kat_blj" role="tab">Kategori Belanja</a>
                </div>
                </li>
            </ul>

            <div class="tab-content">
                @include('master.pagu.section_akun')
                @include('master.pagu.section_jenis_belanja')
                @include('master.pagu.section_kategori_belanja')
            </div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('scripts')
<script id="pagu_h">



</script>
@endpush