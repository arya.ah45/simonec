<div class="tab-pane animation-slide-right container-fluid" id="kat_blj" role="tabpanel">
<div class="row pt-30">
        <div class="col-xxl-12 col-lg-12">
            <button id="showKB" class="btn btn-xs mb-20 btn-outline bg-teal-700 grey-100 "  data-toggle="modal" data-target="#">
                <i class="icon wb-plus" aria-hidden="true"></i> Tambah Data
            </button>
            <table class="table table-bordered table-hover table-striped w-p100" cellspacing="0" id="tb_kb">
                <thead class="text-center">
                    <tr>
                        <th data-priority="1"><b>No.</b></th>
                        <th data-priority="2"><b>Kategori Belanja</b></th>
                        <th><b>Minimal Anggaran</b></th>
                        <th><b>Maksimal Anggaran</b></th>
                        <th><b>Satuan</b></th>
                        <th data-priority="3"><b>Kode Akun</b></th>
                        <th data-priority="4"><b>Jenis Belanja</b></th>
                        <th><b>AKSI</b></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal h-p80 fade modal-slide-from-bottom" id="ModalKB" aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-simple modal-center">
        <div class="modal-content">

            <div class="modal-header">
                <div class="ribbon ribbon-clip px-10 w-300 h-p10" style="z-index:100">
                    <span class="ribbon-inner">
                        <h4 id="judulModalKB" class="modal-title text-white"></h4>
                    </span>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>

            <div class="modal-body" data-keyboard="false" data-backdrop="static" style="height: 400px;overflow-y: auto;">
                <form id="formKategoriBelanja" name="formKategoriBelanja">
                    @csrf

                    <input type="text" class="form-control empty" hidden id="id_kb" name="id" placeholder="Islam">
                    <div class="row">
                        <div class="col-md-12">    
                            <div class="form-group p-10 form-material floating mb-0" data-plugin="formMaterial">
                                <label class="form-control-label mb-0" for="nama">Jenis Belanja</label>
                                <textarea row="2" class="form-control empty" id="kategori_belanja" name="kategori_belanja" placeholder="ex : ATK/BHP- Persediaan untuk Promosi"></textarea>
                                <div class="invalid-feedback">*wajib diisi</div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">    
                            <div class="form-group p-10 form-material floating my-0" data-plugin="formMaterial">
                                <label class="form-control-label mb-0" for="nama">Minimal Anggaran</label>
                                <input type="number" class="form-control empty" id="minimal_anggaran" name="minimal_anggaran" placeholder="ex : Rp.XXX,-"></input>
                                <div class="invalid-feedback">*wajib diisi</div>
                            </div>
                        </div>
                        <div class="col-md-6">    
                            <div class="form-group p-10 form-material floating my-0" data-plugin="formMaterial">
                                <label class="form-control-label mb-0" for="nama">Maksimal Anggaran</label>
                                <input type="number" class="form-control empty" id="maksimal_anggaran" name="maksimal_anggaran" placeholder="ex : Rp.XXX,-"></input>
                                <div class="invalid-feedback">*wajib diisi</div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">    
                            <div class="form-group p-10 form-material my-0" data-plugin="formMaterial">
                                <label class="form-control-label" for="akun">Akun</label>
                                <select class="form-control" id="akun_id" name="kode_akun"></select>
                                <div class="invalid-feedback">*wajib diisi</div>
                            </div>
                        </div>
                        <div class="col-md-6">    
                            <div class="form-group p-10 form-material my-0" data-plugin="formMaterial">
                                <label class="form-control-label" for="jenis_belanja">Jenis Belanja</label>
                                <select class="form-control" id="jenis_belanja" name="id_jenis_belanja"></select>
                                <div class="invalid-feedback">*wajib diisi</div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">    
                            <div class="form-group p-10 form-material floating my-0" data-plugin="formMaterial">
                                <label class="form-control-label mb-0" for="nama">Satuan</label>
                                <textarea row="2" class="form-control empty" id="satuan" name="satuan" placeholder="ex : paket | unit"></textarea>
                                <div class="invalid-feedback">*wajib diisi</div>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-pure" data-dismiss="modal" id="tutup"Tutup</button>
                <button type="button" id="btnSaveKB" class="btn btn-primary">Simpan</button>
            </div>
        </div>
    </div>
</div>

@push('scripts')
<script id="script_akun">

function select2pagu(id_tag,url,placeholder) {
    $(id_tag).select2({
        minimumInputLength: 0,
        // language: {
        //     inputTooShort: function() {
        //         return 'Masukkan minimal 2 digit angka';
        //     }
        // },         
        allowClear : true,
        searchInputPlaceholder: 'Search...',
        placeholder: placeholder,
        ajax: {
        cache: true,
        url: url,
        dataType: 'json',
        type: "GET",
        quietMillis: 50,
        data: function (params) {
            // console.log(params)
            return {
            search: $.trim(params.term)
            };
        },
        processResults: function (data) {
        return {
            results: $.map(data, function (item) {
            return {
                text:item.nama,
                id: item.id
                }
            })
            };
        }
        }
    });               
}

$(document).ready(function () {

    // fix bug title in select2
    $(document).on('mouseenter', '.select2-selection__rendered', function () {
        $('.select2-selection__rendered').removeAttr('title');
    });

    select2pagu('#akun_id',"{{route('select2akunBelanja')}}",'pilih akun belanja...')
    select2pagu('#jenis_belanja',"{{route('select2jenisBelanja')}}",'pilih jenis belanja...')

    // data table server side configurasi
    var table2 = $('#tb_kb').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,        
        ajax: "{{ route('dataTable_kategori_belanja') }}",
        language: {
                        processing: "Sedang diproses...<img height=50 src='{{asset('assets/images/load3.gif')}}'>"
                    },
        columns: [
            {
                data: 'no',
                name: 'no',
                searchable : false,
                orderable:true
            }, {
                data: 'kategori_belanja',
                name: 'kategori_belanja',
                orderable:true
            }, {
                data: 'minimal_anggaran',
                name: 'minimal_anggaran',
                orderable:true
            }, {
                data: 'maksimal_anggaran',
                name: 'maksimal_anggaran',
                orderable:true
            }, {
                data: 'satuan',
                name: 'satuan',
                orderable:true
            }, {
                data: 'kode_akun',
                name: 'kode_akun',
                orderable:true
            }, {
                data: 'jenis_belanja',
                name: 'jenis_belanja',
                orderable:true
            },{
                data: 'aksi',
                name: 'aksi',
                orderable: false,
                searchable: false
            },
        ],
        columnDefs: [
            { className: 'text-center', targets: [0,2,3,4,5,6,7] },
        ]
    });

    $(document).on('click', '.tap_kat_blj', function (e) {        
        e.preventDefault();
        table2.ajax.reload();        
    });

    // aksi button tambah di klik
    $(document).on('click', '#showKB', function () {

        $('#judulModalKB').html('Tambah Data Kategori Belanja');
        $('#id_kb').val('');
        $('#nama').val('');

            $('#ModalKB').modal({
                backdrop: 'static',
                keyboard: false,
                show: true
            })
    });

    // aksi ketika save data dan update
    $('#btnSaveKB').click(function (e) {
        e.preventDefault();
        $(this).html('Sending..');
        $.ajax({
            data: $('#formKategoriBelanja').serialize(),
            url: "{{route('tambahKategoriBelanja')}}",
            type: "POST",
            dataType: 'json',
            success: function (data) {
                $('#formKategoriBelanja').trigger('reset');
                $('#btnSaveKB').html('Simpan');
                $('#ModalKB').modal('hide');
                table2.ajax.reload();
                toastr.success(data);
            },
            error: function (data) {
                console.log('Error:', data);
                $('#btnSaveKB').html('Simpan');
                toastr.success('Kelola Data Gagal');

            }
        })
    });


    $(document).on('click', '.editData_kb', function () {
        // console.log('sjfhdav');
        $('#judulModalKB').html('Edit data Kategori Belanja');
        $('#id_kb').val($(this).data('id'));
        $('#kategori_belanja').val($(this).data('kategori_belanja'));
        $('#minimal_anggaran').val($(this).data('minimal_anggaran'));
        $('#maksimal_anggaran').val($(this).data('maksimal_anggaran'));
        var kode_akun = $(this).data('kode_akun');
        var nama_akun = $(this).data('nama_akun');
        var id_jenis_belanja = $(this).data('id_jenis_belanja');
        var jenis_belanja = $(this).data('jenis_belanja');
        $('#akun_id').html('<option value="'+kode_akun+'">( '+kode_akun+' ) '+nama_akun+'</option>');
        $('#jenis_belanja').html('<option value="'+id_jenis_belanja+'">'+jenis_belanja+'</option>');
        $('#satuan').val($(this).data('satuan'));

        $('#ModalKB').modal({
            backdrop: 'static',
            keyboard: false, // to prevent closing with Esc button (if you want this too)
            show: true
        })
    });

    $(document).on('click', '.deleteData', function () {
        // console.log('deleted');
        var url = "{{url('master/pagu/jenis_belanja/delete/')}}/"+$(this).data('id');
        // console.log(url);
        swal({
        title: "Apakah anda yakin menghapus data ini?",
        text: "Peringatan!! Data yang telah dihapus tidak bisa dikembalikan.",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-warning",
        confirmButtonText: 'Iya',
        cancelButtonText: "Tidak",
        closeOnConfirm: true,
        closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                cek='update';
                console.log(cek);

                    $.ajax({
                        url: url,
                        type: "GET",
                        dataType: 'json',
                        success: function (data) {
                            console.log(data);
                            toastr.success(data);
                            table2.ajax.reload();
                            },
                        error: function (data) {
                            console.log('Error:', data);
                            toastr.error(data);
                            //$('#modalOrg').modal('show');
                            }
                    });
            }else {
            }
        });
    });
});    
</script>
@endpush