<div class="tab-pane animation-slide-right container-fluid" id="jenis_blj" role="tabpanel">
<div class="row pt-30">
        <div class="col-xxl-12 col-lg-12">
            <button id="showJB" class="btn btn-xs mb-20 btn-outline bg-teal-700 grey-100 "  data-toggle="modal" data-target="#">
                <i class="icon wb-plus" aria-hidden="true"></i> Tambah Data
            </button>
            <table class="table table-bordered table-hover table-striped w-p100" cellspacing="0" id="tb_jb">
                <thead class="text-center">
                    <tr>
                        <th><b>No.</b></th>
                        <th><b>Nama</b></th>
                        <th><b>AKSI</b></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal h-p80 fade modal-slide-from-bottom" id="ModalJB" aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-simple modal-center">
        <div class="modal-content">

            <div class="modal-header">
                <div class="ribbon ribbon-clip px-10 w-300 h-25" style="z-index:100">
                    <span class="ribbon-inner">
                        <h4 id="judulModalJB" class="modal-title text-white"></h4>
                    </span>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>

            <div class="modal-body" data-keyboard="false" data-backdrop="static" style="overflow-y: auto;">
                <form id="formJenisBelanja" name="formJenisBelanja">
                    @csrf

                    <input type="text" class="form-control empty" hidden id="id_jb" name="id" placeholder="Islam">
                    
                    <div class="col-md-12 m-10">    
                        <div class="form-group form-material floating row" data-plugin="formMaterial">
                            <label class="form-control-label mb-0" for="nama">Jenis Belanja</label>
                            <input type="text" class="form-control empty" id="nama_jb" name="nama" placeholder="ex : 521211">
                            <div class="invalid-feedback">*wajib diisi</div>
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-pure" data-dismiss="modal" id="tutup"Tutup</button>
                <button type="button" id="btnSaveJB" class="btn btn-primary">Simpan</button>
            </div>
        </div>
    </div>
</div>

@push('scripts')
<script id="script_akun">
$(document).ready(function () {
    // data table server side configurasi
    var table1 = $('#tb_jb').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('dataTable_jenis_belanja') }}",
        language: {
                        processing: "Sedang diproses...<img height=50 src='{{asset('assets/images/load3.gif')}}'>"
                    },
        columns: [
            {
                data: 'no',
                name: 'no',
                searchable : false,
                orderable:true
            }, {
                data: 'nama',
                name: 'nama',
                orderable:true
            },{
                data: 'aksi',
                name: 'aksi',
                orderable: false,
                searchable: false
            },
        ],
        columnDefs: [
            { className: 'text-center', targets: [0,2] },
        ]
    });

    $(document).on('click', '.tap_jenis_blj', function (e) {        
        e.preventDefault();
        table1.ajax.reload();        
    });

    // aksi button tambah di klik
    $(document).on('click', '#showJB', function () {

        $('#judulModalJB').html('Tambah Data Jenis Belanja');
        $('#id_jb').val('');
        $('#nama').val('');

            $('#ModalJB').modal({
                backdrop: 'static',
                keyboard: false,
                show: true
            })
    });

    // aksi ketika save data dan update
    $('#btnSaveJB').click(function (e) {
        e.preventDefault();
        $(this).html('Sending..');
        $.ajax({
            data: $('#formJenisBelanja').serialize(),
            url: "{{route('tambahJenisBelanja')}}",
            type: "POST",
            dataType: 'json',
            success: function (data) {
                $('#formJenisBelanja').trigger('reset');
                $('#btnSaveJB').html('Simpan');
                $('#ModalJB').modal('hide');
                table1.ajax.reload();
                toastr.success(data);
            },
            error: function (data) {
                console.log('Error:', data);
                $('#btnSaveJB').html('Simpan');
                toastr.success('Kelola Data Gagal');

            }
        })
    });


    $(document).on('click', '.editData_jb', function () {
        // console.log('sjfhdav');
        $('#judulModalJB').html('Edit data Jenis Belanja');
        $('#nama_jb').val($(this).data('nama'));
        $('#id_jb').val($(this).data('id'));

        $('#ModalJB').modal({
            backdrop: 'static',
            keyboard: false, // to prevent closing with Esc button (if you want this too)
            show: true
        })
    });

    $(document).on('click', '.deleteData', function () {
        // console.log('deleted');
        var url = "{{url('master/pagu/kategori_belanja/delete/')}}/"+$(this).data('id');
        // console.log(url);
        swal({
        title: "Apakah anda yakin menghapus data ini?",
        text: "Peringatan!! Data yang telah dihapus tidak bisa dikembalikan.",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-warning",
        confirmButtonText: 'Iya',
        cancelButtonText: "Tidak",
        closeOnConfirm: true,
        closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                cek='update';
                console.log(cek);

                    $.ajax({
                        url: url,
                        type: "GET",
                        dataType: 'json',
                        success: function (data) {
                            console.log(data);
                            toastr.success(data);
                            table1.ajax.reload();
                            },
                        error: function (data) {
                            console.log('Error:', data);
                            toastr.error(data);
                            //$('#modalOrg').modal('show');
                            }
                    });
            }else {
            }
        });
    });
});    
</script>
@endpush