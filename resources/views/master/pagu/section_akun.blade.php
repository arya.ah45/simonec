<div class="tab-pane active animation-slide-right container-fluid" id="akun" role="tabpanel">
    <div class="row pt-30">
        <div class="col-xxl-12 col-lg-12">
            <button id="showAkun" class="btn btn-xs mb-20 btn-outline bg-teal-700 grey-100 "  data-toggle="modal" data-target="#">
                <i class="icon wb-plus" aria-hidden="true"></i> Tambah Data
            </button>
            <table class="table table-bordered table-hover table-striped" cellspacing="0" id="tb_akun">
                <thead class="text-center">
                    <tr>
                        <th><b>No.</b></th>
                        <th><b>Kode Akun</b></th>
                        <th><b>Nama Akun</b></th>
                        <th><b>AKSI</b></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal h-p80 fade modal-slide-from-bottom" id="ModalAkun" aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-simple modal-center">
        <div class="modal-content">

            <div class="modal-header">
                <div class="ribbon ribbon-clip px-10 w-300 h-25" style="z-index:100">
                    <span class="ribbon-inner">
                        <h4 id="judulModal" class="modal-title text-white"></h4>
                    </span>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>

            <div class="modal-body" data-keyboard="false" data-backdrop="static" style="height: 150px;overflow-y: auto;">
                <form id="formAkun" name="formAkun">
                    @csrf

                    <input type="text" class="form-control empty" hidden id="id_akun" name="id" placeholder="Islam">
                    
                    <div class="col-md-12 m-10">    
                        <div class="form-group form-material floating row" data-plugin="formMaterial">
                            <label class="form-control-label mb-0" for="nama">Kode Akun</label>
                            <input type="text" class="form-control empty" id="kode_akun" name="kode_akun" placeholder="ex : 521211">
                            <div class="invalid-feedback">*wajib diisi</div>
                        </div>
                    </div>

                    <div class="col-md-12 m-10">
                        <div class="form-group form-material floating row" data-plugin="formMaterial">
                            <label class="form-control-label mb-0" for="nama">Nama Akun</label>
                            <input type="text" class="form-control empty" id="nama_akun" name="nama_akun" placeholder="ex : Belanja Bahan">
                            <div class="invalid-feedback">*wajib diisi</div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-pure" data-dismiss="modal" id="tutup"Tutup</button>
                <button type="button" id="btnSaveAkun" class="btn btn-primary">Simpan</button>
            </div>
        </div>
    </div>
</div>

@push('scripts')
<script id="script_akun">
$(document).ready(function () {
    // data table server side configurasi
    var table = $('#tb_akun').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('dataTable_akun') }}",
        language: {
                        processing: "Sedang diproses...<img height=50 src='{{asset('assets/images/load3.gif')}}'>"
                    },
        columns: [
            {
                data: 'no',
                name: 'no',
                searchable : false,
                orderable:true
            }, {
                data: 'kode_akun',
                name: 'kode_akun',
                orderable:true
            }, {
                data: 'nama_akun',
                name: 'nama_akun',
                orderable:true
            },{
                data: 'aksi',
                name: 'aksi',
                orderable: false,
                searchable: false
            },
        ],
        columnDefs: [
            { className: 'text-center', targets: [0,1,3] },
        ]
    });

    // aksi button tambah di klik
    $(document).on('click', '#showAkun', function () {

        $('#judulModal').html('Tambah Data Akun');
        $('#id_akun').val('');
        $('#nama_akun').val('');
        $('#kode_akun').val('');

            $('#ModalAkun').modal({
                backdrop: 'static',
                keyboard: false,
                show: true
            })
    });

    // aksi ketika save data dan update
    $('#btnSaveAkun').click(function (e) {
        e.preventDefault();
        $(this).html('Sending..');
        $.ajax({
            data: $('#formAkun').serialize(),
            url: "{{route('tambahAkun')}}",
            type: "POST",
            dataType: 'json',
            success: function (data) {
                $('#formAkun').trigger('reset');
                $('#btnSaveAkun').html('Simpan');
                $('#ModalAkun').modal('hide');
                table.ajax.reload();
                toastr.success(data);
            },
            error: function (data) {
                console.log('Error:', data);
                $('#btnSaveAkun').html('Simpan');
                toastr.success('Kelola Data Gagal');

            }
        })
    });


    $(document).on('click', '.editData_akun', function () {
        // console.log('sjfhdav');
        $('#judulModal').html('Edit data Agama');
        $('#nama_akun').val($(this).data('nama_akun'));
        $('#kode_akun').val($(this).data('kode_akun'));
        $('#id_akun').val($(this).data('id'));

        $('#ModalAkun').modal({
            backdrop: 'static',
            keyboard: false, // to prevent closing with Esc button (if you want this too)
            show: true
        })
    });

    $(document).on('click', '.deleteData', function () {
        // console.log('deleted');
        var url = "{{url('master/pagu/akun/delete/')}}/"+$(this).data('id');
        // console.log(url);
        swal({
        title: "Apakah anda yakin menghapus data ini?",
        text: "Peringatan!! Data yang telah dihapus tidak bisa dikembalikan.",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-warning",
        confirmButtonText: 'Iya',
        cancelButtonText: "Tidak",
        closeOnConfirm: true,
        closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                cek='update';
                console.log(cek);

                    $.ajax({
                        url: url,
                        type: "GET",
                        dataType: 'json',
                        success: function (data) {
                            console.log(data);
                            toastr.success(data);
                            table.ajax.reload();
                            },
                        error: function (data) {
                            console.log('Error:', data);
                            toastr.error(data);
                            //$('#modalOrg').modal('show');
                            }
                    });
            }else {
            }
        });
    });
});    
</script>
@endpush