
<meta charset="utf-8" />
<title>SIMONEC</title>
<meta name="description" content="Updates and statistics" />
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
<link rel="canonical" href="https://keenthemes.com/metronic" />
<!--begin::Fonts-->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
<!--end::Fonts-->
<!--begin::Page Vendors Styles(used by this page)-->
<link href="{{asset('assets')}}/assets1/plugins/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />
<!--end::Page Vendors Styles-->
<!--begin::Global Theme Styles(used by all pages)-->
<link href="{{asset('assets')}}/assets1/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
<link href="{{asset('assets')}}/assets1/plugins/custom/prismjs/prismjs.bundle.css" rel="stylesheet" type="text/css" />
<link href="{{asset('assets')}}/assets1/css/style.bundle.css" rel="stylesheet" type="text/css" />
<!--end::Global Theme Styles-->
<!--begin::Layout Themes(used by all pages)-->
<!--end::Layout Themes-->
<link rel="shortcut icon" href="{{asset('assets')}}/assets1/media/logos/favicon.ico" />
{{-- <link rel="stylesheet" href="{{asset('assets')}}/chart/demo.css" /> --}}
<link rel="stylesheet" href="{{asset('assets')}}/chart/jquery.orgchart.css" />
{{-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> --}}
	<!--begin::Page Vendors Styles(used by this page)-->
    <link href="{{asset('assets')}}/assets1/plugins/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
    <!--end::Page Vendors Styles-->

    <!--begin::Page Custom  Add User Styles(used by this page)-->
		<link href="{{asset('assets')}}/assets1/css/pages/wizard/wizard-4.css" rel="stylesheet" type="text/css" />
		<!--end::Page Custom Styles-->

<link rel="stylesheet" href="{{asset('assets/select2/select2.min.css')}}">

<script src="{{asset('js/jquery-3.5.1.js')}}"></script>
<script src="https://rawgit.com/schmich/instascan-builds/master/instascan.min.js"></script>
