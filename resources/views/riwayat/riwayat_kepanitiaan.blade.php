@extends('layouts.app')

@section('content')
<div class="row" data-plugin="matchHeight" data-by-row="true">
    <div class="col-xxl-12 col-lg-12">
        <div class="panel shadow panel-bordered">
                <div class="panel-heading">
                    <h3 class="panel-title">Riwayat Kepanitiaan</h3>
                    <div class="panel-actions ">
                        <a class="panel-action icon wb-minus" aria-expanded="true" data-toggle="panel-collapse" aria-hidden="true"></a>
                        <a class="panel-action icon wb-expand" data-toggle="panel-fullscreen" aria-hidden="true"></a>
                    </div>
                </div>
                <div class="panel-body pb-15 pt-15 ">
                @php
                    $session = Session::get('data_user');
                    $nim = $session['nim'];
                    $flag_lv = $session['flag_lv'];
                @endphp
                @if ($flag_lv == "c")
                    <div class="row pt-3">
                        <div class="col-xxl-5 col-lg-5">
                            <div class="form-group form-material" data-plugin="formMaterial">
                                <label class="form-control-label" for="anggota1">Cari data riwayat</label>
                                <select class="form-control" id="anggota1" name="anggota1"></select>
                                <input hidden type="text" id="proker" name="proker">
                            </div>
                        </div>
                    </div>
                @endif

                    <div class="row pt-3 pr-20">
                        <div class="col-xxl-12 col-lg-12">
                            <table class="table table-bordered table-hover table-striped" cellspacing="0" id="tb_riwayat_kepanitiaan">
                                <thead class="text-center">
                                    <tr>
                                        <th>No</th>
                                        <th>NIM</th>
                                        <th>Nama</th>
                                        <th>Bagian / Seksi</th>
                                        <th>Kegiatan / Proker</th>
                                        <th>Tanggal acara</th>
                                    </tr>
                                </thead>
                                <tbody class="text-center">

                                </tbody>
                                </table>
                        </div>
                    </div>

                </div>
            </div>
    </div>
</div>

@endsection

@push('scripts')
<script>

$(document).ready(function () {
    // fix bug title in select2
    $(document).on('mouseenter', '.select2-selection__rendered', function () {
        $('.select2-selection__rendered').removeAttr('title');
    });

    $('#anggota1').select2({
        minimumInputLength: 0,
        // language: {
        //     inputTooShort: function() {
        //         return 'Masukkan minimal 2 digit angka';
        //     }
        // },
        allowClear : true,
        searchInputPlaceholder: 'Search...',
        placeholder: '- pilih anggota -',
        ajax: {
        cache: true,
        url: "{{ route('select2AnggotaAll') }}",
        dataType: 'json',
        type: "GET",
        quietMillis: 50,
        data: function (params) {
            // console.log(params)
            return {
            search: $.trim(params.term)
            };
        },
        processResults: function (data) {
        return {
            results: $.map(data, function (item) {
            return {
                text:item.nama,
                id: item.id
                }
            })
            };
        }
        }
    });

    var table = $('#tb_riwayat_kepanitiaan').DataTable({
        processing: true,
        serverSide: true,
        "scrollY": "250px",
        "scrollx": "1050px",
        "scrollCollapse": true,
        ajax: "{{ route('DTkepanitiaan') }}",
        language: {
            processing: "Sedang diproses...<img height=50 src='{{asset('assets/images/load3.gif')}}'>"
        },
        columns: [
            {
                data: 'no',
                name: 'no',
                searchable : false,
                orderable:true
            }, {
                data: 'nim',
                name: 'nim',
                orderable:true
            },{
                data: 'nama_pejabat',
                name: 'nama_pejabat',
                orderable: false,
                searchable: false
            },{
                data: 'nama_kepanitiaan',
                name: 'nama_kepanitiaan',
                orderable: false,
                searchable: false
            },{
                data: 'nama_proker',
                name: 'nama_proker',
                orderable: false,
                searchable: false
            },{
                data: 'tgl_proker',
                name: 'tgl_proker',
                orderable: false,
                searchable: false
            },
        ],
        columnDefs: [
            { className: 'text-center', targets: [0,1] },
        ]
    });

$(document).on('change', '#anggota1', function (e) {
    e.preventDefault();
    // console.log($(this).val());
    table.ajax.url( "{{ route('DTkepanitiaan') }}?nim="+$(this).val() ).load();
});

});


</script>
@endpush
