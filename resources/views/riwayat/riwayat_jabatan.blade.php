@extends('layouts.app')

@section('content')
<div class="row" data-plugin="matchHeight" data-by-row="true">
    <div class="col-xxl-12 col-lg-12">
        <div class="panel shadow panel-bordered">
                <div class="panel-heading">
                    <h3 class="panel-title">Riwayat Jabatan</h3>
                    <div class="panel-actions ">
                        <a class="panel-action icon wb-minus" aria-expanded="true" data-toggle="panel-collapse" aria-hidden="true"></a>
                        <a class="panel-action icon wb-expand" data-toggle="panel-fullscreen" aria-hidden="true"></a>
                    </div>
                </div>
                <div class="panel-body pb-15 pt-15 ">
                @php
                    $session = Session::get('data_user');
                    $nim = $session['nim'];
                    $flag_lv = $session['flag_lv'];
                @endphp
                @if ($flag_lv == "c")
                    <div class="row pt-3">
                        <div class="col-xxl-5 col-lg-5">
                            <div class="form-group form-material" data-plugin="formMaterial">
                                <label class="form-control-label" for="anggota">Cari data riwayat</label>
                                <select class="form-control" id="anggota" name="anggota"></select>
                                <input hidden type="text" id="proker" name="proker">
                            </div>
                        </div>
                    </div>
                @endif

                    <div class="row pt-3 pr-20">
                        <div class="col-xxl-12 col-lg-12">
                            <table class="table table-bordered table-hover table-striped" cellspacing="0" id="tb_riwayat">
                                <thead class="text-center">
                                    <tr>
                                        <th>no</th>
                                        <th>NIM</th>
                                        <th>Nama</th>
                                        <th>Jabatan</th>
                                        <th>No SK</th>
                                        <th>TMT</th>
                                        <th>Status</th>
                                        <th>Keangggotaan</th>
                                    </tr>
                                </thead>
                                <tbody class="text-center">

                                </tbody>
                                </table>
                        </div>
                    </div>

                </div>
            </div>
    </div>
</div>

@endsection

@push('scripts')
<script>

$(document).ready(function () {
    // fix bug title in select2
    $(document).on('mouseenter', '.select2-selection__rendered', function () {
        $('.select2-selection__rendered').removeAttr('title');
    });


    $('#anggota').select2({
        minimumInputLength: 0,
        // language: {
        //     inputTooShort: function() {
        //         return 'Masukkan minimal 2 digit angka';
        //     }
        // },
        allowClear : true,
        searchInputPlaceholder: 'Search...',
        placeholder: '- pilih anggota -',
        ajax: {
        cache: true,
        url: "{{ route('select2AnggotaAll') }}",
        dataType: 'json',
        type: "GET",
        quietMillis: 50,
        data: function (params) {
            // console.log(params)
            return {
            search: $.trim(params.term)
            };
        },
        processResults: function (data) {
        return {
            results: $.map(data, function (item) {
            return {
                text:item.nama,
                id: item.id
                }
            })
            };
        }
        }
    });

    var table = $('#tb_riwayat').DataTable({
        processing: true,
        serverSide: true,
        "scrollY": "250px",
        "scrollx": "1050px",
        "scrollCollapse": true,
        ajax: "{{ route('DTjabatan') }}",
        language: {
            processing: "Sedang diproses...<img height=50 src='{{asset('assets/images/load3.gif')}}'>"
        },
        columns: [
            {
                data: 'no',
                name: 'no',
                searchable : false,
                orderable:true
            }, {
                data: 'nim',
                name: 'nim',
                orderable:true
            },{
                data: 'nama_pejabat',
                name: 'nama_pejabat',
                orderable: false,
                searchable: false
            },{
                data: 'nama_jabatan',
                name: 'nama_jabatan',
                orderable: false,
                searchable: false
            },{
                data: 'no_sk',
                name: 'no_sk',
                orderable: false,
                searchable: false
            },{
                data: 'tgl_sk',
                name: 'tgl_sk',
                orderable: false,
                searchable: false
            },{
                data: 'status',
                name: 'status',
                orderable: false,
                searchable: false
            },{
                data: 'ubah',
                name: 'ubah',
                orderable: false,
                searchable: false
            },
        ],
        columnDefs: [
            { className: 'text-center', targets: [0,1,5,6] },
        ]
    });

    $(document).on('click', '.btnUbah', function (e) {
        event.preventDefault();
        var title_confirm = 'Konfirmasi!';
        var text_confirm = 'Apakah Anda Yakin Merubah Status Keanggotaan'+' '+$(this).data('nama');
        var id =$(this).data('id');
        var stat =$(this).data('stat');
        swal({
            title: title_confirm,
            text: text_confirm,
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-success",
            confirmButtonText: 'Iya',
            cancelButtonText: "Tidak",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "get",
                    url: "{{route('updateRj')}}?id="+id+'&stat='+stat,
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                            swal({
                                title: "Sukses!",
                                text: "Perubahan berhasil disimpan",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonClass: "btn-success",
                                confirmButtonText: 'OK',
                                closeOnConfirm: false
                            }, function (isConfirm) {
                                if (isConfirm) {
                                    table.ajax.reload();
                                    swal.close()
                                    return;
                                }else {
                                    check = 0;
                                }
                            });
                        cancel();
                        },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
                return;
            }else {
                check = 0;
            }
        });
    })

$(document).on('change', '#anggota', function (e) {
    e.preventDefault();
    // console.log($(this).val());
    table.ajax.url( "{{ route('DTjabatan') }}?nim="+$(this).val() ).load();
});

});


</script>
@endpush
