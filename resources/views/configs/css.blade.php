<link rel="apple-touch-icon" href="{{asset('assets/images/logo_atas.jpg')}}">
<link rel="shortcut icon" href="{{asset('assets/images/logo_atas.jpg')}}">

<!-- Stylesheets -->
<link rel="stylesheet" href="{{asset('assets/global/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/global/css/bootstrap-extend.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/site.min.css')}}">

<!-- Plugins -->
<link rel="stylesheet" href="{{asset('assets/global/vendor/animsition/animsition.css')}}">
<link rel="stylesheet" href="{{asset('assets/global/vendor/asscrollable/asScrollable.css')}}">
<link rel="stylesheet" href="{{asset('assets/global/vendor/switchery/switchery.css')}}">
<link rel="stylesheet" href="{{asset('assets/global/vendor/intro-js/introjs.css')}}">
<link rel="stylesheet" href="{{asset('assets/global/vendor/slidepanel/slidePanel.css')}}">
<link rel="stylesheet" href="{{asset('assets/global/vendor/jquery-mmenu/jquery-mmenu.css')}}">
<link rel="stylesheet" href="{{asset('assets/global/vendor/flag-icon-css/flag-icon.css')}}">
{{-- <link rel="stylesheet" href="{{asset('assets/global/vendor/summernote/summernote.css')}}"> --}}
<link rel="stylesheet" href="{{asset('assets/global/vendor/bootstrap-markdown/bootstrap-markdown.css')}}">


{{-- DataTable --}}
<link rel="stylesheet" href="{{asset('assets/global/vendor/datatables.net-bs4/dataTables.bootstrap4.css')}}">
<link rel="stylesheet" href="{{asset('assets/global/vendor/datatables.net-fixedheader-bs4/dataTables.fixedheader.bootstrap4.css')}}">
<link rel="stylesheet" href="{{asset('assets/global/vendor/datatables.net-fixedcolumns-bs4/dataTables.fixedcolumns.bootstrap4.css')}}">
<link rel="stylesheet" href="{{asset('assets/global/vendor/datatables.net-rowgroup-bs4/dataTables.rowgroup.bootstrap4.css')}}">
<link rel="stylesheet" href="{{asset('assets/global/vendor/datatables.net-scroller-bs4/dataTables.scroller.bootstrap4.css')}}">
<link rel="stylesheet" href="{{asset('assets/global/vendor/datatables.net-select-bs4/dataTables.select.bootstrap4.css')}}">
<link rel="stylesheet" href="{{asset('assets/global/vendor/datatables.net-responsive-bs4/dataTables.responsive.bootstrap4.css')}}">
<link rel="stylesheet" href="{{asset('assets/global/vendor/datatables.net-buttons-bs4/dataTables.buttons.bootstrap4.css')}}">
<link rel="stylesheet" href="{{asset('assets/examples/css/tables/datatable.css')}}">

{{-- select2 --}}
<link rel="stylesheet" href="{{asset('assets/select2/select2.min.css')}}">

{{-- treview --}}
<link rel="stylesheet" href="{{asset('css/bootstrap-treeview.css')}}">
<link rel="stylesheet" href="{{asset('assets/global/vendor/jstree/jstree.min.css')}}">

{{-- toastr --}}
<link rel="stylesheet" href="{{asset('assets/global/vendor/toastr/toastr.css')}}">

{{-- icon --}}
<link rel="stylesheet" href="{{asset('assets/examples/css/uikit/icons.css')}}">

<!-- Icons -->
<link rel="stylesheet" href="{{asset('assets/global/fonts/font-awesome/font-awesome.css')}}">
<link rel="stylesheet" href="{{asset('assets/global/fonts/7-stroke/7-stroke.css')}}">
<link rel="stylesheet" href="{{asset('assets/global/fonts/ionicons/ionicons.css')}}">
<link rel="stylesheet" href="{{asset('assets/global/fonts/material-design/material-design.css')}}">
<link rel="stylesheet" href="{{asset('assets/global/fonts/mfglabs/mfglabs.css')}}">
<link rel="stylesheet" href="{{asset('assets/global/fonts/open-iconic/open-iconic.css')}}">
<link rel="stylesheet" href="{{asset('assets/global/fonts/themify/themify.css')}}">
<link rel="stylesheet" href="{{asset('assets/global/fonts/weather-icons/weather-icons.css')}}">
<link rel="stylesheet" href="{{asset('assets/global/fonts/glyphicons/glyphicons.css')}}">
<link rel="stylesheet" href="{{asset('assets/global/fonts/octicons/octicons.css')}}">
<link rel="stylesheet" href="{{asset('assets/global/fonts/web-icons/web-icons.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/global/fonts/brand-icons/brand-icons.min.css')}}">

{{-- modals --}}
<link rel="stylesheet" href="{{asset('assets/examples/css/uikit/modals.css')}}">

{{-- sweet alert --}}
<link rel="stylesheet" href="{{asset('assets/global/vendor/bootstrap-sweetalert/sweetalert.css')}}">

{{-- image upload css --}}
<link rel="stylesheet" href="{{asset('assets/global/vendor/blueimp-file-upload/jquery.fileupload.css')}}">
<link rel="stylesheet" href="{{asset('assets/global/vendor/dropify/dropify.css')}}">

{{-- panel transisi --}}
<link rel="stylesheet" href="{{asset('assets/examples/css/layouts/panel-transition.css')}}">

{{-- lain-lain --}}
<link rel="stylesheet" href="{{asset('assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/examples/css/dashboard/v1.css')}}">
<link rel="stylesheet" href="{{asset('assets/global/vendor/asscrollable/asScrollable.css')}}">
<link rel="stylesheet" href="{{asset('assets/global/vendor/nestable/nestable.css')}}">
<link rel="stylesheet" href="{{asset('assets/global/vendor/tasklist/tasklist.css')}}">
<link rel="stylesheet" href="{{asset('assets/global/vendor/sortable/sortable.css')}}">

<link rel="stylesheet" href="{{asset('assets')}}/global/vendor/asscrollable/asScrollable.css">
<link rel="stylesheet" href="{{asset('assets')}}/examples/css/advanced/scrollable.css">

<link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>

{{-- keperluan form --}}
<link rel="stylesheet" href="{{asset('assets')}}/global/vendor/select2/select2.css">
<link rel="stylesheet" href="{{asset('assets')}}/global/vendor/bootstrap-tokenfield/bootstrap-tokenfield.css">
<link rel="stylesheet" href="{{asset('assets')}}/global/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css">
<link rel="stylesheet" href="{{asset('assets')}}/global/vendor/bootstrap-select/bootstrap-select.css">
<link rel="stylesheet" href="{{asset('assets')}}/global/vendor/icheck/icheck.css">
<link rel="stylesheet" href="{{asset('assets')}}/global/vendor/switchery/switchery.css">
<link rel="stylesheet" href="{{asset('assets')}}/global/vendor/asrange/asRange.css">
<link rel="stylesheet" href="{{asset('assets')}}/global/vendor/ionrangeslider/ionrangeslider.min.css">
<link rel="stylesheet" href="{{asset('assets')}}/global/vendor/asspinner/asSpinner.css">
<link rel="stylesheet" href="{{asset('assets')}}/global/vendor/clockpicker/clockpicker.css">
<link rel="stylesheet" href="{{asset('assets')}}/global/vendor/ascolorpicker/asColorPicker.css">
<link rel="stylesheet" href="{{asset('assets')}}/global/vendor/bootstrap-touchspin/bootstrap-touchspin.css">
<link rel="stylesheet" href="{{asset('assets')}}/global/vendor/jquery-labelauty/jquery-labelauty.css">
<link rel="stylesheet" href="{{asset('assets')}}/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css">
<link rel="stylesheet" href="{{asset('assets')}}/global/vendor/bootstrap-maxlength/bootstrap-maxlength.css">
<link rel="stylesheet" href="{{asset('assets')}}/global/vendor/timepicker/jquery-timepicker.css">
<link rel="stylesheet" href="{{asset('assets')}}/global/vendor/jquery-strength/jquery-strength.css">
<link rel="stylesheet" href="{{asset('assets')}}/global/vendor/multi-select/multi-select.css">
<link rel="stylesheet" href="{{asset('assets')}}/global/vendor/typeahead-js/typeahead.css">
<link rel="stylesheet" href="{{asset('assets')}}/examples/css/forms/advanced.css">

<link rel="stylesheet" href="{{asset('assets')}}/global/vendor/blueimp-file-upload/jquery.fileupload.css">
<link rel="stylesheet" href="{{asset('assets')}}/global/vendor/dropify/dropify.css">

<link rel="stylesheet" href="{{asset('assets')}}/examples/css/widgets/statistics.css">

<link rel="stylesheet" href="{{asset('assets')}}/global/vendor/webui-popover/webui-popover.css">
<link rel="stylesheet" href="{{asset('assets')}}/global/vendor/toolbar/toolbar.css">

{{-- Rating --}}
    <link rel="tylesheet" href="asset('css/jquery.rateyo.css')}}"/>
{{-- note --}}

{{-- jqury --}}

<script src="{{url('assets/global/vendor/breakpoints/breakpoints.js')}}"></script>
<script>
    Breakpoints();
</script>
{{-- <script type='text/javascript' src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script> --}}
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> --}}

<script src="{{asset('js/jquery-3.5.1.js')}}"></script>
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> --}}
{{-- <script src="{{asset('js/jquery.dataTables.min.js')}}"></script> --}}
{{-- <script src="{{asset('js/dataTables.bootstrap4.min.js')}}"></script> --}}
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery.Marquee/1.5.0/jquery.marquee.js"></script>  --}}
{{-- <script src="{{asset('js/jquery.marquee.min.js')}}"></script> --}}
{{-- <script src="{{asset('js/jquery.marquee.js')}}"></script> --}}

{{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/orgchart/2.1.3/css/jquery.orgchart.min.css" /> --}}
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/orgchart/2.1.3/js/jquery.orgchart.min.js"></script> --}}


<link rel="stylesheet" href="{{asset('assets')}}/chart/jquery.orgchart.css" />
<script src="{{asset('assets')}}/chart/jquery.orgchart.min.js"></script>

<script src="https://rawgit.com/schmich/instascan-builds/master/instascan.min.js"></script>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.css">
<!-- Latest compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.js"></script>
