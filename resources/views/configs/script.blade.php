<!-- Core  -->
<script src="{{url('assets/global/vendor/jquery/jquery.js')}}"></script>
<script src="{{asset('js/jquery-3.5.1.js')}}"></script>

<script src="{{url('assets/global/vendor/babel-external-helpers/babel-external-helpers.js')}}"></script>
<script src="{{url('assets/global/vendor/popper-js/umd/popper.min.js')}}"></script>
<script src="{{url('assets/global/vendor/bootstrap/bootstrap.js')}}"></script>
<script src="{{url('assets/global/vendor/animsition/animsition.js')}}"></script>
<script src="{{url('assets/global/vendor/mousewheel/jquery.mousewheel.js')}}"></script>
<script src="{{url('assets/global/vendor/asscrollbar/jquery-asScrollbar.js')}}"></script>
<script src="{{url('assets/global/vendor/asscrollable/jquery-asScrollable.js')}}"></script>
<script src="{{url('assets/global/vendor/ashoverscroll/jquery-asHoverScroll.js')}}"></script>

<!-- select2 -->
<script src="{{url('assets/select2/select2.min.js')}}"></script>


<!-- Plugins -->
<script src="{{url('assets/global/vendor/jquery-mmenu/jquery.mmenu.min.all.js')}}"></script>
<script src="{{url('assets/global/vendor/switchery/switchery.js')}}"></script>
<script src="{{url('assets/global/vendor/intro-js/intro.js')}}"></script>
<script src="{{url('assets/global/vendor/screenfull/screenfull.js')}}"></script>
<script src="{{url('assets/global/vendor/slidepanel/jquery-slidePanel.js')}}"></script>
<script src="{{url('assets/global/vendor/skycons/skycons.js')}}"></script>
<script src="{{url('assets/global/vendor/bootstrap-markdown/bootstrap-markdown.js')}}"></script>
<script src="{{url('assets/global/vendor/marked/marked.js')}}"></script>
<script src="{{url('assets/global/vendor/to-markdown/to-markdown.js')}}"></script>


{{-- DataTable script --}}
<script src="{{url('assets/global/vendor/datatables.net/jquery.dataTables.js')}}"></script>
<script src="{{url('assets/global/vendor/datatables.net-bs4/dataTables.bootstrap4.js')}}"></script>
<script src="{{url('assets/global/vendor/datatables.net-fixedheader/dataTables.fixedHeader.js')}}"></script>
<script src="{{url('assets/global/vendor/datatables.net-fixedcolumns/dataTables.fixedColumns.js')}}"></script>
<script src="{{url('assets/global/vendor/datatables.net-rowgroup/dataTables.rowGroup.js')}}"></script>
<script src="{{url('assets/global/vendor/datatables.net-scroller/dataTables.scroller.js')}}"></script>
<script src="{{url('assets/global/vendor/datatables.net-responsive/dataTables.responsive.js')}}"></script>
<script src="{{url('assets/global/vendor/datatables.net-responsive-bs4/responsive.bootstrap4.js')}}"></script>
<script src="{{url('assets/global/vendor/datatables.net-buttons/dataTables.buttons.js')}}"></script>
<script src="{{url('assets/global/vendor/datatables.net-buttons/buttons.html5.js')}}"></script>
<script src="{{url('assets/global/vendor/datatables.net-buttons/buttons.flash.js')}}"></script>
<script src="{{url('assets/global/vendor/datatables.net-buttons/buttons.print.js')}}"></script>
<script src="{{url('assets/global/vendor/datatables.net-buttons/buttons.colVis.js')}}"></script>
<script src="{{url('assets/global/vendor/datatables.net-buttons-bs4/buttons.bootstrap4.js')}}"></script>

{{-- toastr script --}}
<script src="{{url('assets/global/vendor/toastr/toastr.js')}}"></script>

{{-- treview script --}}
<script src="{{url('js/bootstrap-treeview.js')}}"></script>
<script src="{{url('assets/global/vendor/jstree/jstree.min.js')}}"></script>

{{-- treview sweet alert --}}
<script src="{{url('assets/global/vendor/bootstrap-sweetalert/sweetalert.js')}}"></script>

{{-- script lain-lain --}}
<script src="{{url('assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
<script src="{{url('assets/global/vendor/matchheight/jquery.matchHeight-min.js')}}"></script>
<script src="{{url('assets/global/vendor/asrange/jquery-asRange.min.js')}}"></script>
<script src="{{url('assets/global/vendor/bootbox/bootbox.js')}}"></script>
<script src="{{url('assets/global/vendor/draggabilly/draggabilly.pkgd.js')}}"></script>
<script src="{{url('assets/global/vendor/raty/jquery.raty.js')}}"></script>
<script src="{{url('assets/global/vendor/jquery-placeholder/jquery.placeholder.js')}}"></script>

<!-- Scripts -->
<script src="{{url('assets/global/js/Component.js')}}"></script>
<script src="{{url('assets/global/js/Plugin.js')}}"></script>
<script src="{{url('assets/global/js/Base.js')}}"></script>
<script src="{{url('assets/global/js/Config.js')}}"></script>

<script src="{{url('assets/js/Section/Menubar.js')}}"></script>
<script src="{{url('assets/js/Section/GridMenu.js')}}"></script>
<script src="{{url('assets/js/Section/Sidebar.js')}}"></script>
<script src="{{url('assets/js/Section/PageAside.js')}}"></script>
<script src="{{url('assets/js/Plugin/menu.js')}}"></script>

<script src="{{url('assets/global/js/config/colors.js')}}"></script>
<script src="{{url('assets/js/config/tour.js')}}"></script>
<script>
    Config.set('assets', '../assets');
</script>

<!-- Page -->
<script src="{{url('assets/js/Site.js')}}"></script>
<script src="{{url('assets/global/js/Plugin/asscrollable.js')}}"></script>
<script src="{{url('assets/global/js/Plugin/slidepanel.js')}}"></script>
{{-- <script src="{{url('assets/global/js/Plugin/switchery.js')}}"></script> --}}
<script src="{{url('assets/global/js/Plugin/matchheight.js')}}"></script>

<script src="{{url('assets/examples/js/tables/datatable.js')}}"></script>

<script src="{{url('assets/global/js/Plugin/animate-list.js')}}"></script>
<script src="{{url('assets/global/js/Plugin/panel.js')}}"></script>
<script src="{{url('assets/examples/js/layouts/panel-transition.js')}}"></script>

<script src="{{url('assets/global/vendor/sortable/Sortable.js')}}"></script>
<script src="{{url('assets/global/js/Plugin/tasklist.js')}}"></script>

<script src="{{url('assets/global/js/Plugin/responsive-tabs.js')}}"></script>
<script src="{{url('assets/global/js/Plugin/tabs.js')}}"></script>
<script src="{{url('assets/global/js/Plugin/raty.js')}}"></script>

<script src="{{url('assets/examples/js/uikit/panel-actions.js')}}"></script>

<script src="{{url('assets/global/js/Plugin/toastr.js')}}"></script>

<script src="{{url('assets/examples/js/advanced/treeview.js')}}"></script>

<script src="{{url('assets/global/js/Plugin/bootbox.js')}}"></script>
<script src="{{url('assets/global/js/Plugin/bootstrap-sweetalert.js')}}"></script>

<script src="{{url('assets/examples/js/advanced/bootbox-sweetalert.js')}}"></script>

<script src="{{url('assets/global/js/Plugin/jquery-placeholder.js')}}"></script>
<script src="{{url('assets/global/js/Plugin/material.js')}}"></script>



{{-- note
<script src="{{url('assets/global/vendor/summernote/summernote.min.js')}}"></script>
<script src="{{url('assets/global/js/Plugin/summernote.js')}}"></script> --}}

<script src="{{url('assets/global/vendor/jquery-ui/jquery-ui.js')}}"></script>
<script src="{{url('assets/global/vendor/blueimp-tmpl/tmpl.js')}}"></script>
<script src="{{url('assets/global/vendor/blueimp-canvas-to-blob/canvas-to-blob.js')}}"></script>
<script src="{{url('assets/global/vendor/blueimp-load-image/load-image.all.min.js')}}"></script>
<script src="{{url('assets/global/vendor/blueimp-file-upload/jquery.fileupload.js')}}"></script>
<script src="{{url('assets/global/vendor/blueimp-file-upload/jquery.fileupload-process.js')}}"></script>
<script src="{{url('assets/global/vendor/blueimp-file-upload/jquery.fileupload-image.js')}}"></script>
<script src="{{url('assets/global/vendor/blueimp-file-upload/jquery.fileupload-validate.js')}}"></script>
<script src="{{url('assets/global/vendor/blueimp-file-upload/jquery.fileupload-ui.js')}}"></script>
<script src="{{url('assets/global/vendor/dropify/dropify.min.js')}}"></script>

<script src="{{asset('assets')}}/global/vendor/multi-select/jquery.multi-select.js"></script>
<script src="{{asset('assets')}}/global/js/Plugin/multi-select.js"></script>

{{-- <script src="{{url('assets/global/js/Plugin/dropify.js')}}"></script> --}}
<script src="{{url('assets/examples/js/forms/uploads.js')}}"></script>

<script src="{{asset('assets')}}/global/js/Plugin/asscrollable.js"></script>
<script src="{{asset('assets')}}/examples/js/advanced/scrollable.js"></script>

<script src="{{asset('assets')}}/global/js/Plugin/select2.js"></script>
<script src="{{asset('assets')}}/global/js/Plugin/icheck.js"></script>
<script src="{{asset('assets')}}/global/vendor/icheck/icheck.min.js"></script>

{{-- <script src="{{asset('js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('js/dataTables.bootstrap4.min.js')}}"></script> --}}

{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/orgchart/2.1.3/js/jquery.orgchart.min.js"></script> --}}

{{-- <script src="{{url('assets/examples/js/forms/editor-summernote.js')}}"></script> --}}

{{-- <script src="{{asset('js')}}/dropify.js"></script> --}}

{{-- <script src="{{asset('js/jquery.marquee.js')}}"></script> --}}
<script src="{{asset('js/jquery.quicksearch.js')}}"></script>

<script src="{{asset('assets')}}/global/vendor/peity/jquery.peity.min.js"></script>
<script src="{{asset('assets')}}/global/js/Plugin/peity.js"></script>
<script src="{{asset('assets')}}/examples/js/charts/peity.js"></script>

<script src="{{asset('assets')}}/global/vendor/webui-popover/jquery.webui-popover.min.js"></script>
<script src="{{asset('assets')}}/global/vendor/toolbar/jquery.toolbar.js"></script>

<script src="{{asset('assets')}}/global/js/Plugin/webui-popover.js"></script>
<script src="{{asset('assets')}}/global/js/Plugin/toolbar.js"></script>

<script src="{{asset('assets')}}/examples/js/uikit/tooltip-popover.js"></script>

<script type="text/javascript" src="{{asset('js/jquery.rateyo.min.js')}}"></script>

<script>
    Config.set('assets', '../../assets');
</script>
<script type="text/javascript">
    (function(document, window, $) {
        'use strict';

        var Site = window.Site;
        $(document).ready(function() {
            Site.run();
        });
    })(document, window, jQuery);
</script>
