@extends('layouts.app')

@section('content')
<div class="row" data-plugin="matchHeight" data-by-row="true">
    <div class="col-xxl-4 col-lg-4">
        <div class="card border  border-success" style="background-image: linear-gradient(180deg,rgba(255, 255, 255, 0.6),rgba(255, 255, 255, 0.9));">
            <div class="card-block">
                <h4 class="card-title">Recruitment</h4>
                <p class="card-text">Jika diaktifkan maka menu pendaftaran akan muncul di tampilan </p>
                <input type="checkbox" class="status" id="status" name="status" data-id="0"/>
            </div>
        </div>
    </div>

    <div class="col-xxl-4 col-lg-4">
        <div class="card border  border-success" style="background-image: linear-gradient(180deg,rgba(255, 255, 255, 0.6),rgba(255, 255, 255, 0.9));">
            <div class="card-block">
                <h4 class="card-title">Hasil Recruitment</h4>
                <p class="card-text">Jika diaktifkan maka menu hasil recruitmen akan di tampilkan </p>
                <input type="checkbox" class="status" id="statushas" name="statushas" data-id="0"/>
            </div>
        </div>
    </div>
</div>

@endsection

@push('scripts')
<script>
var switchery = [];
var status_setting = '{{$status_setting}}';
var status_hasil = '{{$status_hasil}}';
var cek = 0;
var cek1 = 0;

// Switchery initiation
$('.status').each(function (index, element) {
    var id = $(this).data('id');
    switchery[id] = new Switchery(element);
});

$(document).ready(function () {
    console.log(status_setting);
    console.log(status_hasil);
    if (status_setting == 1) {
        $('#status').trigger('click').attr("checked", "checked");
    }

    if (status_hasil == 1) {
        $('#statushas').trigger('click').attr("checked", "checked");
    }


});

$(document).on('change','#statushas',function (e) {
    e.preventDefault();
    console.log($(this).is(":checked"));
    var statuse = $(this).is(":checked");
    var param = '';


    if (statuse == 1) {
        param = true;
    }else{
        param = false;

    }
    $.ajax({
        type: "get",
        url: "{{route('ubahSettinghasil')}}?statushas="+param,
        dataType: "json",
        success: function (data) {
            if (cek1 == 0) {
            }else{
                console.log(data);
                swal({
                    title: "Sukses!",
                    text: "Pengaturan berhasil disimpan",
                    type: "success",
                    showCancelButton: false,
                    confirmButtonClass: "btn-success",
                    confirmButtonText: 'OK',
                    closeOnConfirm: false
                });
            }
            cek1++;
        }
    });
});

$(document).on('change','#status',function (e) {
    e.preventDefault();
    console.log($(this).is(":checked"));
    var statuse = $(this).is(":checked");
    var param = '';


    if (statuse == 1) {
        param = true;
    }else{
        param = false;

    }
    $.ajax({
        type: "get",
        url: "{{route('ubahSetting')}}?status="+param,
        dataType: "json",
        success: function (data) {
            if (cek1 == 0) {
            }else{
                console.log(data);
                swal({
                    title: "Sukses!",
                    text: "Pengaturan berhasil disimpan",
                    type: "success",
                    showCancelButton: false,
                    confirmButtonClass: "btn-success",
                    confirmButtonText: 'OK',
                    closeOnConfirm: false
                });
            }
            cek1++;
        }
    });
});
</script>
@endpush
