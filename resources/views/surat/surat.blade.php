@extends('layouts.app')

@php
$urlEdit = url()->current();
@endphp
@section('content')



<div class="page-content container-fluid">
    <div class="row" data-plugin="matchHeight" data-by-row="true">
        <div class="col-xxl-12 col-lg-12">
            <div class="panel nav-tabs-horizontal nav-tabs-line panel shadow panel-bordered " data-plugin="tabs">
                <div class="panel-heading panel-heading-tab bg-white">
                    <ul class="nav nav-tabs nav-tabs-solid text-dark" role="tablist">
                        <li class="nav-item"><a class="nav-link active show" data-toggle="tab" href="#panelTab1" aria-controls="panelTab1" role="tab" aria-expanded="true" aria-selected="true">
                                <div class="text-dark py-0">Surat Masuk</div>
                            </a></li>
                        <li class="nav-item tap_surat_keluar"><a class="nav-link" data-toggle="tab" href="#panelTab2" aria-controls="panelTab2" role="tab" aria-selected="false">
                                <div class="text-dark py-0">Surat Keluar</div>
                            </a></li>
                    </ul>
                    {{-- <br class="tampil-title-mobile"> --}}

                </div>

                <div class="panel-body pt-20">
                    <div class="tab-content">
                        <div class="tab-pane active show" id="panelTab1" role="tabpanel">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-xxl-12 col-lg-12">
                                      <a href="" class="btn mb-10 btn-xs btn-success"><i class="fa fa-reply" aria-hidden="true"></i> Kembali</a>
                                      <button id="showMasuk" class="btn btn-xs mb-10 btn-outline bg-teal-700 grey-100 "  data-toggle="modal"
                                      data-target="#">
                                          <i class="icon wb-plus" aria-hidden="true"></i> Tambah Data
                                      </button>
                                      <table class="table table-bordered table-hover table-striped" cellspacing="0" id="tbSuratMasuk">
                                          <thead class="text-center">
                                              <tr>
                                                  <th width="5%" ><b>No.</b></th>
                                                  <th width="35%" ><b>Perihal</b></th>
                                                  <th width="20%" ><b>No surat Masuk </b></th>
                                                  <th width="15%" ><b>Asal Surat</b></th>
                                                  <th width="10%" ><b>Tanggal Surat</b></th>
                                                  <th width="20%" ><b>AKSI</b></th>
                                              </tr>
                                          </thead>
                                      </table>
                                  </div>
                              </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="panelTab2" role="tabpanel">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-xxl-12 col-lg-12">
                                      <a href="" class="btn mb-10 btn-xs btn-success"><i class="fa fa-reply" aria-hidden="true"></i> Kembali</a>
                                      <button id="showKeluar" class="btn btn-xs mb-10 btn-outline bg-teal-700 grey-100 "  data-toggle="modal"
                                      data-target="#">
                                          <i class="icon wb-plus" aria-hidden="true"></i> Tambah Data
                                      </button>
                                      <table class="table table-bordered table-hover table-striped" cellspacing="0" id="tbSuratKeluar">
                                          <thead class="text-center">
                                              <tr>
                                                  <th width="5%" ><b>No.</b></th>
                                                  <th width="45%" ><b>Perihal</b></th>
                                                  <th width="15%" ><b>No surat Keluar </b></th>
                                                  <th width="15%" ><b>Ditujukan Kepada</b></th>
                                                  <th width="10%" ><b>Tanggal Surat</b></th>
                                                  <th width="20%" ><b>AKSI</b></th>
                                              </tr>
                                          </thead>
                                      </table>
                                  </div>
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal h-p80 fade modal-slide-from-bottom p-0" id="detail" aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-simple modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <div class="ribbon ribbon-clip px-10 w-300 h-50" style="z-index:100">
                    <span class="ribbon-inner">
                        <h4 id="judulModal" class="modal-title text-white"></h4>
                    </span>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>

            <div class="modal-body pt-20" data-keyboard="false" data-backdrop="static" style="height:500px;overflow-y: auto;">

                    <div class="col-md-12 row pr-0">
                        <div class="col-md-6">
                            <div class="form-group form-material" data-plugin="formMaterial">
                                <label class="form-control-label" id="nosur" for="nama">No Surat</label>
                                <input type="text" class="form-control" id="no_surat" readonly />
                            </div>
                        </div>
                        <div class="col-md-6 pr-0">
                            <div class="form-group form-material" data-plugin="formMaterial">
                                <label class="form-control-label" id="assur" for="asal_surat">Asal Surat</label>
                                <input type="text" class="form-control" id="asal_surat" readonly />
                            </div>

                        </div>
                    </div>
                    <div class="col-md-12 row pr-0">
                        <div class="col-md-6">
                            <div class="form-group form-material" data-plugin="formMaterial">
                                <label class="form-control-label" id="persur" for="perihal">Perihal Surat</label>
                                <input type="text" class="form-control" id="perihal" readonly/>
                            </div>
                        </div>
                        <div class="col-md-6 pr-0">
                            <div class="form-group form-material" data-plugin="formMaterial">
                                <label class="form-control-label" id="tglsur" for="tgl_surat">Tanggal Masuk Surat</label>
                                <input type="date" class="form-control" id="tgl_surat" readonly/>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 row pr-0">
                        <div class="col-md-1 vertical-align">
                            <div class="bg-purple-400 w-p100 vertical-align-middle" style="height: 10px"></div>
                        </div>
                        <div class="col-md-1">
                            <h4 id="diter">File Surat</h4>
                        </div>
                        <div class="col-md-10 vertical-align pr-5">
                            <div class="bg-purple-400 w-p100 vertical-align-middle" style="height: 10px"></div>
                        </div>
                    </div>
                    <div class="col-md-12 table-responsive-md">
                        <div class="form-group form-material row" data-plugin="formMaterial">
                            <div class="col-md-12">
                                <div class="col-md-12">
                                    <div class="example">
                                        <div class="card center cobak">

                                            <div class="card-block">
                                                <h4 class="card-title text-center" id="file"></h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>



                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-outline btn-outline-secondary" data-dismiss="modal" id="tutup">Tutup</button>
                    <button type="button" id="download" class="btn bg-purple-400 text-white">Download File</button>
                </div>

        </div>
    </div>
</div>

<div class="modal h-p80 fade modal-slide-from-bottom p-0" id="ModalSuratMas" aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-simple modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <div class="ribbon ribbon-clip px-10 w-300 h-50" style="z-index:100">
                    <span class="ribbon-inner">
                        <h4 id="judulModal1" class="modal-title text-white"></h4>
                    </span>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>

            <div class="modal-body pt-20" data-keyboard="false" data-backdrop="static" style="height:500px;overflow-y: auto;">
                <form id="formSuratMasuk" name="formSuratMasuk" action="{{route('TamSurMas')}}" method="POST" enctype="multipart/form-data" class="row">
                    <input hidden name="id" id="id" value="">
                    <input hidden name="jenis_surat" id="jenis_surat" value="1"/>
                    @csrf
                    <div class="col-md-12 row pr-0">
                        <div class="col-md-6">
                            <div class="form-group form-material" data-plugin="formMaterial">
                                <label class="form-control-label" for="nama">No Surat Masuk</label>
                                <input type="text" class="form-control" id="no_surat1" name="no_surat" placeholder="Contoh : Ec/2021/102" data-hint="No Surat Masuk 'cs/s/***'" />
                            </div>
                        </div>
                        <div class="col-md-6 pr-0">
                            <div class="form-group form-material" data-plugin="formMaterial">
                                <label class="form-control-label" for="asal_surat">Asal Surat</label>
                                <input type="text" class="form-control" id="asal_surat1" name="asal_surat" placeholder="Contoh:BEM POLINEMA PSDKU" data-hint="Asal Surat Masuk Instansi Yang Mengirim" />
                            </div>

                        </div>
                    </div>
                    <div class="col-md-12 row pr-0">
                        <div class="col-md-6">
                            <div class="form-group form-material" data-plugin="formMaterial">
                                <label class="form-control-label" for="perihal">Perihal Surat</label>
                                <input type="text" class="form-control" id="perihal1" name="perihal" placeholder="Contoh : Surat Undangan Rapat" data-hint="Informasi Singkat Keperluan Surat" />
                            </div>
                        </div>
                        <div class="col-md-6 pr-0">
                            <div class="form-group form-material" data-plugin="formMaterial">
                                <label class="form-control-label" for="tgl_surat">Tanggal Masuk Surat</label>
                                <input type="date" class="form-control" id="tgl_surat1" name="tgl_surat" data-hint="tanggal surat masuk diterima" />
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 row pr-0">
                        <div class="col-md-1 vertical-align">
                            <div class="bg-purple-400 w-p100 vertical-align-middle" style="height: 10px"></div>
                        </div>
                        <div class="col-md-1">
                            <h4>File Surat</h4>
                        </div>
                        <div class="col-md-10 vertical-align pr-5">
                            <div class="bg-purple-400 w-p100 vertical-align-middle" style="height: 10px"></div>
                        </div>
                    </div>
                    <div class="col-md-12 table-responsive-md">
                        <div class="form-group form-material row" data-plugin="formMaterial">
                            <div class="col-md-12">
                                <input type="file" class="form-controll" value="" name="file1" data-height="300" data-plugin="dropify" accept="application/pdf" data-default-file="{{asset('assets/global/photos/placeholder.png')}}">
                                <input type="text" hidden class="form-controll" id="file2"  name="file1" >
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-outline btn-outline-secondary" data-dismiss="modal" id="tutup">Tutup</button>
                    <button type="submit" id="btnSave" class="btn bg-purple-400 text-white">Simpan</button>
                </div>
                </form>

        </div>
    </div>
</div>
<div class="modal h-p80 fade modal-slide-from-bottom p-0" id="ModalSuratKel" aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-simple modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <div class="ribbon ribbon-clip px-10 w-300 h-50" style="z-index:100">
                    <span class="ribbon-inner">
                        <h4 id="judulModal2" class="modal-title text-white"></h4>
                    </span>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>

            <div class="modal-body pt-20" data-keyboard="false" data-backdrop="static" style="height:500px;overflow-y: auto;">
                <form id="formSuratKeluar" name="formSuratKeluar" action="{{route('TamSurKel')}}" method="POST" enctype="multipart/form-data" class="row">
                    <input hidden name="id" id="id" value="">
                    <input hidden name="jenis_surat" id="jenis_surat" value="2"/>
                    @csrf
                    <div class="col-md-12 row pr-0">
                        <div class="col-md-6">
                            <div class="form-group form-material" data-plugin="formMaterial">
                                <label class="form-control-label" for="nama">No Surat Keluar</label>
                                <input type="text" class="form-control" id="no_surat1" name="no_surat" placeholder="Contoh : Ec/2021/102" data-hint="" />
                            </div>
                        </div>
                        <div class="col-md-6 pr-0">
                            <div class="form-group form-material" data-plugin="formMaterial">
                                <label class="form-control-label" for="asal_surat">Ditujukan Kepada</label>
                                <input type="text" class="form-control" id="asal_surat1" name="asal_surat" Placeholder="Contoh : Seluruh OKI POLINEMA PSDKU " data-hint="Nama Instansi Yang Akan Menerima Surat" />
                            </div>

                        </div>
                    </div>
                    <div class="col-md-12 row pr-0">
                        <div class="col-md-6">
                            <div class="form-group form-material" data-plugin="formMaterial">
                                <label class="form-control-label" for="perihal">Perihal Surat</label>
                                <input type="text" class="form-control" id="perihal1" name="perihal" placeholder="Contoh : Surat Undangan Rapat" data-hint="Informasi Keperluan Surat" />
                            </div>
                        </div>
                        <div class="col-md-6 pr-0">
                            <div class="form-group form-material" data-plugin="formMaterial">
                                <label class="form-control-label" for="tgl_surat">Tanggal Masuk Surat</label>
                                <input type="date" class="form-control" id="tgl_surat1" name="tgl_surat" data-hint="tanggal surat masuk diterima" />
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 row pr-0">
                        <div class="col-md-1 vertical-align">
                            <div class="bg-purple-400 w-p100 vertical-align-middle" style="height: 10px"></div>
                        </div>
                        <div class="col-md-1">
                            <h4>File Surat</h4>
                        </div>
                        <div class="col-md-10 vertical-align pr-5">
                            <div class="bg-purple-400 w-p100 vertical-align-middle" style="height: 10px"></div>
                        </div>
                    </div>
                    <div class="col-md-12 table-responsive-md">
                        <div class="form-group form-material row" data-plugin="formMaterial">
                            <div class="col-md-12">
                                <input type="file" class="form-controll" value="" name="file1" data-height="300" data-plugin="dropify" accept="application/pdf" data-default-file="{{asset('assets/global/photos/placeholder.png')}}">
                                <input type="text" hidden class="form-controll" id="file2"  name="file1" >
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-outline btn-outline-secondary" data-dismiss="modal" id="tutup">Tutup</button>
                    <button type="submit" id="btnSave" class="btn bg-purple-400 text-white">Simpan</button>
                </div>
                </form>

        </div>
    </div>
</div>



<script>
$(document).ready(function () {
    // data table server side configurasi
    var table = $('#tbSuratMasuk').DataTable({
        processing: true,
        serverSide: true,
        "scrollY": "250px",
        "scrollx": "1050px",
        "scrollCollapse": true,
        ajax: "{{route('showSurat')}}",
        language: {
                        processing: "Sedang diproses...<img height=50 src='{{asset('assets/images/load3.gif')}}'>"
                    },
        columns: [
            {
                data: 'no',
                name: 'no',
                searchable : false,
                orderable:true
            }, {
                data: 'perihal',
                name: 'perihal',
                orderable:true
            },{
                data: 'no_surat',
                name: 'no_surat',
                orderable:true
            },{
                data: 'asal_surat',
                name: 'asal_surat',
                orderable:true
            },{
                data: 'tgl_surat',
                name: 'tgl_surat',
                orderable:true
            },{
                data: 'aksi',
                name: 'aksi',
                orderable: false,
                searchable: false
            },
        ],
        columnDefs: [
            { className: 'text-center', targets: [0,1,2,3,4,5] },
        ]
    });

    var table1 = $('#tbSuratKeluar').DataTable({
        processing: true,
        serverSide: true,
        "scrollY": "250px",
        "scrollx": "1050px",
        "scrollCollapse": true,
        ajax: "{{route('showSuratKel')}}",
        language: {
                        processing: "Sedang diproses...<img height=50 src='{{asset('assets/images/load3.gif')}}'>"
                    },
        columns: [
            {
                data: 'no',
                name: 'no',
                searchable : false,
                orderable:true
            }, {
                data: 'perihal',
                name: 'perihal',
                orderable:true
            },{
                data: 'no_surat',
                name: 'no_surat',
                orderable:true
            },{
                data: 'asal_surat',
                name: 'asal_surat',
                orderable:true
            },{
                data: 'tgl_surat',
                name: 'tgl_surat',
                orderable:true
            },{
                data: 'aksi',
                name: 'aksi',
                orderable: false,
                searchable: false
            },
        ],
        columnDefs: [
            { className: 'text-center', targets: [0,1,2,3,4,5] },
        ]
    });

    $(document).on('click', '.tap_surat_keluar', function () {
        console.log('ok');
        table1.ajax.reload();
    });    

    // aksi button tambah di klik
    $(document).on('click', '#showMasuk', function () {
        $('#judulModal1').html('Tambah Surat Masuk');
            $('#ModalSuratMas').modal({
                backdrop: 'static',
                keyboard: false,
                show: true
            })
    });

    $(document).on('click', '#showKeluar', function () {
        $('#judulModal2').html('Tambah Surat Keluar');
            $('#ModalSuratKel').modal({
                backdrop: 'static',
                keyboard: false,
                show: true
            })
    });


    $(document).on('click', '.detail', function (e) {
            e.preventDefault();

        $('#judulModal').html('Detail Surat Masuk');
        $('#diter').html($(this).data('created_at'));
        $('#nosur').html('No Surat Masuk');
        $('#assur').html('Asal Surat Masuk');
        $('#tglsur').html('Tanggal Surat');
        $('#persur').html('Perihal Surat Masuk');
        $('#no_surat').val($(this).data('no_surat'));
        $('#asal_surat').val($(this).data('asal_surat'));
        $('#tgl_surat').val($(this).data('tgl_surat'));
        $('#perihal').val($(this).data('perihal'));
        var a = '{{asset("file_surat")}}/'+$(this).data('file');
        console.log(a);
        $('.cobak').html('<iframe id="tampil" src="'+a+'" height="550" width="100%" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen="" allowtransparency="true"  frameborder="0"></iframe>');
        // $('.cobak').html('<iframe id="tampil" src="{{asset('proposal_file')}}/cek 2.pdf" height="550" width="100%" frameborder="0"></iframe>');

        // $('#tampil').attr('src',''+ );
        $('#file').html($(this).data('file'));
            $('#detail').modal({
                backdrop: 'static',
                keyboard: false,
                show: true
            })
    });

    $(document).on('click', '.detail1', function () {
        $('#judulModal').html('Detail Surat Keluar');
        $('#nosur').html('No Surat Keluar');
        $('#assur').html('Ditujukan Kepada');
        $('#tglsur').html('Tanggal Surat');
        $('#persur').html('Perihal Surat Keluar');
        $('#no_surat').val($(this).data('no_surat'));
        $('#asal_surat').val($(this).data('asal_surat'));
        $('#tgl_surat').val($(this).data('tgl_surat'));
        $('#perihal').val($(this).data('perihal'));
        $('#file').html($(this).data('file'));
        var a = '{{asset("file_surat")}}/'+$(this).data('file');
        console.log(a);
        $('.cobak').html('<iframe id="tampil" src="'+a+'" height="550" width="100%" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen="" allowtransparency="true"  frameborder="0"></iframe>');
        // $('.cobak').html('<iframe id="tampil" src="{{asset('proposal_file')}}/cek 2.pdf" height="550" width="100%" frameborder="0"></iframe>');

            $('#detail').modal({
                backdrop: 'static',
                keyboard: false,
                show: true
            })
    });
    //  get url terkini untuk hapus data
    function url_down() {
        // Get Option Wilayah
        var url_current = '{{url()->current()}}';
        var start = url_current.indexOf("/master");
        var url_true = url_current.substring(0,start)+"/file_surat/";
        // console.log(url_true);
        return url_true;
    }

    $('#download').click(function() {
        var i = $('#file').text();
        window.location.href = url_down()+ i;
        return '{{url()->current()}}';
    });

//  get url terkini untuk hapus data
    function url_wilayah() {
        // Get Option Wilayah
        var url_current = '{{url()->current()}}';
        var start = url_current.indexOf("/Surat");
        var url_true = url_current.substring(0,start)+"/Surat/delete/";
        // console.log(url_true);
        return url_true;
    }

    // aksi ketika save data dan update
    $('#btnSave').click(function (e) {
    $(this).html('Sending..');
        $.ajax({
            dataType: 'json',
            success: function (data) {
                $('#formSuratMasuk').trigger('reset');
                $('#formSuratKeluar').trigger('reset');
                $('#btnSave').html('Simpan');
                $('#ModalSuratMas').modal('hide');
                $('#ModalSuratKel').modal('hide');
                table.ajax.reload();
                table1.ajax.reload();
                toastr.success('Kelola Data Berhasil');
            }

        })
    });


    $(document).on('click', '.editData', function () {
        // console.log('sjfhdav');
        $('#judulModal1').html('Edit Surat Masuk');

        $('#id').val($(this).data('id'));
        $('#no_surat1').val($(this).data('no_surat'));
        $('#asal_surat1').val($(this).data('asal_surat'));
        $('#tgl_surat1').val($(this).data('tgl_surat'));
        $('#perihal1').val($(this).data('perihal'));
        $('#file2').val($(this).data('file'));
        $('#ModalSuratMas').modal({
            backdrop: 'static',
            keyboard: false, // to prevent closing with Esc button (if you want this too)
            show: true
        })
    });

    $(document).on('click', '.deleteData', function () {
        console.log('deleted');
        var url = url_wilayah()+$(this).data('id');
        console.log(url);
        swal({
        title: "Apakah anda yakin menghapus data ini?",
        text: "Peringatan!! Data yang telah dihapus tidak bisa dikembalikan.",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-warning",
        confirmButtonText: 'Iya',
        cancelButtonText: "Tidak",
        closeOnConfirm: true,
        closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                cek='update';
                console.log(cek);

                    $.ajax({
                        url: url,
                        type: "GET",
                        dataType: 'json',
                        success: function (data) {
                            console.log(data);
                            toastr.success(data);
                            table.ajax.reload();
                            table1.ajax.reload();

                            },
                        error: function (data) {
                            console.log('Error:', data);
                            toastr.error(data);
                            //$('#modalOrg').modal('show');
                            }
                    });
            }else {
            }
        });
    });
});

</script>

@endsection
