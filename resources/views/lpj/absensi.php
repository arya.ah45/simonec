absensi
6

Tabel Absensi

Struktur entitas pada tabel absensi dapat dilihat pada tabel dibawah dimana tabel absensi memiliki 6 atribut didalamnya yaitu : id sebagai primary key, id_rapat, nim, id_proker, created_at dan updated_at.


Tabel absensi

=-=-=------------------------------------------=-=

kebutuhan_event
14


Struktur entitas pada tabel kebutuhan_event dapat dilihat pada tabel dibawah dimana tabel kebutuhan_event memiliki 14 atribut didalamnya yaitu : id sebagai primary key, id_proker, id_sie_kepanitiaan, keterangan, belanja, satuan, harga_satuan, jumlah, satuan_real, harga_satuan_real, jumlah_real, nota, id_kabel, created_at dan updated_at.

Tabel kebutuhan_event

=-=-=------------------------------------------=-=

kepanitiaan
6

Struktur entitas pada tabel kepanitiaan dapat dilihat pada tabel dibawah dimana tabel kepanitiaan memiliki 6 atribut didalamnya yaitu : id sebagai primary key, id_proker, id_kepanitiaan, nim, created_at dan updated_at.

Tabel kepanitiaan

=-=-=------------------------------------------=-=

lpj
5

Struktur entitas pada tabel lpj dapat dilihat pada tabel dibawah dimana tabel lpj memiliki 5 atribut didalamnya yaitu : id sebagai primary key, proker_id, nama_file, created_at dan updated_at.

Tabel lpj

=-=-=------------------------------------------=-=

proker
21

Struktur entitas pada tabel proker dapat dilihat pada tabel dibawah dimana tabel proker memiliki 21 atribut didalamnya yaitu : id sebagai primary key, nama, tanggal_mulai, tanggal_selesai, dana_usulan, dana_terpakai, dana_turun, ket_tambahan, penanggungjawab, tujuan, subjek_tujuan, created_by, is_final, terlaksana, dana_mandiri, dana_mandiri_real, dana_turun_real, event, created_at dan updated_at.

Tabel proker

=-=-=------------------------------------------=-=

proposal
5

Struktur entitas pada tabel proposal dapat dilihat pada tabel dibawah dimana tabel proposal memiliki 5 atribut didalamnya yaitu : id sebagai primary key, proker_id, nama_file, created_at dan updated_at.

Tabel proposal

=-=-=------------------------------------------=-=

rapat_event
13

Struktur entitas pada tabel rapat_event dapat dilihat pada tabel dibawah dimana tabel rapat_event memiliki 13 atribut didalamnya yaitu : id sebagai primary key,  id_proker, hasil_rapat, moderator, catatan, tanggal_rapat, created_by, pokok_bahasan, jam, tempat, tipe, created_at dan updated_at.

Tabel rapat_event

=-=-=------------------------------------------=-=

review_lpj
8

Struktur entitas pada tabel review_lpj dapat dilihat pada tabel dibawah dimana tabel review_lpj memiliki 8 atribut didalamnya yaitu : id sebagai primary key, lpj_id, kriteria_id, status, catatan, from_user, created_at dan updated_at.

Tabel review_lpj

=-=-=------------------------------------------=-=

review_proposal
8

Struktur entitas pada tabel review_proposal dapat dilihat pada tabel dibawah dimana tabel review_proposal memiliki 8 atribut didalamnya yaitu : id sebagai primary key, proposal_id, kriteria_id, status, catatan, from_user, created_at dan updated_at.

Tabel review_proposal

=-=-=------------------------------------------=-=

approv_lpj
9

Struktur entitas pada tabel approv_lpj dapat dilihat pada tabel dibawah dimana tabel approv_lpj memiliki 9 atribut didalamnya yaitu : id sebagai primary key, lpj_id, status, created_by, send_to, proker_id, catatan, created_at dan updated_at.

Tabel approv_lpj

=-=-=------------------------------------------=-=

approv_proker
8

Struktur entitas pada tabel approv_proker dapat dilihat pada tabel dibawah dimana tabel approv_proker memiliki 8 atribut didalamnya yaitu : id sebagai primary key, proker_id, status, created_by, send_to, catatan, created_at dan updated_at.

Tabel approv_proker

=-=-=------------------------------------------=-=

approv_proposal
9

Struktur entitas pada tabel approv_proposal dapat dilihat pada tabel dibawah dimana tabel approv_proposal memiliki 9 atribut didalamnya yaitu : id sebagai primary key, proposal_id, status, created_by, send_to, proker_id, catatan, , created_at dan updated_at.

Tabel approv_proposal

=-=-=------------------------------------------=-=

m_sie_kepanitiaan
5

Struktur entitas pada tabel m_sie_kepanitiaan dapat dilihat pada tabel dibawah dimana tabel m_sie_kepanitiaan memiliki 5 atribut didalamnya yaitu : id sebagai primary key, nama, created_by, created_at dan updated_at.

Tabel m_sie_kepanitiaan

=-=-=------------------------------------------=-=

m_kriteria_penilaian
total_field

Struktur entitas pada tabel m_kriteria_penilaian dapat dilihat pada tabel dibawah dimana tabel m_kriteria_penilaian memiliki total_field atribut didalamnya yaitu : id sebagai primary key, nama, created_by, is_lpj, for_user, created_at dan updated_at.

Tabel m_kriteria_penilaian

=-=-=------------------------------------------=-=
