@extends('layouts.app')

@section('content')
<div class="row" data-plugin="matchHeight" data-by-row="true">
    <div class="col-xxl-12 col-lg-12">
        <div class="panel shadow panel-bordered">
                <div class="panel-heading">
                    <h3 class="panel-title">Kelola LPJ</h3>
                    <div class="panel-actions ">
                        <a class="panel-action icon wb-minus" aria-expanded="true" data-toggle="panel-collapse" aria-hidden="true"></a>
                        <a class="panel-action icon wb-expand" data-toggle="panel-fullscreen" aria-hidden="true"></a>
                    </div>
                </div>
                <div class="panel-body pb-15 pt-15 ">
                    <div class="row pt-3">
                        <div class="col-xxl-6 col-lg-6" style="border-right: solid rgba(182, 180, 180, 0.496) 1px">
                            <h5>Filter</h5>
                            <dl class="dl-horizontal row">
                                <dt class="col-sm-1">Tahun</dt>
                                <dd class="col-sm-4"><select type="text" class="form-control form-control-sm" id="filter_tahun" name="filter_tahun">
                                </select></dd>                                
                                <dt class="col-sm-1">Bulan</dt>
                                <dd class="col-sm-6"><select type="text" class="form-control form-control-sm" id="filter_bulan" name="filter_bulan">
                                </select></dd>
                            </dl>                                                                                  
                        </div>
                        <div class="col-xxl-6 col-lg-6">
                            <h5>Aksi</h5>
                            @php
                                $session = Session::get('data_user');
                                $nim = $session['nim'];
                                $flag_lv = $session['flag_lv'];                                 
                            @endphp                                
                            @if ($flag_lv == "c")    
                                <button id="btn_add_lpj" class="btn btn-xs mb-10 btn-outline bg-teal-700 grey-100">
                                    <i class="icon fa-plus" aria-hidden="true"></i> Tambah Data
                                </button> 
                            @endif                      
                        </div>
                    </div>                          
                    <div class="row pt-3">
                        <div class="col-xxl-12 col-lg-12 text-center">                        
                            <table class="table table-bordered table-hover table-striped" cellspacing="0" id="tb_lpj">
                                <thead class="text-center">
                                    <tr>
                                        <th data-priority="1">no</th>
                                        <th data-priority="2">Nama Proker</th>
                                        <th data-priority="3">Tanggal upload</th>
                                        <th data-priority="4">File</th>
                                        <th data-priority="5">Status</th>
                                        <th data-priority="6">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody class="text-center">
                                    
                                </tbody>
                                </table>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</div>

<!-- Modal -->
<div class="modal h-p80 fade modal-slide-from-bottom p-0" id="modalLPJ" aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-simple modal-center">
        <div class="modal-content">

            <div class="modal-header">
                <div class="ribbon ribbon-clip px-10 w-300 h-50" style="z-index:100">
                    <span class="ribbon-inner">
                        <h4 id="judulModal" class="modal-title text-white"></h4>
                    </span>
                </div>                   
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                
            </div>

            <form id="formProposal" name="formProposal" enctype="multipart/form-data">
            <div class="modal-body pt-20" data-keyboard="false" data-backdrop="static" style="overflow-y: auto;">
                <div class="row">

                    <input hidden name="action" id="action" value="tambah">
                    <input hidden name="id" id="id" value="">       
                    @csrf

                    <div class="col-md-12">
                        <div class="form-group form-material" data-plugin="formMaterial">
                            <label class="form-control-label" for="proker_id">Proker</label>
                            <select class="form-control" id="proker_id" name="proker_id" data-warning="warn_proker_id"></select>
                            <input hidden type="text" id="proker" name="proker">
                            <div class="red-700 warn_select2 warn_proker_id" hidden> *wajib diisi, tidak boleh kosong</div> 
                        </div>                    
                    </div>                    
                    <div class="col-md-12">
                        <div class="form-group form-material" data-plugin="formMaterial">
                            <label class="form-control-label" for="nama">File LPJ</label>
                            <input type="file" id="file_proposal" name="file_proposal" accept='.pdf' data-show-remove="false"/>
                            <div class="red-700 warn_select2 warn_file" hidden> *wajib diisi, tidak boleh kosong</div> 
                        </div>
                    </div>    
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-outline btn-outline-secondary" data-dismiss="modal" id="tutup">Tutup</button>
                <button type="submit" id="btnSaveProposal" class="btn bg-purple-400 text-white">Simpan</button>
            </div>
            </form>
        </div>
    </div>

</div>

<div class="modal fade " id="modal_review" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-center" role="document">
        <div class="modal-content ">
        <div class="modal-header">
            <h5 class="modal-title" id="judulModalReviewLPJ">Review LPJ</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body pt-0" style="height: 566px;overflow-y: auto;">
            <form  id="formReview" name="formReview" class="row pr-0">
                @csrf

                <div class="col-md-6 float-sm-left">
                    <iframe id="frameFile" height="540" width="100%" frameborder="0"></iframe>
                </div>
                <div class="col-md-6 row" style="margin-left: 0px;height: 540px;overflow-y: auto;">

                    @php
                        $session = Session::get('data_user');
                        $nim = $session['nim'];
                        $flag_lv = $session['flag_lv']; 
                    @endphp
                    @if ($flag_lv == 'b')
                        <h4>Form penilaian LPJ</h4>
                        <div class="w-p100" style="height: 510px;overflow-y: auto;">
                            <input hidden type="text" id="lpj_id" name="lpj_id">
                            <input hidden type="text" id="proker_id_review" name="proker_id">
                            <input hidden type="text" id="status_ok" name="status_ok">
                            @forelse ($kriteria_pen as $key => $item)
                                <div class="card border-primary w-p100 mb-10">
                                    <div class="card-body pb-0">
                                        <div class="form-group form-material row" data-plugin="formMaterial">
                                            <div class="col-md-10">
                                                <label class="form-control-label" for="nama">{{++$key}}. {{$item->nama}}</label>
                                            </div>
                                            <div class="col-md-2 text-right">
                                                {{-- <input type="checkbox" class="status" id="status{{$item->id}}" name="status[]" value="1" data-plugin="switchery" checked data-disabled="true" data-color="#1fa75a" data-size="small"/>                             --}}
                                                <input type="checkbox" class="status" id="status{{$item->id}}" name="status[]" value="1" data-id="{{$item->id}}"/>
                                            </div>
                                            <div class="col-md-12">
                                                <input hidden type="text" id="kriteria_id{{$item->id}}" name="kriteria_id[]" value="{{$item->id}}" data-id="{{$item->id}}">
                                                <textarea class="form-control catatan" id="catatan{{$item->id}}" name="catatan[]" placeholder="catatan" data-hint=""></textarea>
                                            </div>
                                        </div>   
                                    </div>
                                </div>
                                @empty
                            @endforelse
                                <div class="card border-primary w-p100 mb-10">
                                    <div class="card-body pb-0">
                                        <div class="form-group form-material row" data-plugin="formMaterial">
                                            <div class="col-md-12">
                                                <label class="form-control-label" for="nama">Catatan tambahan (optional)</label>
                                            </div>
                                            <div class="col-md-12">
                                                <textarea class="form-control catatan" id="catatan_tambahan" name="catatan_tambahan" placeholder="catatan" data-hint=""></textarea>
                                            </div>
                                        </div>   
                                    </div>
                                </div>     
                        </div> 
                    @elseif ($flag_lv == 'a')
                        <div class="panel-group panel-group-continuous" id="exampleAccordionContinuous" aria-multiselectable="true" role="tablist" style="width: 100%">
                            <div class="panel">
                                <div class="panel-heading" id="exampleHeadingContinuousOne" role="tab">
                                    <a class="panel-title" data-parent="#exampleAccordionContinuous" data-toggle="collapse"
                                        href="#exampleCollapseContinuousOne" aria-controls="exampleCollapseContinuousOne"
                                        aria-expanded="true">
                                        #Detail Proker
                                    </a>
                                </div>
                                <div class="panel-collapse collapse show" id="exampleCollapseContinuousOne" aria-labelledby="exampleHeadingContinuousOne" role="tabpanel">
                                    <div class="panel-body">
                                        <table class="table table-light table-striped table-hover">
                                            <tbody>
                                                <tr>
                                                    <th>Nama Proker</th>
                                                    <td>:</td>
                                                    <td id="vw_nama_proker">Nama Proker</td>
                                                </tr>
                                                <tr>
                                                    <th>Tipe Proker</th>
                                                    <td>:</td>
                                                    <td id="vw_tipe_proker">Tipe Proker</td>
                                                </tr>
                                                <tr>
                                                    <th>Penanggungjawab Proker</th>
                                                    <td>:</td>
                                                    <td id="vw_pj">Penanggungjawab Proker</td>
                                                </tr>
                                                <tr>
                                                    <th>Tanggal Mulai</th>
                                                    <td>:</td>
                                                    <td id="vw_tgl_mulai">Nama Proker</td>
                                                </tr>
                                                <tr>
                                                    <th>Tanggal Selesai</th>
                                                    <td>:</td>
                                                    <td id="vw_tgl_selesai">Nama Proker</td>
                                                </tr>
                                                <tr>
                                                    <th>Tujuan</th>
                                                    <td>:</td>
                                                    <td id="vw_tujuan">Nama Proker</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="panel">
                                <div class="panel-heading" id="exampleHeadingContinuousTwo" role="tab">
                                    <a class="panel-title collapsed" data-parent="#exampleAccordionContinuous" data-toggle="collapse"
                                        href="#exampleCollapseContinuousTwo" aria-controls="exampleCollapseContinuousTwo"
                                        aria-expanded="false">
                                        #Anggaran
                                    </a>
                                </div>
                                <div class="panel-collapse collapse" id="exampleCollapseContinuousTwo" aria-labelledby="exampleHeadingContinuousTwo" role="tabpanel">
                                    <div class="panel-body">
                                        <table class="table table-light table-striped table-hover" style="font-size: 10px">                                         
                                            <thead>
                                                <tr>
                                                    <th>Kebutuhan</th>
                                                    <th>Harga Satuan / satuan</th>
                                                    <th>Qty</th>
                                                    <th>Total</th>
                                                </tr>
                                            </thead>
                                            <tbody id="base_anggaran">                                                                          
                                            </tbody>
                                        </table>                                        
                                        <table class="table table-light table-striped table-hover">                                         
                                            <tbody>
                                                <tr>
                                                    <th>Total Dana Rencana Kebutuhan</th>
                                                    <td>:</td>
                                                    <td id="vw_dana_rencana">Nama Proker</td>
                                                </tr>
                                                <tr>
                                                    <th>Usulan Dana Kampus</th>
                                                    <td>:</td>
                                                    <td id="vw_dana_kampus">Tipe Proker</td>
                                                </tr>
                                                <tr>
                                                    <th>Usulan Dana Mandiri</th>
                                                    <td>:</td>
                                                    <td id="vw_dana_mandiri">Penanggungjawab Proker</td>
                                                </tr>                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="panel">
                                <div class="panel-heading" id="exampleHeadingContinuousTwo" role="tab">
                                    <a class="panel-title collapsed" data-parent="#exampleAccordionContinuous" data-toggle="collapse"
                                        href="#exampleCollapseContinuousFour" aria-controls="exampleCollapseContinuousFour"
                                        aria-expanded="false">
                                        #Panitia
                                    </a>
                                </div>
                                <div class="panel-collapse collapse" id="exampleCollapseContinuousFour" aria-labelledby="exampleHeadingContinuousTwo" role="tabpanel">
                                    <div class="panel-body">
                                        <table class="table table-light table-striped table-hover" style="font-size: 10px">                                         
                                            <thead>
                                                <tr>
                                                    <th>Seksi</th>
                                                    <th>Nama</th>
                                                    <th>Kelas</th>
                                                    <th>Prodi</th>
                                                </tr>
                                            </thead>
                                            <tbody id="base_panitia">                                       
                                            </tbody>
                                        </table>
                                        <table class="table table-light table-striped table-hover">                                         
                                            <tbody>
                                                <tr>
                                                    <th>Total Panitia</th>
                                                    <td>:</td>
                                                    <td id="vw_total_panitia">Nama Proker</td>
                                                </tr>                                             
                                            </tbody>
                                        </table>                                        
                                    </div>
                                </div>
                            </div>
                            <div class="panel">
                                <div class="panel-heading" id="exampleHeadingContinuousThree" role="tab">
                                    <a class="panel-title collapsed" data-parent="#exampleAccordionContinuous" data-toggle="collapse"
                                        href="#exampleCollapseContinuousThree" aria-controls="exampleCollapseContinuousThree"
                                        aria-expanded="false">
                                        #Form penilaian LPJ
                                    </a>
                                </div>
                                <div class="panel-collapse collapse" id="exampleCollapseContinuousThree" aria-labelledby="exampleHeadingContinuousThree" role="tabpanel">
                                    <div class="panel-body">
                                        <input hidden type="text" id="lpj_id" name="lpj_id">
                                        <input hidden type="text" id="proker_id_review" name="proker_id">
                                        <input hidden type="text" id="status_ok" name="status_ok">
                                            @forelse ($kriteria_pen as $key => $item)
                                                <div class="card border-primary w-p100 mb-10">
                                                    <div class="card-body pb-0">
                                                        <div class="form-group form-material row" data-plugin="formMaterial">
                                                            <div class="col-md-10">
                                                                <label class="form-control-label" for="nama">{{++$key}}. {{$item->nama}}</label>
                                                            </div>
                                                            <div class="col-md-2 text-right">
                                                                <input type="checkbox" class="status" id="status{{$item->id}}" name="status[]" value="1" data-id="{{$item->id}}"/>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <input hidden type="text" id="kriteria_id{{$item->id}}" name="kriteria_id[]" value="{{$item->id}}" data-id="{{$item->id}}">
                                                                <textarea class="form-control catatan" id="catatan{{$item->id}}" name="catatan[]" placeholder="catatan" data-hint=""></textarea>
                                                            </div>
                                                        </div>   
                                                    </div>
                                                </div>
                                                @empty
                                            @endforelse
                                                <div class="card border-primary w-p100 mb-10">
                                                    <div class="card-body pb-0">
                                                        <div class="form-group form-material row" data-plugin="formMaterial">
                                                            <div class="col-md-12">
                                                                <label class="form-control-label" for="nama">Catatan tambahan (optional)</label>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <textarea class="form-control catatan" id="catatan_tambahan" name="catatan_tambahan" placeholder="catatan" data-hint=""></textarea>
                                                            </div>
                                                        </div>   
                                                    </div>
                                                </div>                                     
                                    </div>
                                </div>
                            </div>
                        </div>                    
                    @else
                    <div class="col-md-12">
                        <h3 id="judulCatatan">Catatan Perbaikan</h3>
                        <dl id="base_catatan"></dl>
                        <h4 id="judulCatatanTambahan">Catatan Tambahan</h4>
                        <p id="catatanTambahan"></p>
                    </div>
                    @endif
                </div>                
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" id="btnSaveReview" class="btn bg-purple-400 text-white">Simpan</button>
        </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>

var switchery = [];
var check = 1;

function select_proker (idSelect) { 
    $(idSelect).select2({
        minimumInputLength: 0,
        // language: {
        //     inputTooShort: function() {
        //         return 'Masukkan minimal 2 digit angka';
        //     }
        // },         
        allowClear : true,
        searchInputPlaceholder: 'Search...',
        placeholder: '- pilih proker -',
        ajax: {
        cache: true,
        url: "{{ url('proker/select2') }}?jenis=lpj",
        dataType: 'json',
        type: "GET",
        quietMillis: 50,
        data: function (params) {
            // console.log(params)
            return {
            search: $.trim(params.term)
            };
        },
        processResults: function (data) {
        return {
            results: $.map(data, function (item) {
            return {
                text:item.nama,
                id: item.id
                }
            })
            };
        }
        }
    });     
};    

function select2filter(id_tag,url,placeholder) {
    $(id_tag).select2({
        minimumInputLength: 0,
        // language: {
        //     inputTooShort: function() {
        //         return 'Masukkan minimal 2 digit angka';
        //     }
        // },         
        allowClear : true,
        searchInputPlaceholder: 'Search...',
        placeholder: placeholder,
        ajax: {
        cache: true,
        url: url,
        dataType: 'json',
        type: "GET",
        quietMillis: 50,
        data: function (params) {
            // console.log(params)
            return {
            search: $.trim(params.term)
            };
        },
        processResults: function (data) {
        return {
            results: $.map(data, function (item) {
            return {
                text:item.nama,
                id: item.id
                }
            })
            };
        }
        }
    });               
}

            function vld_file_pdf(id_input,col){
                if ($(id_input).val()=='') {
                    $(id_input).addClass('is-invalid').closest('.col-md-'+col).find('.form-control-label').addClass('text-danger');
                    $(id_input).closest('.form-material').addClass('has-danger').find('.red-700').attr('hidden',false);

                    $(document).on('change',id_input, function () {
                        $(id_input).removeClass('is-invalid').closest('.col-md-'+col).find('.form-control-label').removeClass('text-danger');
                        $(this).removeClass('is-invalid').closest('.form-material').removeClass('has-danger').find('.red-700').attr('hidden',true); 
                    });       
                    check = 0;             
                }else{
                    var file_proposal = $(id_input).val();
                    var getExtension = file_proposal.substring(file_proposal.length-4, file_proposal.length)
                    if (getExtension != '.pdf') {
                        check = 0;
                        $(id_input).addClass('is-invalid').closest('.col-md-'+col).find('.form-control-label').addClass('text-danger');
                        $(id_input).closest('.form-material').addClass('has-danger').find('.red-700').attr('hidden',false).html('extensi file harus .pdf');
                        
                        $(document).on('change',id_input, function () {
                            $(id_input).removeClass('is-invalid').closest('.col-md-'+col).find('.form-control-label').removeClass('text-danger');
                            $(this).removeClass('is-invalid').closest('.form-material').removeClass('has-danger').find('.red-700').attr('hidden',true).html(' *wajib diisi, tidak boleh kosong');   
                        });                                             
                    }else{
                        check = 1;
                    }
                }
            }

            function vld_select(id_input,col){
                if ($(id_input).val()==null) {
                    $(id_input).addClass('is-invalid').closest('.col-md-'+col).find('.form-control-label').addClass('text-danger');
                    $(id_input).closest('.form-material').addClass('has-danger').find('.red-700').attr('hidden',false);                 

                    $(document).on('change',id_input, function () {
                        $(id_input).removeClass('is-invalid').closest('.col-md-'+col).find('.form-control-label').removeClass('text-danger');
                        $(this).removeClass('is-invalid').closest('.form-material').removeClass('has-danger').find('.red-700').attr('hidden',true);
                    });
                    check = 0;                    
                }else{
                    check = 1;
                }
            }

$(document).ready(function () {

    var table = $('#tb_lpj').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,        
        dom: '<"top"f>rt<"bottom"ip><"clear">',
        // "scrollY": "250px",
        // "scrollx": "1050px",
        // "scrollCollapse": true,
        ajax: "{{ route('DTlpj') }}",
        language: {
            processing: "Sedang diproses...<img height=50 src='{{asset('assets/images/load3.gif')}}'>"
        },
        columns: [
            {
                data: 'no',
                name: 'no',
                searchable : false,
                orderable:true
            }, {
                data: 'nama_proker',
                name: 'nama_proker',
                orderable:true
            },{
                data: 'tanggal_upload',
                name: 'tanggal_upload',
                orderable: false,
                searchable: false
            },{
                data: 'download_file',
                name: 'download_file',
                orderable: false,
                searchable: false
            },{
                data: 'status',
                name: 'status',
                orderable: false,
                searchable: false
            },{
                data: 'aksi',
                name: 'aksi',
                orderable: false,
                searchable: false
            },
        ],
        columnDefs: [
            { className: 'text-center', targets: [0,2,3,4,5] },
        ]
    });  

    // Switchery initiation
    $('.status').each(function (index, element) {
        var id = $(this).data('id');
        switchery[id] = new Switchery(element); 
    });

    // fix bug title in select2
    $(document).on('mouseenter', '.select2-selection__rendered', function () {
        $('.select2-selection__rendered').removeAttr('title');
    });

    select2filter('#filter_bulan',"{{route('select2bulanLPJ')}}?tahun=0",'semua bulan');
    select2filter('#filter_tahun',"{{route('select2tahunLPJ')}}?bulan=0",'semua tahun');
    
    $(document).on('change', '#filter_bulan', function (e) {
        e.preventDefault();
        // console.log($(this).val());
        var bulan = $(this).val();
        var tahun = $('#filter_tahun').val();

        select2filter('#filter_tahun',"{{route('select2tahunLPJ')}}?bulan="+bulan,'semua tahun');
        table.ajax.url( "{{ route('DTproposal') }}?tahun="+tahun+"&bulan="+bulan ).load();
    });    

    $(document).on('change', '#filter_tahun', function (e) {
        e.preventDefault();
        // console.log($(this).val());
        var bulan = $('#filter_bulan').val();
        var tahun = $(this).val();

        select2filter('#filter_bulan',"{{route('select2bulanLPJ')}}?tahun="+tahun,'semua bulan');
        table.ajax.url( "{{ route('DTproposal') }}?tahun="+tahun+"&bulan="+bulan ).load();
    });  

    $('#formProposal').on('submit', function(event){
        event.preventDefault();
        var action = $('#action').val();
        var title_confirm = '';
        var text_confirm = '';
        var formdata = new FormData(this);

        if (action == "tambah") {
            title_confirm = 'Anda yakin menyimpan data LPJ ini ?';            
            text_confirm = 'Data yang telah disimpan tidak dapat diedit lagi';
        } else {
            title_confirm = 'Anda yakin mengupdate data LPJ ini ?';            
            text_confirm = 'Data yang telah diupdate tidak dapat diedit lagi';
        }
        // urutan validasi jangan sampe keliru karena bermain 1 arus validasi
        vld_select('#proker_id',12);
        vld_file_pdf('#file_proposal',12);
        // console.log(check);
        if (check == 1) {
            swal({
            title: title_confirm,
            text: text_confirm,
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-success",
            confirmButtonText: 'Iya',
            cancelButtonText: "Tidak",
            closeOnConfirm: true,
            closeOnCancel: true
            }, function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url: "{{route('simpanLPJ')}}",
                        method:"POST",
                        data:formdata,
                        dataType:'JSON',
                        contentType: false,
                        cache: false,
                        processData: false,             
                        success: function (data) {
                            console.log(data);
                            table.ajax.reload();
                            $('#formProposal').trigger('reset');            
                                swal({
                                    title: "Sukses!",
                                    text: "Perubahan berhasil disimpan",
                                    type: "success",
                                    showCancelButton: false,
                                    confirmButtonClass: "btn-success",
                                    confirmButtonText: 'OK',
                                    closeOnConfirm: false
                                });                     
                            $('#modalLPJ').modal('hide');    
                            // toastr.success(data.message);
                            },
                        error: function (data) {
                            console.log('Error:', data);
                            toastr.error('terjadi kesalahan teknis, dimohon mencoba lagi beberapa saat');
                            // toastr.success(data.message);
                            //$('#modalOrg').modal('show');
                        }
                    });  
                    return;
                }else {
                    check = 0;
                }
            });                
        }

    })  

    $(document).on('click', '#btnSaveReview', function (e) {
        e.preventDefault();

        $(this).html('Sending..');
        $(this).attr('disabled');
        var isi = '';

        $('.status').each(function (index, element) {
            var id_st = $(this).data('id');
            var data = 0;
            if ($('#status'+id_st).is(":checked")) {
                data = 1;
            }else{
                data = 0;
            }
            isi += ','+data;
        });
        
        $('#status_ok').val(isi);
        // console.log($('#status_ok').val());

        $.ajax({
            data: $('#formReview').serialize(),
            url: "{{route('simpanReviewLPJ')}}",
            type: "POST",
            dataType: 'json',
            success: function (data) {
                console.log(data);
                swal({
                    title: "Sukses!",
                    text: "review telah disimpan",
                    type: "success",
                    showCancelButton: false,
                    confirmButtonClass: "btn-success",
                    confirmButtonText: 'OK',
                    closeOnConfirm: false
                });      
                $('#btnSaveReview').removeAttr('disabled');
                $('#btnSaveReview').html('Simpan');
                $('#modal_review').modal('hide');
                table.ajax.reload();
                // toastr.success(data.message);
            },
            error: function (data) {
                console.log('Error:', data);
                $('#btnSaveReview').html('Simpan');
                toastr.error('terjadi kesalahan teknis, dimohon mencoba lagi beberapa saat');
            }
        })
    });

    $(document).on('click', '.btnDelete', function () {
        console.log('deleted');
        var url = "{{url('lpj/hapus/')}}/"+$(this).data('id');
        // console.log(url);
        swal({
        title: "Apakah anda yakin menghapus data ini?",
        text: "Data yang telah dihapus tidak dapat direcovery",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-warning",
        confirmButtonText: 'Iya',
        cancelButtonText: "Tidak",
        closeOnConfirm: true,
        closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                    $.ajax({
                        url: url,
                        type: "GET",
                        dataType: 'json',
                        success: function (data) {
                            // console.log(data);
                            swal({
                                title: "Sukses!",
                                text: "Data berhasil dihapus",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonClass: "btn-success",
                                confirmButtonText: 'OK',
                                closeOnConfirm: false
                            });                              
                            // toastr.success(data.message);
                            table.ajax.reload();
                            },
                        error: function (data) {
                            console.log('Error:', data);
                            toastr.success(data.message);
                            //$('#modalOrg').modal('show');
                            }
                    });
            }else {
            }
        });
    });

    $('#file_proposal').dropify({
        messages: {
            'default': 'Drag n Drop file LPJ disini',
            'replace': 'Drag and drop or click to replace',
            'remove':  'Remove',
            'error':   'Ooops, something wrong happended.'
        },
        tpl: {
            wrap:            '<div class="dropify-wrapper"></div>',
            loader:          '<div class="dropify-loader"></div>',
            message:         '<div class="dropify-message" ><span class="file-icon" /> <p style="font-size:12pt;">Drag n Drop file LPJ disini</p></div>',
        }
    });
});


$(document).on('click', '#btn_add_lpj', function (e) {
    select_proker ('#proker_id')    
    $('#btnSave').text("Simpan");
    $('#judulModal').text("Form tambah data LPJ");
    $('#modalLPJ').modal({
        backdrop: 'static',
        keyboard: false, // to prevent closing with Esc button (if you want this too)
        show: true  
    })            
});

$(document).on('click', '.btnReview', function (e) {
    var nama_file = $(this).data('nama_file');
    var id_proposal = $(this).data('id');
    var proker_id = $(this).data('proker_id');
    var status = $(this).data('status');
    var catatan = $(this).data('catatan');
    var flag_lv = $(this).data('flag_lv');

    var nama_proker = $(this).data('nama_proker');
    var tipe_proker = $(this).data('tipe_proker');
    var penanggungjawab = $(this).data('penanggungjawab');
    var tanggal_mulai = $(this).data('tanggal_mulai');
    var tanggal_selesai = $(this).data('tanggal_selesai');
    var tujuan = $(this).data('tujuan');
    var dana_usulan = $(this).data('dana_usulan');
    var dana_turun = $(this).data('dana_turun');
    var dana_mandiri = $(this).data('dana_mandiri');

    $('#vw_nama_proker').html(nama_proker);
    $('#vw_tipe_proker').html(tipe_proker);
    $('#vw_pj').html(penanggungjawab);
    $('#vw_tgl_mulai').html(tanggal_mulai);
    $('#vw_tgl_selesai').html(tanggal_selesai);
    $('#vw_tujuan').html(tujuan);
    $('#vw_dana_rencana').html(dana_usulan);
    $('#vw_dana_kampus').html(dana_turun);
    $('#vw_dana_mandiri').html(dana_mandiri);

    if(status != 0 || status != 2){
        var url = "{{url('DTkebutuhan/')}}/"+proker_id;
        $.getJSON(url , function (data) {
            // console.log(data);
            $.each(data, function (i, v) { 
                console.log(v);

                // console.log(no);
                var sub_tot_p = (v.sub_total_p != null) ? v.sub_total_p.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.") : '';
                var sub_tot_r = (v.sub_total_r != null) ? v.sub_total_r.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.") : '';
                var sub_total_p = 'Rp. '+sub_tot_p;
                var sub_total_r = 'Rp. '+sub_tot_r;
                
                var harga_satuan = (v.harga_satuan != null) ? v.harga_satuan.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.") : '';
                var harga_satuan_real = (v.harga_satuan_real != null) ? v.harga_satuan_real.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.") : '';
                var harga_satuan_p = 'Rp. '+harga_satuan;
                var harga_satuan_r = 'Rp. '+harga_satuan_real;

                var form =  '<tr><td>'+v.keterangan+'</td>';
                    form += '<td>'+harga_satuan_r+' / '+v.satuan+'</td>';
                    form += '<td>'+v.jumlah_r+'</td>';
                    form += '<td>'+sub_total_r+'</td></tr>';

                $('#base_anggaran').append(form);                    
            });
        });

        var url_panitia = "{{url('DTpanitia/')}}/"+proker_id;
        $.getJSON(url_panitia , function (data) {
            // console.log(data);
            $.each(data, function (i, v) { 

                console.log(v);
                var form_pan =  '<tr><td>'+v.nama_kepanitiaan+'</td>';
                    form_pan += '<td>'+v.nama_mahasiswa+'</td>';
                    form_pan += '<td>'+v.nama_kelas+'</td>';
                    form_pan += '<td>'+v.nama_prodi_s+'</td></tr>';                                         
                $('#base_panitia').append(form_pan);                
            });
            $('#vw_total_panitia').html(data.length);
        });        
    }

    if(status == 0 || status == 3){
        var url_panitia = "{{url('lpj/review/get/')}}/"+id_proposal+'/'+flag_lv;
        console.log(url_panitia);
        $.getJSON(url_panitia , function (data) {
            console.log(data);
            $.each(data, function (i, v) { 
                console.log(v.status == 1);
                
                if (v.status == 1) {
                    $('#status'+v.kriteria_id).trigger('click').attr("checked", "checked");
                }else{
                }

                $('#kriteria_id'+v.kriteria_id).val(v.kriteria_id);
                $('#catatan'+v.kriteria_id).text(v.catatan);            
            });
        });
        $('#catatan_tambahan').val(catatan);
    }

    $('#frameFile').attr('src','{{asset('lpj_file')}}/'+nama_file);

    $('#lpj_id').val(id_proposal);
    $('#proker_id_review').val(proker_id);

    $('#btnSaveReview').text("Simpan");
    $('#modal_review').modal({
        backdrop: 'static',
        keyboard: false, // to prevent closing with Esc button (if you want this too)
        show: true  
    })
});

$(document).on('click', '.btnCekReview', function (e) {
    var nama_file = $(this).data('nama_file');
    var id_lpj = $(this).data('id');
    var catatan = $(this).data('catatan');
    var flag_lv = $(this).data('flag_lv');

    $('#frameFile').attr('src','{{asset('lpj_file')}}/'+nama_file);

    var url_panitia = "{{url('getHasilReviewLPJ')}}/?id_lpj="+id_lpj+'&flag_lv='+flag_lv;
    $.getJSON(url_panitia , function (data) {
        console.log(data);
        $('#base_catatan').append(data);                
    });

    if (flag_lv == 'a') {
        $('#judulCatatan').html('Catatan perbaikan dari DPK');
    }else{
        $('#judulCatatan').html('Catatan perbaikan dari Ketua UKM');
    }

    $('#judulModalReviewLPJ').html('Hasil Review LPJ');

    $('#catatanTambahan').html(catatan);

    $('.catatan').attr('disabled', true);
    $('#btnSaveReview').attr('hidden', true);

    $('#modal_review').modal({
        backdrop: 'static',
        keyboard: false, // to prevent closing with Esc button (if you want this too)
        show: true  
    })        
});

function setSwitchery(switchElement, checkedBool) {
    if((checkedBool && !switchElement.isChecked()) || (!checkedBool && switchElement.isChecked())) {
        switchElement.setPosition(true);
        switchElement.handleOnchange(true);
    }
}

$("#modal_review").on("hidden.bs.modal", function() {
    $('#judulModalReviewLPJ').html('Review LPJ');

    $('.catatan').removeAttr('disabled');
    $('.catatan').html('');
    $('#base_catatan').html('');                
    $('#base_anggaran').html('');                
    $('#base_panitia').html('');                
    $('#btnSaveReview').attr('hidden', false);

    $('#formReview').trigger('reset');

    $('.status').each(function (index, element) {
        var id = $(this).data('id');
        switchery[id].enable();
        //Checks the switch
        // setSwitchery(switchery[id], true);
        //Unchecks the switch
        setSwitchery(switchery[id], false);        
    });
});

$(document).on('click', '.btn_perbaikan', function (e) {
    e.preventDefault();

    var url_file = '{{asset('proposal_file')}}/'+$(this).data('nama_file');
    var proker_id = $(this).data('proker_id');
    var id_proposal = $(this).data('id');
    var nama_proker = $(this).data('nama_proker');
    var drEvent = $('#file_proposal').dropify({
        defaultFile: url_file
    });
    drEvent = drEvent.data('dropify');
    drEvent.resetPreview();
    drEvent.clearElement();
    drEvent.settings.defaultFile = url_file;
    drEvent.destroy();
    drEvent.init();

    select_proker ('#proker_id')
    
    $('#proker_id').html('<option value="'+proker_id+'" selected>'+nama_proker+'</option>').attr('disabled',true);
    $('#id').val(id_proposal);
    $('#action').val('perbaikan');
    $('#proker').val($('#proker_id :checked').val());

    $('#btnSaveProposal').text("Perbaiki");
    $('#judulModal').text("Form Perbaikan data Proposal");
    $('#modalLPJ').modal({
        backdrop: 'static',
        keyboard: false, // to prevent closing with Esc button (if you want this too)
        show: true  
    })            
});

$("#modalLPJ").on("hidden.bs.modal", function() {
    var drEvent = $('#file_proposal').dropify();
    drEvent = drEvent.data('dropify');
    drEvent.resetPreview();
    drEvent.clearElement();    
    $('#proker_id').html('').attr('disabled',false);
    $('#id').val(null);
    $('#action').val('tambah');

    $('.form-control').removeAttr('disabled', true).removeClass('is-invalid');
    $('#formProposal').trigger('reset');
    $('.form-control-label').removeClass('text-danger')
    $('.form-group').removeClass('has-danger')
    $('.warn_select2').attr('hidden',true);       
    // console.log($('#id').val());
});

</script>
@endpush