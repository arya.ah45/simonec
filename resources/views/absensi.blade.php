@extends('layouts.app')

@section('content')
{{-- Style Scanner QR --}}

<div class="row">
    <div class="col-md-12">
            <div class="panel pan">
              <div class="panel-heading" style="border: none">
                <h3 class="panel-title">Absensi</h3>
                <div class="panel-actions panel-actions-keep">
                  <a class="panel-action icon md-fullscreen" data-toggle="panel-fullscreen" aria-hidden="true"></a>
                </div>
              </div>
              <div class="panel-body">
                <div class="row">
                    <div class="col-md-6 row">
                        <div class="col-md-12">
                            <div class="form-group form-material" data-plugin="formMaterial">
                                <label class="form-control-label" for="id_rapat">Nama Rapat</label>
                                <input type="text" class="form-control" id="id_rapat" name="id_rapat" placeholder="" />
                                <input type="text" class="form-control" id="nama_rapat" name="nama_rapat" value="rapat A" placeholder="" />
                            </div>                            
                        </div>
                        <div class="col-md-12">
                            <div class="form-group form-material" data-plugin="formMaterial">
                                <label class="form-control-label" for="tgl_absen">Tanggal</label>
                                <input type="date" class="form-control" id="tgl_absen" name="tgl_absen" placeholder="" />
                            </div>                            
                        </div>
                        <div class="col-md-12">
                            <div class="form-group form-material" data-plugin="formMaterial">
                                <label class="form-control-label" for="jam_absen">Jam</label>
                                <input type="time" class="form-control" id="jam_absen" name="jam_absen" placeholder="" />
                            </div>                            
                        </div>
                    </div>
                    <div class="col-md-6">
                        <video id="preview2"></video>
                    </div>
                </div>
              </div>
            </div>
</div>


@endsection

@push('scripts')
    <script type="text/javascript">

        $(document).ready(function () {
            scannerInstanscan();
        });

        var scanner = new Instascan.Scanner({
            video: document.getElementById('preview2'),
            scanPeriod: 5,
            mirror: false
        });

        function animasi_leave(this_) {
            $(this_).css({
                "border": "solid",
                "border-color": "#3e8ef7"
            });
        }

        function animasi_enter(this_) {
            $(this_).css({
                "border": "",
                "border-color": ""
            });
        }

        // FUNCTION SCANNER
        function scannerInstanscan() {
            Instascan.Camera.getCameras().then(function(cameras) {
                if (cameras.length > 0) {
                    if (cameras.length > 1) {
                        $("#div_select").show();
                        $("#popup_camera").html("");
                        $("#popup_camera").append("<option value=''> -- Pilih Kamera -- </option>");
                        for (i in cameras) {
                            $("#popup_camera").append("<option value='" + [i] + "'>" + cameras[i].name + "</option>");
                        }

                        if (cameras[1] != "") {
                            scanner.start(cameras[1]);
                            $("#popup_camera").val(1).change();
                        } else {
                            alert('Kamera tidak ditemukan!');
                        }
                    } else {
                        if (cameras[0] != "") {
                            scanner.start(cameras[0]);
                            $("#popup_camera").val(0).change();
                        } else {
                            alert('Kamera tidak ditemukan!');
                        }
                    }

                    $("#popup_camera").change(function() {
                        var id = $("#popup_camera :selected").val();

                        if (id != '') {
                            if (cameras[id] != "") {
                                scanner.start(cameras[id]);
                            } else {
                                alert('Kamera tidak ditemukan!');
                            }
                        }
                    })

                } else {
                    console.error('No cameras found.');
                    alert('No cameras found.');
                }
            }).catch(function(e) {
                console.error(e);
                alert(e);
            });
        }

        scanner.addListener('scan', function(content) {
            if (content != '') {
                console.log(content);
                $('#username').val(content);
                $('#form_login').submit();
                // $("#modal_qr").modal("hide");
                // var item = content.split("||");
                // $('#customer').text(item[1]);
                // $('#no_hp').text(item[2]);
                // $('#paket').text(item[3]);
                // $('#tgl').text(item[6]);
                // $('#berat').text(item[4]);
                // $('#total').text(item[5]);
                // $('#link').attr("data-id",item[0]);
                // $("#dtl_transaksi").modal("show");

                // console.log(item);
                // window.location.href = item;
            } else {
                var item = content.split("||");
                // window.location.href = "/" + item;
                console.log(item);
                setTimeout(function() {
                    swal({
                        title: "Data Error!",
                        text: "Content dari Kode QR tersebut Kosong!",
                        type: "error"
                    }, function() {
                        $("#modal_qr").modal("hide");
                    }, 1000);
                })
            }
        });
    </script>
@endpush