
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap material admin template">
    <meta name="author" content="">

    <title>Login | SIMONEC 0.1 </title>

    <link rel="apple-touch-icon" href="{{asset('assets')}}/images/apple-touch-icon.png">
    <link rel="shortcut icon" href="{{asset('assets')}}/images/favicon.ico">

    <!-- Stylesheets -->
    <link rel="stylesheet" href="{{asset('assets')}}/global/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{asset('assets')}}/global/css/bootstrap-extend.min.css">
    <link rel="stylesheet" href="{{asset('assets')}}/css/site.min.css">

    <!-- Plugins -->
    <link rel="stylesheet" href="{{asset('assets')}}/global/vendor/animsition/animsition.css">
    <link rel="stylesheet" href="{{asset('assets')}}/global/vendor/asscrollable/asScrollable.css">
    <link rel="stylesheet" href="{{asset('assets')}}/global/vendor/switchery/switchery.css">
    <link rel="stylesheet" href="{{asset('assets')}}/global/vendor/intro-js/introjs.css">
    <link rel="stylesheet" href="{{asset('assets')}}/global/vendor/slidepanel/slidePanel.css">
    <link rel="stylesheet" href="{{asset('assets')}}/global/vendor/jquery-mmenu/jquery-mmenu.css">
    <link rel="stylesheet" href="{{asset('assets')}}/global/vendor/flag-icon-css/flag-icon.css">
    <link rel="stylesheet" href="{{asset('assets')}}/global/vendor/waves/waves.css">
        <link rel="stylesheet" href="{{asset('assets')}}/examples/css/pages/login-v3.css">


    <!-- Fonts -->
    <link rel="stylesheet" href="{{asset('assets')}}/global/fonts/material-design/material-design.min.css">
    <link rel="stylesheet" href="{{asset('assets')}}/global/fonts/brand-icons/brand-icons.min.css">
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>

    <script src="{{asset('assets')}}/global/vendor/breakpoints/breakpoints.js"></script>
    <script src="https://rawgit.com/schmich/instascan-builds/master/instascan.min.js"></script>

    <script>
      Breakpoints();
    </script>


<style>
    .page{
        /* background-image: linear-gradient(100deg,#E040FB,#c51162 ); */
        background: url({{asset('background/IMG_20191019_092829.jpg')}}) no-repeat center center fixed;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
    }
    .page .bg-filter{
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background: linear-gradient(170deg,#78088b59,#8a1a3f57 );
    }
    .panel{
        background: rgba(255, 255, 255, 0.87);
    }
</style>
{{-- Switch --}}
<style>
    .onoffswitch {
        position: relative;
        -webkit-user-select:none; -moz-user-select:none; -ms-user-select: none;
    }
    .onoffswitch-checkbox {
        position: absolute;
        opacity: 0;
        pointer-events: none;
    }
    .onoffswitch-label {
        display: block; overflow: hidden; cursor: pointer;
        border: 2px solid #999999; border-radius: 0px;
    }
    .onoffswitch-inner {
        display: block; width: 200%; margin-left: -100%;
        transition: margin 0.3s ease-in 0s;
    }
    .onoffswitch-inner:before, .onoffswitch-inner:after {
        display: block; float: left; width: 50%; height: 31px; padding: 0; line-height: 27px;
        font-size: 14px; color: white; font-family: Trebuchet, Arial, sans-serif; font-weight: bold;
        box-sizing: border-box;
        border: 2px solid transparent;
        background-clip: padding-box;
    }
    .onoffswitch-inner:before {
        content: "QR CODE";
        text-align: center;
        background-color: #CCCCCC; color: #C827F0;
    }
    .onoffswitch-inner:after {
        content: "NORMAL";
        text-align: center;
        background-color: #CCCCCC; color: #858585;
    }
    .onoffswitch-switch {
        display: block; width: 17px; margin: 0px;
        background: #6B6666;
        position: absolute; top: 0; bottom: 0;
        right: .onoffswitch.width;
        transition: all 0.3s ease-in 0s;
    }
    .onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-inner {
        margin-left: 0;
    }
    .onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-switch {
        right: 0px;
        background-color: #C028DB;
    }
</style>

{{-- Style Scanner QR --}}
<style>
    #preview {
        width: 100%;
        height: 250;
        margin: 0px auto;
    }
</style>
  </head>
  <body class="animsition page-login-v3 layout-full">
    <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->


    <!-- Page -->
    <div class="page vertical-align text-center" data-animsition-in="fade-in" data-animsition-out="fade-out">>
      <div class="bg-filter"></div>
        <div class="page-content vertical-align-middle">
        <div class="panel">
          <div class="panel-body">
            <div class="brand">
                <img class="brand-img mb-40" src="{{asset('assets/images/LOGO-simonec.png')}}" height="120px" alt="...">
              <h2 class="brand-text font-size-18 mb-25">LOGIN</h2>
            </div>

            @if(Session::has('QR'))
                <div class="alert alert-danger alert-dismissible mb-25">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{Session::get('QR')}}
                </div>
            @endif

            <form method="post" action="{{route('cek_login')}}" autocomplete="off" id="form_login" class="m-0">
             @csrf
                <div class="onoffswitch mb-30">
                    <input type="checkbox" name="qr" class="onoffswitch-checkbox" id="chk_qr" tabindex="0" value="qr">
                    <label class="onoffswitch-label" for="chk_qr">
                        <span class="onoffswitch-inner"></span>
                        <span class="onoffswitch-switch"></span>
                    </label>
                </div>

                <div class="normal_login mt-10">
                    <div class="form-group form-material floating mt-10 mb-0" data-plugin="formMaterial">
                        <input id="username" type="text" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" autofocus>

                        @error('username')
                            <span class="invalid-feedback text-left" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        <label class="floating-label">Username</label>
                    </div>
                    <div class="form-group form-material floating mt-20" data-plugin="formMaterial">
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password">

                        @error('password')
                            <span class="invalid-feedback text-left" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        <label class="floating-label">Password</label>
                    </div>
                    <button class="btn btn-outline bg-purple-a300 text-white btn-block mt-5">SIGN IN</button>
                </div>
            </form>

            <div class="qr_login">
                <div class="form-group row">
                    <div class="col-md-12" id="div_select" style="display: none">
                        <div class="form-group">
                            <label class="control-label">Kamera</label>
                            <select class="form-control select" id="popup_camera">

                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-12" id="">
                        <video id="preview"></video>
                    </div>
                </div>
            </div>

          </div>
        </div>

        <footer class="page-copyright page-copyright-inverse">
          <p>© {{Date('Y')}} EC PSDKU Polinema Kediri.</p>

        </footer>
      </div>
    </div>
    <!-- End Page -->


    <!-- Core  -->
    <script src="{{asset('assets')}}/global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
    {{-- <script src="{{asset('assets')}}/global/vendor/jquery/jquery.js"></script> --}}
    <script src="{{asset('js')}}/jquery-3.5.1.js"></script>
    <script src="{{asset('assets')}}/global/vendor/popper-js/umd/popper.min.js"></script>
    <script src="{{asset('assets')}}/global/vendor/bootstrap/bootstrap.js"></script>
    <script src="{{asset('assets')}}/global/vendor/animsition/animsition.js"></script>
    <script src="{{asset('assets')}}/global/vendor/mousewheel/jquery.mousewheel.js"></script>
    <script src="{{asset('assets')}}/global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
    <script src="{{asset('assets')}}/global/vendor/asscrollable/jquery-asScrollable.js"></script>
    <script src="{{asset('assets')}}/global/vendor/waves/waves.js"></script>

    <!-- Plugins -->
    <script src="{{asset('assets')}}/global/vendor/jquery-mmenu/jquery.mmenu.min.all.js"></script>
    <script src="{{asset('assets')}}/global/vendor/switchery/switchery.js"></script>
    <script src="{{asset('assets')}}/global/vendor/intro-js/intro.js"></script>
    <script src="{{asset('assets')}}/global/vendor/screenfull/screenfull.js"></script>
    <script src="{{asset('assets')}}/global/vendor/slidepanel/jquery-slidePanel.js"></script>
        <script src="{{asset('assets')}}/global/vendor/jquery-placeholder/jquery.placeholder.js"></script>

    <!-- Scripts -->
    <script src="{{asset('assets')}}/global/js/Component.js"></script>
    <script src="{{asset('assets')}}/global/js/Plugin.js"></script>
    <script src="{{asset('assets')}}/global/js/Base.js"></script>
    <script src="{{asset('assets')}}/global/js/Config.js"></script>

    <script src="{{asset('assets')}}/js/Section/Menubar.js"></script>
    <script src="{{asset('assets')}}/js/Section/Sidebar.js"></script>
    <script src="{{asset('assets')}}/js/Section/PageAside.js"></script>
    <script src="{{asset('assets')}}/js/Section/GridMenu.js"></script>

    <!-- Config -->
    <script src="{{asset('assets')}}/global/js/config/colors.js"></script>
    <script src="{{asset('assets')}}/js/config/tour.js"></script>
    <script>Config.set('assets', '{{asset('assets')}}');</script>

    <!-- Page -->
    <script src="{{asset('assets')}}/js/Site.js"></script>
    <script src="{{asset('assets')}}/global/js/Plugin/asscrollable.js"></script>
    <script src="{{asset('assets')}}/global/js/Plugin/slidepanel.js"></script>
    <script src="{{asset('assets')}}/global/js/Plugin/switchery.js"></script>
        <script src="{{asset('assets')}}/global/js/Plugin/jquery-placeholder.js"></script>
        <script src="{{asset('assets')}}/global/js/Plugin/material.js"></script>

    <script type="text/javascript">

        var scanner = new Instascan.Scanner({
            video: document.getElementById('preview'),
            scanPeriod: 5,
            mirror: false
        });

        function animasi_leave(this_) {
            $(this_).css({
                "border": "solid",
                "border-color": "#3e8ef7"
            });
        }

        function animasi_enter(this_) {
            $(this_).css({
                "border": "",
                "border-color": ""
            });
        }


        // FUNCTION SCANNER
        function scannerInstanscan() {
            Instascan.Camera.getCameras().then(function(cameras) {
                if (cameras.length > 0) {
                    if (cameras.length > 1) {
                        $("#div_select").show();
                        $("#popup_camera").html("");
                        $("#popup_camera").append("<option value=''> -- Pilih Kamera -- </option>");
                        for (i in cameras) {
                            $("#popup_camera").append("<option value='" + [i] + "'>" + cameras[i].name + "</option>");
                        }

                        if (cameras[1] != "") {
                            scanner.start(cameras[1]);
                            $("#popup_camera").val(1).change();
                        } else {
                            alert('Kamera tidak ditemukan!');
                        }
                    } else {
                        if (cameras[0] != "") {
                            scanner.start(cameras[0]);
                            $("#popup_camera").val(0).change();
                        } else {
                            alert('Kamera tidak ditemukan!');
                        }
                    }

                    $("#popup_camera").change(function() {
                        var id = $("#popup_camera :selected").val();

                        if (id != '') {
                            if (cameras[id] != "") {
                                scanner.start(cameras[id]);
                            } else {
                                alert('Kamera tidak ditemukan!');
                            }
                        }
                    })

                } else {
                    console.error('No cameras found.');
                    alert('No cameras found.');
                }
            }).catch(function(e) {
                console.error(e);
                alert(e);
            });
        }

        scanner.addListener('scan', function(content) {
            if (content != '') {
                console.log(content);
                $('#username').val(content);
                $('#form_login').submit();
                // $("#modal_qr").modal("hide");
                // var item = content.split("||");
                // $('#customer').text(item[1]);
                // $('#no_hp').text(item[2]);
                // $('#paket').text(item[3]);
                // $('#tgl').text(item[6]);
                // $('#berat').text(item[4]);
                // $('#total').text(item[5]);
                // $('#link').attr("data-id",item[0]);
                // $("#dtl_transaksi").modal("show");

                // console.log(item);
                // window.location.href = item;
            } else {
                var item = content.split("||");
                // window.location.href = "/" + item;
                console.log(item);
                setTimeout(function() {
                    swal({
                        title: "Data Error!",
                        text: "Content dari Kode QR tersebut Kosong!",
                        type: "error"
                    }, function() {
                        $("#modal_qr").modal("hide");
                    }, 1000);
                })
            }
        });
    </script>
    <script>
      (function(document, window, $){
        'use strict';

        var Site = window.Site;
        $(document).ready(function(){

        Site.run();

        $('.qr_login').attr('style', 'display:none');
        $('#chk_qr').change(function (e) {
            e.preventDefault();
            if($(this).prop('checked') == true){
                $('.normal_login').attr('style', 'display:none');
                $('.qr_login').removeAttr('style', 'display:none');
                scannerInstanscan();
            }else{
                scanner.stop();
                $('.qr_login').attr('style', 'display:none');
                $('.normal_login').removeAttr('style', 'display:none');
            }
        });

        });
      })(document, window, jQuery);
    </script>

  </body>
</html>
