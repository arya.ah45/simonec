<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>{{config('app.name')}}</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 4.1.1 -->
    {{-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@coreui/coreui@2.1.16/dist/css/coreui.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@icon/coreui-icons-free@1.0.1-alpha.1/coreui-icons-free.css">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.3.0/css/flag-icon.min.css"> --}}

    @include('config1.assets')

    <style type="text/css">
    @font-face {
        font-family:'Parikesit';
        src: url("{{asset('public/assets/fonts/Parikesit.otf')}}");
    }
    @font-face {
        font-family:'TechnaSans-Regular';
        src: url("{{asset('public/assets/fonts/TechnaSans-Regular.otf')}}");
    }

    .font1{
      font-family:'Parikesit';
    }
    .font2{
      font-family:'TechnaSans-Regular';
    }
    .font2.bgcol{
      color: #3e8ef7;
      font-family:'TechnaSans-Regular';
    }

    </style>

</head>

<body class="animsition page-login-v2 layout-full page-dark">
    <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->


    <!-- Page -->
    <div class="page" data-animsition-in="fade-in" data-animsition-out="fade-out">
      <div class="page-content">
        <div class="page-brand-info pt-80">
          <div class="brand mb-0 pl-0 pb-0 ">
            <h2 class="brand-text font-size-60 font2 ml-0 mb-0">SIMONEC 1.0</h2>
          </div>
          <p class="font-size-30 mt-0">Sistem Informasi Managemen Englsih Club</p>
          {{-- <p class="font-size-20">Badan Kepegawaian Pendidikan Dan Pelatihan Daerah</p> --}}
          {{-- <p class="font-size-30 mt-0">PSDKU POLINEMA di KEDIRI</p> --}}
        </div>

        <div class="page-login-main pt-70">
          <div class="brand text-center">
            <img class="brand-img mb-40" src="{{asset('assets/images/LOGO-simonec.png')}}" height="120px" alt="...">
          </div>
          <div class="brand hidden-md-up">
            <h3 class="text-center font-size-40 font2 bgcol py-0">SIMONEC 1.0</h3>
          </div>

          <h3 class="font-size-24 font2">LOGIN</h3>
          @if($errors->any())
            <div class="alert alert-danger text-center" style="width:100%;">
                <i class="fa fa-exclamation-triangle">
                </i>
                @foreach($errors->all() as $error)
                {{ $error }}
                @endforeach
            </div>
            @endif
          <form role="form" action="javascript:;" class=" form form_connection"  autocomplete="off" id="formLogin" name="formLogin">
            @csrf
            <div class="form-group form-material floating" data-plugin="formMaterial">
              <input type="text" class="form-control empty" id="username" name="username">
              <label class="floating-label" for="inputEmail">NIM</label>
            </div>
            <div class="form-group form-material floating" data-plugin="formMaterial">
              <input type="password" class="form-control empty {{ $errors->has('password')?'is-invalid':'' }}" id="inputPassword" name="password">
                {{-- @if ($errors->has('password'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
                @endif --}}
              <label class="floating-label" for="inputPassword">Password</label>
            </div>

            <button type="submit" class="btn bg-purple-500 text-white btn-block" id="btnLogin">Masuk</button>
          </form>

          {{-- <p>No account? <a href="register-v2.html">Sign Up</a></p> --}}

          <footer class="page-copyright">
            {{-- <p>WEBSITE BY Creation Studio</p> --}}
            <p class = "font-size-20">© {{date('Y')}}. UKM English Club PSDKU Polinema Di Kediri</p>
          </footer>
        </div>

      </div>
    </div>
    <!-- End Page -->


@include('config1.script')

  </body>
<script>
$(document).ready(function () {
        $(document).on("submit","form.form_connection",function(e){

          var username = $('form.form_connection #username').val();
          var password = $('form.form_connection #inputPassword').val();
           $('#btnLogin').html('Logging in..');
           $.ajax({
                  type: "POST",
                  url:"",
                  data: {
                        "_token": "{{ csrf_token() }}",
                        "username": username,
                        "password": password
                        },
                  dataType:'json',

                  complete:function(msg){
                    if (msg.responseJSON.success == 'ok') {
                      document.location = "";
                    } else {
                      // document.location = "{{ url('/home') }}";
                      $('#formLogin').trigger('reset');
                      $('#btnLogin').html('Masuk');
                    //   toastr.error(msg.responseJSON.success);
                      // console.log(msg.responseJSON.success);
                    }


                  },

            });
         });
});

</script>

</html>
