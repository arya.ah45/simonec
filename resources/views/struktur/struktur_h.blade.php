@extends('layouts.app')

@section('content')
@php
        $user 		= Session::get("data_user");
        $flag_lv = $user['flag_lv'];

@endphp
<div class="row" data-plugin="matchHeight" data-by-row="true">
    <div class="col-xxl-12 col-lg-12">
        <div class="panel shadow panel-bordered">
                <div class="panel-heading">
                    <h3 class="panel-title">Struktur Organisasi</h3>
                    <div class="panel-actions ">
                        <a class="panel-action icon wb-minus" aria-expanded="true" data-toggle="panel-collapse" aria-hidden="true"></a>
                        <a class="panel-action icon wb-expand" data-toggle="panel-fullscreen" aria-hidden="true"></a>
                    </div>
                </div>
<script>
    // $(document).ready(function () {
    //                 $("#chart-container").orgchart({
    //                     // container: $("#chart-container"),
    //                     data: $('#organisation'),
    //                     interactive: true,
    //                     fade: true,
    //                     speed: 'slow'
    //                 });
    // });
    (function($) {
        $(function() {
            // var oc = $('#chart-container').orgchart({
            //   'data': $('#ul-data')
            // });
            var oc = $('#ul-data').orgChart({
                container: $("#chart-container"),
                interactive: true,
                fade: true,
                speed: 'slow'
            });
        });
    })(jQuery);

</script>
                <div class="panel-body pb-15 pt-15 ">
                    <form id="formRiwayat" name="formRiwayat">
                    @csrf
                    <div class="row pt-3">
                        <div class="col-xxl-12 col-lg-12 table-responsive">
                            <h5>Bagan Struktur Organisasi</h5>
                        </div>
                        <div class="col-xxl-12 col-lg-12 table-responsive">
                            <ul id="ul-data" hidden>

                                    <li><em>Ketua UKM </em>
                                        <ul>
                                            <li class="special">Wakil Ketua
                                                <ul>
                                                    <li>Sekertaris I
                                                        <ul>
                                                            <li> Sekertaris II</li>
                                                        </ul>
                                                    </li>
                                                    <li>Divisi
                                                        <ul>
                                                            <li>Reasoning</li>
                                                            <li>News Publication</li>
                                                            <li>Talent
                                                                <ul>
                                                                    <li>Debate</li>
                                                                    <li>Study club</li>
                                                                    <li>Art and Sport</li>
                                                                </ul>
                                                            </li>
                                                            <li>Welafare</li>
                                                            <li>Advocation</li>
                                                        </ul>
                                                    </li>
                                                    <li>Bendahara I
                                                        <ul>
                                                            <li> Bendahara II
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            <div id="chart-container"></div>
                        </div>
                    </div>
                    <div class="row pt-20">
                        <div class="col-xxl-12 col-lg-12 table-responsive">
                            <div class="row">
                                <div class="col-md-6 pt-5">
                                    <h5 class="mb-0">Detail Struktur Organisasi</h5>

                                </div>
                                <div class="col-md-6 pb-15 text-right">
                                    @if ( Session::get('data_user')['flag_lv'] == "c" )
                                    <button class="btn btn-xs btn-round w-60 btn-warning" id="btn_edit_riwayat">Edit</button>
                                    @endif

                                    <button class="btn btn-xs btn-round w-60 btn-secondary" id="btn_cancel_riwayat" hidden="true">cancel</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-12 col-lg-12 table-responsive-md">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group" data-plugin="formMaterial">
                                        <label class="form-control-label" for="pokok_bahasan"><strong>Tahun</strong></label>
                                        <select name="tahun_riwayat" class="form-control-plaintext" id="tahun_riwayat">
                                            @foreach ($list_tahun as $key => $item)
                                                <option {{($list_tahun[$key] == $tahun) ? 'selected' : ''}} value="{{$list_tahun[$key]}}">{{$list_tahun[$key]}}</option>
                                            @endforeach
                                        </select>
                                        <input hidden type="text" name="tahun_rj" id="tahun_rj" value="{{$tahun}}">
                                        <div class="invalid-feedback">*wajib diisi</div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group" data-plugin="formMaterial">
                                        <label class="form-control-label" for="nomor_sk"><strong>Nomor SK</strong></label>
                                        <input disabled type="text" class="form-control-plaintext" id="nomor_sk" name="nomor_sk" placeholder="ex : No 323 tahun 2020" value="{{$nomor_sk}}"/>
                                        <div class="invalid-feedback">*wajib diisi</div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group" data-plugin="formMaterial">
                                        <label class="form-control-label" for="tanggal_sk"><strong>Tanggal SK</strong></label>
                                        <input disabled type="date" class="form-control-plaintext" id="tanggal_sk" name="tanggal_sk" value="{{$tanggal_sk}}"/>
                                        <div class="invalid-feedback">*wajib diisi</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-12 col-lg-12 table-responsive-md">


                            <table class="table table-striped table-hover table-bordered table-responsive-md">
                                <thead class="thead-inverse text-center">
                                    <tr>
                                        <th rowspan="2" width="5%">#</th>
                                        <th rowspan="2" width="25%">Jabatan</th>
                                        <th rowspan="2" width="30%">Nama</th>
                                        <th colspan="3" width="50%">Keterangan</th>
                                        <th rowspan="2" width="3%" class="td_delete" hidden="true"><i class="icon fa-trash" aria-hidden="true"></i></th>
                                    </tr>
                                    <tr>
                                        <th width="10%">Kelas</th>
                                        <th width="15%">Prodi</th>
                                        <th width="15%">NIM</th>
                                    </tr>
                                    </thead>
                                    <tbody id="row_riwayat">
                                        @php
                                            $no = $nos = $cek_angg = $cek_div = 0;
                                            $cek_open_div = "";
                                        @endphp
                                        @foreach ($data as $keys => $item)

                                            @if (strlen($item['kode_struktur']) <= 4)
                                                <tr class="{{$item['kode_struktur']}}">
                                                    <td scope="row"><div class="form-group form-material mb-0" data-plugin="formMaterial"><input disabled type="text" class="form-control-plaintext no" name="no[]" value="{{$no+1}}"/></div></td>
                                                    <td><div class="form-group form-material mb-0" data-plugin="formMaterial"><input disabled type="text" class="form-control-plaintext nama_jabatan" name="nama_jabatan[]" value="{{$item['isi']['nama_jabatan']}}"/></div></td>
                                                    <td><div class="form-group form-material mb-0" data-plugin="formMaterial"><input hidden type="text" class="form-control-plaintext id_rj" name="id_rj[]" value="{{$item['isi']['id_rj']}}"/>
                                                    <input disabled type="text" class="form-control-plaintext pejabat" name="pejabat[]" value="{{$item['isi']['pejabat']}}"/>
                                                    <input hidden type="text" class="form-control-plaintext kode_struktur" name="kode_struktur[]" value="{{$item['kode_struktur']}}"/>
                                                    <input hidden type="text" class="form-control-plaintext kode_struktur_detail" name="kode_struktur_detail[]" value="{{$item['isi']['kode_struktur_detail']}}"/>
                                                    <select hidden  class="form-control-plaintext section_pengurus form-control-sm nim" name="nim[]" data-id="{{$keys}}" data-kode_struktur="{{$item['kode_struktur']}}">
                                                    <option value="{{$item['isi']['nim']}}" selected>{{$item['isi']['pejabat']}}</option></select><div class="red-700 alert_wajib" hidden>*wajib diisi</div></div></td>
                                                    <td><div class="form-group form-material mb-0" data-plugin="formMaterial"><input disabled type="text" class="form-control-plaintext kelas" id="kelas_{{$item['kode_struktur']}}_{{$keys}}" name="kelas[]" value="{{$item['isi']['kelas']}}"/></div></td>
                                                    <td><div class="form-group form-material mb-0" data-plugin="formMaterial"><input disabled type="text" class="form-control-plaintext prodi" id="prodi_{{$item['kode_struktur']}}_{{$keys}}" name="prodi[]" value="{{$item['isi']['prodi']}}"/></div></td>
                                                    <td><div class="form-group form-material mb-0" data-plugin="formMaterial"><input disabled type="text" class="form-control-plaintext nimX"  id="nim_{{$item['kode_struktur']}}_{{$keys}}" name="nimX[]" value="{{$item['isi']['nim']}}"/></div></td>
                                                    <td class="td_delete" hidden="true"></td>
                                                </tr>
                                                @php
                                                    $nos = $no;
                                                    $no++;
                                                @endphp
                                            @else
                                                @foreach ($item['isi'] as $key => $item1)
                                                    @if (substr($item1['nama_jabatan'],0,7) == 'Anggota')
                                                        @php
                                                        $cek_angg += 1;
                                                        @endphp
                                                        @if ($cek_angg == 1)
                                                            <tr  id="{{$item['kode_struktur']}}_{{$key}}" class="{{$item['kode_struktur']}}">
                                                                <td scope="row"><div class="form-group form-material mb-0" data-plugin="formMaterial"><input disabled type="text" class="form-control-plaintext no" name="no[]" value="{{$no+1}}"/></div></td>
                                                                <td id="span_{{$item['kode_struktur']}}" {{($item['total_anggota'] == 0) ? '' : 'rowspan='.$item['total_anggota']}} data-row="{{$item['total_anggota']}}"><div class="form-group form-material mb-0" data-plugin="formMaterial">
                                                                <input disabled type="text" class="form-control-plaintext nama_jabatan" name="nama_jabatan[]" value="{{$item1['nama_jabatan']}}"/></div>
                                                                <button hidden class="btn btn-xs btn-round w-100 btn-success addRow" data-kode_struktur="{{$item['kode_struktur']}}" data-kode_struktur_detail="{{$item1['kode_struktur_detail']}}">+ tambah data</button></td></td>
                                                                <td><div class="form-group form-material mb-0" data-plugin="formMaterial">
                                                                <input hidden type="text" class="form-control-plaintext id_rj" name="id_rj[]" value="{{$item1['id_rj']}}"/>
                                                                <input disabled type="text" class="form-control-plaintext pejabat" name="pejabat[]" value="{{$item1['pejabat']}}"/>
                                                                <input hidden type="text" class="form-control-plaintext kode_struktur" name="kode_struktur[]" value="{{$item['kode_struktur']}}"/>
                                                                <input hidden type="text" class="form-control-plaintext kode_struktur_detail" name="kode_struktur_detail[]" value="{{$item1['kode_struktur_detail']}}"/>
                                                                <select hidden   class="form-control-plaintext section_pengurus form-control-sm nim" name="nim[]" data-id="{{$key}}" data-kode_struktur="{{$item['kode_struktur']}}">
                                                                <option value="{{$item1['nim']}}" selected>{{$item1['pejabat']}}</option></select><div class="red-700 alert_wajib" hidden>*wajib diisi</div></div></td>
                                                                <td><div class="form-group form-material mb-0" data-plugin="formMaterial"><input disabled type="text" class="form-control-plaintext kelas" id="kelas_{{$item['kode_struktur']}}_{{$key}}" name="kelas[]" value="{{$item1['kelas']}}"/></div></td>
                                                                <td><div class="form-group form-material mb-0" data-plugin="formMaterial"><input disabled type="text" class="form-control-plaintext prodi" id="prodi_{{$item['kode_struktur']}}_{{$key}}" name="prodi[]" value="{{$item1['prodi']}}"/></div></td>
                                                                <td><div class="form-group form-material mb-0" data-plugin="formMaterial"><input disabled type="text" class="form-control-plaintext nimX" id="nim_{{$item['kode_struktur']}}_{{$key}}" name="nimX[]" value="{{$item1['nim']}}"/></div></td>
                                                                <td class="td_delete" hidden="true"></td>
                                                            </tr>
                                                        @else
                                                            <tr id="{{$item['kode_struktur']}}_{{$key}}" class="{{$item['kode_struktur']}}">
                                                                <td scope="row"><div class="form-group form-material mb-0" data-plugin="formMaterial"><input disabled type="text" class="form-control-plaintext no" name="no[]" value="{{$no+1}}"/></div></td>
                                                                <td hidden><div class="form-group form-material mb-0" data-plugin="formMaterial"><input disabled type="text" class="form-control-plaintext nama_jabatan" name="nama_jabatan[]" value="{{$item1['nama_jabatan']}}"/></div>
                                                                <td><div class="form-group form-material mb-0" data-plugin="formMaterial">
                                                                <input hidden type="text" class="form-control-plaintext id_rj" name="id_rj[]" value="{{$item1['id_rj']}}"/>
                                                                <input disabled type="text" class="form-control-plaintext pejabat" name="pejabat[]" value="{{$item1['pejabat']}}"/>
                                                                <input hidden type="text" class="form-control-plaintext kode_struktur" name="kode_struktur[]" value="{{$item['kode_struktur']}}"/>
                                                                <input hidden type="text" class="form-control-plaintext kode_struktur_detail" name="kode_struktur_detail[]" value="{{$item1['kode_struktur_detail']}}"/>
                                                                <select hidden   class="form-control-plaintext section_pengurus form-control-sm nim" name="nim[]" data-id="{{$key}}" data-kode_struktur="{{$item['kode_struktur']}}">
                                                                <option value="{{$item1['nim']}}" selected>{{$item1['pejabat']}}</option></select><div class="red-700 alert_wajib" hidden>*wajib diisi</div></div></td>
                                                                <td><div class="form-group form-material mb-0" data-plugin="formMaterial"><input disabled type="text" class="form-control-plaintext kelas" id="kelas_{{$item['kode_struktur']}}_{{$key}}" name="kelas[]" value="{{$item1['kelas']}}"/></div></td>
                                                                <td><div class="form-group form-material mb-0" data-plugin="formMaterial"><input disabled type="text" class="form-control-plaintext prodi" id="prodi_{{$item['kode_struktur']}}_{{$key}}" name="prodi[]" value="{{$item1['prodi']}}"/></div></td>
                                                                <td><div class="form-group form-material mb-0" data-plugin="formMaterial"><input disabled type="text" class="form-control-plaintext nimX" id="nim_{{$item['kode_struktur']}}_{{$key}}" name="nimX[]" value="{{$item1['nim']}}"/></div></td>
                                                                <td class="text-center pt-15 td_delete" hidden="true"><button type="button" class="btn  btn-danger btn-xs rmRow" data-kode_struktur="{{$item['kode_struktur']}}" data-id="{{$key}}" data-id_rj="{{$item1['id_rj']}}">x</button></td>
                                                            </tr>
                                                        @endif
                                                    @else
                                                        <tr class="{{$item['kode_struktur']}}">
                                                            <td scope="row"><div class="form-group form-material mb-0" data-plugin="formMaterial"><input disabled type="text" class="form-control-plaintext no" name="no[]" value="{{$no+1}}"/></div></td>
                                                            <td><div class="form-group form-material mb-0" data-plugin="formMaterial"><input disabled type="text" class="form-control-plaintext nama_jabatan" name="nama_jabatan[]" value="{{$item1['nama_jabatan']}}"/></div></td></td>
                                                            <td><div class="form-group form-material mb-0" data-plugin="formMaterial">
                                                            <input hidden type="text" class="form-control-plaintext id_rj" name="id_rj[]" value="{{$item1['id_rj']}}"/>
                                                            <input disabled type="text" class="form-control-plaintext pejabat" name="pejabat[]" value="{{$item1['pejabat']}}"/>
                                                            <input hidden type="text" class="form-control-plaintext kode_struktur" name="kode_struktur[]" value="{{$item['kode_struktur']}}"/>
                                                            <input hidden type="text" class="form-control-plaintext kode_struktur_detail" name="kode_struktur_detail[]" value="{{$item1['kode_struktur_detail']}}"/>
                                                            <select hidden  class="form-control-plaintext section_pengurus form-control-sm nim" name="nim[]" data-id="{{$key}}" data-kode_struktur="{{$item['kode_struktur']}}">
                                                            <option value="{{$item1['nim']}}" selected>{{$item1['pejabat']}}</option></select><div class="red-700 alert_wajib" hidden>*wajib diisi</div></div></td>
                                                            <td><div class="form-group form-material mb-0" data-plugin="formMaterial"><input disabled type="text" class="form-control-plaintext kelas" id="kelas_{{$item['kode_struktur']}}_{{$key}}" name="kelas[]" value="{{$item1['kelas']}}"/></div></td>
                                                            <td><div class="form-group form-material mb-0" data-plugin="formMaterial"><input disabled type="text" class="form-control-plaintext prodi" id="prodi_{{$item['kode_struktur']}}_{{$key}}" name="prodi[]" value="{{$item1['prodi']}}"/></div></td>
                                                            <td><div class="form-group form-material mb-0" data-plugin="formMaterial"><input disabled type="text" class="form-control-plaintext nimX" id="nim_{{$item['kode_struktur']}}_{{$key}}" name="nimX[]" value="{{$item1['nim']}}"/></div></td>
                                                            <td class="td_delete" hidden="true"></td>
                                                        </tr>
                                                    @endif
                                                @php
                                                    $nos = $no;
                                                    $no++;
                                                @endphp
                                                @endforeach
                                                @php
                                                    $cek_angg = 0;
                                                @endphp
                                            @endif
                                        @endforeach
                                    </tbody>
                            </table>
                            <input hidden type="text" name="deleted_id" id="deleted_id">
                        </div>

                        <div class="col-xxl-12 col-lg-12 table-responsive-md">
                            <button hidden type="button" id="btnSave" class="btn bg-purple-400 text-white">Simpan</button>
                        </div>

                    </div>
                    </form>
                </div>
            </div>

    </div>
</div>

@endsection

@push('scripts')
<script>
var rowspan = rowspanNow = 0;
var id_deleted = '';

function select_nim (idSelect_nim) {
    $(idSelect_nim).select2({
        minimumInputLength: 0,
        // language: {
        //     inputTooShort: function() {
        //         return 'Masukkan minimal 2 digit angka';
        //     }
        // },
        allowClear : true,
        selectionTitleAttribute: false,
        searchInputPlaceholder: 'Search...',
        placeholder: 'pilih NIM',
        ajax: {
        cache: true,
        url: "{{ route('select2AnggotaRiwayat') }}",
        dataType: 'json',
        type: "GET",
        quietMillis: 50,
        data: function (params) {
            // console.log(params)
            return {
            search: $.trim(params.term)
            };
        },
        processResults: function (data) {
        return {
            results: $.map(data, function (item) {
            return {
                text:item.nama,
                id: item.id
                }
            })
            };
        }
        }
    });
};

$(document).ready(function () {
    // fix bug title in select2
    $(document).on('mouseenter', '.select2-selection__rendered', function () {
        $('.select2-selection__rendered').removeAttr('title');
    });

    $('#tahun_riwayat').select2();

    $('.nodes').attr('style', 'width: 100%;');

    var no = nos = cek_angg = 0;
    // var url_riwayat = "{{url('getStruktur/')}}?tahun="+"2020-01-02";
    // $.getJSON(url_riwayat , function (data) {
    //     console.log(data);
    //     $.each(data, function (i, v) {


    //         // var formP =  '<tr class="'+idtag_pan+' baris"><td><div class="form-group form-material mb-0" data-plugin="formMaterial"><input type="text" class="form-control-plaintext section_pengurus form-control-sm pan_no" name="pan_no[]" value="'+ no_pan++ +'"/></div>';
    //         //     formP += '</td><td><div class="form-group form-material mb-0" data-plugin="formMaterial">';

    //         // $('#row_riwayat').append(formP);

    //         // $('.'+idtag_pan).each(function () {
    //         //     nameSelect = '.pan_sie_panitia'+count_pan;
    //         //     selecta(nameSelect);
    //         //     nameSelect2 = '.pan_nim'+count_pan;
    //         //     selecta_nim_panitia(nameSelect2);
    //         //     // console.log(nameSelect);
    //         // });
    //         // count_pan = count_pan + 1;
    //     });
    // });
});


$(document).on('click','#btn_edit_riwayat', function (e) {
    e.preventDefault();
    $('.pejabat').attr('hidden', true);
    $('.addRow').attr('hidden', false);
    $('.td_delete').attr('hidden', false);
    $('#btn_cancel_riwayat').attr('hidden', false);
    $(this).attr('hidden', true);
    $('#btnSave').attr('hidden', false);
    $('#tanggal_sk').addClass('form-control').removeClass('form-control-plaintext').attr('disabled',false);
    $('#nomor_sk').addClass('form-control').removeClass('form-control-plaintext').attr('disabled',false);
    $('#tahun_riwayat').attr('disabled',true);

    $('.nim').attr('hidden', false);
    select_nim ('.nim');
});

$(document).on('click','#btn_cancel_riwayat', function (e) {
    e.preventDefault();
    $('.pejabat').attr('hidden', false);
    $('.addRow').attr('hidden', true);
    $('.td_delete').attr('hidden', true);
    $('#btn_edit_riwayat').attr('hidden', false);
    $(this).attr('hidden', true);
    $('#btnSave').attr('hidden', true);
    $('#tanggal_sk').addClass('form-control-plaintext').removeClass('form-control').attr('disabled',true);
    $('#nomor_sk').addClass('form-control-plaintext').removeClass('form-control').attr('disabled',true);
    $('#tahun_riwayat').attr('disabled',false);

    $('.nim').attr('hidden', true);
    $('.select2').attr('hidden', true);
    $('#tahun_riwayat').select2();

});

$(document).on('click','.rmRow', function (e) {
    e.preventDefault();
    var id = $(this).data('id');
    var kode_struktur = $(this).data('kode_struktur');
    var rowspan = $('#span_'+kode_struktur).data('row');
    var id_rj = $(this).data('id_rj');

    console.log(rowspan);
    // console.log(id+' '+kode_struktur);
    $('#'+kode_struktur+'_'+id).remove();
    var rowspanNow = rowspan - 1;
    $('#span_'+kode_struktur).data('row', rowspanNow).attr('rowspan',rowspanNow);
    // console.log();

    var last_count = 1;
    $('.no').each(function () {
        $(this).val(last_count);
        last_count++;
    });

    id_deleted += id_rj+',';
    $('#deleted_id').val(id_deleted);

});

$(document).on('click','.addRow', function (e) {
    e.preventDefault();
    var kode_struktur = $(this).data('kode_struktur');
    var kode_struktur_detail = $(this).data('kode_struktur_detail');
    var id = $('.'+kode_struktur).length;
    console.log(id);
    var last_id = id - 1;
    var new_id = id;

    var content =  '<tr id="'+kode_struktur+'_'+new_id+'" class="'+kode_struktur+'">';
        content += '<td scope="row"><div class="form-group form-material mb-0" data-plugin="formMaterial">';
        content += '<input disabled type="text" class="form-control-plaintext no" name="no[]" value=""/></div></td>';
        content += '<td hidden><div class="form-group form-material mb-0" data-plugin="formMaterial">';
        content += '<input disabled type="text" class="form-control-plaintext nama_jabatan" name="nama_jabatan[]" value=""/></div>';
        content += '<td><div class="form-group form-material mb-0" data-plugin="formMaterial">';
        content += '<input hidden type="text" class="form-control-plaintext id_rj" name="id_rj[]" value=""/>';
        content += '<input  hidden disabled type="text" class="form-control-plaintext pejabat" name="pejabat[]" value=""/>';
        content += '<input  hidden type="text" class="form-control-plaintext kode_struktur" name="kode_struktur[]" value="'+kode_struktur+'"/>';
        content += '<input  hidden type="text" class="form-control-plaintext kode_struktur_detail" name="kode_struktur_detail[]" value="'+kode_struktur_detail+'"/>';
        content += '<select hidden class="form-control-plaintext section_pengurus form-control-sm nim" name="nim[]" data-id="'+new_id+'" data-kode_struktur="'+kode_struktur+'">';
        content += '</select><div class="red-700 alert_wajib" hidden>*wajib diisi</div></div></td><td><div class="form-group form-material mb-0" data-plugin="formMaterial"><input disabled type="text" class="form-control-plaintext kelas" id="kelas_'+kode_struktur+'_'+new_id+'" name="kelas[]" value=""/></div></td>';
        content += '<td><div class="form-group form-material mb-0" data-plugin="formMaterial"><input disabled type="text" class="form-control-plaintext prodi" id="prodi_'+kode_struktur+'_'+new_id+'" name="prodi[]" value=""/></div></td>';
        content += '<td><div class="form-group form-material mb-0" data-plugin="formMaterial"><input disabled type="text" class="form-control-plaintext nimX" id="nim_'+kode_struktur+'_'+new_id+'" name="nimX[]" value=""/></div></td>';
        content += '<td class="text-center pt-15 td_delete"><button type="button" class="btn  btn-danger btn-xs rmRow" data-kode_struktur="'+kode_struktur+'" data-id="'+new_id+'" data-id_rj="new">x</button></td>                                                            ';
        content += '</tr>';

    // id="prodi_{{$item['kode_struktur']}}_{{$key}}"
    // id="nim_{{$item['kode_struktur']}}_{{$key}}"
    $('#'+kode_struktur+'_'+last_id).after(content);

    if (kode_struktur.length > 8) {
        rowspanNow = $('.'+kode_struktur).length - 1;
    }else{
        rowspanNow = $('.'+kode_struktur).length - 2;
    }
    $('#span_'+kode_struktur).data('row', rowspanNow).attr('rowspan',rowspanNow);

    select_nim ('.nim');

    var last_count = 1;
    $('.no').each(function () {
        $(this).val(last_count);
        last_count++;
    });
});

$(document).on('change','.nim', function (e) {
    var nim = $(this).val();
    var id = $(this).data('id');
    var kode_struktur = $(this).data('kode_struktur');
    var cek = 0;
    var hasil_cek = true;
    // $(this).addClass('is-invalid').closest('.form-material').addClass('has-danger');

    $('.nim').each(function (index, element) {
        if ($(this).val() == nim) {
            cek++;
        }
        if (cek > 1) {
            hasil_cek = false;
            return false;
        }
    });

    if (hasil_cek == true) {
        $.ajax({
            url: "{{url('/anggota/getkelpro/')}}/"+nim,
            type: "GET",
            dataType: 'json',
            success: function (data) {
                if(jQuery.isEmptyObject(data) == false){
                    $('#kelas_'+kode_struktur+'_'+id).val(data.nama_kelas);
                    $('#prodi_'+kode_struktur+'_'+id).val(data.nama_prodi);
                    $('#nim_'+kode_struktur+'_'+id).val(nim);
                }else{}
                },
            error: function (data) {
                console.log('Error:', data);
                // toastr.success(data.message);
                //$('#modalOrg').modal('show');
                }
        });
    }else{
        swal({
        title: "NIM YANG ANDA MASUKAN SAMA",
        text:"setiap orang hanya punya 1 jabatan",
        type: "warning",
        showCancelButton: false,
        confirmButtonClass: "btn-warning",
        confirmButtonText: 'Perbaiki',
        cancelButtonText: "Tidak",
        closeOnConfirm: true,
        closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
            }else {
            }
        });
                $(this).html('').select2('data', {id: null, text: null})
                console.log('TOLOL');
    }
});

$('#btnSave').click(function (e) {
    e.preventDefault();

        var checker = true;
        $('.nim').each(function () {
            // var id = $(this).data('id');
            var kebutuhan = $(this).val();
            var iki = $(this);
            if(kebutuhan == null || kebutuhan == ''){
            // console.log(kebutuhan);
            // console.log(iki.next());
                checker = false;
                iki.next().next().attr('hidden',false);
                $(this).change(function () {
                    $(this).closest('.alert_wajib').attr('hidden',true);;
                });
            }

            // console.log(id+';'+kebutuhan+';'+jumlah+';'+satuan+';'+harga_satuan+';'+sie_panitia);
        });
        // checker = true;
        // console.log(checker);
    if( checker == true ){
        console.log('HORE');
        $(this).html('Sending..');
        $(this).attr('disabled');
        $.ajax({
            data: $('#formRiwayat').serialize(),
            url: "{{route('simpanRiwayatJabatan')}}",
            type: "POST",
            dataType: 'json',
            success: function (data) {
                console.log(data);
                if (data.code == '265') {
                    swal({
                        title: "Error!",
                        text: data.message,
                        type: "warning",
                        showCancelButton: false,
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: 'OK',
                        closeOnConfirm: false
                    });
                    $('#tanggal_sk').focus();
                } else {
                    swal({
                        title: "Sukses!",
                        text: data.message,
                        type: "success",
                        showCancelButton: false,
                        confirmButtonClass: "btn-success",
                        confirmButtonText: 'OK',
                        closeOnConfirm: false
                    });

                    $('.pejabat').attr('hidden', false);
                    $('.addRow').attr('hidden', true);
                    $('.td_delete').attr('hidden', true);
                    $('#btn_edit_riwayat').attr('hidden', false);
                    $('#btn_cancel_riwayat').attr('hidden', true);
                    $('#btnSave').attr('hidden', true);
                    $('#tanggal_sk').addClass('form-control-plaintext').removeClass('form-control').attr('disabled',true);
                    $('#nomor_sk').addClass('form-control-plaintext').removeClass('form-control').attr('disabled',true);
                    $('#tahun_riwayat').attr('disabled',false);

                    $('.nim').attr('hidden', true);
                    $('.select2').attr('hidden', true);
                    $('#tahun_riwayat').select2();
                }
                // $('#formRiwayat').trigger('reset');
                $('#btnSave').removeAttr('disabled');
                $('#btnSave').html('Simpan');

                // toastr.success(data.message);
            },
            error: function (data) {
                console.log('Error:', data);
                $('#btnSave').html('Simpan');
                toastr.error('Kelola Data Gagal');
            }
        })
    }else{
        swal({
            title: "Alert!",
            text: "semua input field wajib di isi",
            type: "warning",
            showCancelButton: false,
            confirmButtonClass: "btn-danger",
            confirmButtonText: 'OK',
            closeOnConfirm: false
        });
    };
});


$(document).on('change','#tahun_riwayat', function (e) {
    var tahun = $('#tahun_riwayat :checked').val();
    console.log(tahun);
    window.location = "{{url('struktur/')}}/"+tahun;

});
</script>
@endpush
