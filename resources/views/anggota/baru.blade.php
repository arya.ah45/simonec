@extends('layouts.app')

@section('content')
@php
        $user 		= Session::get("data_user");
        $flag_lv = $user['flag_lv'];

@endphp
<div class="row main_base" data-by-row="true">  
    <div class="col-xxl-12 col-lg-12 panel_awalan">
        <div class="panel shadow panel-bordered">
            <div class="panel-heading">
                <di class="panel-title">Penerimaan Anggota Baru</di>
                <div class="panel-actions ">
                    <a class="panel-action icon wb-minus" aria-expanded="true" data-toggle="panel-collapse" aria-hidden="true"></a>
                    <a class="panel-action icon wb-expand" data-toggle="panel-fullscreen" aria-hidden="true"></a>
                </div>
            </div>                  
            <div class="panel-body pb-35 pt-35 ">
            <form id="anggota_baru" name="anggota_baru">
                @csrf  
                <div class="row">
                    <div class="col-md-12">
                            <select class="form-control" name="nim_anggota_baru[]" id="nim_anggota_baru" multiple="multiple">
                                @foreach ($data_tes_tulis as $item)
                                    <option value="{{$item->nim}}">{{$item->nama.' ( '.$item->nama_kelas.' / '.$item->nama_prodi.' )'}}</option>
                                @endforeach
                            </select>
                    </div>
                </div>
                <div class="row pt-20">
                    <div class="col-md-12">
                        <dl class="dl-horizontal row">
                            <dt class="col-sm-4">Total anggota baru terpilih :</dt>
                            <dd class="col-sm-8" id="total"></dd>
                        </dl>                    
                    </div>
                    <div class="col-md-12">
                        <button class="btn btn-outline-success btn-block btn_simpan_annew">SIMPAN</button>
                    </div>
                </div>
            </form>
            </div>
        </div>        
    </div>
</div>

@endsection

@push('scripts')
<script>
    $(document).ready(function () {
        // $('#nim_anggota_baru').multiSelect()
        $('#nim_anggota_baru').multiSelect({
            selectableHeader: "<div class='h4'>Calon anggota</div>",
            selectionHeader: "<div class='h4'>Anggota diterima</div>",
            selectableFooter: "<input type='text' class='form-control' autocomplete='off' placeholder='cari nama ..'>",
            selectionFooter: "<input type='text' class='form-control' autocomplete='off' placeholder='cari nama ..'>",
            afterInit: function(ms){
                var that = this,
                    $selectableSearch = that.$selectableUl.next(),
                    $selectionSearch = that.$selectionUl.next(),
                    selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
                    selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';

                that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                .on('keydown', function(e){
                if (e.which === 40){
                    that.$selectableUl.focus();
                    return false;
                }
                });

                that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                .on('keydown', function(e){
                if (e.which == 40){
                    that.$selectionUl.focus();
                    return false;
                }
                });
            },
            afterSelect: function(){
                this.qs1.cache();
                this.qs2.cache();
            },
            afterDeselect: function(){
                this.qs1.cache();
                this.qs2.cache();
            }            
        });        
        $('#ms-nim_anggota_baru').css('width', '100%');
        // $('#nim_anggota_baru').multiSelect('select_all');
    
        var rr = $('#nim_anggota_baru :selected').length;
        $('#total').text(rr);        

    });

    $(document).on('click','.btn_simpan_annew', function (e) {
        e.preventDefault();


        var nim_baru = $('#nim_anggota_baru').val();

        if (nim_baru == 0) {            
            swal({
                title: "Data belum ada yang dipilih!",
                text: "periksa kembali pekerjaan anda",
                type: "warning",
                showCancelButton: false,
                confirmButtonClass: "btn-danger",
                confirmButtonText: 'OK',
                closeOnConfirm: false
            });  
        }else{
            $(this).html('Sending..');
            $(this).attr('disabled',true);            
            $.ajax({
                data: $('#anggota_baru').serialize(),
                url: "{{route('simpan_new_anggota')}}",
                type: "POST",
                dataType: 'json',
                success: function (data) {
                    console.log(data);
                    if (data.code == '200') {
                        swal({
                            title: "Sukses!",
                            text: data.message,
                            type: "success",
                            showCancelButton: false,
                            confirmButtonClass: "btn-success",
                            confirmButtonText: 'OK',
                            closeOnConfirm: false
                        });     
                    }                
                    $('#anggota_baru').trigger('reset');
                    $('.btn_simpan_annew').html('SIMPAN');
                    $('.btn_simpan_annew').attr('disabled',false);   
                    // $('.soal_jawaban').remove();
                    // $("html, body").animate({scrollTop: 0}, 500);                
                },
                error: function (data) {
                    console.log('Error:', data);
                    $('#btnSave').html('Simpan');
                    toastr.error('Kelola Data Gagal');
                }
            })        
        }

    });

$(document).on('change','#nim_anggota_baru',function(){

    var rr = $('#nim_anggota_baru :selected').length;
    $('#total').text(rr);
        console.log(rr);
});
</script>
@endpush