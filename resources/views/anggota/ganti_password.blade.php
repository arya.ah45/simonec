@extends('layouts.app')

@section('content')
@php
        $user 		= Session::get("data_user");
        $flag_lv = $user['flag_lv'];

@endphp
<div class="row main_base" data-by-row="true">  
    <div class="col-xxl-12 col-lg-12 panel_awalan">
        <div class="panel shadow panel-bordered">
            <div class="panel-heading">
                <di class="panel-title">Tabel Akun</di>
                <div class="panel-actions ">
                    <a class="panel-action icon wb-minus" aria-expanded="true" data-toggle="panel-collapse" aria-hidden="true"></a>
                    <a class="panel-action icon wb-expand" data-toggle="panel-fullscreen" aria-hidden="true"></a>
                </div>
            </div>                  
            <div class="panel-body py-10">
                <div class="row py-10">              
                    <div class="col-md-12 table-responsive-xl">
                        <table class="table table-light table-striped table-bordered" style="font-size: 12px;color: rgb(74, 74, 74)" id="tb_password">
                            <thead class="abu-soft">
                                <tr>
                                    <th class="text-center">No.</th>
                                    <th class="text-center">NIM</th>
                                    <th class="text-center">Nama</th>
                                    <th class="text-center">Kelas / Prodi</th>
                                    <th class="text-center">Username</th>
                                    <th class="text-center">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>  
                    </div>
                </div>                 
            </div>
        </div>        
    </div>
</div>


<!-- Modal -->
<div class="modal fade modal-slide-from-bottom p-0" id="modalPassword" aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-simple">
        <div class="modal-content">

            <div class="modal-header">
                <div class="ribbon ribbon-clip px-10 w-300 h-50" style="z-index:100">
                    <span class="ribbon-inner">
                        <h4 id="judulModal" class="modal-title text-white"></h4>
                    </span>
                </div>                   
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                
            </div>

            <div class="modal-body pt-20 pl-20" data-keyboard="false" data-backdrop="static" style="overflow-y: auto;">
                <form id="formPass" name="formPass">
                    @csrf
                    <input hidden name="id_proker" id="id_proker">       

                    <div class="row pt-10 px-20">
                        
                        <div class="col-md-6">
                            <div class="form-group form-material" data-plugin="formMaterial">
                                <label class="form-control-label" for="nim">NIM</label>
                                <input type="text" readonly class="form-control" id="nim" name="nim"/>
                                <div class="invalid-feedback">*wajib diisi</div>                                        
                            </div>
                        </div>        
                        
                        <div class="col-md-6">
                            <div class="form-group form-material" data-plugin="formMaterial">
                                <label class="form-control-label" for="nama">Nama</label>
                                <input type="text" readonly class="form-control" id="nama" name="nama"/>
                                <div class="invalid-feedback">*wajib diisi</div>  
                            </div>
                        </div>
                        
                        <div class="col-md-12">
                            <div class="form-group form-material" data-plugin="formMaterial">
                                <label class="form-control-label" for="username">Username</label>
                                <input type="text" class="form-control" id="username" name="username" placeholder="username.."/>
                                <div class="invalid-feedback">*wajib diisi</div>  
                            </div>
                        </div>
                        
                        <div class="col-md-12">
                            <div class="form-group form-material passW" data-plugin="formMaterial">
                                <label class="form-control-label" for="password">Password</label>
                                <input type="password" min="6" class="form-control inPass" id="password" name="password" placeholder="kata sandi.."/>
                                <div class="invalid-feedback invPass">*wajib diisi</div>  
                            </div>                                
                        </div>    
                        <div class="col-md-12">
                            <div class="form-group form-material passW" data-plugin="formMaterial">
                                <label class="form-control-label" for="re_password">Konfirmasi password</label>
                                <input type="password" min="6" class="form-control inPass" id="re_password" name="re_password" placeholder="ketik ulang password"/>
                                <div class="invalid-feedback invPass">*wajib diisi</div>  
                            </div>
                        </div>   
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-outline btn-outline-secondary" data-dismiss="modal" id="tutup">Tutup</button>
                <button type="button" id="btnSave" class="btn bg-purple-400 text-white">Simpan</button>
            </div>
        </div>
    </div>

</div>
@endsection

@push('scripts')
<script>
var validasi = true;

function vld_text(id_input,col){
    if ($(id_input).val()=='') {
        $(id_input).addClass('is-invalid').closest('.col-md-'+col).find('.form-control-label').addClass('text-danger');
        $(id_input).closest('.form-material').addClass('has-danger');
        $(id_input).focus(function () {
            $(this).removeClass('is-invalid').closest('.form-material').removeClass('has-danger');
        });
        $(id_input).keyup(function () {
            $(this).closest('.col-md-'+col).find('.form-control-label').removeClass('text-danger');
        });
    }
}

$(document).ready(function () {
    var table = $('#tb_password').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,        
        ajax: "{{ route('ganti_password') }}",
        language: {
            processing: "Sedang diproses...<img height=50 src='{{asset('assets/images/load3.gif')}}'>"
        },
        columns: [
            {
                data: 'no',
                name: 'no',
                searchable : false,
                orderable:true
            }, {
                data: 'nim',
                name: 'nim',
                orderable:true,
                searchable: true
            },{
                data: 'nama_mhs',
                name: 'nama_mhs',
                orderable: true,
                searchable: true
            },{
                data: 'kelas_prodi',
                name: 'kelas_prodi',
                orderable: true,
                searchable: true
            },{
                data: 'username',
                name: 'username',
                orderable: true,
                searchable: true
            },{
                data: 'aksi',
                name: 'aksi',
                orderable: true,
                searchable: true
            },
        ],
        columnDefs: [
            { className: 'text-center', targets: [0,2,3,4,5] },
        ]
    });     

    $(document).on('click', '#btnSave', function (e) {
        e.preventDefault;
        vld_text('#username',12);
        vld_text('#password',12);
        vld_text('#re_password',12);

        if (    validasi == true && 
                $('#username').val()!='' &&
                $('#password').val()!='' &&
                $('#re_password').val()!=''
        ) {
            $(this).html('Sending..');
            $(this).attr('disabled');

            $.ajax({
                data: $('#formPass').serialize(),
                url: "{{route('simpanPerubahanPass')}}",
                type: "POST",
                dataType: 'json',
                success: function (data) {
                    console.log(data);

                    $('.invPass').html('*wajib diisi');
                    swal({
                        title: "Sukses!",
                        text: "Password berhasil disimpan!",
                        type: "success",
                        showCancelButton: false,
                        confirmButtonClass: "btn-success",
                        confirmButtonText: 'OK',
                        closeOnConfirm: false
                    });      
                    $('#btnSave').removeAttr('disabled');
                    $('#btnSave').html('Simpan');
                    $('#formPass').trigger('reset');
                    $('#modalPassword').modal('hide');        
                    table.ajax.reload();
                    // toastr.success(data.message);
                },
                error: function (data) {
                    console.log('Error:', data);
                    $('#btnSave').html('Simpan');
                    toastr.error('Kelola Data Gagal');
                }
            })
        }
    });    
});

$(document).on('click', '.btnEdit', function () {
    // console.log('sjfhdav');
    $('#judulModal').html('Edit Akun');
    $('#id').val($(this).data('id'));
    $('#nama').val($(this).data('nama_mhs'));         
    $('#nim').val($(this).data('nim'));         
    $('#username').val($(this).data('username'));         

    $('#modalPassword').modal({
        backdrop: 'static',
        keyboard: false, // to prevent closing with Esc button (if you want this too)
        show: true
    })
});

$(document).on('change', '#re_password', function () {
    // console.log('sjfhdav');passW
    
    var rePass = $(this).val();
    var Pass = $('#password').val();

        console.log((rePass != Pass));
    if (rePass != Pass) {
        $('.passW').addClass('has-danger');
        $('.invPass').html('Password dan konfirmasi password salah');
        $('.inPass').addClass('is-invalid');
        validasi = false;
    }else{
        $('.invPass').html('*wajib diisi');
        $('.passW').removeClass('has-danger');
        $('.inPass').removeClass('is-invalid');

    }
});

$("#modalPassword").on("hidden.bs.modal", function () {
    $('.form-control').removeClass('is-invalid');
    $('.form-group').removeClass('has-danger');
    $('#formPass').trigger('reset');   
    $('.form-control-label').removeClass('text-danger')                    
});
</script>
@endpush