@extends('layouts.app')

@section('content')
<div class="row" data-plugin="matchHeight" data-by-row="true">
@php
    use App\Helpers\DeHelper2;
    use App\Helpers\DeHelper;

    $user 		= Session::get("data_user");
    $flag_lv = $user['flag_lv'];
    $nim = $user['nim'];
    $nama = $user['nama'];
    $key_menu = $user['key_menu'];
    $role = DeHelper2::getRole($key_menu);
@endphp

    <div class="col-lg-3">
        <div class="card card-shadow text-center" style="background-image: linear-gradient(180deg,rgba(255, 255, 255, 0.6),rgba(255, 255, 255, 0.9));">
            <div class="card-block">
                <a class="avatar avatar-lg" href="javascript:void(0)">
                    <img src="{{asset('foto_anggota')}}/{{$data_ang->foto}}" height="200px" alt="...">
                </a>
                <h4 class="profile-user">{{$data_ang->nama}}</h4>
                <p class="profile-job">{{$data_ang->jabatan}}</p>

               <a href="{{route('cetakKartuAnggota')}}"> <button type="button"  class="btn btn-success">Download Kartu Anggota</button></a>
            </div>
            <div class="card-footer">
            <div class="row no-space">
                <div class="col-12">
                <strong class="profile-stat-count">2000</strong>
                <span>Event</span>
                </div>
            </div>
            </div>
        </div>
    </div>



    <div class="col-lg-9">
        <div class="panel shadow panel-bordered">
            <div class="panel-heading">
                <h3 class="panel-title">Profil Anggota</h3>
                <div class="panel-actions">
                    <a class="panel-action icon wb-minus" aria-expanded="true" data-toggle="panel-collapse" aria-hidden="true"></a>
                    <a class="panel-action icon wb-expand" data-toggle="panel-fullscreen" aria-hidden="true"></a>
                </div>
            </div>
            <div class="panel-body pb-0 pt-15 ">
                <form id="profilForm" name="profilForm" enctype="multipart/form-data">
                <div class="row">
                    @csrf
                    <input hidden type="text" id="id" name="id" value="{{$data_ang->id}}">
                    <div class="col-md-6 col-lg-6">
                        <dl class="dl-horizontal row">

                            {{-- ------------NIM------------- --}}
                            <dt class="col-sm-4">
                                <div class="form-group">
                                    <input value="NIM" type="text" disabled name="titik2" class="form-control-plaintext form-control-md text-bold">
                                </div>
                            </dt>
                            <dd class="col-sm-1">
                                <div class="form-group">
                                    <input type="text" disabled name="titik2" class="form-control-plaintext form-control-md" value=":">
                                </div>
                            </dd>
                            <dd class="col-sm-7">
                                <div class="form-group">
                                    <input disabled name="nim" id="nim" value="{{$data_ang->nim}}" type="text" class="form-control form-control-md">
                                </div>
                            </dd>

                            {{-- -------------NAMA------------ --}}
                            <dt class="col-sm-4">
                                <div class="form-group">
                                    <input value="Nama Lengkap" type="text" disabled name="titik2" class="form-control-plaintext form-control-md text-bold">
                                </div>
                            </dt>
                            <dd class="col-sm-1">
                                <div class="form-group">
                                    <input type="text" disabled name="titik2" class="form-control-plaintext form-control-md" value=":">
                                </div>
                            </dd>
                            <dd class="col-sm-7">
                                <div class="form-group">
                                    <input disabled name="nama" id="nama" value="{{$data_ang->nama}}" type="text" class="form-control form-control-md">
                                </div>
                            </dd>

                            {{-- ------------PRODI------------- --}}
                            <dt class="col-sm-4">
                                <div class="form-group">
                                    <input value="Prodi" type="text" disabled name="titik2" class="form-control-plaintext form-control-md text-bold">
                                </div>
                            </dt>
                            <dd class="col-sm-1">
                                <div class="form-group">
                                    <input type="text" disabled name="titik2" class="form-control-plaintext form-control-md" value=":">
                                </div>
                            </dd>
                            <dd class="col-sm-7">
                                <div class="form-group">
                                    <input disabled name="id_prodi_v" id="id_prodi_v" value="{{$data_ang->nama_prodi}}" type="text" class="form-control vieww form-control-md">
                                    <select hidden name="id_prodi" id="id_prodi" type="text" class="form-control form-control-md selectkan">
                                        @forelse ($prodi as $item)
                                        <option {{($data_ang->id_prodi == $item->id) ? 'selected' : '' }} value="{{$item->id}}">{{$item->nama}}</option>
                                        @empty
                                        @endforelse
                                    <select>
                                </div>
                            </dd>

                            {{-- -------------Kelas------------ --}}
                            <dt class="col-sm-4">
                                <div class="form-group">
                                    <input value="Kelas" type="text" disabled name="titik2" class="form-control-plaintext form-control-md text-bold">
                                </div>
                            </dt>
                            <dd class="col-sm-1">
                                <div class="form-group">
                                    <input type="text" disabled name="titik2" class="form-control-plaintext form-control-md" value=":">
                                </div>
                            </dd>
                            <dd class="col-sm-7">
                                <div class="form-group">
                                    <input disabled name="id_kelas_v" id="id_kelas_v" value="{{$data_ang->nama_kelas}}" type="text" class="form-control vieww form-control-md">
                                    <select hidden name="id_kelas" id="id_kelas" type="text" class="form-control form-control-md selectkan">
                                        @forelse ($kelas as $item)
                                        <option {{($data_ang->id_kelas == $item->id) ? 'selected' : '' }} value="{{$item->id}}">{{$item->nama}}</option>
                                        @empty
                                        @endforelse
                                    <select>
                                </div>
                            </dd>

                            {{-- ------------Tanggal lahir------------- --}}
                            <dt class="col-sm-4">
                                <div class="form-group">
                                    <input value="Tanggal Lahir" type="text" disabled name="titik2" class="form-control-plaintext form-control-md text-bold">
                                </div>
                            </dt>
                            <dd class="col-sm-1">
                                <div class="form-group">
                                    <input type="text" disabled name="titik2" class="form-control-plaintext form-control-md" value=":">
                                </div>
                            </dd>
                            <dd class="col-sm-7">
                                <div class="form-group">
                                    <input disabled name="tanggal_lahir" id="tanggal_lahir" value="{{$data_ang->tanggal_lahir}}" type="date" class="form-control form-control-md">
                                </div>
                            </dd>

                            {{-- -------------Jenis Kelamin------------ --}}
                            <dt class="col-sm-4">
                                <div class="form-group">
                                    <input value="Jenis Kelamin" type="text" disabled name="titik2" class="form-control-plaintext form-control-md text-bold">
                                </div>
                            </dt>
                            <dd class="col-sm-1">
                                <div class="form-group">
                                    <input type="text" disabled name="titik2" class="form-control-plaintext form-control-md" value=":">
                                </div>
                            </dd>
                            <dd class="col-sm-7">
                                <div class="form-group">
                                    <input disabled name="jenis_kelamin_v" id="jenis_kelamin_v" value="" type="text" class="form-control vieww form-control-md">
                                    <select hidden name="jenis_kelamin" id="jenis_kelamin" type="text" class="form-control form-control-md selectkan">
                                        <option {{($data_ang->jenis_kelamin == 'L') ? 'selected':''}} value="L">Laki-Laki</option>
                                        <option {{($data_ang->jenis_kelamin == 'P') ? 'selected':''}} value="P">Perempuan</option>
                                    <select>
                                </div>
                            </dd>

                            {{-- ------------Email------------- --}}
                            <dt class="col-sm-4">
                                <div class="form-group">
                                    <input value="E-mail" type="text" disabled name="titik2" class="form-control-plaintext form-control-md text-bold">
                                </div>
                            </dt>
                            <dd class="col-sm-1">
                                <div class="form-group">
                                    <input type="text" disabled name="titik2" class="form-control-plaintext form-control-md" value=":">
                                </div>
                            </dd>
                            <dd class="col-sm-7">
                                <div class="form-group">
                                    <input disabled name="email" id="email" value="{{$data_ang->email}}" type="email" class="form-control form-control-md">
                                </div>
                            </dd>
                        </dl>
                    </div>
                    <div class="col-md-6  col-lg-6">
                        <dl class="dl-horizontal row">

                            {{-- -------------Prov------------ --}}
                            <dt class="col-sm-4">
                                <div class="form-group">
                                    <input value="Provinsi" type="text" disabled name="titik2" class="form-control-plaintext form-control-md text-bold">
                                </div>
                            </dt>
                            <dd class="col-sm-1">
                                <div class="form-group">
                                    <input type="text" disabled name="titik2" class="form-control-plaintext form-control-md" value=":">
                                </div>
                            </dd>
                            <dd class="col-sm-7">
                                <div class="form-group">
                                    <input disabled name="id_provinsi_v" id="id_provinsi_v" value="{{$data_ang->nama_provinsi}}" type="text" class="form-control vieww form-control-md">
                                    <select hidden name="id_provinsi" id="id_provinsi" type="text" class="form-control form-control-md selectkan">
                                        <option value="{{$data_ang->id_provinsi}}">{{$data_ang->nama_provinsi}}</option>
                                    <select>
                                </div>
                            </dd>

                            {{-- -------------Kota------------ --}}
                            <dt class="col-sm-4">
                                <div class="form-group">
                                    <input value="Kota" type="text" disabled name="titik2" class="form-control-plaintext form-control-md text-bold">
                                </div>
                            </dt>
                            <dd class="col-sm-1">
                                <div class="form-group">
                                    <input type="text" disabled name="titik2" class="form-control-plaintext form-control-md" value=":">
                                </div>
                            </dd>
                            <dd class="col-sm-7">
                                <div class="form-group">
                                    <input disabled name="id_kota_v" id="id_kota_v" value="{{$data_ang->nama_kota}}" type="text" class="form-control vieww form-control-md">
                                    <select hidden name="id_kota" id="id_kota" type="text" class="form-control form-control-md selectkan">
                                        <option value="{{$data_ang->id_kota}}">{{$data_ang->nama_kota}}</option>
                                    <select>
                                </div>
                            </dd>

                            {{-- -------------Kecamatan------------ --}}
                            <dt class="col-sm-4">
                                <div class="form-group">
                                    <input value="Kecamatan" type="text" disabled name="titik2" class="form-control-plaintext form-control-md text-bold">
                                </div>
                            </dt>
                            <dd class="col-sm-1">
                                <div class="form-group">
                                    <input type="text" disabled name="titik2" class="form-control-plaintext form-control-md" value=":">
                                </div>
                            </dd>
                            <dd class="col-sm-7">
                                <div class="form-group">
                                    <input disabled name="id_kecamatan_v" id="id_kecamatan_v" value="{{$data_ang->nama_kecamatan}}" type="text" class="form-control vieww form-control-md">
                                    <select hidden name="id_kecamatan" id="id_kecamatan" type="text" class="form-control form-control-md selectkan">
                                        <option value="{{$data_ang->id_kecamatan}}">{{$data_ang->nama_kecamatan}}</option>
                                    <select>
                                </div>
                            </dd>

                            {{-- -------------Desa------------ --}}
                            <dt class="col-sm-4">
                                <div class="form-group">
                                    <input value="Desa" type="text" disabled name="titik2" class="form-control-plaintext form-control-md text-bold">
                                </div>
                            </dt>
                            <dd class="col-sm-1">
                                <div class="form-group">
                                    <input type="text" disabled name="titik2" class="form-control-plaintext form-control-md" value=":">
                                </div>
                            </dd>
                            <dd class="col-sm-7">
                                <div class="form-group">
                                    <input disabled name="id_desa_v" id="id_desa_v" value="{{$data_ang->nama_desa}}" type="text" class="form-control vieww form-control-md">
                                    <select hidden name="id_desa" id="id_desa" type="text" class="form-control form-control-md selectkan">
                                        <option value="{{$data_ang->id_desa}}">{{$data_ang->nama_desa}}</option>
                                    <select>
                                </div>
                            </dd>

                            {{-- ------------No. HP------------- --}}
                            <dt class="col-sm-4">
                                <div class="form-group">
                                    <input value="No. HP" type="text" disabled name="titik2" class="form-control-plaintext form-control-md text-bold">
                                </div>
                            </dt>
                            <dd class="col-sm-1">
                                <div class="form-group">
                                    <input type="text" disabled name="titik2" class="form-control-plaintext form-control-md" value=":">
                                </div>
                            </dd>
                            <dd class="col-sm-7">
                                <div class="form-group">
                                    <input disabled name="no_hp" id="no_hp" value="{{$data_ang->no_hp}}" type="number" class="form-control form-control-md">
                                </div>
                            </dd>

                            {{-- ------------Agama------------- --}}
                            <dt class="col-sm-4">
                                <div class="form-group">
                                    <input value="Agama" type="text" disabled name="titik2" class="form-control-plaintext form-control-md text-bold">
                                </div>
                            </dt>
                            <dd class="col-sm-1">
                                <div class="form-group">
                                    <input type="text" disabled name="titik2" class="form-control-plaintext form-control-md" value=":">
                                </div>
                            </dd>
                            <dd class="col-sm-7">
                                <div class="form-group">
                                    <input disabled name="id_agama_v" id="id_agama_v" value="{{$data_ang->nama_agama}}" type="text" class="form-control vieww form-control-md">
                                    <select hidden name="id_agama" id="id_agama" type="text" class="form-control form-control-md selectkan">
                                        @forelse ($agama as $item)
                                        <option {{($data_ang->id_agama == $item->id) ? 'selected' : '' }} value="{{$item->id}}">{{$item->nama}}</option>
                                        @empty
                                        @endforelse
                                    <select>
                                </div>
                            </dd>

                            {{-- ------------Foto------------- --}}
                            <dt class="col-sm-4">
                                <div class="form-group">
                                    <input value="Foto" type="text" disabled name="titik2" class="form-control-plaintext form-control-md text-bold">
                                </div>
                            </dt>
                            <dd class="col-sm-1">
                                <div class="form-group">
                                    <input type="text" disabled name="titik2" class="form-control-plaintext form-control-md" value=":">
                                </div>
                            </dd>
                            <dd class="col-sm-7">
                                <div class="form-group">
                                    <input disabled name="foto_v" id="foto_v" value="{{$data_ang->foto}}" type="text" class="form-control vieww form-control-md">
                                    <input hidden name="foto" id="foto" type="file" class="form-control form-control-md selectkan">
                                </div>
                            </dd>

                        </dl>
                    </div>
                    <div class="col-xxl-12 col-lg-12 aksi1 pb-20">
                        <a class="btn btn-outline-warning btn-block" id="btnEdit">Edit</a>
                    </div>
                    <div hidden class="col-xxl-12 col-lg-12 text-center aksi2 pb-20">
                        <button class="btn btn-outline-secondary" id="btnCancel">Cancel</button>
                        <button class="btn btn-outline-success">Simpan</button>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>

    <div class="col-xxl-12 col-lg-12">
        <div class="panel shadow panel-bordered ">
                <div class="panel-heading">
                    <h3 class="panel-title">Riwayat Jabatan</h3>
                    <div class="panel-actions ">
                        <a class="panel-action icon wb-minus" aria-expanded="true" data-toggle="panel-collapse" aria-hidden="true"></a>
                        <a class="panel-action icon wb-expand" data-toggle="panel-fullscreen" aria-hidden="true"></a>
                    </div>
                </div>
                <div class="panel-body pb-15 pt-15 ">
                @php
                    $session = Session::get('data_user');
                    $nim = $session['nim'];
                    $flag_lv = $session['flag_lv'];
                @endphp
                @if ($flag_lv == "c")
                    <div class="row pt-3">
                        <div class="col-xxl-5 col-lg-5">
                            <div class="form-group form-material" data-plugin="formMaterial" hidden>
                                <label class="form-control-label" for="anggota">Cari data riwayat</label>
                                <select class="form-control" id="anggota" name="anggota"></select>
                                <input hidden type="text" id="proker" name="proker">
                            </div>
                        </div>
                    </div>
                @endif

                    <div class="row pt-3 pr-20">
                        <div class="col-xxl-12 col-lg-12">
                            <table class="table table-bordered table-hover table-striped" cellspacing="0" id="tb_riwayat">
                                <thead class="text-center">
                                    <tr>
                                        <th>no</th>
                                        <th>NIM</th>
                                        <th>Nama</th>
                                        <th>Jabatan</th>
                                        <th>No SK</th>
                                        <th>TMT</th>
                                        <th>Status</th>
                                        <th>Keanggotaan</th>
                                    </tr>
                                </thead>
                                <tbody class="text-center">

                                </tbody>
                                </table>
                        </div>
                    </div>

                </div>
            </div>
    </div>

    <div class="col-xxl-12 col-lg-12">
        <div class="panel shadow panel-bordered">
                <div class="panel-heading">
                    <h3 class="panel-title">Riwayat Kepanitiaan</h3>
                    <div class="panel-actions ">
                        <a class="panel-action icon wb-minus" aria-expanded="true" data-toggle="panel-collapse" aria-hidden="true"></a>
                        <a class="panel-action icon wb-expand" data-toggle="panel-fullscreen" aria-hidden="true"></a>
                    </div>
                </div>
                <div class="panel-body pb-15 pt-15 ">
                @php
                    $session = Session::get('data_user');
                    $nim = $session['nim'];
                    $flag_lv = $session['flag_lv'];
                @endphp
                @if ($flag_lv == "c")
                    <div class="row pt-3" >
                        <div class="col-xxl-5 col-lg-5">
                            <div class="form-group form-material" data-plugin="formMaterial" hidden>
                                <label class="form-control-label" for="anggota1">Cari data riwayat</label>
                                <select class="form-control" id="anggota1" name="anggota1"></select>
                                <input hidden type="text" id="proker" name="proker">
                            </div>
                        </div>
                    </div>
                @endif

                    <div class="row pt-3 pr-20">
                        <div class="col-xxl-12 col-lg-12">
                            <table class="table table-bordered table-hover table-striped" cellspacing="0" id="tb_riwayat_kepanitiaan">
                                <thead class="text-center">
                                    <tr>
                                        <th>No</th>
                                        <th>NIM</th>
                                        <th>Nama</th>
                                        <th>Bagian / Seksi</th>
                                        <th>Kegiatan / Proker</th>
                                        <th>Tanggal acara</th>
                                    </tr>
                                </thead>
                                <tbody class="text-center">

                                </tbody>
                                </table>
                        </div>
                    </div>

                </div>
            </div>
    </div>

</div>
@endsection


@push('scripts')
<script>

var switchery = [];
var check = 1;
var id_provinsi = id_kota = id_kecamatan = '';

function select2Profil(id_tag,url,placeholder) {
    $(id_tag).select2({
        minimumInputLength: 0,
        // language: {
        //     inputTooShort: function() {
        //         return 'Masukkan minimal 2 digit angka';
        //     }
        // },
        allowClear : true,
        searchInputPlaceholder: 'Search...',
        placeholder: placeholder,
        ajax: {
        cache: true,
        url: url,
        dataType: 'json',
        type: "GET",
        quietMillis: 50,
        data: function (params) {
            // console.log(params)
            return {
            search: $.trim(params.term)
            };
        },
        processResults: function (data) {
        return {
            results: $.map(data, function (item) {
            return {
                text:item.nama,
                id: item.id
                }
            })
            };
        }
        }
    });
}

function vld_file_pdf(id_input,col){
    if ($(id_input).val()=='') {
        $(id_input).addClass('is-invalid').closest('.col-md-'+col).find('.form-control-label').addClass('text-danger');
        $(id_input).closest('.form-material').addClass('has-danger').find('.red-700').attr('hidden',false);

        $(document).on('change',id_input, function () {
            $(id_input).removeClass('is-invalid').closest('.col-md-'+col).find('.form-control-label').removeClass('text-danger');
            $(this).removeClass('is-invalid').closest('.form-material').removeClass('has-danger').find('.red-700').attr('hidden',true);
        });
        check = 0;
    }else{
        var file_proposal = $(id_input).val();
        var getExtension = file_proposal.substring(file_proposal.length-4, file_proposal.length)
        if (getExtension != '.pdf') {
            check = 0;
            $(id_input).addClass('is-invalid').closest('.col-md-'+col).find('.form-control-label').addClass('text-danger');
            $(id_input).closest('.form-material').addClass('has-danger').find('.red-700').attr('hidden',false).html('extensi file harus .pdf');

            $(document).on('change',id_input, function () {
                $(id_input).removeClass('is-invalid').closest('.col-md-'+col).find('.form-control-label').removeClass('text-danger');
                $(this).removeClass('is-invalid').closest('.form-material').removeClass('has-danger').find('.red-700').attr('hidden',true).html(' *wajib diisi, tidak boleh kosong');
            });
        }else{
            check = 1;
        }
    }
}

function vld_select(id_input,col){
    if ($(id_input).val()==null) {
        $(id_input).addClass('is-invalid').closest('.col-md-'+col).find('.form-control-label').addClass('text-danger');
        $(id_input).closest('.form-material').addClass('has-danger').find('.red-700').attr('hidden',false);

        $(document).on('change',id_input, function () {
            $(id_input).removeClass('is-invalid').closest('.col-md-'+col).find('.form-control-label').removeClass('text-danger');
            $(this).removeClass('is-invalid').closest('.form-material').removeClass('has-danger').find('.red-700').attr('hidden',true);
        });
        check = 0;
    }else{
        check = 1;
    }
}

$(document).ready(function () {

      // fix bug title in select2
      $(document).on('mouseenter', '.select2-selection__rendered', function () {
        $('.select2-selection__rendered').removeAttr('title');
    });

    // $('#anggota').select2({
    //     minimumInputLength: 0,
    //     // language: {
    //     //     inputTooShort: function() {
    //     //         return 'Masukkan minimal 2 digit angka';
    //     //     }
    //     // },
    //     allowClear : true,
    //     searchInputPlaceholder: 'Search...',
    //     placeholder: '- pilih anggota -',
    //     ajax: {
    //     cache: true,
    //     url: "{{ route('select2AnggotaAll') }}",
    //     dataType: 'json',
    //     type: "GET",
    //     quietMillis: 50,
    //     data: function (params) {
    //         // console.log(params)
    //         return {
    //         search: $.trim(params.term)
    //         };
    //     },
    //     processResults: function (data) {
    //     return {
    //         results: $.map(data, function (item) {
    //         return {
    //             text:item.nama,
    //             id: item.id
    //             }
    //         })
    //         };
    //     }
    //     }
    // });

    var table = $('#tb_riwayat').DataTable({
        processing: true,
        serverSide: true,
        "scrollY": "250px",
        "scrollx": "1050px",
        "scrollCollapse": true,
        ajax: "{{ route('DTjabatan') }}",
        language: {
            processing: "Sedang diproses...<img height=50 src='{{asset('assets/images/load3.gif')}}'>"
        },
        columns: [
            {
                data: 'no',
                name: 'no',
                searchable : false,
                orderable:true
            }, {
                data: 'nim',
                name: 'nim',
                orderable:true
            },{
                data: 'nama_pejabat',
                name: 'nama_pejabat',
                orderable: false,
                searchable: false
            },{
                data: 'nama_jabatan',
                name: 'nama_jabatan',
                orderable: false,
                searchable: false
            },{
                data: 'no_sk',
                name: 'no_sk',
                orderable: false,
                searchable: false
            },{
                data: 'tgl_sk',
                name: 'tgl_sk',
                orderable: false,
                searchable: false
            },{
                data: 'status',
                name: 'status',
                orderable: false,
                searchable: false
            },{
                data: 'status_keanggotaan',
                name: 'status_keanggotaan',
                orderable: false,
                searchable: false
            },
        ],
        columnDefs: [
            { className: 'text-center', targets: [0,1,5,6] },
        ]
    });

    $(document).on('change', '#anggota', function (e) {
        e.preventDefault();
        // console.log($(this).val());
        table.ajax.url( "{{ route('DTjabatan') }}?nim="+$(this).val() ).load();
    });

    //  kepanitiaan

    // $('#anggota1').select2({
    //     minimumInputLength: 0,
    //     // language: {
    //     //     inputTooShort: function() {
    //     //         return 'Masukkan minimal 2 digit angka';
    //     //     }
    //     // },
    //     allowClear : true,
    //     searchInputPlaceholder: 'Search...',
    //     placeholder: '- pilih anggota -',
    //     ajax: {
    //     cache: true,
    //     url: "{{ route('select2AnggotaAll') }}",
    //     dataType: 'json',
    //     type: "GET",
    //     quietMillis: 50,
    //     data: function (params) {
    //         // console.log(params)
    //         return {
    //         search: $.trim(params.term)
    //         };
    //     },
    //     processResults: function (data) {
    //     return {
    //         results: $.map(data, function (item) {
    //         return {
    //             text:item.nama,
    //             id: item.id
    //             }
    //         })
    //         };
    //     }
    //     }
    // });

    var table1 = $('#tb_riwayat_kepanitiaan').DataTable({
        processing: true,
        serverSide: true,
        "scrollY": "250px",
        "scrollx": "1050px",
        "scrollCollapse": true,
        ajax: "{{ route('DTkepanitiaan') }}",
        language: {
            processing: "Sedang diproses...<img height=50 src='{{asset('assets/images/load3.gif')}}'>"
        },
        columns: [
            {
                data: 'no',
                name: 'no',
                searchable : false,
                orderable:true
            }, {
                data: 'nim',
                name: 'nim',
                orderable:true
            },{
                data: 'nama_pejabat',
                name: 'nama_pejabat',
                orderable: false,
                searchable: false
            },{
                data: 'nama_kepanitiaan',
                name: 'nama_kepanitiaan',
                orderable: false,
                searchable: false
            },{
                data: 'nama_proker',
                name: 'nama_proker',
                orderable: false,
                searchable: false
            },{
                data: 'tgl_proker',
                name: 'tgl_proker',
                orderable: false,
                searchable: false
            },
        ],
        columnDefs: [
            { className: 'text-center', targets: [0,1] },
        ]
    });

    $(document).on('change', '#anggota1', function (e) {
        e.preventDefault();
        // console.log($(this).val());
        table1.ajax.url( "{{ route('DTkepanitiaan') }}?nim="+$(this).val() ).load();
    });


    // Kelamin Show
    var kelam = $('#jenis_kelamin :checked').html();
    $('#jenis_kelamin_v').val(kelam);

    // fix bug title in select2
    $(document).on('mouseenter', '.select2-selection__rendered', function () {
        $('.select2-selection__rendered').removeAttr('title');
    });


    $(document).on('change','#id_provinsi', function (e) {
        e.preventDefault();
        id_provinsi = $(this).val();
        url = "{{ route('select2Kota') }}?id_provinsi="+id_provinsi;
        console.log(id_provinsi);
        select2Profil('#id_kota', url,'Pilih kota..');
    });

    $(document).on('change','#id_kota', function (e) {
        e.preventDefault();
        id_kota = $(this).val();
        url = "{{ route('select2Kecamatan') }}?id_kota="+id_kota;
        console.log(id_kota);
        select2Profil('#id_kecamatan', url,'Pilih kecamatan..');
    });

    $(document).on('change','#id_kecamatan', function (e) {
        e.preventDefault();
        id_kecamatan = $(this).val();
        url = "{{ route('select2Desa') }}?id_kecamatan="+id_kecamatan;
        console.log(id_kecamatan);
        select2Profil('#id_desa', url,'Pilih desa..');
    });



    function cancel() {
        $('.form-control').attr('disabled', true);
        $('.vieww').attr('hidden', false);
        $('.selectkan').attr('hidden', true);
        $('.aksi1').attr('hidden', false);
        $('.aksi2').attr('hidden', true);
        $('.select2').attr('hidden', true);
    }

    $(document).on('click', '#btnCancel', function (e) {
        e.preventDefault();
        cancel();
    });

    $(document).on('click', '#btnEdit', function (e) {
        e.preventDefault();

        $('.form-control').attr('disabled', false);
        $('.vieww').attr('hidden', true);
        $('.selectkan').attr('hidden', false);
        $('.aksi1').attr('hidden', true);
        $('.aksi2').attr('hidden', false);
        $('.select2').attr('hidden', false);

        select2Profil("#id_provinsi","{{ route('select2Provinsi') }}",'Pilih Provinsi');
        $("#id_kota").select2({placeholder: 'Silahkan pilih provinsi terlebih dahulu',});
        $("#id_kecamatan").select2({placeholder: 'Silahkan pilih kota terlebih dahulu',});
        $("#id_desa").select2({placeholder: 'Silahkan pilih kecamatan terlebih dahulu',});

        id_provinsi = $('#id_provinsi').val();
        url = "{{ route('select2Kota') }}?id_provinsi="+id_provinsi;
        select2Profil('#id_kota', url,'Pilih kota..');

        id_kota = $('#id_kota').val();
        url = "{{ route('select2Kecamatan') }}?id_kota="+id_kota;
        select2Profil('#id_kecamatan', url,'Pilih kecamatan..');

        id_kecamatan = $('#id_kecamatan').val();
        url = "{{ route('select2Desa') }}?id_kecamatan="+id_kecamatan;
        select2Profil('#id_desa', url,'Pilih desa..');
    });

    $('#profilForm').on('submit', function(event){
        event.preventDefault();
        var title_confirm = 'Konfirmasi!';
        var text_confirm = 'Anda yakin mengupdate data diri anda?';

        var formdata = new FormData(this);

        swal({
            title: title_confirm,
            text: text_confirm,
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-success",
            confirmButtonText: 'Iya',
            cancelButtonText: "Tidak",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    url: "{{route('simpanProfilAnggota')}}",
                    method:"POST",
                    data:formdata,
                    dataType:'JSON',
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (data) {
                        console.log(data);
                            swal({
                                title: "Sukses!",
                                text: "Perubahan berhasil disimpan",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonClass: "btn-success",
                                confirmButtonText: 'OK',
                                closeOnConfirm: false
                            }, function (isConfirm) {
                                if (isConfirm) {
                                    location.reload(true);
                                    return;
                                }else {
                                    check = 0;
                                }
                            });
                        cancel();
                        },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
                return;
            }else {
                check = 0;
            }
        });
    })
});


</script>
@endpush
