<div class="site-menubar" style="">
    <div class="site-menubar-body pt-20">
        <div>
            <div>
                <ul class="site-menu">
                    @php
                        $data_session = Session::get('data_user');
                        $key_menu = $data_session['key_menu'];
                        $dataSideMenu = DeHelper::sideMenu($key_menu);
                    @endphp

                    @foreach ($dataSideMenu as $item)
                        {!!$item!!}
                    @endforeach 
                </ul>
            </div>
        </div>
    </div>
</div>


