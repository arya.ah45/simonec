    <nav class="site-navbar navbar navbar-default navbar-fixed-top navbar-mega" role="navigation">

        <div class="navbar-header bg-purple-a200 ">
            <button type="button" class="navbar-toggler hamburger hamburger-close navbar-toggler-left hided"
                data-toggle="menubar">
                <span class="sr-only">Toggle navigation</span>
                <span class="hamburger-bar"></span>
            </button>
            <button type="button" class="navbar-toggler collapsed" data-target="#site-navbar-collapse"
                data-toggle="collapse">
                <i class="icon md-more" aria-hidden="true"></i>
            </button>
            <div class="navbar-brand navbar-brand-center">
                <img class="navbar-brand-logo"  src="{{asset('assets/images/LOGO-simonec-dark.png')}}" title="">
                <span class="navbar-brand-text hidden-xs-down" >SIMONEC</span>
            </div>
        </div>

        <div class="navbar-container container-fluid bg-purple-a300 text-white ">
            <!-- Navbar Collapse -->
            <div class="collapse navbar-collapse navbar-collapse-toolbar" id="site-navbar-collapse" style="border: none">
                <!-- Navbar Toolbar -->
                <ul class="nav navbar-toolbar">
                    <li class="nav-item hidden-sm-down" id="toggleFullscreen">
                        <a class="nav-link icon icon-fullscreen grey-100" data-toggle="fullscreen" href="#" role="button">
                            <span class="sr-only">Toggle fullscreen</span>
                        </a>
                    </li>
                </ul>
                <!-- End Navbar Toolbar -->

                <!-- Navbar Toolbar Right -->
                <ul class="nav navbar-toolbar navbar-right navbar-toolbar-right">
                    <li class="nav-item dropdown">
                        <a class="nav-link" id="notif_badge" data-toggle="dropdown" href="javascript:void(0)" title="Notifications" aria-expanded="false" data-animation="scale-up" role="button">
                            <i class="icon md-notifications text-white" aria-hidden="true"></i>
                            @php
                            use App\Helpers\DeHelper2;
                            $user 		= Session::get("data_user");
                            $nim = $user['nim'];
                            $key_menu = $user['key_menu'];
                            $notif = DeHelper2::getNotif($key_menu,$nim);
                            @endphp

                            @if ($notif['count'] == 0)
                            @else
                            <span class="badge badge-pill badge-danger up">{{$notif['count']}}</span>
                            @endif
                        </a>


                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-media" role="menu">
                            <div class="dropdown-menu-header">
                                <h5>NOTIFICATIONS</h5>
                                <span class="badge badge-round badge-danger data_baru">Data baru {{$notif['count']}}</span>
                            </div>

                            <div class="list-group">
                                <div data-role="container">
                                    <div data-role="content" id="notifikasi">

                                        @foreach ($notif['data_notif'] as $item)
                                        <a class="list-group-item dropdown-item" href="{{url('/').$item['url']}}" role="menuitem">
                                            <div class="media">
                                                <div class="pr-10">
                                                    {!! DeHelper::getNotifIcon($item['status']) !!}
                                                </div>
                                                <div class="media-body">
                                                    <h6 class="media-heading">

                                                        <div class="marquee">
                                                            <div class="marquee__content">
                                                                <ul class="list-inline">
                                                                <li>|&nbsp;&nbsp;{{$item['pesan']}}&nbsp;&nbsp;</li>
                                                                <li>|&nbsp;&nbsp;{{$item['pesan']}}&nbsp;&nbsp;</li>
                                                                <li>|&nbsp;&nbsp;{{$item['pesan']}}&nbsp;&nbsp;</li>
                                                                <li>|&nbsp;&nbsp;{{$item['pesan']}}&nbsp;&nbsp;</li>
                                                                <li>|&nbsp;&nbsp;{{$item['pesan']}}&nbsp;&nbsp;</li>
                                                                </ul>
                                                                <ul class="list-inline">
                                                                <li>|&nbsp;&nbsp;{{$item['pesan']}}&nbsp;&nbsp;</li>
                                                                <li>|&nbsp;&nbsp;{{$item['pesan']}}&nbsp;&nbsp;</li>
                                                                <li>|&nbsp;&nbsp;{{$item['pesan']}}&nbsp;&nbsp;</li>
                                                                <li>|&nbsp;&nbsp;{{$item['pesan']}}&nbsp;&nbsp;</li>
                                                                <li>|&nbsp;&nbsp;{{$item['pesan']}}&nbsp;&nbsp;</li>
                                                                </ul>
                                                                <ul class="list-inline">
                                                                <li>|&nbsp;&nbsp;{{$item['pesan']}}&nbsp;&nbsp;</li>
                                                                <li>|&nbsp;&nbsp;{{$item['pesan']}}&nbsp;&nbsp;</li>
                                                                <li>|&nbsp;&nbsp;{{$item['pesan']}}&nbsp;&nbsp;</li>
                                                                <li>|&nbsp;&nbsp;{{$item['pesan']}}&nbsp;&nbsp;</li>
                                                                <li>|&nbsp;&nbsp;{{$item['pesan']}}&nbsp;&nbsp;</li>
                                                                </ul>
                                                            </div>
                                                            </div>
                                                        {{-- <marquee style="width: 100%;">{{$item['pesan']}} {{$item['pesan']}} {{$item['pesan']}} {{$item['pesan']}}</marquee> --}}
                                                    </h6>
                                                    <time class="media-meta" datetime="2017-06-12T20:50:48+08:00">{{$item['lama']}}</time>
                                                </div>
                                            </div>

                                        </a>
                                        @endforeach

                                    </div>
                                </div>
                            </div>
                            <div class="dropdown-menu-footer">
                                <a class="dropdown-item" href="#" role="menuitem">
                                    All notifications
                                </a>
                            </div>
                        </div>
                    </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link navbar-avatar" data-toggle="dropdown" href="#" aria-expanded="false"
                            data-animation="scale-up" role="button">
                            <span class="avatar avatar-online" style="height: 30px">
                                <img src="{{asset('foto_anggota').'/'.Session::get("data_user")['foto']}}" style="height: 30px" alt="...">
                                <i></i>
                            </span>
                        </a>
                        <div class="dropdown-menu" role="menu">
                            <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                                            <strong>Login sebagai :</strong>
                                            <h4 class="mt-0">
                                            {{Session::get('data_user')['nama']}} - {{DeHelper2::getRole(Session::get('data_user')['flag_lv'])}}
                                            </h4>
                            </a>
                            <a class="dropdown-item" role="menuitem" href="{{route('beranda')}}" >
                                <i class="icon fa-home" aria-hidden="true"></i> Beranda
                            </a>
                            <a class="dropdown-item" role="menuitem" id="btn_gpswd_umum">
                                <i class="icon fa-unlock-alt" aria-hidden="true"></i> Ganti Password
                            </a>
                            <div class="dropdown-divider"></div>
                            <a href="{{ route('logout') }}" class="dropdown-item" role="menuitem"  onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <i class="icon md-power" aria-hidden="true"></i> Logout
                            </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>

                        </div>
                    </li>
                </ul>
                <!-- End Navbar Toolbar Right -->
            </div>
            <!-- End Navbar Collapse -->

        </div>
    </nav>

<!-- Modal -->
<div class="modal fade modal-slide-from-bottom p-0" id="modalPasswordUmumm" aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-simple">
        <div class="modal-content">

            <div class="modal-header">
                <div class="ribbon ribbon-clip px-10 w-300 h-50" style="z-index:100">
                    <span class="ribbon-inner">
                        <h4 id="judulModal_gpswd_umum" class="modal-title text-white"></h4>
                    </span>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>

            </div>

            <div class="modal-body pt-20 pl-20" data-keyboard="false" data-backdrop="static" style="overflow-y: auto;">
                <form id="formPass_gpswd_umum" name="formPass_gpswd_umum" autocomplete="on">
                    @csrf
                    <div class="row pt-10 px-20">

                        <div class="col-md-6">
                            <div class="form-group form-material" data-plugin="formMaterial">
                                <label class="form-control-label" for="nim_gpswd_umum">NIM</label>
                                <input type="text" readonly class="form-control" id="nim_gpswd_umum" name="nim"/>
                                <div class="invalid-feedback">*wajib diisi</div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group form-material" data-plugin="formMaterial">
                                <label class="form-control-label" for="nama_gpswd_umum">Nama</label>
                                <input type="text" readonly class="form-control" id="nama_gpswd_umum" name="nama"/>
                                <div class="invalid-feedback">*wajib diisi</div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group form-material" data-plugin="formMaterial">
                                <label class="form-control-label" for="username_gpswd_umum">Username</label>
                                <input type="text" class="form-control" id="username_gpswd_umum" name="username" placeholder="username.."/>
                                <div class="invalid-feedback">*wajib diisi</div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group form-material passW" data-plugin="formMaterial">
                                <label class="form-control-label" for="password_gpswd_umum">Password</label>
                                <input type="password" class="form-control inPass" id="password_gpswd_umum" name="password" placeholder="kata sandi.."/>
                                <div class="invalid-feedback invPass">*wajib diisi</div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group form-material passW" data-plugin="formMaterial">
                                <label class="form-control-label" for="re_password_gpswd_umum">Konfirmasi password</label>
                                <input type="password" class="form-control inPass" id="re_password_gpswd_umum" name="re_password" placeholder="ketik ulang password"/>
                                <div class="invalid-feedback invPass">*wajib diisi</div>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-outline btn-outline-secondary" data-dismiss="modal" id="tutup">Tutup</button>
                <button type="button" id="btnSave_gpswd_umum" class="btn bg-purple-400 text-white">Simpan</button>
            </div>
        </div>
    </div>

</div>

@push('scripts')
<script>
var validasi = true;

function vld_text(id_input,col){
    if ($(id_input).val()=='') {
        $(id_input).addClass('is-invalid').closest('.col-md-'+col).find('.form-control-label').addClass('text-danger');
        $(id_input).closest('.form-material').addClass('has-danger');
        $(id_input).focus(function () {
            $(this).removeClass('is-invalid').closest('.form-material').removeClass('has-danger');
        });
        $(id_input).keyup(function () {
            $(this).closest('.col-md-'+col).find('.form-control-label').removeClass('text-danger');
        });
    }
}

    $(document).on('click', '#btnSave_gpswd_umum', function (e) {
        e.preventDefault;
        vld_text('#username_gpswd_umum',12);
        vld_text('#password_gpswd_umum',12);
        vld_text('#re_password_gpswd_umum',12);

        if (    validasi == true &&
                $('#username').val()!='' &&
                $('#password_gpswd_umum').val()!='' &&
                $('#re_password').val()!=''
        ) {
            $(this).html('Sending..');
            $(this).attr('disabled');

            $.ajax({
                data: $('#formPass_gpswd_umum').serialize(),
                url: "{{route('simpanPerubahanPass')}}",
                type: "POST",
                dataType: 'json',
                success: function (data) {
                    console.log(data);

                    $('.invPass').html('*wajib diisi');
                    swal({
                        title: "Sukses!",
                        text: "Password berhasil disimpan!",
                        type: "success",
                        showCancelButton: false,
                        confirmButtonClass: "btn-success",
                        confirmButtonText: 'OK',
                        closeOnConfirm: false
                    });
                    $('#btnSave_gpswd_umum').removeAttr('disabled');
                    $('#btnSave_gpswd_umum').html('Simpan');
                    $('#formPass_gpswd_umum').trigger('reset');
                    $('#modalPasswordUmumm').modal('hide');
                    // toastr.success(data.message);
                },
                error: function (data) {
                    console.log('Error:', data);
                    $('#btnSave_gpswd_umum').html('Simpan');
                    toastr.error('Kelola Data Gagal');
                }
            })
        }
    });

$(document).on('click', '#btn_gpswd_umum', function () {
    // console.log('sjfhdav');

    $('#judulModal_gpswd_umum').html('Edit Akun');
    $('#nama_gpswd_umum').val("{{Session::get("data_user")['nama']}}");
    $('#nim_gpswd_umum').val("{{Session::get("data_user")['nim']}}");

    $('#modalPasswordUmumm').modal({
        backdrop: 'static',
        keyboard: false, // to prevent closing with Esc button (if you want this too)
        show: true
    })
});

$(document).on('change', '#re_password_gpswd_umum', function () {
    // console.log('sjfhdav');passW

    var rePass = $(this).val();
    var Pass = $('#password_gpswd_umum').val();

        console.log((rePass != Pass));
    if (rePass != Pass) {
        $('.passW').addClass('has-danger');
        $('.invPass').html('Password dan konfirmasi password salah');
        $('.inPass').addClass('is-invalid');
        validasi = false;
    }else{
        $('.invPass').html('*wajib diisi');
        $('.passW').removeClass('has-danger');
        $('.inPass').removeClass('is-invalid');

    }
});

$("#modalPasswordUmumm").on("hidden.bs.modal", function () {
    $('.form-control').removeClass('is-invalid');
    $('.form-group').removeClass('has-danger');
    $('#formPass_gpswd_umum').trigger('reset');
    $('.form-control-label').removeClass('text-danger')
});
</script>
@endpush
