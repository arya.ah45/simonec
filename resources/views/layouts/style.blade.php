{{-- Style Page --}}
<style>
    .page{    
        height: 100%;
        /* background-image: linear-gradient(170deg,#efa1fd,#fce4ec ); */
    }
    .bg{
        position: fixed;
        left: 0;
        right: 0;
        z-index: 0;
        display: block;        
        width: 100%;
        height: 100%;        
        background: url({{asset('background/bg1.jpg')}}) no-repeat center center fixed;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;     
        /* -webkit-filter: blur(1px);
        -moz-filter: blur(1px);
        -o-filter: blur(1px);
        -ms-filter: blur(1px);
        filter: blur(1px);         */
    }
    .bg .bg-filter{
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;        
        background: linear-gradient(170deg,#78088b59,#8a1a3f57 );
    }
</style>

{{-- Style Side Bar --}}
<style>
    .mm-panels{
        height: 100%;
    }      
    .site-menubar{
        background-image: linear-gradient(180deg,rgba(255, 255, 255, 0.83),rgba(255, 255, 255, 0.83),rgba(255, 255, 255, 0.87),rgba(255, 255, 255, 0.87),rgba(255, 255, 255, 0.87),rgba(255, 255, 255, 0.87),rgba(255, 255, 255, 0.87),rgba(255, 255, 255, 0.87),rgba(255, 255, 255, 0.6),rgba(255, 255, 255, 0.3),rgba(255, 255, 255, 0.02) );
    }
    .mm-panels, .mm-panels > .mm-panel{
        background: none;
    }
    .site-menu-item{

    }
    .cat-side-menu{
        white-space: nowrap; 
        overflow: hidden;        
        text-overflow: ellipsis;         
        padding-left: 20px;
        font-weight: 800;
        color: #b153c2;
    }      
</style>

{{-- Navbar --}}
<style>
    .navbar-container{
        background-image: linear-gradient(100deg,#E040FB,#c51162 );
    }
</style>

{{-- Footer --}}
<style>
    .site-footer{
        background-image: linear-gradient(100deg,rgba(26, 26, 26, 0.0),rgba(64, 10, 73, 0.6));
    }
</style>

{{-- Panel --}}
<style>
    .panel-heading{
        background-image: linear-gradient(180deg,rgba(255, 255, 255, 0.6),rgba(255, 255, 255, 0.9));
    }
    .panel-body{
        background: rgba(255, 255, 255, 0.9);
        height: 100%;
    }
</style>

{{-- Panel --}}
<style>
    .modal-header{
        background-image: linear-gradient(180deg,rgba(255, 255, 255, 0.6),rgba(255, 255, 255, 0.9));
    }
    .modal-body{
        background: rgba(255, 255, 255, 0.9);
    }
    .modal-footer{
        background-image: linear-gradient(180deg,rgba(255, 255, 255, 0.9),rgba(255, 255, 255, 0.6));
    }
</style>

{{-- Lain - lain --}}
<style>
    .pinky1{
        background:#E040FB;
        color: white;
    }
    .pinky2{
        background:#c51162;
        color: white;
    }
    .abu1{
        background:#e7e7e7;
        color: black;
    }
    .abu2{
        background:#d5d5d5;
        color: black;
    }
    .merah{
        background:#ffcdfc;
        color: black;
    }
    .hijau{
        background:#d8ffcd;
        color: black;
    }
    .pinky1-soft{
        background:#e596f1;
    }
    .pinky1-soft-1{
        background:#fef6ff;
    }
    .pinky1-soft-1:hover{
        background:#f0f0f0;
    }
    .pinky1-soft-2{
        background:#fafafa;
    }
    .pinky1-soft-2:hover{
        background:#f0f0f0;
    }
    .pinky2-soft{
        background:#ffc5df;
    }
    .pinky2-soft-1{
        background:#fff3f9;
    }
    .pinky2-soft-1:hover{
        background:#f0f0f0;
    }
    .pinky2-soft-2{
        background:#fafafa;
    }    
    .pinky2-soft-2:hover{
        background:#f0f0f0;
    }    
    .abu-soft{
        background:#fab0f6;
    }
</style>

{{-- style panel --}}
<style>
.panel.is-fullscreen {
    position: fixed;
    top: 0;
    left: 0;
    z-index: 9999;
    width: 100%;
    height: 700px;
    border-radius: 0
}
</style>

<style>
    #preview2 {
        width: 100%;
        height: 250;
        margin: 0px auto;
    }

    #preview3 {
        width: 100%;
        height: 100%;
        margin: 0px auto;
    }
</style>

<style>
    .dropify-wrapper .dropify-message .file-icon{
        font-size: 12px;
    }
</style>

<style>
    .marquee {
    animation-play-state: paused;          
    margin: 0 auto;
    width: 100%;
    height: 30px;
    white-space: nowrap;
    overflow: hidden;
    box-sizing: border-box;
    position: relative;
    }
    .marquee:before,
    .marquee:after {
    position: absolute;
    top: 0;
    width: 50px;
    height: 30px;
    content: "";
    z-index: 1;
    }
    .marquee:before {
    left: 0;
    }
    .marquee:after {
    right: 0;
    }
    .marquee__content {
    width: 600%;
    display: flex;
    line-height: 30px;
    animation: marquee 30s linear infinite forwards;
    }
    .marquee__content:hover {
    animation-play-state: paused;
    }
    .list-inline {
    display: flex;
    justify-content: space-around;
    width: 100%;
    /* reset list */
    list-style: none;
    padding: 0;
    margin: 0;
    }
    @keyframes marquee {
    0% {
        transform: translateX(0);
    }
    100% {
        transform: translateX(-66.6%);
    }
    }
</style>

<style>
    .nodes{
        width: 200px;
    }
</style>

<style>
    .orgChart{
        overflow-y: auto;
    }
    div.orgChart{
        margin: 0px;

    }
    .chart-container{
        padding: 0px;
    }
</style>

<style>
    .select2-container.select2-container--default.select2-container--open  {
    z-index: 5000;
    }    
</style>

<style>
    .th_landing{
        border-collapse: collapse;
        border-spacing: 0;
        border: solid 1px rgb(250, 80, 80);        
    }
</style>
