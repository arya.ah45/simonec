<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap material admin template">
    <meta name="author" content="">

    <title>SIMONEC.0.1</title>
    {{-- @include('layouts.title') --}}

    @include('configs.css')
    @include('layouts.style')
  </head>

  <body class="animsition site-navbar-small">

    @include('layouts.navbar')
    @include('layouts.sidebar')


    <!-- Page -->
    <div class="page ">
      <div class="bg">
        <div class="bg-filter"></div>
      </div>
      <div class="page-content ">
        @yield('content')
      </div>
    </div>
    <!-- End Page -->

    <!-- Footer -->
    <footer class="site-footer" style="position: fixed; bottom: 0px; width: 100%; z-index: 3000;margin-left: 0px;border-top:0px;">
      <div class="site-footer-legal"></div>
      <div class="site-footer-right">
        © {{Date('Y')}} EC PSDKU Polinema Kediri
      </div>
    </footer>

  </body>

@include('configs.script')
@stack('scripts')
</html>
