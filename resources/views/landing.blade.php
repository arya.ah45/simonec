@extends('layouts.app')

@section('content')
<div class="row" data-plugin="matchHeight" data-by-row="true">
@php
    use App\Helpers\DeHelper2;
    use App\Helpers\DeHelper;

    $user 		= Session::get("data_user");
    $flag_lv = $user['flag_lv'];
    $nim = $user['nim'];
    $nama = $user['nama'];
    $key_menu = $user['key_menu'];
    $role = DeHelper2::getRole($key_menu);
@endphp

    <div class="col-md-12">
        <!-- Card -->
        <div class="card card-block p-30" style="background-image: linear-gradient(180deg,rgba(255, 255, 255, 0.6),rgba(255, 255, 255, 0.9));">
        <div class="card-watermark darker font-size-80 m-15"><i class="icon fa-child" aria-hidden="true"></i></div>
        <div class="counter counter-md counter-inverse text-left grey-800">
            <div class="counter-number-group grey-800">
            <span class="counter-number">Selamat datang di Sistem Informasi Manajemen Organisasi English Club (SIMONEC)</span>
            <span class="counter-number-related text-capitalize"></span>
            </div>
            <div class="counter-label text-capitalize"><b>{{$nama}}</b> anda sekarang adalah <b>{{$role}}</b></div>
        </div>
        </div>
        <!-- End Card -->
    </div>

    @if (substr($key_menu,0,1) != "e")
    <div class="col-md-6">
        <div class="panel" id="projects">

            <div class="panel-body p-15" style="background: rgba(255, 255, 255, 0.95);height: ;overflow-y: auto;">
                <script src="https://code.highcharts.com/highcharts.js"></script>
                <script src="https://code.highcharts.com/modules/series-label.js"></script>
                <script src="https://code.highcharts.com/modules/exporting.js"></script>
                <script src="https://code.highcharts.com/modules/export-data.js"></script>
                <script src="https://code.highcharts.com/modules/accessibility.js"></script>
                <figure class="highcharts-figure">
                    <div id="container"></div>
                    <p class="highcharts-description">
                        Basic line chart showing trends in a dataset. This chart includes the
                        <code>series-label</code> module, which adds a label to each line for
                        enhanced readability.
                    </p>
                </figure>

            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel" id="projects">
            <div class="panel-heading">
                <h3 class="panel-title">Riwayat Jabatan Anda</h3>
            </div>
            <div class="panel-body p-15" style="background: rgba(255, 255, 255, 0.95);height: 425px ;overflow-y: auto;">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover table-striped" id="tb_riwayat">
                        <thead class="text-center">
                            <tr>
                                <th>no</th>
                                <th>NIM</th>
                                <th>Nama</th>
                                <th>Jabatan</th>
                                <th>No SK</th>
                                <th>TMT</th>
                                <th>Status</th>
                                <th>Keanggotaan</th>
                            </tr>
                        </thead>
                        <tbody class="text-center">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @else
    <div class="col-md-6">
        <div class="panel" id="projects">

            <div class="panel-body p-15" style="background: rgba(255, 255, 255, 0.95);height: ;overflow-y: auto;">
                <script src="https://code.highcharts.com/highcharts.js"></script>
                <script src="https://code.highcharts.com/modules/series-label.js"></script>
                <script src="https://code.highcharts.com/modules/exporting.js"></script>
                <script src="https://code.highcharts.com/modules/export-data.js"></script>
                <script src="https://code.highcharts.com/modules/accessibility.js"></script>
                <figure class="highcharts-figure">
                    <div id="container"></div>
                    <p class="highcharts-description">
                        grafik yang menunjukan jumlah pendaftad , Jumlah peserta Diterima
                        <code>dan</code> jumlah peserta Tidak Diterima pada setiap tahun Penerimaan Anggota
                        Baru UKM English Club
                    </p>
                </figure>

            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel" id="projects">
            <div class="panel-heading">
                <h3 class="panel-title">Riwayat Jabatan Anda</h3>
            </div>
            <div class="panel-body p-15" style="background: rgba(255, 255, 255, 0.95);height: 425px ;overflow-y: auto;">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover table-striped" id="tb_riwayat">
                        <thead class="text-center">
                            <tr>
                                <th>no</th>
                                <th>NIM</th>
                                <th>Nama</th>
                                <th>Jabatan</th>
                                <th>No SK</th>
                                <th>TMT</th>
                                <th>Status</th>
                                <th>Keanggotaan</th>
                            </tr>
                        </thead>
                        <tbody class="text-center">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
        <div class="col-xl-2 col-lg-2 col-md-4 col-sm-4">
            <!-- Card -->
            <div class="card card-block p-30" style="background-image: linear-gradient(100deg,rgba(251, 64, 226, 0.625),rgba(197, 17, 98, 0.7) );height: 295px;">
                <div class="counter counter-lg counter-inverse">
                <div class="counter-label">
                    <div class="font-size-30">{{date('Y')}}</div>
                    <div class="font-size-14">Total Proker</div>
                </div>
                <div class="counter-number-group text-center" style="width:100%;position:absolute;bottom:30px;left:0;">
                    <span class="counter-number">{{DeHelper2::totalProker()}}</span>
                    <span class="counter-number-related font-size-30"></span>
                </div>
                </div>
            </div>
            <!-- End Card -->
        </div>

        <div class="col-xl-4 col-lg-4 col-md-8 col-sm-8">
            <div class="row">
                <div class="col-md-12">
                    <!-- Card -->
                    <figure class="overlay overlay-hover flex-row justify-content-between mb-15">
                        <div class="overlay-figure card  p-30" style="background-image: linear-gradient(100deg,rgba(17, 123, 90, 0.625),rgba(51, 171, 69, 0.646) );">
                            <div class="card-watermark darker font-size-80 m-15"><i class="icon md-assignment" aria-hidden="true"></i></div>
                            <div class="counter counter-md counter-inverse text-left">
                                <div class="counter-label text-capitalize">Proker di tahun {{date('Y')}}</div>
                                <div class="counter-number-group">
                                    <span class="counter-number">{{DeHelper2::totalProkerTerlaksana()['count']}}</span>
                                    <span class="counter-number-related text-capitalize">Terlaksana</span>
                                </div>
                            </div>
                            <figcaption class="overlay-panel overlay-background overlay-slide-bottom text-center vertical-align">
                                <button type="button" class="btn btn-outline text-white bg-grey-600 vertical-align-middle modalHijau">Detail</button>
                            </figcaption>
                        </div>
                    </figure>

                    <!-- End Card -->
                </div>
                <div class="col-md-12">
                    <!-- Card -->
                    <figure class="overlay overlay-hover flex-row justify-content-between mt-20 ">
                        <div class="overlay-figure card p-30" style="background-image: linear-gradient(100deg,rgba(191, 30, 78, 0.625),rgba(250, 64, 40, 0.646) );">
                            <div class="card-watermark darker font-size-80 m-15"><i class="icon md-assignment" aria-hidden="true"></i></div>
                            <div class="counter counter-md counter-inverse text-left">
                                <div class="counter-label text-capitalize">Proker di tahun {{date('Y')}}</div>
                                <div class="counter-number-group">
                                    <span class="counter-number">{{DeHelper2::totalProkerTakTerlaksana()['count']}}</span>
                                    <span class="counter-number-related text-capitalize">Tidak Terlaksana</span>
                                </div>
                            </div>
                            <figcaption class="overlay-panel overlay-background overlay-slide-bottom text-center vertical-align">
                                <button type="button" class="btn btn-outline text-white bg-grey-600 vertical-align-middle modalMerah">Detail</button>
                            </figcaption>
                        </div>
                    </figure>

                    <!-- End Card -->
                </div>
            </div>
        </div>
                                <div class="modal fade modal-3d-slit" id="modal_merah" aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog" tabindex="-1">
                                <div class="modal-dialog modal-center">
                                    <div class="modal-content">
                                    <div class="modal-header grey-100" style="background-image: linear-gradient(100deg,rgba(191, 30, 78, 0.625),rgba(250, 64, 40, 0.646) );">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                        <h4 class="modal-title text-white">List Proker tidak terlaksana</h4>
                                    </div>
                                    <div class="modal-body pb-0">
                                        @forelse (DeHelper2::totalProkerTakTerlaksana()['data'] as $key => $item)
                                        <h3 class="counter-label text-capitalize mt-0">{{++$key}}. {{$item->nama}}</h3>
                                        <blockquote class="blockquote custom-blockquote blockquote-danger">
                                        <p class="mb-0">Evaluasi</p>
                                        <footer class="blockquote-footer">{{$item->evaluasi}}</footer>
                                        </blockquote>
                                        @empty
                                        @endforelse
                                    </div>
                                    </div>
                                </div>
                                </div>

                                <div class="modal fade modal-3d-slit" id="modal_hijau" aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog" tabindex="-1">
                                <div class="modal-dialog modal-center">
                                    <div class="modal-content">
                                    <div class="modal-header grey-100" style="background-image: linear-gradient(100deg,rgba(17, 123, 90, 0.625),rgba(51, 171, 69, 0.646) );">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                        <h4 class="modal-title text-white">List Proker terlaksana</h4>
                                    </div>
                                    <div class="modal-body pb-0">
                                        @forelse (DeHelper2::totalProkerTerlaksana()['data'] as $key => $item)
                                        <h3 class="counter-label text-capitalize mt-0">{{++$key}}. {{$item->nama}}</h3>
                                        <blockquote class="blockquote custom-blockquote blockquote-success">
                                        <p class="mb-0">Evaluasi</p>
                                        <footer class="blockquote-footer">{{$item->evaluasi}}</footer>
                                        </blockquote>
                                        @empty
                                        @endforelse
                                    </div>
                                    </div>
                                </div>
                                </div>
        <div class="col-xl-6 col-lg-6 col-sm-12">
            <div class="panel" id="projects">
                <div class="panel-heading">
                    <h3 class="panel-title">Rekap Data Proker tahun {{date('Y')}}</h3>
                </div>
                <div class="panel-body p-0" style="background: rgba(255, 255, 255, 0.85);height: 225px;overflow-y: auto;">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover grey-800">
                            <thead>
                                <tr>
                                    <th rowspan="2" class="px-15 text-center" style="border-collapse: collapse;border-spacing: 0;border: solid 1px rgba(94, 94, 94, 0.523);border-left: solid 1px rgba(255, 255, 255, 0.523);"><b>Nama Proker</b></th>
                                    <th rowspan="2" class="px-15 text-center" style="border-collapse: collapse;border-spacing: 0;border: solid 1px rgba(94, 94, 94, 0.523);"><b>Tanggal Mulai</b></th>
                                    <th rowspan="1" class="px-15 text-center" style="border-collapse: collapse;border-spacing: 0;border: solid 1px rgba(94, 94, 94, 0.523);border-right: solid 1px rgba(255, 255, 255, 0.523);" colspan="3"><b>Proses pengajuan</b></th>
                                </tr>
                                <tr>
                                    <th class="px-15 text-center" style="border-collapse: collapse;border-spacing: 0;border: solid 1px rgba(94, 94, 94, 0.523);"><b>Proker</b></th>
                                    <th class="px-15 text-center" style="border-collapse: collapse;border-spacing: 0;border: solid 1px rgba(94, 94, 94, 0.523);"><b>Proposal</b></th>
                                    <th class="px-15 text-center" style="border-collapse: collapse;border-spacing: 0;border: solid 1px rgba(94, 94, 94, 0.523);border-right: solid 1px rgba(255, 255, 255, 0.523);"><b>LPJ</b></th>
                                </tr>
                            </thead>
                            <tbody style="">
                                @forelse (DeHelper2::rekap_proker() as $item)
                                    <tr>
                                        <td style="border-collapse: collapse;border-spacing: 0;border: solid 1px rgba(94, 94, 94, 0.523);border-left: solid 1px rgba(255, 255, 255, 0.523);">{{$item['nama_proker']}}</td>
                                        <td align="center" style="border-collapse: collapse;border-spacing: 0;border: solid 1px rgba(94, 94, 94, 0.523);">{{$item['tanggal_mulai']}}</td>
                                        <td align="center" style="border-collapse: collapse;border-spacing: 0;border: solid 1px rgba(94, 94, 94, 0.523);"><span data-plugin="peityPie" data-skin="orange">{{$item['status_proker']}}</span></td>
                                        <td align="center" style="border-collapse: collapse;border-spacing: 0;border: solid 1px rgba(94, 94, 94, 0.523);">
                                            @if($item['status_proposal'] == 'X') {{$item['status_proposal']}} @else <span data-plugin="peityPie" data-skin="purple">{{$item['status_proposal']}}</span> @endif</td>
                                        <td align="center" style="border-collapse: collapse;border-spacing: 0;border: solid 1px rgba(94, 94, 94, 0.523);border-right: solid 1px rgba(255, 255, 255, 0.523);">
                                            @if($item['status_lpj'] == 'X') {{$item['status_lpj']}} @else <span data-plugin="peityPie" data-skin="purple">{{$item['status_lpj']}}</span> @endif
                                        </td>
                                    </tr>
                                @empty
                                <tr>
                                    <td colspan="5" class="text-center" style="border-collapse: collapse;border-spacing: 0;border: solid 1px rgba(94, 94, 94, 0.523);border-left: solid 1px rgba(255, 255, 255, 0.523);border-right: solid 1px rgba(255, 255, 255, 0.523);">- Belum ada data -</td>
                                </tr>

                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>

    @endif

    <div class="col-md-4">
        <!-- Card -->
        <div class="card card-block p-30" style="background-image: linear-gradient(180deg,rgba(255, 255, 255, 0.625),rgba(255, 255, 255, 0.725),rgba(255, 255, 255, 0.825),rgba(255, 255, 255, 0.725),rgba(236, 139, 207, 0.625));">
        <div class="card-watermark darker font-size-80 m-15"><i class="icon md-accounts" aria-hidden="true"></i></div>
        <div class="counter counter-md counter-inverse text-left grey-800">
            <div class="counter-number-group grey-800">
            <span class="counter-number">{{DeHelper2::totalAnggota()['total_anggota_aktif']}}</span>
            <span class="counter-number-related text-capitalize">Anggota Aktif</span>
            </div>
            <div class="counter-label text-capitalize">total anggota aktif di tahun {{date('Y')}}</div>
        </div>
        </div>
        <!-- End Card -->
    </div>

    <div class="col-md-4">
        <!-- Card -->
        <div class="card card-block p-30" style="background-image: linear-gradient(180deg,rgba(255, 255, 255, 0.625),rgba(255, 255, 255, 0.725),rgba(255, 255, 255, 0.825),rgba(255, 255, 255, 0.725),rgba(194, 139, 236, 0.625));">
        <div class="card-watermark darker font-size-80 m-15"><i class="icon md-accounts" aria-hidden="true"></i></div>
        <div class="counter counter-md counter-inverse text-left grey-800">
            <div class="counter-number-group grey-800">
            <span class="counter-number">{{DeHelper2::totalAnggota()['total_anggota']}}</span>
            <span class="counter-number-related text-capitalize">Anggota</span>
            </div>
            <div class="counter-label text-capitalize">total anggota di tahun {{date('Y')}}</div>
        </div>
        </div>
        <!-- End Card -->
    </div>

    <div class="col-md-4">
        <!-- Card -->
        <div class="card card-block p-30" style="background-image: linear-gradient(180deg,rgba(255, 255, 255, 0.625),rgba(255, 255, 255, 0.725),rgba(255, 255, 255, 0.825),rgba(255, 255, 255, 0.725),rgba(139, 207, 236, 0.625));">
        <div class="card-watermark darker font-size-80 m-15"><i class="icon md-accounts" aria-hidden="true"></i></div>
        <div class="counter counter-md counter-inverse text-left grey-800">
            <div class="counter-number-group grey-800">
            <span class="counter-number">{{DeHelper2::totalAnggota()['total_pengurus']}}</span>
            <span class="counter-number-related text-capitalize">Pengurus</span>
            </div>
            <div class="counter-label text-capitalize">total pengurus di tahun {{date('Y')}}</div>
        </div>
        </div>
        <!-- End Card -->
    </div>



</div>
@endsection

@push('scripts')
<script>
Highcharts.chart('container', {

        title: {
            text: 'Grafik Minat Pendaftar Ec Tiap Tahun'
        },

        subtitle: {
            text: 'Simonec'
        },

        yAxis: {
            title: {
                text: 'Jumlah Peminat'
            }
        },

        xAxis: {
            accessibility: {
                rangeDescription: 'Tahun Pendaftaran'
            }
        },

        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle'
        },

        plotOptions: {
            series: {
                label: {
                    connectorAllowed: false
                },
                pointStart: 2018
            }
},

series: [{
    name: 'Pendaftar',
    data: {!!json_encode($data)!!}
},{
    name: 'Diterima',
    data: {!!json_encode($dataLos)!!}
},{
    name: 'Tidak Diterima',
    data: {!!json_encode($dataGal)!!}
}],

responsive: {
    rules: [{
        condition: {
            maxWidth: 500
        },
        chartOptions: {
            legend: {
                layout: 'horizontal',
                align: 'center',
                verticalAlign: 'bottom'
            }
        }
    }]
}

});
var table = $('#tb_riwayat').DataTable({
        processing: true,
        serverSide: true,
        "scrollY": "250px",
        "scrollx": "1050px",
        "scrollCollapse": true,
        ajax: "{{ route('DTjabatan') }}",
        language: {
            processing: "Sedang diproses...<img height=50 src='{{asset('assets/images/load3.gif')}}'>"
        },
        columns: [
            {
                data: 'no',
                name: 'no',
                searchable : false,
                orderable:true
            }, {
                data: 'nim',
                name: 'nim',
                orderable:true
            },{
                data: 'nama_pejabat',
                name: 'nama_pejabat',
                orderable: false,
                searchable: false
            },{
                data: 'nama_jabatan',
                name: 'nama_jabatan',
                orderable: false,
                searchable: false
            },{
                data: 'no_sk',
                name: 'no_sk',
                orderable: false,
                searchable: false
            },{
                data: 'tgl_sk',
                name: 'tgl_sk',
                orderable: false,
                searchable: false
            },{
                data: 'status',
                name: 'status',
                orderable: false,
                searchable: false
            },{
                data: 'status_keanggotaan',
                name: 'status_keanggotaan',
                orderable: false,
                searchable: false
            },
        ],
        columnDefs: [
            { className: 'text-center', targets: [0,1,5,6] },
        ]
    });
$(document).on('click', '.modalMerah', function (e) {
    e.preventDefault();
    $('#modal_merah').modal({
        show: true
    })
});
$(document).on('click', '.modalHijau', function (e) {
    e.preventDefault();
    $('#modal_hijau').modal({
        show: true
    })
});
</script>
@endpush
