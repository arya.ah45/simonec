<?php $hari_ini = date("Y-m-d"); ?>

<!DOCTYPE html>
<html>

<head>
	<title>Daftar Hasil Test</title>

    <script src="{{asset('js/jquery-3.5.1.js')}}"></script>

	<style type="text/css">
		body {
			font-family: "Times New Roman", Times, serif;
			font-size: 12pt;
		}

		.text-center {
			text-align: center;
		}

		.text-left {
			text-align: left;
		}

		.text-right {
			text-align: right;
		}

		.text-uppercase {
			text-transform: uppercase;
		}

		.text-lowercase {
			text-transform: lowercase;
		}

		.text-capital {
			text-transform: capitalize;
		}

		.text-underline {
			text-decoration: underline;
			text-decoration-color: #000;
		}

		.font-sm {
			font-size: 12px;
		}

		.bg-red {
			background-color: red;
		}

		.bg-grey {
			background-color: rgb(220, 220, 220);
		}

		.table {
			border-collapse: collapse;
			border-spacing: 0;
			width: 100%;
			border: solid 1px black;
		}

		.table th,
		.table td {
			border: 1px solid black;
			font-size: 12px;
			padding: 5px;
		}

		.mb-0 {
			margin-bottom: 0px;
		}

		.mt-0 {
			margin-top: 0px;
		}

		.my-0 {
			margin-bottom: 0px;
			margin-top: 0px;
		}

		.mb-1 {
			margin-bottom: 1.5px;
		}

		.mar {
			margin-top: 10px;
			margin-bottom: 10px
		}

		hr {
			display: block;
			margin-top: 0.3em;
			margin-bottom: -0.2em;
			margin-left: auto;
			margin-right: auto;
			border-style: inset;
			border-width: 3px;
			background: black;
		}

		ol {
			display: block;
			margin-top: 0em;
			margin-bottom: 1em;
			margin-left: 0;
			margin-right: 0;
			padding-left: 17px;
			padding-top: -15px;
		}
	</style>
</head>

<body>

		<table border="0" style="width: 100%;">
			<tr>
				<td style="width: 15%;padding-bottom:0px;" align="left"><img src="{{ asset("assets/images/logo.png") }}" style="width: 100px;"></td>
				<td style="width: 70%" class="text-center">
					<p style="font-size:12pt;margin-bottom:0px"> <b>POLITEKNIK NEGERI MALANG</b> <br> <b>PSDKU KEDIRI</b> <br> <b>ENGLISH CLUB</b> </p>
					<p style="font-size:11pt;margin-top:0px;margin-bottom:5px"> Sekretariat : Jl. Lingkar Maskumambang, Kediri 64119, Kampus 2 Gedung D <br> Telp. (0354) 683128 – Fax. (0354) 683128 <br> Email : englishclubppk2020@gmail.com </p>
				</td>
				<td style="width: 15%;padding-bottom:0px;" align="right"><img src="{{ asset("assets/images/logo_polinema.png") }}" style="width: 100px;"></td>
			</tr>
		</table>
		<hr style="height: 1px;">

	<!-- &nbsp;&nbsp;&nbsp;&nbsp; -->
	<p class="text-center" style="margin-bottom:0px;"><b>Hasil Test English Club Tahun Penerimaan {{Date('Y')}}</b></p>

	<br>
	<table class="table" style="width: 100%;" id="tabel_panitia">
		<tr>
			<td class="text-center" style="width: 5%;">NO</td>
			<td class="text-center" style="width: 20%;">Nim</td>
			<td class="text-center" style="width: 20%;">Nama</td>
			<td class="text-center" style="width: 20%;">Prodi</td>
			<td class="text-center" style="width: 15%;">Kelas</td>
			<td class="text-center" style="width: 15%;">Status</td>
		</tr>

		@foreach ($data_hasil_test as $key => $item)
        <tr>
			<td class="text-center" style="width: 5%;">{{++$key}}</td>
			<td style="width: 20%;">{{$item['nim']}}</td>
			<td style="width: 20%;">{{$item['nama']}}</td>
			<td class="text-center" style="width: 20%;">{{$item['nama_prodi']}}</td>
			<td class="text-center" style="width: 15%;">{{$item['nama_kelas']}}</td>
            @if ($item['is_anggota'] == 3)
			<td class="text-center" style="width: 15%;"><span class="label label-pill label-inline label-success mr-2">diterima</span></td>
            @else
			<td class="text-center" style="width: 15%;"><span class="label label-pill label-inline label-danger  mr-2">tidak diterima</span></td>
            @endif
        </tr>
        @endforeach

	</table>
</body>

</html>
