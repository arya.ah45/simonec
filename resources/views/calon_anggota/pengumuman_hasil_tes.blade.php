@extends('calon_anggota.calon_anggota')

@section('content')

<div class="card card-custom card-stretch gutter-b">
    <div class="card-header border-0">
    <h3 class="card-title font-weight-bolder text-info">List Anggota Baru English Club tahun {{date('Y')}}</h3>
    </div>
    <div class="card-body pt-2 row">

        @forelse ($ang_baru as $item)
            <div class="d-flex mb-10 col-md-4">
                <div class="symbol symbol-40 symbol-light-white mr-5">
                    <div class="symbol-label">
                        <img src="{{asset('foto_anggota')}}/{{$item->foto}}" class="h-75 align-self-end" alt="" />
                    </div>
                </div>
                <div class="d-flex flex-column font-weight-bold">
                    <a href="#" class="text-dark text-hover-primary mb-1 font-size-lg">{{$item->nama}}</a>
                    <span class="text-muted">{{$item->nama_kelas.' '.$item->nama_prodi}}</span>
                </div>
            </div>
        @empty
        @endforelse

    </div>
</div>

@endsection

@push('scripts')
<script>

</script>
@endpush
