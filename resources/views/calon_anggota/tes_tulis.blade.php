@extends('calon_anggota.calon_anggota')

@section('content')

    <div class="card card-custom overflow-hidden position-relative mb-8 card_awalan">
        <div class="card-body d-flex justify-content-center flex-column px-20 py-20">
            <div class="row">
                <div class="col-md-6">
                    <h3 class="text-dark text-center mb-4 display-4">Halaman Tes Tulis</h3>

                    <div class="separator separator-solid separator-border-2 separator-info"></div>

                    <blockquote class="blockquote P-0 pt-4">
                        <p class="mb-0 ">Selamat datang calon anggota English Club di halaman tes tulis.
                                        Pada halaman ini kalian akan melakukan tes tulis.
                                        Untuk lanjut menuju soal tes tulis, silahkan scan kartu tes anda dengan menggunakan kamera gadget anda. Maka secara otomatis sistem akan menampilkan soal tes tulis</p>
                    </blockquote>
                </div>
                <div class="col-md-6">
                    <div class="qr_login">
                        <div class="form-group row">
                            <div class="col-md-12" id="div_select" style="display: none">
                                <div class="form-group">
                                    <label class="control-label">Kamera</label>
                                    <select class="form-control select" id="popup_camera">

                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12" id="">
                                <video id="preview2"></video>
                            </div>
                        </div>
                    </div>
                    {{-- <video id="preview2"></video> --}}
                </div>
            </div>
        </div>
    </div>

    <form id="form_jawaban" name="form_jawaban">
        @csrf
    <div class="card card-custom overflow-hidden position-relative mb-8 card_isi" hidden>
        <div class="card-body d-flex justify-content-center flex-column px-20 py-10">
            <h3 class="display-4 text-info">#Biodata Peserta</h3>
            <div class="row">
                <div class="col-md-2">
                    <blockquote class="blockquote P-0 pt-1">
                        <p class="mb-0 ">NIM :</p>
                    </blockquote>
                </div>
                <div class="col-md-9">
                    <blockquote class="blockquote P-0 pt-1">
                        <p class="mb-0" id="v_nim">SAPI</p>
                        <input hidden class="form-control" name="nim" id="nim" value=""/>
                    </blockquote>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <blockquote class="blockquote P-0 pt-1">
                        <p class="mb-0 ">Nama Lengkap :</p>
                    </blockquote>
                </div>
                <div class="col-md-9">
                    <blockquote class="blockquote P-0 pt-1">
                        <p class="mb-0" id="v_nama">sapi</p>
                    </blockquote>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <blockquote class="blockquote P-0 pt-1">
                        <p class="mb-0 ">Kelas :</p>
                    </blockquote>
                </div>
                <div class="col-md-9">
                    <blockquote class="blockquote P-0 pt-1">
                        <p class="mb-0" id="v_kelas">sapi</p>
                        <input hidden class="form-control" name="kelas" id="kelas" value=""/>
                    </blockquote>
                </div>
            </div>
        </div>
    </div>

    <div class="card card-custom overflow-hidden position-relative mb-8 card_isi" hidden>
        <div class="card-body d-flex justify-content-center flex-column px-20 py-10">
            <div class="row">
                <div class="col-md-12">
                    <blockquote class="blockquote P-0 pt-4">
                        <h3 class="display-4 text-danger">#Catatan</h3>
                        <p class="mb-0 ">
                            Jawablah semua pertanyaan dengan jujur dan sungguh-sungguh. Semua pertanyaan <span class="text-danger">WAJIB</span> dijawab. Tes tulis hanya dapat dilakukan sekali.
                        </p>
                    </blockquote>
                </div>
            </div>
        </div>
    </div>

    @foreach ($soal as $key => $item)
    <div class="card card-custom overflow-hidden position-relative mb-8 card_isi" hidden>
        <div class="card-body d-flex justify-content-center flex-column px-20 py-10">
            <div class="row">
                <div class="col-md-6">
                    <blockquote class="blockquote P-0 pt-4">
                        <h3 class="display-4">Soal {{$key+1}}</h3>
                        <p class="mb-0 ">
                            {{$item->nama}}
                        </p>
                    </blockquote>
                </div>
                <div class="col-md-6">
                    <h3 class="display-4 pt-2">Jawaban</h3>
                    <div class="form-group mb-1 pt-3">
                        <input hidden class="form-control id_soal" name="id_soal[]" value="{{$item->id}}"/>
                        <textarea class="form-control jawaban" name="jawaban[]" rows="3"></textarea>
                    </div>
                </div>
            </div>
        </div>
        @if (++$key == $jumlah_soal)
        <div class="card-footer d-flex justify-content-center">
            {{-- <a href="#" class="btn btn-light-danger font-weight-bold mr-4">
                <i class="flaticon-danger"></i>Batal
            </a> --}}
            <a href="#" class="btn btn-light-success" id="btn_kirim_jawaban">
                <i class="flaticon-paper-plane"></i> Kirim
            </a>
        </div>
        @endif
    </div>
    @endforeach

    </form>

    <div class="card card-custom overflow-hidden position-relative mb-8 card_ending" hidden>
        <div class="card-body d-flex justify-content-center flex-column px-20 py-40">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="text-dark text-center mb-4 display-4">Selamat anda telah menyelesaikan Tes Tulis</h3>
                    <div class="separator separator-solid separator-border-2 separator-info"></div>
                    <blockquote class="blockquote P-0 pt-4">
                        <p class="mb-0 text-center">" Setelah melakukan tes tulis anda harus mengikuti tes wawancara. untuk detail lebih lanjut silahkan menghubungi panitia."</p>
                    </blockquote>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script>

    $(document).ready(function () {
        scannerInstanscan() ;
        KTAutosize.init();

    });

    var scanner = new Instascan.Scanner({
        video: document.getElementById('preview2'),
        scanPeriod: 5,
        mirror: false
    });

    function animasi_leave(this_) {
        $(this_).css({
            "border": "solid",
            "border-color": "#3e8ef7"
        });
    }

    function animasi_enter(this_) {
        $(this_).css({
            "border": "",
            "border-color": ""
        });
    }

    // FUNCTION SCANNER
    function scannerInstanscan() {
        Instascan.Camera.getCameras().then(function(cameras) {
            if (cameras.length > 0) {
                if (cameras.length > 1) {
                    $("#div_select").show();
                    $("#popup_camera").html("");
                    $("#popup_camera").append("<option value=''> -- Pilih Kamera -- </option>");
                    for (i in cameras) {
                        $("#popup_camera").append("<option value='" + [i] + "'>" + cameras[i].name + "</option>");
                    }

                    if (cameras[1] != "") {
                        scanner.start(cameras[1]);
                        $("#popup_camera").val(1).change();
                    } else {
                        alert('Kamera tidak ditemukan!');
                    }
                } else {
                    if (cameras[0] != "") {
                        scanner.start(cameras[0]);
                        $("#popup_camera").val(0).change();
                    } else {
                        alert('Kamera tidak ditemukan!');
                    }
                }

                $("#popup_camera").change(function() {
                    var id = $("#popup_camera :selected").val();

                    if (id != '') {
                        if (cameras[id] != "") {
                            scanner.start(cameras[id]);
                        } else {
                            alert('Kamera tidak ditemukan!');
                        }
                    }
                })

            } else {
                console.error('No cameras found.');
                alert('No cameras found.');
            }
        }).catch(function(e) {
            console.error(e);
            alert(e);
        });
    }



    scanner.addListener('scan', function(content) {
        if (content != '') {
            console.log(content);

            $.ajax({
                url: "{{route('cekTesTulis')}}?nim="+content,
                type: "GET",
                dataType: 'json',
                success: function (data) {
                    console.log(data);
                    if (data.code == 0) {
                        // toastr.error(data.message);
                        swal.fire({
                            title: "Data Error!",
                            text: "Content dari Kode QR tersebut Kosong!",
                            type: "error"
                        }, function() {
                            $("#modalAbsensi1").modal("hide");
                        }, 1000);
                        // location.reload();
                    }else{
                        $('#nim').val(data.data.nim_mhs);
                        $('#nama').val(data.data.nama_mhs);
                        $('#v_nama').text(data.data.nama_mhs);
                        $('#v_nim').text(data.data.nim_mhs);
                        $('#v_kelas').text(data.data.nama_kelas+' / '+data.data.nama_prodi);

                        $('.card_isi').attr('hidden', false);
                        $('.card_awalan').attr('hidden', true);
                        scanner.stop();
                    }
                    // // $('#formAboff').trigger('reset');
                    // $('.nim_absen').val('');
                    // $('#btnSave_aboff').html('Simpan');
                    // $('#btnSave_aboff').removeAttr('disabled',true);
                    // $('#modalAbsensi1').modal('hide');
                },
                error: function (data) {
                    console.log('Error:', data);
                    $('#btnSave_aboff').removeAttr('disabled',true);
                    $('#btnSave_aboff').html('Simpan');
                    toastr.error('Kelola Data Gagal');
                }
            })
        } else {
            var item = content.split("||");
            // window.location.href = "/" + item;
            console.log(item);
            setTimeout(function() {

            })
        }
    });

    var KTAutosize = function () {
        // Private functions
        var demos = function () {
        // basic demo
        var jawaban = $('.jawaban');
        autosize(jawaban);
        }
        return {
            // public functions
            init: function() {
                demos();
            }
        };
    }();

    $(document).on('click','#btn_kirim_jawaban', function (e) {
        e.preventDefault();
        $(this).html('Sending..');
        $(this).attr('disabled',true);

        $.ajax({
            data: $('#form_jawaban').serialize(),
            url: "{{route('simpanJawaban')}}",
            type: "POST",
            dataType: 'json',
            success: function (data) {
                console.log(data);
                $('#form_jawaban').trigger('reset');
                $('.card_isi').attr('hidden', true);

                $('.card_ending').attr('hidden', false);
                $('#btn_kirim_jawaban').html('<i class="flaticon-paper-plane"></i> Kirim');
                $('#btn_kirim_jawaban').attr('disabled',false);
            },
            error: function (data) {
                console.log('Error:', data);
                $('#btnSave').html('Simpan');
                toastr.error('Kelola Data Gagal');
            }
        })
    });

</script>
@endpush
