@extends('calon_anggota.calon_anggota')

@section('content')
<div class="container">
    <!--begin::Hero-->
    <div class="card card-custom overflow-hidden position-relative mb-8">
        <!--begin::SVG-->
        <div class="position-absolute bottom-0 left-0 right-0 d-none d-lg-flex flex-row-fluid">
            <span class="svg-icon svg-icon-full flex-row-fluid svg-icon-dark opacity-3">
                <!--begin::Svg Icon | path:assets/media/bg/home-2-bg.svg-->
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 1200 172" style="enable-background:new 0 0 1200 172;" xml:space="preserve">
                <path class="st0" d="M2058,94c0,0,16-14,73-14s153,92,248,92s286-145,456-145s183,34,292,34s131-54,131-54v172H2058V94z"></path>
                <path class="st0" d="M0,87c0,0,16-14,73-14s153,92,248,92S607,20,777,20s183,34,292,34s131-54,131-54v172H0V87z"></path>
                </svg>
                <!--end::Svg Icon-->
            </span>
        </div>
        <div class="position-absolute d-flex top-0 right-0 offset-lg-5 col-lg-7 opacity-30 opacity-lg-100">
            <span class="svg-icon svg-icon-full flex-row-fluid p-15">
                {{-- <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 1200 172" style="enable-background:new 0 0 1200 172;" xml:space="preserve"> --}}
				{{-- <div style="background-image: url({{asset('assets')}}/images/logo.png)"></div> --}}
                <div class="text-center">
                <img alt="Logo" src="{{asset('assets/images/logo.png')}}" class="st0" height="60%" width="60%" />
                </div>
                {{-- </svg> --}}

            </span>
        </div>
        <!--end::SVG-->
        <!--begin::Hero Body-->
        <div class="card-body d-flex justify-content-center flex-column col-lg-6 px-8 py-20 px-lg-20 py-lg-40">
            <!--begin::Heading-->
            <h2 class="text-dark font-weight-bolder text-center mb-8">ENGLISH CLUB POLINEMA PSDKU KEDIRI </h2>

            <div class="separator separator-solid separator-border-2 separator-primary"></div>
            <blockquote class="blockquote P-0">
                <p class="mb-0 text-center">Merupakan Sebuah Organisasi /  Unit Kegiatan Mahasiswa (UKM) yang Berfokus Untuk Menambah Wawasan Mahasiswa Dalam Kemampuan Berbahasa Inggris</p>
                <p class="mb-0 text-center">UKM Ini Didirikan Di kampus POLITEKNIK KEDIRI Pada Tahun 2018</p>
            </blockquote>
            <!--end::Form-->
        </div>
        <!--end::Hero Body-->
        <!--begin::Navigation-->
        <div class="position-relative px-20">
            <ul class="nav nav-tabs nav-tabs-line" role="tablist">
                <!--begin::Nav Item-->
                <li class="nav-item mr-14 mt-4">
                    <a class="nav-link font-size-h5 font-weight-bold border-2 pb-6 mx-0" data-toggle="tab" href="#kt_tab_pane_1_1">Struktur UKM</a>
                </li>
                <!--end::Nav Item-->
                <!--begin::Nav Item-->
                <li class="nav-item mr-14 mt-4">
                    <a class="nav-link font-size-h5 font-weight-bold border-2 pb-6 mx-0" data-toggle="tab" href="#kt_tab_pane_2_2">Divisi UKM</a>
                </li>
                <!--end::Nav Item-->
                <!--begin::Nav Item-->
                <li class="nav-item mr-14 mt-4">
                    <a class="nav-link font-size-h5 font-weight-bold border-2 pb-6 mx-0" data-toggle="tab" href="#kt_tab_pane_2_1" >Pengumuman</a>
                </li>
                <!--end::Nav Item-->
            </ul>
        </div>
        <!--end::Navigation-->
    </div>
    <!--end::Hero-->
    <div class="tab-content mt-5" id="myTabLIist18">
    @yield('content1')
    </div>

</div>
@endsection