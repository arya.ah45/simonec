@extends('calon_anggota.calon_anggota')

@section('content')
<div class="container">
    <div class="card card-custom card-transparent">
        <div class="card-body p-0">
            <div class="wizard wizard-4" id="kt_wizard" data-wizard-state="step-first" data-wizard-clickable="true">
                <div class="wizard-nav">
                    <div class="wizard-steps">
                        <div class="wizard-step" data-wizard-type="step" data-wizard-state="current">
                            <div class="wizard-wrapper">
                                <div class="wizard-number">1</div>
                                <div class="wizard-label">
                                    <div class="wizard-title">Data Diri</div>
                                    <div class="wizard-desc">Detai Data Diri Anda</div>
                                </div>
                            </div>
                        </div>
                        <div class="wizard-step" data-wizard-type="step">
                            <div class="wizard-wrapper">
                                <div class="wizard-number">2</div>
                                <div class="wizard-label">
                                    <div class="wizard-title">Kelas</div>
                                    <div class="wizard-desc">Detail Kelas Anda</div>
                                </div>
                            </div>
                        </div>
                        <div class="wizard-step" data-wizard-type="step">
                            <div class="wizard-wrapper">
                                <div class="wizard-number">3</div>
                                <div class="wizard-label">
                                    <div class="wizard-title">Alamat</div>
                                    <div class="wizard-desc">Detail Alamat Anda</div>
                                </div>
                            </div>
                        </div>
                        <div class="wizard-step" data-wizard-type="step">
                            <div class="wizard-wrapper">
                                <div class="wizard-number">4</div>
                                <div class="wizard-label">
                                    <div class="wizard-title">Konfirmasi</div>
                                    <div class="wizard-desc">Konfirmasi data diri</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card card-custom card-shadowless rounded-top-0">
                    <div class="card-body p-0">
                        <div class="row justify-content-center py-8 px-8 py-lg-15 px-lg-10">
                            <div class="col-xl-12 col-xxl-10">
                                <form id="pendaftaranForm" name="pendaftaranForm" enctype="multipart/form-data">
                                    @csrf
                                    <div class="row justify-content-center">
                                        <div class="col-xl-9">
                                            {{-- BIODATA --}}
                                            <div class="my-5 step" data-wizard-type="step-content" data-wizard-state="current">
                                                <h5 class="text-dark font-weight-bold mb-10">Data Diri:</h5>

                                                <div class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label text-left">Foto Diri</label>
                                                    <div class="col-lg-9 col-xl-9">
                                                        <div class="image-input image-input-outline" id="kt_user_add_avatar">
                                                            <div class="image-input-wrapper" style="background-image: url()"></div>
                                                            <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Change avatar">
                                                                <i class="fa fa-pen icon-sm text-muted"></i>
                                                                <input type="file" id="foto" name="foto" accept=".png, .jpg, .jpeg" />
                                                                <input type="hidden" name="foto_remove" />
                                                            </label>
                                                            <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="Cancel avatar">
                                                                <i class="ki ki-bold-close icon-xs text-muted"></i>
                                                            </span>
                                                        </div>
                                                        <span class="form-text  text-muted">Ukuran Max Foto 2MB</span>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">NIM</label>
                                                    <div class="col-lg-9 col-xl-9">
                                                        <input class="form-control form-control-solid form-control-lg" id="nim" name="nim" type="text" value="" />
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">Nama Lengkap</label>
                                                    <div class="col-lg-9 col-xl-9">
                                                        <input class="form-control form-control-solid form-control-lg" id="nama" name="nama" type="text" value="" />
                                                    </div>
                                                </div>
                                                    @php
                                                    // format tanggal
                                                            use Carbon\Carbon;

                                                            $maxDate = Carbon::now()->subYears(19)->format('Y').'-12-31';
                                                            $newDate = date("Y-m-d", strtotime($maxDate));

                                                        @endphp
                                                <div class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">Tanggal Lahir </label>
                                                    <div class="col-lg-9 col-xl-9">


                                                        <input class="form-control form-control-solid form-control-lg" max="{{$newDate}}" id="tanggal_lahir" name="tanggal_lahir" type="date" value="" />
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-form-label col-xl-3 col-lg-3">Jenis Kelamin</label>
                                                    <div class="col-xl-9 col-lg-9">
                                                        <select class="form-control form-control-lg form-control-solid" id="jenis_kelamin" name="jenis_kelamin">
                                                            <option disabled selected value="">Pilih Jenis Kamin...</option>
                                                            <option value="L">Laki Laki</option>
                                                            <option value="P">Perempuan</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-form-label col-xl-3 col-lg-3">Agama</label>
                                                    <div class="col-xl-9 col-lg-9">
                                                        <select class="form-control form-control-lg form-control-solid" id="agama" name="agama">
                                                            <option disabled selected value="">Pilih Agama..</option>
                                                            @foreach ($agama as $key )
                                                            <option value="{{$key->id}}">{{$key->nama}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">No. HP</label>
                                                    <div class="col-lg-9 col-xl-9">
                                                        <div class="input-group input-group-solid input-group-lg">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">
                                                                    <i class="la la-phone"></i>
                                                                </span>
                                                            </div>
                                                            <input type="text" class="form-control form-control-solid form-control-lg" id="no_hp" name="no_hp" value="" placeholder="contoh +6289XXXXXXXX" />
                                                        </div>
                                                        <span class="form-text text-muted">Pastikan no Hp minimal 11 digit.(*+62 = 0) </span>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">Email</label>
                                                    <div class="col-lg-9 col-xl-9">
                                                        <div class="input-group input-group-solid input-group-lg">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">
                                                                    <i class="la la-at"></i>
                                                                </span>
                                                            </div>
                                                            <input type="text" class="form-control form-control-solid form-control-lg" id="email" name="email" value="" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            {{-- KELAS --}}
                                            <div class="my-5 step" data-wizard-type="step-content">
                                                <h5 class="text-dark font-weight-bold mb-10 mt-5">Kelas</h5>
                                                <div class="form-group row">
                                                    <label class="col-form-label col-xl-3 col-lg-3">Program Studi</label>
                                                    <div class="col-xl-9 col-lg-9">
                                                        <select class="form-control form-control-lg form-control-solid" id="id_prodi" name="id_prodi">
                                                            <option disabled value="">Pilih Prodi</option>
                                                            @foreach ($prodi as $key )
                                                            <option value="{{$key->id}}">{{$key->nama}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-form-label col-xl-3 col-lg-3">Kelas</label>
                                                    <div class="col-xl-9 col-lg-9">
                                                        <select class="form-control form-control-lg form-control-solid" id="id_kelas" name="id_kelas">
                                                            <option disabled value="">Pilih Kelas</option>
                                                            @foreach ($kelas as $key )
                                                            <option value="{{$key->id}}">{{$key->nama}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            {{-- ALAMAT --}}
                                            <div class="my-5 step" data-wizard-type="step-content">
                                                <h5 class="mb-10 font-weight-bold text-dark">Alamat</h5>
                                                <div class="form-group row">
                                                    <label class="col-form-label col-xl-3 col-lg-3">Provinsi</label>
                                                    <div class="col-xl-9 col-lg-9">
                                                        <select class="form-control form-control-lg form-control-solid" id="provinsi" name="id_provinsi">
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-form-label col-xl-3 col-lg-3">Kota / Kabupaten</label>
                                                    <div class="col-xl-9 col-lg-9">
                                                        <select class="form-control form-control-lg form-control-solid"  id="kota" name="id_kota">
                                                            {{-- <option disabled value="">Pilih Kota / Kabupaten</option>
                                                            @foreach ($kota as $key )
                                                            <option value="{{$key->id}}">{{$key->nama}}</option>
                                                            @endforeach --}}
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-form-label col-xl-3 col-lg-3">Kecamatan</label>
                                                    <div class="col-xl-9 col-lg-9">
                                                        <select class="form-control form-control-lg form-control-solid" id="kecamatan" name="id_kecamatan">
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-form-label col-xl-3 col-lg-3">Desa</label>
                                                    <div class="col-xl-9 col-lg-9">
                                                        <select class="form-control form-control-lg form-control-solid" id="desa" name="id_desa">
                                                            <option disabled value="">Pilih Desa</option>

                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="separator separator-dashed my-10"></div>

                                                <h5 class="text-dark font-weight-bold mb-10">Detail Alamat Anda</h5>
                                                <div class="form-group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">Alamat</label>
                                                    <div class="col-lg-9 col-xl-9">
                                                        <input class="form-control form-control-solid form-control-lg" id="alamat" name="alamat" type="text" placeholder="Jl Masjid RT ** / RW ** xxxxxx" />
                                                    </div>
                                                </div>
                                            </div>

                                            {{-- KONFIRMASI --}}
                                            <div class="my-5 step" data-wizard-type="step-content">
                                                <h5 class="mb-10 font-weight-bold text-dark">Konfirmasi</h5>
                                                <div class="border-bottom mb-5 pb-5">
                                                    <div class="font-weight-bolder mb-3">Setelah menekan tombol <b>Submit</b> anda tidak dapat melakukan perubahan biodata diri anda.</div>

                                                </div>

                                                <div class="border-bottom mb-5 pb-5">
                                                    <div class="font-weight-bolder mb-3">Maka pastikan data yang dimasukkan merupakan data yang <b>valid</b></div>

                                                </div>

                                                <div>
                                                    <div class="font-weight-bolder">Kami rekomendasikan untuk melakukan pengecekan lagi dengan menekan tombol <b>Previous</b></div>

                                                </div>
                                            </div>

                                            <div class="d-flex justify-content-between border-top pt-10 mt-15">
                                                <div class="mr-2">
                                                    <button type="button" id="prev-step" class="btn btn-light-primary font-weight-bolder px-9 py-4" data-wizard-type="action-prev">Previous</button>
                                                </div>
                                                <div>
                                                    <button hidden id="btnSavePendaftaran" type="submit" class="btn btn-success font-weight-bolder px-9 py-4">Submit</button>
                                                    <button type="button" id="next-step" class="btn btn-primary font-weight-bolder px-9 py-4" data-wizard-type="action-next">Next</button>
                                                    <button hidden type="button" id="keHome" class="btn btn-success font-weight-bolder px-9 py-4">KEMBALI KE MENU UTAMA</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

@push('scripts')
<script>
var id_provinsi = id_kota = id_kecamatan = id_pendaftar = '';
var counter = 1;

function select2Wilayah(id_select, url) {
    $(id_select).select2({
        minimumInputLength: 0,
        language: {
            inputTooShort: function() {
                return 'Masukkan Nama Kota';
            }
        },
        searchInputPlaceholder: 'Search...',
        placeholder: 'Pilih Kota',
        ajax: {
        cache: false,
        url: url,
        dataType: 'json',
        type: "GET",
        quietMillis: 50,
        data: function (params) {

            return {
            search: $.trim(params.term)
            };
        },
        processResults: function (data) {
        return {
            results: $.map(data, function (item) {
            return {
                text:item.nama,
                id: item.id
                }
            })
            };
        }
        }
    });
};

$(document).ready(function () {
    // fix bug title in select2
    $(document).on('mouseenter', '.select2-selection__rendered', function () {
        $('.select2-selection__rendered').removeAttr('title');
    });

    $('.nodes').attr('style', 'width: 100%;');
    // $('#provinsi').select2();
    // $('#kota').select2();
    // $('#kecamatan').select2();
    // $('#desa').select2();

    $("#provinsi").select2({
        minimumInputLength: 0,
        language: {
            inputTooShort: function() {
                return 'Masukkan Nama Provinsi';
            }
        },
        searchInputPlaceholder: 'Search...',
        placeholder: 'Pilih Provinsi',
        ajax: {
        cache: false,
        url: "{{ route('select2Provinsi') }}",
        dataType: 'json',
        type: "GET",
        quietMillis: 50,
        data: function (params) {

            return {
            search: $.trim(params.term)
            };
        },
        processResults: function (data) {
        return {
            results: $.map(data, function (item) {
            return {
                text:item.nama,
                id: item.id
                }
            })
            };
        }
        }
    });

    $("#kota").select2({placeholder: 'Silahkan pilih provinsi terlebih dahulu',});
    $("#kecamatan").select2({placeholder: 'Silahkan pilih kota terlebih dahulu',});
    $("#desa").select2({placeholder: 'Silahkan pilih kecamatan terlebih dahulu',});

    // $("#provinsi").html('<option value="35">JAWA TIMUR</option>');

});

$(document).on('change','#tanggal_lahir', function (e) {
    console.log($(this).val());
});

$(document).on('change','#provinsi', function (e) {
    e.preventDefault();
    id_provinsi = $(this).val();
    url = "{{ route('select2Kota') }}?id_provinsi="+id_provinsi;
    console.log(id_provinsi);
    select2Wilayah('#kota', url);
});

$(document).on('change','#kota', function (e) {
    e.preventDefault();
    id_kota = $(this).val();
    url = "{{ route('select2Kecamatan') }}?id_kota="+id_kota;
    console.log(id_kota);
    select2Wilayah('#kecamatan', url);
});

$(document).on('change','#kecamatan', function (e) {
    e.preventDefault();
    id_kecamatan = $(this).val();
    url = "{{ route('select2Desa') }}?id_kecamatan="+id_kecamatan;
    console.log(id_kecamatan);
    select2Wilayah('#desa', url);
});

$('#pendaftaranForm').on('submit', function(e){
    e.preventDefault();
    counter = 1;
    var formdata = new FormData(this);

    $.ajax({
        url: "{{route('simpanPendaftaran')}}",
        method:"POST",
        data:formdata,
        dataType:'JSON',
        contentType: false,
        cache: false,
        processData: false,
        success: function (data) {
            console.log(data);
            $('#pendaftaranForm').trigger('reset');
            id_pendaftar = data;
                Swal.fire({
                    title: "Sukses!",
                    text: "klik tombol dibawah ini untuk download kartu ujian",
                    type: "success",
                    showCancelButton: false,
                    confirmButtonClass: "btn-success btnSukses",
                    confirmButtonText: 'DOWNLOAD',
                    closeOnConfirm: false
                }).then(function(){
                    document.location = "{{url('recruitment/daftar/cetak_kartu')}}/"+id_pendaftar;
                });
            $('#btnSavePendaftaran').attr('hidden', true);
            $('#prev-step').attr('hidden', true);
            $('#keHome').attr('hidden', false);
            // $('#modalProposal').modal('hide');
            // toastr.success(data.message);
            },
        error: function (data) {
            console.log('Error:', data);
            // toastr.success(data.message);
            //$('#modalOrg').modal('show');
        }
    });
});

$(document).on('click','.btnSukses', function (e) {
    document.location = "{{url('recruitment/daftar/cetak_kartu')}}/"+id_pendaftar;
    id_pendaftar = '';
    console.log(id_pendaftar);
    return false;
});

$('#keHome').click(function (e) {
    // e.preventDefault();
    document.location = "{{url('recruitment/daftar')}}";
});

$('#next-step').click(function (e) {
    e.preventDefault();
    counter = counter + 1;
    if (counter == 3) {
            $('#btnSavePendaftaran').attr('hidden', false);
    }
});
</script>
@endpush
