@extends('calon_anggota.index')

@section('content1')

 <!--Halaman Struktur-->
<div class="tab-pane fade" id="kt_tab_pane_1_1" role="tabpanel" aria-labelledby="kt_tab_pane_1_1">
    <div class="card card-custom gutter-b">
        <div class="card-header">
            <div class="card-title">
                <h3 class="card-label">
                Struktur UKM
                <small>English Club</small>
                </h3>
            </div>
        </div>
        <div class="card-body">
            <script>
                $(function () {
                    $("#organisation").orgChart({
                        container: $("#main"),
                        interactive: true,
                        fade: true,
                        speed: 'slow'
                    });

                });
            </script>

            <table class="datatable-bordered datatable-head-custom datatable-table" id="kt_datatable" style="display: block;">
                <tbody class="text-center">
                    <div id="main"></div>
                </tbody>
            </table>
            <ul id="organisation" hidden>
                <li><em>Ketua Ukm </em>
                    <ul>
                        <li class="special">Wakil Ketua
                            <ul>
                                <li>Sekertaris I
                                    <ul>
                                        <li> Sekertaris II</li>
                                    </ul>
                                </li>
                                <li>Divisi
                                    <ul>
                                        <li>Reasoning</li>
                                        <li>News Publication</li>
                                        <li>Talent
                                            <ul>
                                                <li>Debate</li>
                                                <li>Study club</li>
                                                <li>Art and Sport</li>
                                            </ul>
                                        </li>
                                        <li>Welafare</li>
                                        <li>Advocation</li>
                                    </ul>
                                </li>
                                <li>Bendahara I
                                    <ul>
                                        <li> Bendahara II
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>

        </div>
    </div>
    <!--end::Section-->

    <!--begin::Section-->
    <div class="row">
        <div class="col-lg-6">
            <!--begin::Callout-->
            <div class="card card-custom p-6 mb-8 mb-lg-0">
                <div class="card-body">
                    <div class="row">
                        <!--begin::Content-->
                        <div class="col-sm-7">
                            <h2 class="text-dark mb-4">want to know more about us !!!</h2>
                            <p class="text-dark-50 font-size-lg">Penasaran Dengan English Club Klik Tombol Disamping Ya </p>
                        </div>
                        <!--end::Content-->
                        <!--begin::Button-->
                        <div class="col-sm-5 d-flex align-items-center justify-content-sm-end">
                            <a href="https://instagram.com/ec_psdku_polinema_di_kediri?igshid=1rnl9v49yty1c" class="btn font-weight-bolder text-uppercase font-size-lg btn-danger py-3 px-6">
                            <i class="icon-xl fab fa-instagram-square"></i>HERE !!!</a>
                        </div>

                        <!--end::Button-->
                    </div>
                </div>
            </div>
            <!--end::Callout-->
        </div>
        <div class="col-lg-6">
            <!--begin::Callout-->
            <div class="card card-custom p-6">
                <div class="card-body">
                    <div class="row">
                        <!--begin::Content-->
                        <div class="col-sm-7">
                            <h2 class="text-dark mb-4">Live Chat</h2>
                            <p class="text-dark-50 font-size-lg">Jika Ada Pertanyaan Atau Kendala Dalam Simonec Hubungi Kami </p>
                        </div>
                        <!--end::Content-->
                        <!--begin::Button-->
                        <div class="col-sm-5 d-flex align-items-center justify-content-sm-end">
                            <a href="" data-toggle="modal" data-target="#kt_chat_modal" class="btn font-weight-bolder text-uppercase font-size-lg btn-success py-3 px-6">
                                <i class="icon-xl fab fa-whatsapp"></i>Whats'Up</a>
                        </div>
                        <!--end::Button-->
                    </div>
                </div>
            </div>
            <!--end::Callout-->
        </div>
    </div>
</div>

{{-- halaman Divisi --}}
<div class="tab-pane fade" id="kt_tab_pane_2_2" role="tabpanel" aria-labelledby="kt_tab_pane_2_2">
    <!--begin::Section-->
    <div class="row">
       <div class="col-lg-6">
           <div class="card mb-8">
               <div class="card-body">
                   <div class="p-6">
                       <h2 class="text-dark mb-8">Reasoning Departement</h2>
                       <!--begin::Accordion-->
                       <div class="accordion accordion-light accordion-light-borderless accordion-svg-toggle" id="accordionExample7">
                           <!--begin::Item-->
                           <div class="card">
                               <!--begin::Header-->
                               <div class="card-header" id="headingOne7">
                                   <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseOne7" aria-expanded="false" role="button">
                                       <span class="svg-icon svg-icon-primary">
                                           <!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Angle-double-right.svg-->
                                           <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                               <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                   <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                                   <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero"></path>
                                                   <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999)"></path>
                                               </g>
                                           </svg>
                                           <!--end::Svg Icon-->
                                       </span>
                                       <div class="card-label text-dark pl-4">Penjelasan Divisi</div>
                                   </div>
                               </div>
                               <!--end::Header-->
                               <!--begin::Body-->
                               <div id="collapseOne7" class="collapse" aria-labelledby="headingOne7" data-parent="#accordionExample7" style="">
                                <div class="card-body text-dark-50 font-size-lg pl-12">Anim pariatur
                                    cliche reprehenderit, enim eiusmod high life accusamus terry
                                    richardson ad squid. 3 wolf moon officia aute, non cupidatat
                                    skateboard dolor brunch. Food truck quinoa nesciunt laborum
                                    eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on
                                    it squid single-origin coffee nulla assumenda shoreditch et.
                                    Nihil anim keffiyeh helvetica, craft beer labore wes anderson
                                    cred nesciunt sapiente ea proident. Ad vegan excepteur butcher
                                    vice lomo.</div>
                                </div>
                               <!--end::Body-->
                           </div>
                           <!--end::Item-->
                       </div>
                       <!--end::Accordion-->
                   </div>
               </div>
           </div>
       </div>
       <div class="col-lg-6">
        <div class="card mb-8">
            <div class="card-body">
                <div class="p-6">
                    <h2 class="text-dark mb-8">News &amp; Publication Departement</h2>
                    <!--begin::Accordion-->
                    <div class="accordion accordion-light accordion-light-borderless accordion-svg-toggle" id="accordionExample7">
                        <!--begin::Item-->
                        <div class="card">
                            <!--begin::Header-->
                            <div class="card-header" id="headingOne7">
                                <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseOne7" aria-expanded="false" role="button">
                                    <span class="svg-icon svg-icon-primary">
                                        <!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Angle-double-right.svg-->
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                                <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero"></path>
                                                <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999)"></path>
                                            </g>
                                        </svg>
                                        <!--end::Svg Icon-->
                                    </span>
                                    <div class="card-label text-dark pl-4">Penjelasan Divisi</div>
                                </div>
                            </div>
                            <!--end::Header-->
                            <!--begin::Body-->
                            <div id="collapseOne7" class="collapse" aria-labelledby="headingOne7" data-parent="#accordionExample7" style="">
                                <div class="card-body text-dark-50 font-size-lg pl-12">Anim pariatur
                                    cliche reprehenderit, enim eiusmod high life accusamus terry
                                    richardson ad squid. 3 wolf moon officia aute, non cupidatat
                                    skateboard dolor brunch. Food truck quinoa nesciunt laborum
                                    eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on
                                    it squid single-origin coffee nulla assumenda shoreditch et.
                                    Nihil anim keffiyeh helvetica, craft beer labore wes anderson
                                    cred nesciunt sapiente ea proident. Ad vegan excepteur butcher
                                    vice lomo.</div>
                            </div>
                            <!--end::Body-->
                        </div>
                        <!--end::Item-->
                        <!--begin::Item-->

                        <!--end::Item-->

                    </div>
                    <!--end::Accordion-->
                </div>
            </div>
        </div>
       </div>
       <div class="col-lg-6">
        <div class="card mb-8">
            <div class="card-body">
                <div class="p-6">
                    <h2 class="text-dark mb-8">Talent Departement</h2>
                    <!--begin::Accordion-->
                    <div class="accordion accordion-light accordion-light-borderless accordion-svg-toggle" id="accordionExample7">
                        <!--begin::Item-->
                        <div class="card">
                            <!--begin::Header-->
                            <div class="card-header" id="headingOne7">
                                <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseOne7" aria-expanded="false" role="button">
                                    <span class="svg-icon svg-icon-primary">
                                        <!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Angle-double-right.svg-->
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                                <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero"></path>
                                                <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999)"></path>
                                            </g>
                                        </svg>
                                        <!--end::Svg Icon-->
                                    </span>
                                    <div class="card-label text-dark pl-4">Penjelasan Divisi</div>
                                </div>
                            </div>
                            <!--end::Header-->
                            <!--begin::Body-->
                            <div id="collapseOne7" class="collapse" aria-labelledby="headingOne7" data-parent="#accordionExample7" style="">
                                <div class="card-body text-dark-50 font-size-lg pl-12">Anim pariatur
                                     cliche reprehenderit, enim eiusmod high life accusamus terry
                                     richardson ad squid. 3 wolf moon officia aute, non cupidatat
                                     skateboard dolor brunch. Food truck quinoa nesciunt laborum
                                     eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on
                                     it squid single-origin coffee nulla assumenda shoreditch et.
                                     Nihil anim keffiyeh helvetica, craft beer labore wes anderson
                                     cred nesciunt sapiente ea proident. Ad vegan excepteur butcher
                                     vice lomo.</div>
                            </div>
                            <!--end::Body-->
                        </div>
                        <!--end::Item-->
                        <!--begin::Item-->

                        <!--end::Item-->
                    </div>
                    <!--end::Accordion-->
                </div>
            </div>
        </div>
       </div>
       <div class="col-lg-6">
        <div class="card mb-8">
            <div class="card-body">
                <div class="p-6">
                    <h2 class="text-dark mb-8">Debate Departement</h2>
                    <!--begin::Accordion-->
                    <div class="accordion accordion-light accordion-light-borderless accordion-svg-toggle" id="accordionExample7">
                        <!--begin::Item-->
                        <div class="card">
                            <!--begin::Header-->
                            <div class="card-header" id="headingOne7">
                                <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseOne7" aria-expanded="false" role="button">
                                    <span class="svg-icon svg-icon-primary">
                                        <!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Angle-double-right.svg-->
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                                <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero"></path>
                                                <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999)"></path>
                                            </g>
                                        </svg>
                                        <!--end::Svg Icon-->
                                    </span>
                                    <div class="card-label text-dark pl-4">Penjelasan Divisi</div>
                                </div>
                            </div>
                            <!--end::Header-->
                            <!--begin::Body-->
                            <div id="collapseOne7" class="collapse" aria-labelledby="headingOne7" data-parent="#accordionExample7" style="">
                                <div class="card-body text-dark-50 font-size-lg pl-12">Anim pariatur
                                    cliche reprehenderit, enim eiusmod high life accusamus terry
                                    richardson ad squid. 3 wolf moon officia aute, non cupidatat
                                    skateboard dolor brunch. Food truck quinoa nesciunt laborum
                                    eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on
                                    it squid single-origin coffee nulla assumenda shoreditch et.
                                    Nihil anim keffiyeh helvetica, craft beer labore wes anderson
                                    cred nesciunt sapiente ea proident. Ad vegan excepteur butcher
                                    vice lomo.</div>
                            </div>
                            <!--end::Body-->
                        </div>
                        <!--end::Item-->
                        <!--begin::Item-->

                        <!--end::Item-->

                    </div>
                    <!--end::Accordion-->
                </div>
            </div>
        </div>
       </div>

    </div>
</div>


{{-- halaman pengumuman --}}
<div class="tab-pane fade" id="kt_tab_pane_2_1" role="tabpanel" aria-labelledby="kt_tab_pane_2_1">
    <div class="card card-custom gutter-b">
        <div class="card-header">
            <div class="card-title">
                <h3 class="card-label">
                News
                <small>English Club</small>
                </h3>
            </div>
        </div>
        <div class="card-body">
            <div class="p-6">
                <div class="accordion accordion-light accordion-light-borderless accordion-svg-toggle" id="accordionExample7">

                    <div class="card">
                        <!--begin::Header-->
                        <div class="card-header" id="headingThree7">
                            <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseThree7" aria-expanded="true" role="button">
                                <span class="svg-icon svg-icon-primary">
                                    <!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Angle-double-right.svg-->
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                            <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero"></path>
                                            <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999)"></path>
                                        </g>
                                    </svg>
                                    <!--end::Svg Icon-->
                                </span>
                                <div class="card-label text-dark pl-4"> Hasil Test Anggota Baru {{Date('Y')}}</div>
                            </div>
                        </div>

                        <div id="collapseThree7" class="collapse" aria-labelledby="headingThree7" data-parent="#accordionExample7">
                            @if (DeHelper::setting_hasil_pendaftaran() == 'hidden')
                                <div class="card-body text-dark-50 font-size-lg pl-12">
                                    {{-- card here --}}
                                    <div class="d-flex align-items-center bg-light-info rounded p-5">
                                        <!--begin::Icon-->
                                        <span class="svg-icon svg-icon-info mr-5">
                                            <span class="svg-icon svg-icon-lg">
                                                <!--begin::Svg Icon | path:assets/media/svg/icons/General/Attachment2.svg-->
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <path d="M11.7573593,15.2426407 L8.75735931,15.2426407 C8.20507456,15.2426407 7.75735931,15.6903559 7.75735931,16.2426407 C7.75735931,16.7949254 8.20507456,17.2426407 8.75735931,17.2426407 L11.7573593,17.2426407 L11.7573593,18.2426407 C11.7573593,19.3472102 10.8619288,20.2426407 9.75735931,20.2426407 L5.75735931,20.2426407 C4.65278981,20.2426407 3.75735931,19.3472102 3.75735931,18.2426407 L3.75735931,14.2426407 C3.75735931,13.1380712 4.65278981,12.2426407 5.75735931,12.2426407 L9.75735931,12.2426407 C10.8619288,12.2426407 11.7573593,13.1380712 11.7573593,14.2426407 L11.7573593,15.2426407 Z" fill="#000000" opacity="0.3" transform="translate(7.757359, 16.242641) rotate(-45.000000) translate(-7.757359, -16.242641)"></path>
                                                        <path d="M12.2426407,8.75735931 L15.2426407,8.75735931 C15.7949254,8.75735931 16.2426407,8.30964406 16.2426407,7.75735931 C16.2426407,7.20507456 15.7949254,6.75735931 15.2426407,6.75735931 L12.2426407,6.75735931 L12.2426407,5.75735931 C12.2426407,4.65278981 13.1380712,3.75735931 14.2426407,3.75735931 L18.2426407,3.75735931 C19.3472102,3.75735931 20.2426407,4.65278981 20.2426407,5.75735931 L20.2426407,9.75735931 C20.2426407,10.8619288 19.3472102,11.7573593 18.2426407,11.7573593 L14.2426407,11.7573593 C13.1380712,11.7573593 12.2426407,10.8619288 12.2426407,9.75735931 L12.2426407,8.75735931 Z" fill="#000000" transform="translate(16.242641, 7.757359) rotate(-45.000000) translate(-16.242641, -7.757359)"></path>
                                                        <path d="M5.89339828,3.42893219 C6.44568303,3.42893219 6.89339828,3.87664744 6.89339828,4.42893219 L6.89339828,6.42893219 C6.89339828,6.98121694 6.44568303,7.42893219 5.89339828,7.42893219 C5.34111353,7.42893219 4.89339828,6.98121694 4.89339828,6.42893219 L4.89339828,4.42893219 C4.89339828,3.87664744 5.34111353,3.42893219 5.89339828,3.42893219 Z M11.4289322,5.13603897 C11.8194565,5.52656326 11.8194565,6.15972824 11.4289322,6.55025253 L10.0147186,7.96446609 C9.62419433,8.35499039 8.99102936,8.35499039 8.60050506,7.96446609 C8.20998077,7.5739418 8.20998077,6.94077682 8.60050506,6.55025253 L10.0147186,5.13603897 C10.4052429,4.74551468 11.0384079,4.74551468 11.4289322,5.13603897 Z M0.600505063,5.13603897 C0.991029355,4.74551468 1.62419433,4.74551468 2.01471863,5.13603897 L3.42893219,6.55025253 C3.81945648,6.94077682 3.81945648,7.5739418 3.42893219,7.96446609 C3.0384079,8.35499039 2.40524292,8.35499039 2.01471863,7.96446609 L0.600505063,6.55025253 C0.209980772,6.15972824 0.209980772,5.52656326 0.600505063,5.13603897 Z" fill="#000000" opacity="0.3" transform="translate(6.014719, 5.843146) rotate(-45.000000) translate(-6.014719, -5.843146)"></path>
                                                        <path d="M17.9142136,15.4497475 C18.4664983,15.4497475 18.9142136,15.8974627 18.9142136,16.4497475 L18.9142136,18.4497475 C18.9142136,19.0020322 18.4664983,19.4497475 17.9142136,19.4497475 C17.3619288,19.4497475 16.9142136,19.0020322 16.9142136,18.4497475 L16.9142136,16.4497475 C16.9142136,15.8974627 17.3619288,15.4497475 17.9142136,15.4497475 Z M23.4497475,17.1568542 C23.8402718,17.5473785 23.8402718,18.1805435 23.4497475,18.5710678 L22.0355339,19.9852814 C21.6450096,20.3758057 21.0118446,20.3758057 20.6213203,19.9852814 C20.2307961,19.5947571 20.2307961,18.9615921 20.6213203,18.5710678 L22.0355339,17.1568542 C22.4260582,16.76633 23.0592232,16.76633 23.4497475,17.1568542 Z M12.6213203,17.1568542 C13.0118446,16.76633 13.6450096,16.76633 14.0355339,17.1568542 L15.4497475,18.5710678 C15.8402718,18.9615921 15.8402718,19.5947571 15.4497475,19.9852814 C15.0592232,20.3758057 14.4260582,20.3758057 14.0355339,19.9852814 L12.6213203,18.5710678 C12.2307961,18.1805435 12.2307961,17.5473785 12.6213203,17.1568542 Z" fill="#000000" opacity="0.3" transform="translate(18.035534, 17.863961) scale(1, -1) rotate(45.000000) translate(-18.035534, -17.863961)"></path>
                                                    </g>
                                                </svg>
                                                <!--end::Svg Icon-->
                                            </span>
                                        </span>
                                        <!--end::Icon-->
                                        <!--begin::Title-->
                                        <div class="d-flex flex-column flex-grow-1 mr-2">
                                            <a href="" class="font-weight-bold text-dark-75 text-hover-primary font-size-lg mb-1 text-align">
                                                <span class="font-weight-bolder text-info py-1 font-size-lg">Test Sedang Berlangsung </span>
                                            </a>

                                        </div>
                                        <!--end::Title-->
                                        <!--begin::Lable-->
                                        <span class="font-weight-bolder text-info py-1 font-size-lg">
                                            <img height=50 src="{{asset('assets/images/load3.gif')}}">
                                        </span>
                                        <!--end::Lable-->
                                    </div>
                                </div>
                            @else
                                <div class="card-body text-dark-50 font-size-lg pl-12" >
                                    <div class="d-flex align-items-center bg-light-info rounded p-5">
                                        <!--begin::Icon-->
                                        <span class="svg-icon svg-icon-info mr-5">
                                            <span class="svg-icon svg-icon-lg">
                                                <!--begin::Svg Icon | path:assets/media/svg/icons/General/Attachment2.svg-->
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <path d="M11.7573593,15.2426407 L8.75735931,15.2426407 C8.20507456,15.2426407 7.75735931,15.6903559 7.75735931,16.2426407 C7.75735931,16.7949254 8.20507456,17.2426407 8.75735931,17.2426407 L11.7573593,17.2426407 L11.7573593,18.2426407 C11.7573593,19.3472102 10.8619288,20.2426407 9.75735931,20.2426407 L5.75735931,20.2426407 C4.65278981,20.2426407 3.75735931,19.3472102 3.75735931,18.2426407 L3.75735931,14.2426407 C3.75735931,13.1380712 4.65278981,12.2426407 5.75735931,12.2426407 L9.75735931,12.2426407 C10.8619288,12.2426407 11.7573593,13.1380712 11.7573593,14.2426407 L11.7573593,15.2426407 Z" fill="#000000" opacity="0.3" transform="translate(7.757359, 16.242641) rotate(-45.000000) translate(-7.757359, -16.242641)"></path>
                                                        <path d="M12.2426407,8.75735931 L15.2426407,8.75735931 C15.7949254,8.75735931 16.2426407,8.30964406 16.2426407,7.75735931 C16.2426407,7.20507456 15.7949254,6.75735931 15.2426407,6.75735931 L12.2426407,6.75735931 L12.2426407,5.75735931 C12.2426407,4.65278981 13.1380712,3.75735931 14.2426407,3.75735931 L18.2426407,3.75735931 C19.3472102,3.75735931 20.2426407,4.65278981 20.2426407,5.75735931 L20.2426407,9.75735931 C20.2426407,10.8619288 19.3472102,11.7573593 18.2426407,11.7573593 L14.2426407,11.7573593 C13.1380712,11.7573593 12.2426407,10.8619288 12.2426407,9.75735931 L12.2426407,8.75735931 Z" fill="#000000" transform="translate(16.242641, 7.757359) rotate(-45.000000) translate(-16.242641, -7.757359)"></path>
                                                        <path d="M5.89339828,3.42893219 C6.44568303,3.42893219 6.89339828,3.87664744 6.89339828,4.42893219 L6.89339828,6.42893219 C6.89339828,6.98121694 6.44568303,7.42893219 5.89339828,7.42893219 C5.34111353,7.42893219 4.89339828,6.98121694 4.89339828,6.42893219 L4.89339828,4.42893219 C4.89339828,3.87664744 5.34111353,3.42893219 5.89339828,3.42893219 Z M11.4289322,5.13603897 C11.8194565,5.52656326 11.8194565,6.15972824 11.4289322,6.55025253 L10.0147186,7.96446609 C9.62419433,8.35499039 8.99102936,8.35499039 8.60050506,7.96446609 C8.20998077,7.5739418 8.20998077,6.94077682 8.60050506,6.55025253 L10.0147186,5.13603897 C10.4052429,4.74551468 11.0384079,4.74551468 11.4289322,5.13603897 Z M0.600505063,5.13603897 C0.991029355,4.74551468 1.62419433,4.74551468 2.01471863,5.13603897 L3.42893219,6.55025253 C3.81945648,6.94077682 3.81945648,7.5739418 3.42893219,7.96446609 C3.0384079,8.35499039 2.40524292,8.35499039 2.01471863,7.96446609 L0.600505063,6.55025253 C0.209980772,6.15972824 0.209980772,5.52656326 0.600505063,5.13603897 Z" fill="#000000" opacity="0.3" transform="translate(6.014719, 5.843146) rotate(-45.000000) translate(-6.014719, -5.843146)"></path>
                                                        <path d="M17.9142136,15.4497475 C18.4664983,15.4497475 18.9142136,15.8974627 18.9142136,16.4497475 L18.9142136,18.4497475 C18.9142136,19.0020322 18.4664983,19.4497475 17.9142136,19.4497475 C17.3619288,19.4497475 16.9142136,19.0020322 16.9142136,18.4497475 L16.9142136,16.4497475 C16.9142136,15.8974627 17.3619288,15.4497475 17.9142136,15.4497475 Z M23.4497475,17.1568542 C23.8402718,17.5473785 23.8402718,18.1805435 23.4497475,18.5710678 L22.0355339,19.9852814 C21.6450096,20.3758057 21.0118446,20.3758057 20.6213203,19.9852814 C20.2307961,19.5947571 20.2307961,18.9615921 20.6213203,18.5710678 L22.0355339,17.1568542 C22.4260582,16.76633 23.0592232,16.76633 23.4497475,17.1568542 Z M12.6213203,17.1568542 C13.0118446,16.76633 13.6450096,16.76633 14.0355339,17.1568542 L15.4497475,18.5710678 C15.8402718,18.9615921 15.8402718,19.5947571 15.4497475,19.9852814 C15.0592232,20.3758057 14.4260582,20.3758057 14.0355339,19.9852814 L12.6213203,18.5710678 C12.2307961,18.1805435 12.2307961,17.5473785 12.6213203,17.1568542 Z" fill="#000000" opacity="0.3" transform="translate(18.035534, 17.863961) scale(1, -1) rotate(45.000000) translate(-18.035534, -17.863961)"></path>
                                                    </g>
                                                </svg>
                                                <!--end::Svg Icon-->
                                            </span>
                                        </span>
                                        <!--end::Icon-->
                                        <!--begin::Title-->
                                        <div class="d-flex flex-column flex-grow-1 mr-2">
                                            <a href="" class="font-weight-bold text-dark-75 text-hover-primary font-size-lg mb-1 text-align">
                                                <span class="font-weight-bolder text-info py-1 font-size-lg">Jika Ingin Melihat Semua Hasil Test klik Button Disamping  </span>
                                            </a>

                                        </div>
                                        <!--end::Title-->
                                        <!--begin::Lable-->
                                        <span class="font-weight-bolder text-info py-1 font-size-lg">
                                            <a href="{{route('cetak_hasil_test')}}"  class="btn font-weight-bolder text-uppercase font-size-lg btn-success py-3 px-6">
                                                <i class="icon-xl fab fa-"></i>Download</a>
                                        </span>
                                        <!--end::Lable-->
                                    </div>
                                    <br>
                                    <table class="table table-separate table-hover table-head-custom table-checkable"  id="tb_hasil_tes">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nim</th>
                                                <th>Nama</th>
                                                <th>Program Studi</th>
                                                <th>Kelas</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>

                                </div>
                            @endif
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Item-->
                </div>
                <!--end::Accordion-->
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        var table = $('#tb_hasil_tes').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ajax: "{{ route('showAnggotaDiterima') }}",
            language: {
                processing: "Sedang diproses...<img height=50 src='{{asset('assets/images/load3.gif')}}'>"
            },
            columns: [
                {
                    data: 'no',
                    name: 'no',
                    searchable : false,
                    orderable:true
                }, {
                    data: 'nim',
                    name: 'nim',
                    orderable:true
                },{
                    data: 'nama',
                    name: 'nama',
                    orderable: false,
                    searchable: true
                },{
                    data: 'nama_prodi',
                    name: 'nama_prodi',
                    orderable: false,
                    searchable: true
                },{
                    data: 'nama_kelas',
                    name: 'nama_kelas',
                    orderable: false,
                    searchable: true
                },{
                    data: 'is_anggota',
                    name: 'is_anggota',
                    orderable: false,
                    searchable: true
                },
            ],
            columnDefs: [
                { className: 'text-center', targets: [0,1,2,3,4,5] },
            ]
        });

    });

</script>

@endsection
