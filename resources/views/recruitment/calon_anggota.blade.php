@extends('layouts.app')

@section('content')
<div class="page-content container-fluid">
    <div class="row" data-plugin="matchHeight" data-by-row="true">
        <div class="col-xxl-12 col-lg-12">

            <div class="card card-shadow" style="background-image: linear-gradient(180deg,rgba(255, 255, 255, 0.6),rgba(255, 255, 255, 0.9));">
                <div class="card-block py-0">
                    <div class="row no-space py-0">
                        <div class="col-md-6">
                            <div class="btn-block py-10">
                                <h4 class="profile-user">Calon Anggota</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-xxl-12 col-lg-12">
            <div class="panel nav-tabs-horizontal nav-tabs-line panel shadow panel-bordered " data-plugin="tabs">
                <div class="panel-heading panel-heading-tab bg-white">
                    <ul class="nav nav-tabs nav-tabs-solid text-dark" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active show tap_daftar" data-toggle="tab" href="#panelTab1" aria-controls="panelTab1" role="tab" aria-expanded="true" aria-selected="true">
                                <div class="text-dark py-0">Pendaftar</div>
                            </a></li>
                        <li class="nav-item tap_tes_tulis">
                            <a class="nav-link" data-toggle="tab" href="#panelTab2" aria-controls="panelTab2" role="tab" aria-selected="false">
                                <div class="text-dark py-0">Selesai Tes Tulis</div>
                            </a></li>
                        <li class="nav-item tap_tes_wawancara">
                            <a class="nav-link" data-toggle="tab" href="#panelTab3" aria-controls="panelTab3" role="tab" aria-selected="false">
                                <div class="text-dark py-0">Selesai Tes Wawancara</div>
                            </a></li>
                    </ul>
                    {{-- <br class="tampil-title-mobile"> --}}

                </div>

                <div class="panel-body pt-20">
                    <div class="tab-content">

                        <div class="tab-pane active show" id="panelTab1" role="tabpanel">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-xxl-12 col-lg-12">

                                        <table class="table table-bordered table-hover table-striped" cellspacing="0" id="tb_pendaftar">
                                            <thead class="text-center">
                                                <tr>
                                                    <th><b>No.</b></th>
                                                    <th><b>NIM</b></th>
                                                    <th><b>NAMA</b></th>
                                                    <th><b>Kelas</b></th>
                                                    <th><b>Prodi</b></th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="panelTab2" role="tabpanel">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-xxl-12 col-lg-12">

                                        <table class="table table-bordered table-hover table-striped" cellspacing="0" id="tb_tes_tulis">
                                            <thead class="text-center">
                                                <tr>
                                                    <th><b>No.</b></th>
                                                    <th><b>NIM</b></th>
                                                    <th><b>NAMA</b></th>
                                                    <th><b>Kelas</b></th>
                                                    <th><b>Prodi</b></th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="panelTab3" role="tabpanel">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-xxl-12 col-lg-12">

                                        <table class="table table-bordered table-hover table-striped" cellspacing="0" id="tb_tes_wawancara">
                                            <thead class="text-center">
                                                <t>
                                                    <th><b>No.</b></th>
                                                    <th><b>NIM</b></th>
                                                    <th><b>NAMA</b></th>
                                                    <th><b>Kelas</b></th>
                                                    <th><b>Prodi</b></th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>

$(document).ready(function () {
    // data table server side configurasi
    var table = $('#tb_pendaftar').DataTable({
        processing: true,
        serverSide: true,
        "scrollY": "250px",
        "scrollx": "1050px",
        "scrollCollapse": true,
        ajax: "{{route('DTCalonAnggota')}}?jenis=0",
        language: {
                        processing: "Sedang diproses...<img height=50 src='{{asset('assets/images/load3.gif')}}'>"
                    },
        columns: [
            {
                data: 'no',
                name: 'no',
                searchable : false,
                orderable:true
            }, {
                data: 'nim',
                name: 'nim',
                orderable:true,
                searchable: false
            },{
                data: 'nama',
                name: 'nama',
                orderable:true,
                searchable: false
            },{
                data: 'kelas',
                name: 'kelas',
                orderable:true
            },{
                data: 'prodi',
                name: 'prodi',
                orderable:true
            },
        ],
        columnDefs: [
            { className: 'text-center', targets: [0,1,3,4] },
        ]
    });

    var table1 = $('#tb_tes_tulis').DataTable({
        processing: true,
        serverSide: true,
        "scrollY": "250px",
        "scrollx": "1050px",
        "scrollCollapse": true,
        ajax: "{{route('DTCalonAnggota')}}?jenis=1",
        language: {
                        processing: "Sedang diproses...<img height=50 src='{{asset('assets/images/load3.gif')}}'>"
                    },
        columns: [
            {
                data: 'no',
                name: 'no',
                searchable : false,
                orderable:true
            }, {
                data: 'nim',
                name: 'nim',
                orderable:true,
                searchable: false
            },{
                data: 'nama',
                name: 'nama',
                orderable:true,
                searchable: false

            },{
                data: 'kelas',
                name: 'kelas',
                orderable:true
            },{
                data: 'prodi',
                name: 'prodi',
                orderable:true
            },
        ],
        columnDefs: [
            { className: 'text-center', targets: [0,1,3,4] },
        ]
    });

    var table2 = $('#tb_tes_wawancara').DataTable({
        processing: true,
        serverSide: true,
        "scrollY": "250px",
        "scrollx": "1050px",
        "scrollCollapse": true,
        ajax: "{{route('DTCalonAnggota')}}?jenis=2",
        language: {
                        processing: "Sedang diproses...<img height=50 src='{{asset('assets/images/load3.gif')}}'>"
                    },
        columns: [
            {
                data: 'no',
                name: 'no',
                searchable : false,
                orderable:true
            }, {
                data: 'nim',
                name: 'nim',
                orderable:true,
                searchable: false
            },{
                data: 'nama',
                name: 'nama',
                orderable:true,
                searchable: false

            },{
                data: 'kelas',
                name: 'kelas',
                orderable:true
            },{
                data: 'prodi',
                name: 'prodi',
                orderable:true
            },
        ],
        columnDefs: [
            { className: 'text-center', targets: [0,1,3,4] },
        ]
    });

    $(document).on('click', '.tap_tes_tulis', function () {
        console.log('ok');
        table1.ajax.reload();
    });

    $(document).on('click', '.tap_tes_wawancara', function () {
        console.log('ok');
        table2.ajax.reload();
    });

    $(document).on('click', '.tap_daftar', function () {
        console.log('ok');
        table.ajax.reload();
    });

    
});

</script>

@endpush

