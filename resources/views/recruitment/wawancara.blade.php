@extends('layouts.app')

@section('content')
@php
        $user 		= Session::get("data_user");
        $flag_lv = $user['flag_lv'];

@endphp
<div class="row main_base" data-by-row="true">

<form id="form_wawancara" name="form_wawancara" class="container-fluid">
    <div class="col-xxl-12 col-lg-12 " class="container-fluid">
        <div class="card card-shadow" style="background-image: linear-gradient(180deg,rgba(255, 255, 255, 0.6),rgba(255, 255, 255, 0.9));">
            <div class="card-block py-0">
                <div class="row no-space py-0">
                    <div class="col-md-6">
                        <div class="btn-block py-10">
                            <h4 class="profile-user">Calon Anggota</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xxl-12 col-lg-12" class="container-fluid">
        <div class="panel nav-tabs-horizontal nav-tabs-line panel shadow panel-bordered " data-plugin="tabs">
            <div class="panel-heading panel-heading-tab bg-white">
                <ul class="nav nav-tabs nav-tabs-solid text-dark" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active show tap_daftar" data-toggle="tab" href="#panelTab1" aria-controls="panelTab1" role="tab" aria-expanded="true" aria-selected="true">
                            <div class="text-dark py-0">Pendaftar</div>
                        </a></li>
                    <li class="nav-item tap_tes_tulis">
                        <a class="nav-link" data-toggle="tab" href="#panelTab2" aria-controls="panelTab2" role="tab" aria-selected="false">
                            <div class="text-dark py-0">Selesai Tes Tulis</div>
                        </a></li>
                    <li class="nav-item tap_tes_wawancara">
                        <a class="nav-link" data-toggle="tab" href="#panelTab3" aria-controls="panelTab3" role="tab" aria-selected="false">
                            <div class="text-dark py-0">Selesai Tes Wawancara</div>
                        </a></li>
                </ul>
                {{-- <br class="tampil-title-mobile"> --}}

            </div>

            <div class="panel-body pt-20">
                <div class="tab-content">

                    <div class="tab-pane active show" id="panelTab1" role="tabpanel">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xxl-12 col-lg-12">

                                    <table class="table table-bordered table-hover table-striped" cellspacing="0" id="tb_pendaftar">
                                        <thead class="text-center">
                                            <tr>
                                                <th><b>No.</b></th>
                                                <th><b>NIM</b></th>
                                                <th><b>NAMA</b></th>
                                                <th><b>Kelas</b></th>
                                                <th><b>Prodi</b></th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane" id="panelTab2" role="tabpanel">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xxl-12 col-lg-12">

                                    <table class="table table-bordered table-hover table-striped" cellspacing="0" id="tb_tes_tulis">
                                        <thead class="text-center">
                                            <tr>
                                                <th><b>No.</b></th>
                                                <th><b>NIM</b></th>
                                                <th><b>NAMA</b></th>
                                                <th><b>Kelas</b></th>
                                                <th><b>Prodi</b></th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane" id="panelTab3" role="tabpanel">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xxl-12 col-lg-12">

                                    <table class="table table-bordered table-hover table-striped" cellspacing="0" id="tb_tes_wawancara">
                                        <thead class="text-center">
                                            <t>
                                                <th><b>No.</b></th>
                                                <th><b>NIM</b></th>
                                                <th><b>NAMA</b></th>
                                                <th><b>Kelas</b></th>
                                                <th><b>Prodi</b></th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        @csrf
    <div class="col-xxl-12 col-lg-12 panel_awalan">
        <div class="panel shadow panel-bordered">
            <div class="panel-heading">
                <di class="panel-title">Tes Wawancara</di>
                <div class="panel-actions ">
                    <a class="panel-action icon wb-minus" aria-expanded="true" data-toggle="panel-collapse" aria-hidden="true"></a>
                    <a class="panel-action icon wb-expand" data-toggle="panel-fullscreen" aria-hidden="true"></a>
                </div>
            </div>
            <div class="panel-body pb-35 pt-35 ">
                <div class="row">
                    <div class="col-md-6">
                        <h3 class="text-dark text-center mb-4 display-4">Interview!</h3>

                        <div class="separator separator-solid separator-border-2 separator-info"></div>

                        <blockquote class="blockquote P-0 pt-4">
                            <p class="mb-0 ">Selamat datang di halaman tes wawancara.
                                            Pada halaman ini kalian ditugaskan untuk mewawancarai calon anggota baru English Club.
                                            Pertama-tama silahkan <b> scan kartu tes dengan menggunakan kamera gadget anda</b>   .
                                            Maka secara otomatis sistem akan menampilkan jawaban tes tulis dari calon anggota English Club</p>
                        </blockquote>
                    </div>
                    <div class="col-md-6">
                        <div class="qr_login">
                            <div class="form-group row">
                                <div class="col-md-12" id="div_select" style="display: none">
                                    <div class="form-group">
                                        <label class="control-label">Kamera</label>
                                        <select class="form-control select" id="popup_camera">

                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12" id="">
                                    <video id="preview2"></video>
                                </div>
                            </div>
                        </div>
                        {{-- <video id="preview2"></video> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xxl-12 col-lg-12 panel_isi" hidden>
        <div class="panel shadow panel-bordered">
            <div class="panel-body pb-35 pt-35 ">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="text-dark text-center mb-4 display-5">Perhatian!</h3>

                        <div class="separator separator-solid separator-border-2 separator-info"></div>

                        <blockquote class="blockquote P-0 pt-4">
                            <p class="mb-0 ">
                                Lakukan tes wawancara dengan sungguh-sungguh, santai dan sesuai tujuan. berikut tips wawancara yang dapat diterapkan :
                            </p>
                            <ol>
                                <li>Perhatikan pertanyaan dan standar kelulusan</li>
                                <li>Perhatikan Sikap dan penampilan</li>
                                <li>Perhatikan eye contact kandidat, kepercayan dirinya, dan gerak-geriknya</li>
                            </ol>
                        </blockquote>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="col-xxl-12 col-lg-12 panel_isi sebelum_soal" hidden>
        <div class="panel shadow panel-bordered">
            <div class="panel-body pb-35 pt-35 ">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="display-5 purple-600">#Biodata Peserta</h3>
                        <dl class="dl-horizontal row">
                            <dt class="col-sm-3">NIM</dt>
                            <dd class="col-sm-9" id="v_nim"></dd>

                            <dt class="col-sm-3">Nama lengkap</dt>
                            <dd class="col-sm-9" id="v_nama"></dd>
                            <dt class="col-sm-3">Kelas / Prodi</dt>
                            <dd class="col-sm-9" id="v_kelas"></dd>
                        </dl>
                        <input hidden class="form-control" name="nim" id="nim" value=""/>
                        <input hidden class="form-control" name="kelas" id="kelas" value=""/>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xxl-12 col-lg-12 panel_isi mb-30" hidden>
        <div class="panel shadow panel-bordered">
            <div class="panel-heading">
                <h3 class="panel-title display-5">Konfirmasi selesai wawancara</h3>
            </div>
            <div class="panel-body b-15 pt-15 ">
                <button class="btn btn-outline-info btn-block btn_selesai">SELESAI</button>
            </div>
        </div>
    </div>
    </form>

</div>

@endsection

@push('scripts')
<script>
    $(document).ready(function () {
        scannerInstanscan() ;

          // data table server side configurasi
    var table = $('#tb_pendaftar').DataTable({
        processing: true,
        serverSide: true,
        "scrollY": "250px",
        "scrollx": "1050px",
        "scrollCollapse": true,
        ajax: "{{route('DTCalonAnggota')}}?jenis=0",
        language: {
                        processing: "Sedang diproses...<img height=50 src='{{asset('assets/images/load3.gif')}}'>"
                    },
        columns: [
            {
                data: 'no',
                name: 'no',
                searchable : false,
                orderable:true
            }, {
                data: 'nim',
                name: 'nim',
                orderable:true,
                searchable: false
            },{
                data: 'nama',
                name: 'nama',
                orderable:true,
                searchable: false
            },{
                data: 'kelas',
                name: 'kelas',
                orderable:true
            },{
                data: 'prodi',
                name: 'prodi',
                orderable:true
            },
        ],
        columnDefs: [
            { className: 'text-center', targets: [0,1,3,4] },
        ]
    });

    var table1 = $('#tb_tes_tulis').DataTable({
        processing: true,
        serverSide: true,
        "scrollY": "250px",
        "scrollx": "1050px",
        "scrollCollapse": true,
        ajax: "{{route('DTCalonAnggota')}}?jenis=1",
        language: {
                        processing: "Sedang diproses...<img height=50 src='{{asset('assets/images/load3.gif')}}'>"
                    },
        columns: [
            {
                data: 'no',
                name: 'no',
                searchable : false,
                orderable:true
            }, {
                data: 'nim',
                name: 'nim',
                orderable:true,
                searchable: false
            },{
                data: 'nama',
                name: 'nama',
                orderable:true,
                searchable: false

            },{
                data: 'kelas',
                name: 'kelas',
                orderable:true
            },{
                data: 'prodi',
                name: 'prodi',
                orderable:true
            },
        ],
        columnDefs: [
            { className: 'text-center', targets: [0,1,3,4] },
        ]
    });

    var table2 = $('#tb_tes_wawancara').DataTable({
        processing: true,
        serverSide: true,
        "scrollY": "250px",
        "scrollx": "1050px",
        "scrollCollapse": true,
        ajax: "{{route('DTCalonAnggota')}}?jenis=2",
        language: {
                        processing: "Sedang diproses...<img height=50 src='{{asset('assets/images/load3.gif')}}'>"
                    },
        columns: [
            {
                data: 'no',
                name: 'no',
                searchable : false,
                orderable:true
            }, {
                data: 'nim',
                name: 'nim',
                orderable:true,
                searchable: false
            },{
                data: 'nama',
                name: 'nama',
                orderable:true,
                searchable: false

            },{
                data: 'kelas',
                name: 'kelas',
                orderable:true
            },{
                data: 'prodi',
                name: 'prodi',
                orderable:true
            },
        ],
        columnDefs: [
            { className: 'text-center', targets: [0,1,3,4] },
        ]
    });

    $(document).on('click', '.tap_tes_tulis', function () {
        console.log('ok');
        table1.ajax.reload();
    });

    $(document).on('click', '.tap_tes_wawancara', function () {
        console.log('ok');
        table2.ajax.reload();
    });

    $(document).on('click', '.tap_daftar', function () {
        console.log('ok');
        table.ajax.reload();
    });

    });

    var scanner = new Instascan.Scanner({
        video: document.getElementById('preview2'),
        scanPeriod: 5,
        mirror: false
    });

    function animasi_leave(this_) {
        $(this_).css({
            "border": "solid",
            "border-color": "#3e8ef7"
        });
    }

    function animasi_enter(this_) {
        $(this_).css({
            "border": "",
            "border-color": ""
        });
    }

    // FUNCTION SCANNER
    function scannerInstanscan() {
        Instascan.Camera.getCameras().then(function(cameras) {
            if (cameras.length > 0) {
                if (cameras.length > 1) {
                    $("#div_select").show();
                    $("#popup_camera").html("");
                    $("#popup_camera").append("<option value=''> -- Pilih Kamera -- </option>");
                    for (i in cameras) {
                        $("#popup_camera").append("<option value='" + [i] + "'>" + cameras[i].name + "</option>");
                    }

                    if (cameras[1] != "") {
                        scanner.start(cameras[1]);
                        $("#popup_camera").val(1).change();
                    } else {
                        alert('Kamera tidak ditemukan!');
                    }
                } else {
                    if (cameras[0] != "") {
                        scanner.start(cameras[0]);
                        $("#popup_camera").val(0).change();
                    } else {
                        alert('Kamera tidak ditemukan!');
                    }
                }

                $("#popup_camera").change(function() {
                    var id = $("#popup_camera :selected").val();

                    if (id != '') {
                        if (cameras[id] != "") {
                            scanner.start(cameras[id]);
                        } else {
                            alert('Kamera tidak ditemukan!');
                        }
                    }
                })

            } else {
                console.error('No cameras found.');
                alert('No cameras found.');
            }
        }).catch(function(e) {
            console.error(e);
            alert(e);
        });
    }

    scanner.addListener('scan', function(content) {
        if (content != '') {
            console.log(content);

            $.ajax({
                url: "{{route('getDataWawancara')}}?nim="+content,
                type: "GET",
                dataType: 'json',
                success: function (data) {
                    console.log(data);
                    if (data.code === 0) {
                        swal({
                            title: 'Warning!',
                            text: data.message,
                            type: "warning",
                            showCancelButton: false,
                            confirmButtonClass: "btn-success",
                            confirmButtonText: 'OK, Mengerti',
                            closeOnConfirm: true,
                            closeOnCancel: true
                            })
                    }else{
                        $('#nim').val(data.nim_calon);
                        $('#nama').val(data.nama_mhs);
                        $('#v_nim').text(data.nim_calon);
                        $('#v_nama').text(data.nama_mhs);
                        $('#v_kelas').text(data.nama_kelas+' / '+data.nama_prodi);

                        $('.panel_isi').attr('hidden', false);
                        $('.panel_awalan').attr('hidden', true);
                        $('.sebelum_soal').after(data.content);
                        scanner.stop();
                        $("html, body").animate({scrollTop: 0}, 500);
                    }
                    // // $('#formAboff').trigger('reset');
                    // $('.nim_absen').val('');
                    // $('#btnSave_aboff').html('Simpan');
                    // $('#btnSave_aboff').removeAttr('disabled',true);
                    // $('#modalAbsensi1').modal('hide');
                },
                error: function (data) {
                    console.log('Error:', data);
                    $('#btnSave_aboff').removeAttr('disabled',true);
                    $('#btnSave_aboff').html('Simpan');
                    toastr.error('Kelola Data Gagal');
                }
            })
        } else {
            var item = content.split("||");
            // window.location.href = "/" + item;
            console.log(item);
            setTimeout(function() {
                swal({
                    title: "Data Error!",
                    text: "Content dari Kode QR tersebut Kosong!",
                    type: "error"
                }, function() {
                    $("#modalAbsensi1").modal("hide");
                }, 1000);
            })
        }
    });

    $(document).on('click','.btn_selesai', function (e) {
        e.preventDefault();
        $(this).html('Sending..');
        $(this).attr('disabled',true);

        $.ajax({
            data: $('#form_wawancara').serialize(),
            url: "{{route('konfirmasiSelesaiWawancara')}}",
            type: "POST",
            dataType: 'json',
            success: function (data) {
                console.log(data);
                if (data.code == '200') {
                    swal({
                        title: "Sukses!",
                        text: data.message,
                        type: "success",
                        showCancelButton: false,
                        confirmButtonClass: "btn-success",
                        confirmButtonText: 'OK',
                        closeOnConfirm: false
                    });

        }
                scanner.start();
                $('#form_wawancara').trigger('reset');
                $('.panel_isi').attr('hidden', true);

                $('.panel_awalan').attr('hidden', false);
                $('.btn_selesai').html('SELESAI');
                $('.btn_selesai').attr('disabled',false);
                $('.soal_jawaban').remove();
                $("html, body").animate({scrollTop: 0}, 500);
            },
            error: function (data) {
                console.log('Error:', data);
                $('#btnSave').html('Simpan');
                toastr.error('Kelola Data Gagal');
            }
        })
    });
</script>
@endpush
