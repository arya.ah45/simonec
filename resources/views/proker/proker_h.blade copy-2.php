@extends('layouts.app')

@section('content')
@php
        $user 		= Session::get("data_user");
        $flag_lv = $user['flag_lv'];

@endphp
<div class="row" data-plugin="matchHeight" data-by-row="true">
    <div class="col-xxl-12 col-lg-12">
        <div class="panel shadow panel-bordered">
                <div class="panel-heading">
                    <h3 class="panel-title">
                            @if ($cek_page == 'normal')
                                List Proker    
                            @else
                                List Proker terhapus 
                            @endif
                        </h3>
                    <div class="panel-actions ">
                        <a class="panel-action icon wb-minus" aria-expanded="true" data-toggle="panel-collapse" aria-hidden="true"></a>
                        <a class="panel-action icon wb-expand" data-toggle="panel-fullscreen" aria-hidden="true"></a>
                    </div>
                </div>
                <div class="panel-body pb-15 pt-15 ">
                    <div class="row pt-3">
                        @if ($cek_page == 'normal')
                        <div class="col-xxl-6 col-lg-6" style="border-right: solid rgba(182, 180, 180, 0.496) 1px">
                            <h5>Filter</h5>
                            <dl class="dl-horizontal row">
                                <dt class="col-sm-1">Tahun</dt>
                                <dd class="col-sm-4"><select type="text" class="form-control form-control-sm" id="filter_tahun" name="filter_tahun">
                                </select></dd>                                
                                <dt class="col-sm-1">Bulan</dt>
                                <dd class="col-sm-6"><select type="text" class="form-control form-control-sm" id="filter_bulan" name="filter_bulan">
                                </select></dd>
                            </dl>                                                                                  
                        </div>
                        @endif
                        <div class="col-xxl-6 col-lg-6">
                            @if ($cek_page == 'normal')
                            @if ($flag_lv == "c")    
                                    <h5>Aksi</h5>
                                    <button id="showModal" class="btn btn-xs mb-10 btn-outline bg-teal-700 grey-100">
                                        <i class="icon fa-plus" aria-hidden="true"></i> Tambah Data
                                    </button>                         
                                    <a href="{{route('proker_deleted')}}" id="toProkerRecovery" class="btn btn-xs mb-10 btn-outline bg-light-blue-700 grey-100">
                                        <i class="icon fa-refresh" aria-hidden="true"></i> Data Recovery
                                    </a>    
                                @endif
                            @else
                                <a href="{{route('proker')}}" id="btnKembali" class="btn btn-xs mb-10 btn-outline btn-secondary">
                                    <i class="icon fa-mail-reply" aria-hidden="true"></i> Kembali
                                </a>  
                            @endif                        
                        </div>
                    </div>                    
                    <div class="row pt-3">
                        <div class="col-xxl-12 col-lg-12">
                        
                        </div>
                        <div class="col-xxl-12 col-lg-12">
                            <table class="table table-bordered table-hover table-striped" cellspacing="0" id="tb_proker">
                                <thead class="text-center">
                                    <tr>
                                        <th data-priority="1" rowspan="2">No.</th>
                                        <th data-priority="2" rowspan="2">Nama Proker</th>
                                        <th data-priority="3" rowspan="2">Tanggal</th>
                                        <th data-priority="4" rowspan="2">Penanggungjawab</th>
                                        <th data-priority="5" rowspan="2">Jenis</th>
                                        <th data-priority="6" colspan="2">Status</th>
                                        <th rowspan="2">aksi</th>
                                    </tr>
                                    <tr>
                                        <th>Pengajuan</th>
                                        <th>Pelaksanaan</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                                </table>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</div>

<!-- Modal -->
<div class="modal h-p80 fade modal-slide-from-bottom p-0" id="modalProker" aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-simple modal-lg modal-center">
        <div class="modal-content">

            <div class="modal-header">
                <div class="ribbon ribbon-clip px-10 w-300 h-50" style="z-index:100">
                    <span class="ribbon-inner">
                        <h4 id="judulModal" class="modal-title text-white"></h4>
                    </span>
                </div>                   
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                
            </div>

            <div class="modal-body pt-20" data-keyboard="false" data-backdrop="static" style="height:500px;overflow-y: auto;">
                <form id="formProker" name="formProker" class="row px-15 py-10">
                    <input hidden name="action" id="action" value="tambah">
                    <input hidden name="id" id="id" value="">       
                    <input hidden name="total_biaya" id="total_biaya"/> 
                    @csrf

                    <div class="col-md-12 row pr-0">
                        <div class="col-md-6">
                            <div class="form-group form-material" data-plugin="formMaterial">
                                <label class="form-control-label" for="nama">Nama Proker</label>
                                <input type="text" class="form-control" id="nama" name="nama" placeholder="nama Proker" data-hint="" />
                                <div class="invalid-feedback">*wajib diisi</div> 
                            </div>
                        </div>                        
                        <div class="col-md-3 pr-0">
                            <div class="form-group form-material" data-plugin="formMaterial">
                                <label class="form-control-label" for="tipe_proker">Tipe Proker</label>
                                <select class="form-control" id="tipe_proker" name="tipe_proker" placeholder="tipe proker">
                                    <option value="0" selected disabled>pilih Tipe </option>
                                    <option value="1">NORMAL</option>
                                    <option value="2">EVENT</option>
                                </select>
                                <div class="invalid-feedback">*wajib diisi</div> 
                            </div>
                        </div>                        
                        <div class="col-md-3 pr-0">
                            <div class="form-group form-material" data-plugin="formMaterial">
                                <label class="form-control-label" for="penanggungjawab">Penanggungjawab</label>
                                <select type="text" class="form-control" id="penanggungjawab" name="penanggungjawab"></select>
                                {{-- <input type="text" hidden class="form-control" disabled id="penanggungjawab_v" name="penanggungjawab" placeholder="nama Proker" data-hint="" /> --}}
                                <div class="invalid-feedback">*wajib diisi</div> 
                            </div>
                        </div>                        
                    </div>    
                    <div class="col-md-12 row pr-0">
                        <div class="col-md-6">
                            <div class="form-group form-material" data-plugin="formMaterial">
                                <label class="form-control-label" for="tgl_mulai">Tanggal Mulai</label>
                                <input type="date" class="form-control" min="{{date('Y').'-01-01'}}" max="{{(date('Y')+1).'-12-31'}}" id="tgl_mulai" name="tgl_mulai" data-hint="tanggal Proker dilaksanakan" />
                                <div class="invalid-feedback">*wajib diisi</div> 
                            </div>
                        </div>                        
                        <div class="col-md-6 pr-0">
                            <div class="form-group form-material" data-plugin="formMaterial">
                                <label class="form-control-label" for="tgl_selesai">Tanggal Selesai</label>
                                <input type="date" class="form-control"  min="{{date('Y').'-01-01'}}" max="{{(date('Y')+1).'-12-31'}}" id="tgl_selesai" name="tgl_selesai" data-hint="tanggal Proker selesai" />
                                <div class="invalid-feedback">*wajib diisi</div> 
                            </div>
                        </div>                        
                    </div>
                    <div class="col-md-12 row pr-0">
                        <div class="col-md-6">
                            <div class="form-group form-material" data-plugin="formMaterial">
                                <label class="form-control-label" for="tujuan">Tujuan </label>
                                <textarea class="form-control" id="tujuan" name="tujuan" data-hint="tujuan utama Proker diselenggarakan" rows="1" placeholder="tujuan dilaksanakannya proker"></textarea>
                                <div class="invalid-feedback">*wajib diisi</div> 
                            </div>
                        </div>                        
                        <div class="col-md-6 pr-0">
                            <div class="form-group form-material" data-plugin="formMaterial">
                                <label class="form-control-label" for="subjek_tujuan">Proker ditujukan kepada</label>
                                <input type="text" class="form-control" id="subjek_tujuan" name="subjek_tujuan" data-hint="subjek tujuan dari proker" placeholder="subjek tujuan"/>
                                <div class="invalid-feedback">*wajib diisi</div> 
                            </div>
                        </div>                        
                    </div>

                    <div class="col-md-12 row pr-0">
                        <div class="col-md-12 pr-0">
                            <div class="form-group form-material" data-plugin="formMaterial">
                                <label class="form-control-label" for="ket_tambahan">Keterangan tambahan</label>
                                <textarea class="form-control" id="ket_tambahan" name="ket_tambahan" data-hint="Keterangan Proker" placeholder="keterangan tambahan sifatnya tidak wajib"></textarea>
                                <div class="invalid-feedback">*wajib diisi</div> 
                            </div>
                        </div>  
                    </div>

                    <div class="col-md-12 row pr-0">
                        <div class="col-md-1 vertical-align pr-0">
                            <div class="bg-purple-400 w-p100 vertical-align-middle" style="height: 10px"></div>
                        </div>   
                        <div class="col-md-2 text-center px-0">
                            <h4>Detail Kebutuhan</h4>
                        </div>                        
                        <div class="col-md-9 vertical-align px-0">
                            <div class="bg-purple-400 w-p100 vertical-align-middle" style="height: 10px"></div>
                        </div>                        
                    </div>

                    <div class="col-md-12 table-responsive-md">
                        <table class="table table-bordered table-hover table-striped" cellspacing="0" id="tbSoal">
                            <thead class="text-center">
                                <tr>
                                    <th width="30%">Kebutuhan</th>
                                    <th width="10%">Qty</th>
                                    <th width="10%">Satuan</th>
                                    <th width="20%">Harga_satuan</th>
                                    <th width="25%">Sie</th>
                                    <th width="5%"><button type="button" class="btn btn-floating btn-info btn-xs addForm">+</button></th>
                                </tr>
                            </thead>
                            <tbody class="kb_grub">
                                <tr class="kebutuhan_field" id="kebutuhan_field0" data-id="0"><td><div class="form-group mb-0 form-material" data-plugin="formMaterial">
                                <select class="form-control kebutuhan" id="kebutuhan0" name="kebutuhan[]" data-id="0"></select>
                                <input hidden type="text" class="form-control kabel" id="kabel0" name="kabel[]" data-id="0" placeholder="Qty"/>
                                <div class="invalid-feedback">*wajib diisi</div> 
                                </div></td><td><div class="form-group mb-0 form-material" data-plugin="formMaterial">
                                <input type="number" class="form-control jumlah" id="jumlah0" name="jumlah[]" data-id="0" placeholder="Qty"/><div class="invalid-feedback">*wajib diisi</div>  
                                </div></td><td><div class="form-group mb-0 form-material" data-plugin="formMaterial">
                                <input type="text" class="form-control satuan" id="satuan0" name="satuan[]" data-id="0" placeholder="Satuan"/><div class="invalid-feedback">*wajib diisi</div>  
                                </div></td><td><div class="form-group mb-0 form-material" data-plugin="formMaterial">
                                <input type="text" class="form-control harga_satuan" id="harga_satuan0" name="harga_satuan[]" data-id="0" placeholder="Harga satuan"/><div class="invalid-feedback">*wajib diisi</div>  
                                </div></td><td> 
                                <select type="text" class="form-control sie_panitia" id="sie_panitia0" name="sie_panitia[]" data-id="0">
                                </select><div class="invalid-feedback">*wajib diisi</div> </td><td class="text-center"><button type="button" class="btn btn-floating btn-danger btn-xs rmForm" data-id="0">x</button></td></tr>                                
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-12 row pr-0">
                        <div class="col-md-6">
                            <div class="form-group form-material" data-plugin="formMaterial">
                                <label class="form-control-label" for="dana_turun">Usulan dana kampus</label>
                                <input type="text" class="form-control" id="dana_turun" name="dana_turun" data-hint="usulan dana kampus" placeholder="usulan dana dari kampus"/>
                                <div class="invalid-feedback">*wajib diisi</div> 
                            </div>
                        </div>                        
                        <div class="col-md-6 pr-0">
                            <div class="form-group form-material" data-plugin="formMaterial">
                                <label class="form-control-label" for="dana_mandiri">usulan dana mandiri</label>
                                <input type="text" class="form-control" id="dana_mandiri" name="dana_mandiri" data-hint="usulan dana mandiri" placeholder="usulan dana dari internal UKM EC"/>
                                <div class="invalid-feedback">*wajib diisi</div> 
                            </div>
                        </div>                        
                    </div>                    
                </form>
            </div>
            <div class="modal-footer">
                <div class="container-fluid row" style="font-weight: bold">
                    Total biaya : <div id="show_total">0</div>            
                </div>
                <button type="button" class="btn btn-default btn-outline btn-outline-secondary" data-dismiss="modal" id="tutup">Tutup</button>
                <button type="button" id="btnSave" class="btn bg-purple-400 text-white">Simpan</button>
            </div>
        </div>
    </div>

</div>

<div class="modal fade " id="modal_tolak" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Catatan</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
        <form  id="formtolak" name="formtolak">
        @csrf
        <input hidden name="id_proker" id="id_proker">
        <div class="form-group row" >
          <div class="col-md-12 pt-0">
            <textarea class="form-control" name="catatan" id="textareaDefault" rows="3" style="margin-top: 0px; margin-bottom: 0px; height: 100px;"></textarea>
            <div class="invalid-feedback">*wajib diisi</div>
            </div>
          </div>
        </div>
        </form>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" id="btnTolak" class="btn btn-danger">Tolak</button>
        </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
        // variabel penting
        var count = 1;
        var nameSelect = '';
        var subBiaya = 0;
        var total = 0;
        var idtag = '';
        var cek = 1;
        var table = '';
        var cek_page = '{{$cek_page}}';
        var isiselect2_ifnormal = '';
        var input_dana_kampus = 0;
        var input_dana_mandiri = 0;

        var form_def =  '<tr class="kebutuhan_field" id="kebutuhan_field0" data-id="0"><td><div class="form-group form-material" data-plugin="formMaterial">';
            form_def += '<input  hidden type="text" class="form-control kabel" id="kabel0" name="kabel[]" data-id="0" placeholder="Qty"/>';
            form_def += '<select class="form-control kebutuhan" id="kebutuhan0" name="kebutuhan[]" data-id="0"></select><div class="invalid-feedback">*wajib diisi</div>';
            form_def += '</div></td><td><div class="form-group form-material" data-plugin="formMaterial">';
            form_def += '<input type="number" class="form-control jumlah" id="jumlah0" name="jumlah[]" data-id="0" placeholder="Qty"/><div class="invalid-feedback">*wajib diisi</div>';
            form_def += '</div></td><td><div class="form-group form-material" data-plugin="formMaterial">';
            form_def += '<input type="text" class="form-control satuan" id="satuan0" name="satuan[]" data-id="0" placeholder="Satuan"/><div class="invalid-feedback">*wajib diisi</div>';
            form_def += '</div></td><td><div class="form-group form-material" data-plugin="formMaterial">';
            form_def += '<input type="text" class="form-control harga_satuan" id="harga_satuan0" name="harga_satuan[]" data-id="0" placeholder="Harga satuan"/><div class="invalid-feedback">*wajib diisi</div>';
            form_def += '</div></td><td>';
            form_def += '<select type="text" class="form-control sie_panitia" id="sie_panitia0" name="sie_panitia[]" data-id="0">';
        form_def += '</select><div class="invalid-feedback">*wajib diisi</div> </td><td class="text-center"><button type="button" class="btn btn-floating btn-danger btn-xs rmForm" data-id="0">x</button></td></tr>';

        function select2Pagu (id_tag) { 
            $(id_tag).select2({
                minimumInputLength: 0,
                // language: {
                //     inputTooShort: function() {
                //         return 'Masukkan minimal 2 digit angka';
                //     }
                // },         
                allowClear : true,
                searchInputPlaceholder: 'Search...',
                placeholder: 'pilih Kategori Anggaran',
                ajax: {
                    cache: true,
                    url: "{{ route('select2Pagu') }}",
                    dataType: 'json',
                    type: "GET",
                    quietMillis: 50,
                    data: function (params) {
                        // console.log(params)
                        return {
                        search: $.trim(params.term)
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: $.map(data, function (item) {
                            return {
                                text:item.nama,
                                id: item.id,
                                satuan:item.satuan
                                }
                            })
                        };
                    },             
                }
            });     
        };              

        //  get url terkini untuk hapus data
        function url_wilayah() {
            // Get Option Wilayah
            var url_current = '{{url()->current()}}';
            var start = url_current.indexOf("/proker");
            var url_true = url_current.substring(0,start)+"/proker/delete";
            console.log(url_true);
            return url_true;
        }

        /* Fungsi formatRupiah */
        function formatRupiah(angka, prefix){
            var number_string = angka.replace(/[^,\d]/g, '').toString(),
            split           = number_string.split(','),
            sisa            = split[0].length % 3,
            rupiah          = split[0].substr(0, sisa),
            ribuan          = split[0].substr(sisa).match(/\d{3}/gi);

                // tambahkan titik jika yang di input sudah menjadi angka ribuan
                if(ribuan){
                    separator = sisa ? '.' : '';
                    rupiah += separator + ribuan.join('.');
                }

                rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
                return prefix == undefined ? rupiah : (rupiah ? 'Rp.' + rupiah : '');
        }

        function sum_total() {
            total = 0;
            subBiaya = 0;
            hitung = 0;
            $('.harga_satuan').each(function(){
                var kali = $('#jumlah'+$(this).data('id')).val();
                // console.log(kali+' '+$(this).val());
                subBiaya = kali * parseInt(((($(this).val().replace('.','')).replace('.','')).replace('.','')).replace('Rp',''));
                total += subBiaya;
                hitung = hitung + 1;
            });
            if(isNaN(total)) {
            total = 0;
            }                        
            $("input[name='total_biaya']").val(total);           
            $("#show_total").html('Rp.'+total.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
        }

        function selecta (idSelect) { 
            $(idSelect).select2({
                minimumInputLength: 0,
                // language: {
                //     inputTooShort: function() {
                //         return 'Masukkan minimal 2 digit angka';
                //     }
                // },         
                allowClear : true,
                searchInputPlaceholder: 'Search...',
                placeholder: 'pilih sie',
                ajax: {
                cache: true,
                url: "{{ url('master/kepanitiaan/select2') }}?tipe_event="+$('#tipe_proker').val(),
                dataType: 'json',
                type: "GET",
                quietMillis: 50,
                data: function (params) {
                    // console.log(params)
                    return {
                    search: $.trim(params.term)
                    };
                },
                processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                    return {
                        text:item.nama,
                        id: item.id
                        }
                    })
                    };
                }
                }
            });     
        };    

        function selecta_nim (idSelect_nim) { 
            $(idSelect_nim).select2({
                minimumInputLength: 0,
                // language: {
                //     inputTooShort: function() {
                //         return 'Masukkan minimal 2 digit angka';
                //     }
                // },         
                allowClear : true,
                searchInputPlaceholder: 'Search...',
                placeholder: 'pilih NIM',
                ajax: {
                cache: true,
                url: "{{ url('/anggota/select2') }}",
                dataType: 'json',
                type: "GET",
                quietMillis: 50,
                data: function (params) {
                    // console.log(params)
                    return {
                    search: $.trim(params.term)
                    };
                },
                processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                    return {
                        text:item.nama,
                        id: item.id
                        }
                    })
                    };
                }
                }
            });     
        };    

        function gen_select2_panitia(counter) {
            nameSelect = '#sie_panitia'+counter;
            selecta(nameSelect);            
            console.log(nameSelect);            
        }

        function tambah_form(params) {
            // console.log(count);

            idtag = 'kebutuhan_field'+count;
            var form =  '<tr class="kebutuhan_field" id="'+idtag+'" data-id="'+count+'"><td><div class="form-group mb-0 form-material" data-plugin="formMaterial">';
                form += '<input hidden type="text" class="form-control kabel" id="kabel'+count+'" name="kabel[]" data-id="'+count+'" placeholder="Qty"/>';
                form += '<select class="form-control kebutuhan" id="kebutuhan'+count+'" name="kebutuhan[]" data-id="'+count+'"></select><div class="invalid-feedback">*wajib diisi</div>';
                form += '</div></td><td><div class="form-group mb-0 form-material" data-plugin="formMaterial">';
                form += '<input type="number" class="form-control jumlah" id="jumlah'+count+'" name="jumlah[]" data-id="'+count+'" placeholder="Qty"/><div class="invalid-feedback">*wajib diisi</div>';
                form += '</div></td><td><div class="form-group mb-0 form-material" data-plugin="formMaterial">';
                form += '<input type="text" class="form-control satuan" id="satuan'+count+'" name="satuan[]" data-id="'+count+'" placeholder="Satuan"/><div class="invalid-feedback">*wajib diisi</div>';
                form += '</div></td><td><div class="form-group mb-0 form-material" data-plugin="formMaterial">';
                form += '<input type="text" class="form-control harga_satuan" id="harga_satuan'+count+'" name="harga_satuan[]" data-id="'+count+'" placeholder="Harga satuan"/><div class="invalid-feedback">*wajib diisi</div>';
                form += '</div></td><td>';
                form += '<select type="text" class="form-control sie_panitia" id="sie_panitia'+count+'" name="sie_panitia[]" data-id="'+count+'">';
                form += isiselect2_ifnormal;
                form += '</select><div class="invalid-feedback">*wajib diisi</div> </td><td class="text-center"><button type="button" class="btn btn-floating btn-danger btn-xs rmForm" data-id="'+count+'">x</button></td></tr>';
            $('.kb_grub').append(form);
            select2Pagu('#kebutuhan'+count);
            gen_select2_panitia(count)
            count = count + 1;            
        }

        function select2filter(id_tag,url,placeholder) {
            $(id_tag).select2({
                minimumInputLength: 0,
                // language: {
                //     inputTooShort: function() {
                //         return 'Masukkan minimal 2 digit angka';
                //     }
                // },         
                allowClear : true,
                searchInputPlaceholder: 'Search...',
                placeholder: placeholder,
                ajax: {
                cache: true,
                url: url,
                dataType: 'json',
                type: "GET",
                quietMillis: 50,
                data: function (params) {
                    // console.log(params)
                    return {
                    search: $.trim(params.term)
                    };
                },
                processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                    return {
                        text:item.nama,
                        id: item.id
                        }
                    })
                    };
                }
                }
            });               
        }

        $(document).on('mouseenter', '.st_pengajuan', function (e) {

            e.preventDefault();
            // $(this).webuiPopover({content:'Content',trigger:'hover'});

        });

        $(document).on('mouseenter', '#tb_proker', function (e) {
            e.preventDefault();
            // $(this).webuiPopover({content:'Content',trigger:'hover'});
            $("#tb_proker tbody tr").each(function() {
                // Within tr we find the last td child element and get content
                var catatan = $(this).find(".st_pengajuan").data('catatan');
                $(this).find(".st_pengajuan").webuiPopover({title:'Catatan',content:catatan,closeable:true,trigger:'hover'});

                var tanggal = $(this).find(".st_nama").data('tanggal');
                var pj = $(this).find(".st_nama").data('pj');

                var info_nama = '<table class="table table-bordered">'
                    info_nama += '<tr>'
                    info_nama += '<td><b>PJ</b></td>'
                    info_nama += '<td>'+pj+'</td>'
                    info_nama += ' </tr>'
                    info_nama += '<tr>'
                    info_nama += '<td><b>Tanggal</b></td>'
                    info_nama += '<td>'+tanggal+'</td>'
                    info_nama += ' </tr>'
                    info_nama += ' </table>'

                $(this).find(".st_nama").webuiPopover({title:'INFO',content:info_nama,closeable:true,trigger:'hover'});

                // console.log('POL');
            }); 
        });

        $(document).ready(function () {
            // fix bug title in select2
            $(document).on('mouseenter', '.select2-selection__rendered', function () {
                $('.select2-selection__rendered').removeAttr('title');
            });
                        
            select2filter('#filter_bulan',"{{route('select2bulanProker')}}?tahun=0",'semua bulan');
            select2filter('#filter_tahun',"{{route('select2tahunProker')}}?bulan=0",'semua tahun');

            select2Pagu('#kebutuhan0');

            if (cek_page == 'normal') {
                var table = $('#tb_proker').DataTable({
                    "paging": true,                    
                    processing: true,
                    serverSide: true,
                    responsive: true,        
                    ajax: "{{ route('DTproker') }}?nim="+"{{(isset($nim)) ? $nim : ''}}",
                    language: {
                        processing: "Sedang diproses...<img height=50 src='{{asset('assets/images/load3.gif')}}'>"
                    },
                    columns: [
                        {
                            data: 'no',
                            name: 'no',
                            searchable : true,
                            orderable:true
                        }, {
                            data: 'nama',
                            name: 'nama',
                            orderable:true
                        },{
                            data: 'tanggal',
                            name: 'tanggal',
                            orderable: true,
                            searchable: true
                        },{
                            data: 'nama_pj',
                            name: 'penanggungjawab',
                            orderable: true,
                            searchable: true
                        },{
                            data: 'stat_event',
                            name: 'status event',
                            orderable: true,
                            searchable: true
                        },{
                            data: 'status',
                            name: 'status',
                            orderable: true,
                            searchable: true
                        },{
                            data: 'terlaksana',
                            name: 'terlaksana',
                            orderable: true,
                            searchable: true
                        },{
                            data: 'aksi',
                            name: 'aksi',
                            orderable: true,
                            searchable: true
                        },
                    ],
                    columnDefs: [
                        { className: 'text-center', targets: [0,4,5,6] },
                    ]
                });                
            } else {
                var table = $('#tb_proker').DataTable({
                    processing: true,
                    serverSide: true,
                    responsive: true,        
                    ajax: "{{ route('DTprokerDeleted') }}",
                    language: {
                        processing: "Sedang diproses...<img height=50 src='{{asset('assets/images/load3.gif')}}'>"
                    },
                    columns: [
                        {
                            data: 'no',
                            name: 'no',
                            searchable : true,
                            orderable:true
                        }, {
                            data: 'nama',
                            name: 'nama',
                            orderable:true
                        },{
                            data: 'tanggal',
                            name: 'tanggal',
                            orderable: true,
                            searchable: true
                        },{
                            data: 'nama_pj',
                            name: 'penanggungjawab',
                            orderable: true,
                            searchable: true
                        },{
                            data: 'stat_event',
                            name: 'status event',
                            orderable: true,
                            searchable: true
                        },{
                            data: 'status',
                            name: 'status',
                            orderable: true,
                            searchable: true
                        },{
                            data: 'terlaksana',
                            name: 'terlaksana',
                            orderable: true,
                            searchable: true
                        },{
                            data: 'aksi',
                            name: 'aksi',
                            orderable: true,
                            searchable: true
                        },
                    ],
                    columnDefs: [
                        { className: 'text-center', targets: [0,1,2,3,4,5,6] },
                    ]
                });
            }

            $(document).on('change', '#filter_bulan', function (e) {
                e.preventDefault();
                // console.log($(this).val());
                var bulan = $(this).val();
                var tahun = $('#filter_tahun').val();

                select2filter('#filter_tahun',"{{route('select2tahunProker')}}?bulan="+bulan,'semua tahun');
                table.ajax.url( "{{ route('DTproker') }}?nim="+"{{(isset($nim)) ? $nim : ''}}&tahun="+tahun+"&bulan="+bulan ).load();
            });    

            $(document).on('change', '#filter_tahun', function (e) {
                e.preventDefault();
                // console.log($(this).val());
                var bulan = $('#filter_bulan').val();
                var tahun = $(this).val();

                select2filter('#filter_bulan',"{{route('select2bulanProker')}}?tahun="+tahun,'semua bulan');
                table.ajax.url( "{{ route('DTproker') }}?nim="+"{{(isset($nim)) ? $nim : ''}}&tahun="+tahun+"&bulan="+bulan ).load();
            });    

            function vld_text(id_input,col){
                if ($(id_input).val()=='') {
                    $(id_input).addClass('is-invalid').closest('.col-md-'+col).find('.form-control-label').addClass('text-danger');
                    $(id_input).closest('.form-material').addClass('has-danger');
                    $(id_input).focus(function () {
                        $(this).removeClass('is-invalid').closest('.form-material').removeClass('has-danger');
                    });
                    $(id_input).keyup(function () {
                        $(this).closest('.col-md-'+col).find('.form-control-label').removeClass('text-danger');
                    });
                }
            }

            function vld_select(id_input,col){
                if ($(id_input).val()==null) {
                    $(id_input).addClass('is-invalid').closest('.col-md-'+col).find('.form-control-label').addClass('text-danger');
                    $(id_input).closest('.form-material').addClass('has-danger');                    
                    $(id_input).focus(function () {
                        $(this).removeClass('is-invalid').closest('.form-material').removeClass('has-danger');
                    });
                    $(document).on('change',id_input, function () {
                        console.log('select1 ok');
                        $(this).removeClass('is-invalid').closest('.col-md-'+col).find('.form-control-label').removeClass('text-danger');
                        $(this).removeClass('is-invalid').closest('.form-material').removeClass('has-danger');                        
                    });
                }
            }

            function vld_date(id_input,col){
                if ($(id_input).val()=='') {
                    $(id_input).addClass('is-invalid').closest('.col-md-'+col).find('.form-control-label').addClass('text-danger');
                    $(id_input).closest('.form-material').addClass('has-danger');                    
                    $(id_input).focus(function () {
                        $(this).removeClass('is-invalid').closest('.form-material').removeClass('has-danger');
                    });
                    $(document).on('change',id_input, function () {
                        console.log('select1 ok');
                        $(this).removeClass('is-invalid').closest('.col-md-'+col).find('.form-control-label').removeClass('text-danger');
                        $(this).removeClass('is-invalid').closest('.form-material').removeClass('has-danger');                        
                    });
                }
            }
            
            // aksi ketika save data dan update
            $('#btnSave').click(function (e) {
                e.preventDefault();

                    var checker = true;
                    $('.kebutuhan_field').each(function () {         
                        var id = $(this).data('id');
                        var kebutuhan = $('#kebutuhan'+id).val();
                        var jumlah = $('#jumlah'+id).val();
                        var satuan = $('#satuan'+id).val();
                        var harga_satuan = $('#harga_satuan'+id).val();
                        var sie_panitia = $('#sie_panitia'+id).val();
                        
                        if(kebutuhan == null){
                            checker = false;
                            $('#kebutuhan'+id).addClass('is-invalid').closest('.form-material').addClass('has-danger');   
                            $('.kebutuhan').keyup(function () {
                                $('#kebutuhan'+$(this).data('id')).removeClass('is-invalid').closest('.form-material').removeClass('has-danger');
                            });
                        }
                        if(jumlah == ''){
                            checker = false;                            
                            $('#jumlah'+id).addClass('is-invalid').closest('.form-material').addClass('has-danger');
                            $('.jumlah').change(function () {
                                $('#jumlah'+$(this).data('id')).removeClass('is-invalid').closest('.form-material').removeClass('has-danger');
                            });                            
                        }
                        if(satuan == ''){
                            checker = false;                            
                            $('#satuan'+id).addClass('is-invalid').closest('.form-material').addClass('has-danger');   
                            $('.satuan').keyup(function () {
                                $('#satuan'+$(this).data('id')).removeClass('is-invalid').closest('.form-material').removeClass('has-danger');
                            });                                   
                        }
                        if(harga_satuan == ''){
                            checker = false;                            
                            $('#harga_satuan'+id).addClass('is-invalid').closest('.form-material').addClass('has-danger');  
                            $('.harga_satuan').keyup(function () {
                                $('#harga_satuan'+$(this).data('id')).removeClass('is-invalid').closest('.form-material').removeClass('has-danger');
                            });                                    
                        }
                        if(sie_panitia == null){
                            checker = false;                            
                            $('#sie_panitia'+id).addClass('is-invalid').closest('.form-material').addClass('has-danger'); 
                            $('.sie_panitia').change(function () {
                                $('#sie_panitia'+$(this).data('id')).removeClass('is-invalid').closest('.form-material').removeClass('has-danger');
                            });                                     
                        }
                        
                        // console.log(id+';'+kebutuhan+';'+jumlah+';'+satuan+';'+harga_satuan+';'+sie_panitia);
                    });
                    // console.log(checker);

                vld_text('#nama',6);
                vld_date('#tgl_mulai',6);
                vld_date('#tgl_selesai',6);
                vld_text('#tujuan',6);
                vld_text('#dana_turun',6);
                vld_text('#dana_mandiri',6);
                vld_text('#subjek_tujuan',6);
                vld_select('#tipe_proker',3);
                vld_select('#penanggungjawab',3);

                if(
                    $('#nama').val()!='' &&
                    $('#tipe_proker').val()!=null &&                    
                    $('#penanggungjawab').val()!=null &&                    
                    $('#tgl_mulai').val()!='' &&                    
                    $('#tgl_selesai').val()!='' &&                    
                    $('#tujuan').val()!='' &&                    
                    $('#dana_turun').val()!='' &&                    
                    $('#dana_mandiri').val()!='' &&                    
                    $('#subjek_tujuan').val()!='' &&                    
                    checker == true                  
                ){
                    $(this).html('Sending..');
                    $(this).attr('disabled');
                    $.ajax({
                        data: $('#formProker').serialize(),
                        url: "{{route('proker_diapain')}}",
                        type: "POST",
                        dataType: 'json',
                        success: function (data) {
                            swal({
                                title: "Sukses!",
                                text: "data proker berhasil disimpan",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonClass: "btn-success",
                                confirmButtonText: 'OK',
                                closeOnConfirm: false
                            });      
                            $('#btnSave').removeAttr('disabled');
                            console.log(data);
                            $('#formProker').trigger('reset');
                            $('#btnSave').html('Simpan');
                            $('#modalProker').modal('hide');
                            $('.kb_grub').html('');
                            $('.kb_grub').html(form_def);
                            tambah_form();                    
                            table.ajax.reload();
                            // toastr.success(data.message);
                        },
                        error: function (data) {
                            console.log('Error:', data);
                            $('#btnSave').html('Simpan');
                            toastr.error('Kelola Data Gagal');
                        }
                    })

                };                                
            });   

            $(document).on('click', '.btnDelete', function () {
                console.log('deleted');
                var url = "{{url('proker/delete/')}}/"+$(this).data('id');
                // console.log(url);
                swal({
                title: "Apakah anda yakin menghapus data ini?",
                text: "Data yang telah dihapus masih dapat direcovery",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-warning",
                confirmButtonText: 'Iya',
                cancelButtonText: "Tidak",
                closeOnConfirm: true,
                closeOnCancel: true
                }, function (isConfirm) {
                    if (isConfirm) {
                            $.ajax({
                                url: url,
                                type: "GET",
                                dataType: 'json',
                                success: function (data) {
                                    // console.log(data);
                                    swal({
                                        title: "Sukses!",
                                        text: data.message,
                                        type: "success",
                                        showCancelButton: false,
                                        confirmButtonClass: "btn-success",
                                        confirmButtonText: 'OK',
                                        closeOnConfirm: false
                                    });                                      
                                    table.ajax.reload();
                                    },
                                error: function (data) {
                                    console.log('Error:', data);
                                    toastr.error(data.message);
                                    //$('#modalOrg').modal('show');
                                    }
                            });
                    }else {
                    }
                });
            });
            
            $(document).on('click', '.btnTDelete', function () {
                var url = "{{url('proker/true_delete/')}}/"+$(this).data('id');
                // console.log(url);
                swal({
                title: "Apakah anda yakin menghapus data ini?",
                text: "Data yang telah dihapus tidak dapat direcovery lagi",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-warning",
                confirmButtonText: 'Iya',
                cancelButtonText: "Tidak",
                closeOnConfirm: true,
                closeOnCancel: true
                }, function (isConfirm) {
                    if (isConfirm) {
                            $.ajax({
                                url: url,
                                type: "GET",
                                dataType: 'json',
                                success: function (data) {
                                    // console.log(data);
                                    swal({
                                        title: "Sukses!",
                                        text: data.message,
                                        type: "success",
                                        showCancelButton: false,
                                        confirmButtonClass: "btn-success",
                                        confirmButtonText: 'OK',
                                        closeOnConfirm: false
                                    });                                     
                                    table.ajax.reload();
                                    },
                                error: function (data) {
                                    console.log('Error:', data);
                                    toastr.error(data.message);
                                    //$('#modalOrg').modal('show');
                                    }
                            });
                    }else {
                    }
                });
            });
            
            $(document).on('click', '.btnRecover', function () {
                var url = "{{url('proker/recovery/')}}/"+$(this).data('id');
                // console.log(url);
                swal({
                title: "Apakah anda yakin merecovey data ini?",
                text: "Status approval data yang direcovery akan kembali seperti semula",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-warning",
                confirmButtonText: 'Iya',
                cancelButtonText: "Tidak",
                closeOnConfirm: true,
                closeOnCancel: true
                }, function (isConfirm) {
                    if (isConfirm) {
                            $.ajax({
                                url: url,
                                type: "GET",
                                dataType: 'json',
                                success: function (data) {
                                    // console.log(data);
                                    swal({
                                        title: "Sukses!",
                                        text: data.message,
                                        type: "success",
                                        showCancelButton: false,
                                        confirmButtonClass: "btn-success",
                                        confirmButtonText: 'OK',
                                        closeOnConfirm: false
                                    });                                       
                                    // toastr.success(data.message);
                                    table.ajax.reload();
                                    },
                                error: function (data) {
                                    console.log('Error:', data);
                                    toastr.error(data.message);
                                    //$('#modalOrg').modal('show');
                                    }
                            });
                    }else {
                    }
                });
            });
            
            $(document).on('click', '.btnAprove', function (e) {
                console.log($(this).data('id'));
                var id = $(this).data('id');
                var url = "{{url('proker/approval/')}}/approve/"+$(this).data('id');
                // console.log(url);
                swal({
                title: "Yakin menyetujui Proker ini?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-warning",
                confirmButtonText: 'Iya',
                cancelButtonText: "Tidak",
                closeOnConfirm: true,
                closeOnCancel: true
                }, function (isConfirm) {
                    if (isConfirm) {
                            $.ajax({
                                url: url,
                                type: "GET",
                                dataType: 'json',
                                success: function (data) {
                                    console.log(data);
                                    toastr.success(data.message);
                                    table.ajax.reload();
                                    },
                                error: function (data) {
                                    console.log('Error:', data);
                                    toastr.success(data.message);
                                    //$('#modalOrg').modal('show');
                                    }
                            });
                    }else {
                    }
                });
            })

            $(document).on('click', '#btnTolak', function (e) {
                var urlT = "{{url('proker/refuse/')}}";
                console.log(urlT);
                $.ajax({
                data: $('#formtolak').serialize(),
                url: urlT,
                type: "POST",
                dataType: 'json',
                success: function(data) {
                    console.log(data);
                    $('#formtolak').trigger('reset');
                    $('#modal_tolak').modal('hide');
                    toastr.success(data.message);
                    table.ajax.reload();
                },
                error: function(data) {
                    console.log('Error:', data);
                    //$('#modalRPendidikan').modal('show');
                }
                });
            })

            $(document).on('click','.show_modal_tolak',function(){
                var id = $(this).data('id');
                var id_peg_pengaju = $(this).data('pegawai_id');

                $('#id_proker').val(id);
                $('#modal_tolak').modal('show');
            });
                        
        });

        $('#tgl_mulai').blur(function () {
            $('#tgl_selesai').prop('min', $('#tgl_mulai').val());
        });
        $('#tgl_selesai').blur(function () {
            // $('#mulai').prop('max',  $('#mulai').val());  
        });
        
            // aksi button tambah di klik
        $(document).on('click', '#showModal', function () {
            $('#judulModal').html('Form pengajuan Proker');
            $('#tipe_proker').select2();            
            selecta_nim('#penanggungjawab');
            selecta('.sie_panitia');               
            $('#id').val('');

                $('#modalProker').modal({
                    backdrop: 'static',
                    keyboard: false,
                    show: true
                })
        });

        function get_detail_kebutuhan(is_disabled,id_proker) {
            var url = 'proker/detail/kebutuhan_event/'+id_proker;
            $.getJSON(url , function (data) {
                console.log(data);
                $('.kebutuhan_field').remove();                                              
                $.each(data, function (i, v) {

                    var uang = 'Rp. '+v.harga_satuan.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
                    idtag = 'kebutuhan_field'+count;

                    var form =  '<tr class="kebutuhan_field" id="'+idtag+'" data-id="'+count+'"><td><div class="form-group mb-0 form-material" data-plugin="formMaterial">';
                        form += '<input hidden type="text" class="form-control kabel" id="kabel'+count+'" name="kabel[]" data-id="'+count+'" value="'+v.keterangan+'" placeholder="Qty"/>';
                        form += '<select '+is_disabled+' class="form-control kebutuhan" id="kebutuhan'+count+'" name="kebutuhan[]" data-id="'+count+'">';
                        if(v.id_kabel != null){
                            form += '<option value="'+v.id_kabel+'" selected>'+v.keterangan+'</option>';
                        }                        
                        form += '</select><div class="invalid-feedback">*wajib diisi</div>';
                        form += '</div></td><td><div class="form-group mb-0 form-material" data-plugin="formMaterial">';
                        form += '<input '+is_disabled+' type="number" class="form-control jumlah" id="jumlah'+count+'" name="jumlah[]" data-id="'+count+'" value="'+v.jumlah+'" placeholder="Qty"/><div class="invalid-feedback">*wajib diisi</div>';
                        form += '</div></td><td><div class="form-group mb-0 form-material" data-plugin="formMaterial">';
                        form += '<input '+is_disabled+' type="text" class="form-control satuan" id="satuan'+count+'" name="satuan[]" data-id="'+count+'" value="'+v.satuan+'" placeholder="Satuan"/><div class="invalid-feedback">*wajib diisi</div>';
                        form += '</div></td><td><div class="form-group mb-0 form-material" data-plugin="formMaterial">';
                        form += '<input '+is_disabled+' type="text" class="form-control harga_satuan" id="harga_satuan'+count+'" name="harga_satuan[]" data-id="'+count+'" value="'+uang+'" placeholder="Harga satuan"/><div class="invalid-feedback">*wajib diisi</div>';
                        form += '</div></td><td>';
                        form += '<select '+is_disabled+' type="text" class="form-control sie_panitia" id="sie_panitia'+count+'" name="sie_panitia[]" data-id="'+count+'">';
                        if(v.id_sie_kepanitiaan != null){
                            form += '<option value="'+v.id_sie_kepanitiaan+'" selected>'+v.nama_kepanitiaan+'</option>';
                        }
                        form += '</select><div class="invalid-feedback">*wajib diisi</div> </td><td class="text-center"><button type="button" class="btn btn-floating btn-danger btn-xs rmForm" data-id="'+count+'">x</button></td></tr>';
                
                        $('.kb_grub').append(form);
                        gen_select2_panitia(count);
                        select2Pagu('#kebutuhan'+count);
                    
                    count = count + 1;                       
                });
                sum_total();                   
            });            
        }

        $(document).on('click', '.btnDetail', function () {
            // console.log('sjfhdav');
            $('#tipe_proker').select2();            
            selecta_nim('#penanggungjawab');            
            $('#judulModal').html('Edit data Soal');
            $('#action').val('edit');
            $('#id').val($(this).data('id'));
            $('#nama').val($(this).data('nama')).attr('disabled',true);
            $('#penanggungjawab').html('<option value="'+$(this).data('penanggungjawab')+'" selected>( '+$(this).data('penanggungjawab')+' ) '+$(this).data('nama_pj')+'</option>').attr('disabled',true);
            $('#tipe_proker').val($(this).data('event')).trigger('change').attr('disabled',true);   
            console.log($(this).data('event'));         
            $('#tgl_mulai').val($(this).data('tanggal_mulai')).attr('disabled',true);
            $('#tgl_selesai').val($(this).data('tanggal_selesai')).attr('disabled',true);
            $('#tujuan').val($(this).data('tujuan')).attr('disabled',true);
            $('#ket_tambahan').val($(this).data('ket_tambahan')).attr('disabled',true);
            $('#subjek_tujuan').val($(this).data('subjek_tujuan'));
            $('#dana_mandiri').val('Rp.'+$(this).data('dana_mandiri').toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
            $('#dana_turun').val('Rp.'+$(this).data('dana_turun').toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
            
            get_detail_kebutuhan('disabled',$(this).data('id'));
            var tgl_ = $('#tgl_mulai').val();
            $('#tgl_selesai').attr('min', tgl_);
            $('#modalProker').modal({
                backdrop: 'static',
                keyboard: false, // to prevent closing with Esc button (if you want this too)
                show: true
            })
        });

        $(document).on('click', '.btn_perbaikan', function () {
            // console.log('sjfhdav');
            $('#tipe_proker').select2();            
            selecta_nim('#penanggungjawab');            
            $('#judulModal').html('Edit data Soal');
            $('#action').val('perbaikan');
            $('#id').val($(this).data('id'));
            $('#nama').val($(this).data('nama'));
            $('#penanggungjawab').html('<option value="'+$(this).data('penanggungjawab')+'" selected>( '+$(this).data('penanggungjawab')+' ) '+$(this).data('nama_pj')+'</option>');
            $('#tipe_proker').val($(this).data('event')).trigger('change');
            $('#tgl_mulai').val($(this).data('tanggal_mulai'));
            $('#tgl_selesai').val($(this).data('tanggal_selesai'));
            $('#tujuan').val($(this).data('tujuan'));
            $('#ket_tambahan').val($(this).data('ket_tambahan'));
            $('#subjek_tujuan').val($(this).data('subjek_tujuan'));
            $('#dana_mandiri').val('Rp.'+$(this).data('dana_mandiri').toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
            $('#dana_turun').val('Rp.'+$(this).data('dana_turun').toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));

            get_detail_kebutuhan('',$(this).data('id'));
            var tgl_ = $('#tgl_mulai').val();
            $('#tgl_selesai').attr('min', tgl_);
            $('#modalProker').modal({
                backdrop: 'static',
                keyboard: false, // to prevent closing with Esc button (if you want this too)
                show: true
            })
        });

        $(document).on('click', '.btnEdit', function () {
            // console.log('sjfhdav');
            $('#tipe_proker').select2();            
            selecta_nim('#penanggungjawab');            
            $('#judulModal').html('Edit data Soal');
            $('#action').val('edit');
            $('#id').val($(this).data('id'));
            $('#nama').val($(this).data('nama'));
            $('#penanggungjawab').html('<option value="'+$(this).data('penanggungjawab')+'" selected>( '+$(this).data('penanggungjawab')+' ) '+$(this).data('nama_pj')+'</option>');
            $('#tipe_proker').val($(this).data('event')).trigger('change');
            $('#tgl_mulai').val($(this).data('tanggal_mulai'));
            $('#tgl_selesai').val($(this).data('tanggal_selesai'));
            $('#tujuan').val($(this).data('tujuan'));
            $('#ket_tambahan').val($(this).data('ket_tambahan'));
            $('#subjek_tujuan').val($(this).data('subjek_tujuan'));
            $('#dana_mandiri').val('Rp.'+$(this).data('dana_mandiri').toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
            $('#dana_turun').val('Rp.'+$(this).data('dana_turun').toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));

            get_detail_kebutuhan('',$(this).data('id'));            
            var tgl_ = $('#tgl_mulai').val();
            $('#tgl_selesai').attr('min', tgl_);
            $('#modalProker').modal({
                backdrop: 'static',
                keyboard: false, // to prevent closing with Esc button (if you want this too)
                show: true
            })
        });

        $("#modalProker").on("hidden.bs.modal", function () {
            $('.form-control').removeAttr('disabled', true).removeClass('is-invalid');
            $('#penanggungjawab').html('');
            $('#btnSave').removeAttr('style', 'display:none');
            $('#formProker').trigger('reset');
            $('.form-control-label').removeClass('text-danger')            
            $('.kb_grub').html('');
            $('.kb_grub').html(form_def);
            select2Pagu('#kebutuhan0');
            sum_total();             
        });
        // ----------------------------------------------------------------------------- kebutuhan ==========================================



        $(document).on('keyup', '.harga_satuan', function (e) {
            var data = $(this).val();
            var hasil = formatRupiah(data, '');
            $(this).val(hasil);
            sum_total();
        });

        $(document).on('keyup', '#dana_turun', function (e) {
            var data = $(this).val();
            var hasil = formatRupiah(data, '');
            $(this).val(hasil);
        });

        $(document).on('keyup', '#dana_mandiri', function (e) {
            var data = $(this).val();
            var hasil = formatRupiah(data, '');
            $(this).val(hasil);
        });

        $(document).ready(function () {
            $('.addForm').click(function(e) {
                tambah_form();
            });            
        });

        $(document).on('keyup', '.jumlah', function (e) {
            sum_total();
        });

        $(document).on('click', '.rmForm', function () {
            var id = $(this).data('id');

            $('#kebutuhan_field'+id).remove();
            sum_total();
        });        

        $(document).on('change', '#tipe_proker', function (e) {
            selecta('.sie_panitia');   
            var id = $(this).val();
            if (id == 1) {
                
                isiselect2_ifnormal = '<option value="'+"{{DeHelper2::getPanitiaIfNormal()->id}}"+'" selected>'+"{{DeHelper2::getPanitiaIfNormal()->nama}}"+'</option>';
                
                // $('#penanggungjawab').html('<option value="'+"{{Session::get('data_user')['nim']}}"+'" selected>( '+"{{Session::get('data_user')['nim']}}"+' ) '+"{{Session::get('data_user')['nama']}}"+'</option>');
                // $('#penanggungjawab').attr('disabled',true);
                // $('#penanggungjawab_v').attr('disabled',false);
                // $('#penanggungjawab_v').val($('#penanggungjawab').val());

                $('.sie_panitia').each(function (index, element) {
                    $(this).html(isiselect2_ifnormal);
                });
            }else{
                isiselect2_ifnormal = '';
                // $('#penanggungjawab_v').attr('disabled',true);
                // $('#penanggungjawab_v').val('');
                // $('#penanggungjawab').html('');
                // $('#penanggungjawab').attr('disabled',false);

                $('.sie_panitia').each(function (index, element) {
                    $(this).html(isiselect2_ifnormal);
                });
            }
            console.log();
        });

        $(document).on('change', '.kebutuhan', function (e) {
            // console.log(this);
            kabel_id = $(this).val();
            id = $(this).data('id');
            // console.log(id);
            var url = "{{route('getAnggaranDetail')}}?id_kabel="+kabel_id;
            $.getJSON(url , function (data) {
                // console.log(data);
                $('#kabel'+id).val(data.kategori_belanja);
                $('#satuan'+id).val(data.satuan);
            });            
        });

        $(document).on('change', '#tgl_mulai', function (e) {
            // console.log(this);
            var tgl_ = $(this).val();
            $('#tgl_selesai').attr('min', tgl_);
        });
    
        $(document).on('change', '#dana_turun', function (e) {
            
            input_dana_kampus = parseInt(((($(this).val().replace('.','')).replace('.','')).replace('.','')).replace('Rp',''));
            console.log(total);
            console.log(input_dana_kampus);
            var perbandingan = input_dana_kampus + input_dana_mandiri - total;
            console.log(perbandingan);
            
            if (perbandingan > 0) {
                swal({
                    title: 'Perhatian!',
                    text: "Dana usulan melebihi dana Kebutuhan",
                    type: "warning",
                    showCancelButton: false,
                    confirmButtonClass: "btn-warning",
                    confirmButtonText: 'Ok, Mengerti',
                    cancelButtonText: "Tidak",
                    closeOnConfirm: true,
                    closeOnCancel: true
                }, function (isConfirm) {
                    if (isConfirm) {
                        $('#dana_turun').val('');
                    }else {
                    }
                });                   
            }
            
        });    
    
        $(document).on('change', '#dana_mandiri', function (e) {
            
            input_dana_mandiri = parseInt(((($(this).val().replace('.','')).replace('.','')).replace('.','')).replace('Rp',''));
            console.log(total);
            console.log(input_dana_mandiri);
            var perbandingan = input_dana_mandiri + input_dana_kampus - total;
            console.log(perbandingan);
            
            if (perbandingan > 0) {
                swal({
                    title: 'Perhatian!',
                    text: "Dana usulan melebihi dana Kebutuhan",
                    type: "warning",
                    showCancelButton: false,
                    confirmButtonClass: "btn-warning",
                    confirmButtonText: 'Ok, Mengerti',
                    cancelButtonText: "Tidak",
                    closeOnConfirm: true,
                    closeOnCancel: true
                }, function (isConfirm) {
                    if (isConfirm) {
                        $('#dana_mandiri').val('');
                    }else {
                    }
                });                   
            }
            
        });    
</script>
@endpush