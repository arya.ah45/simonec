<div class="tab-pane animation-slide-right container-fluid" id="pengurus" role="tabpanel">
    <div class="row py-20">           
        <div class="col-md-6">
            <h4>Tabel Pengurus / Panitia</h4>
        </div>  
        
        <div class="col-md-6 text-right">
            {!! $btn_edit_pengurus!!}
            <a href="{{route('pdfPanitia',$id_proker)}}" class="btn btn-xs btn-round w-100 bg-red-600 text-white" id="btn_excel_pan">cetak PDF</a>
            <button class="btn btn-xs btn-round w-60 btn-secondary btn_cancel_pan" style="display:none">cancel</button>
            <button class="btn btn-xs btn-round w-100 btn-info addForm_pan" style="display:none">+ tambah data</button>
        </div>  
        <div class="col-md-12 table-responsive-xxl">
        <form id="form_panitia" class="form_panitia">      
        <input type="text" name="id_proker"  value="{{$id_proker}}" hidden>
        @csrf
            <table class="table table-light table-striped table-bordered" style="font-size: 12px;color: black">
                <thead class="abu-soft">
                    <tr>
                        <th width="5%" class="text-center">No</th>
                        <th width="30%" class="text-center" id="head_kolom_sie_peng">Sie Kepanitiaan</th>
                        <th width="30%" class="text-center">NIM</th>
                        <th width="10%" class="text-center">Kelas</th>
                        <th width="20%" class="text-center">Prodi</th>
                        <th width="5%" class="text-center bg-red-500 text-white td_delete_pan" style="display:none" ><i class="icon fa-trash" aria-hidden="true"></i></th>
                    </tr>
                </thead>
                <tbody class="kb_grub_panitia">
                    <tr class="kepanitiaan kepanitiaan0 baris" data-id="0"><td><div class="form-group form-material mb-0" data-plugin="formMaterial"><input type="text" class="form-control-plaintext section_pengurus form-control-sm pan_no" name="pan_no[]" value="1"/></div>
                    </td><td><div class="form-group form-material mb-0" data-plugin="formMaterial">
                    <input hidden type="text" class="form-control-plaintext section_pengurus select-pan form-control-sm pan_id" id="pan_id0"  name="pan_id[]" value="" />
                    <select disabled class="form-control-plaintext section_pengurus form-control-sm pan_sie_panitia pan_sie_panitia0" name="pan_sie_panitia[]"></select>
                    <div class="invalid-feedback">*wajib diisi</div></div></td><td><div class="form-group form-material mb-0" data-plugin="formMaterial">
                    <select disabled class="form-control-plaintext section_pengurus form-control-sm pan_nim pan_nim0" name="pan_nim[]" data-id="0"></select>
                    <div class="invalid-feedback">*wajib diisi</div></div></td><td><div class="form-group form-material mb-0" data-plugin="formMaterial">
                    <input disabled readonly type="text" class="form-control-plaintext section_pengurus select-pan form-control-sm pan_kelas" id="pan_kelas0" data-id="0" name="pan_kelas[]" placeholder="otomatis terisi"/>
                    <div class="invalid-feedback">*wajib diisi</div></div></td><td><div class="form-group form-material mb-0" data-plugin="formMaterial">
                    <input disabled readonly type="text" class="form-control-plaintext section_pengurus select-pan form-control-sm pan_prodi" id="pan_prodi0" data-id="0" name="pan_prodi[]" placeholder="otomatis terisi"/>
                    <div class="invalid-feedback">*wajib diisi</div></div></td><td class="text-center pt-15 td_delete_pan" style="display:none"><button type="button" class="btn  btn-danger btn-xs rmForm_pan" data-kebutuhan_id="" data-id="'+count+'">x</button></td></tr>
                </tbody>
            </table>  
        </form>            
        </div>  
        <div class="col-md-12 text-right">
            <button class="btn btn-xs btn-round w-60 btn-success" id="btnSave_pan" style="display:none">simpan</button>
            <button class="btn btn-xs btn-round w-60 btn-secondary btn_cancel_pan" style="display:none">cancel</button>
        </div>  
    </div>
</div>

@push('scripts')
<script id="script_pengurus">
        // variabel penting
        var count_pan = no_pan = 1;
        var nameSelect = nameSelect2 = idtag_pan = '';
        var checker_pan = true;

        // -=============================================== READY ==============================================-
        $(document).ready(function () {
            selecta('.pan_sie_panitia0');
            selecta_nim_panitia('.pan_nim0');
            // $('.select2-container').attr('style', 'display:none');
            $('.addForm_pan').click(function(e) {
                tambah_form_panitia();
            });            

            var url_panitia = "{{url('DTpanitia/')}}/"+"{{$id_proker}}";
            $.getJSON(url_panitia , function (data) {
                // console.log(data);
                $.each(data, function (i, v) { 
                    if ($('.kepanitiaan0').length)
                    {
                        // console.log('enek');
                        $('.kepanitiaan0').remove();                                              
                    }
                    // console.log(v);
                    // console.log(count_pan);
                    // console.log(idtag_pan);
                    // console.log(no);

                    idtag_pan = 'kepanitiaan'+count_pan;
                    var formP =  '<tr class="kepanitiaan '+idtag_pan+' baris" data-id="'+count_pan+'"><td><div class="form-group form-material mb-0" data-plugin="formMaterial"><input type="text" class="form-control-plaintext section_pengurus form-control-sm pan_no" name="pan_no[]" value="'+ no_pan++ +'"/></div>';
                        formP += '</td><td><div class="form-group form-material mb-0" data-plugin="formMaterial">';
                        formP += '<input hidden type="text" class="form-control-plaintext section_pengurus select-pan form-control-sm pan_id"  name="pan_id[]" value="'+v.id+'" />';
                        formP += '<select disabled class="form-control-plaintext section_pengurus form-control-sm pan_sie_panitia pan_sie_panitia'+count_pan+'" name="pan_sie_panitia[]">';
                        if(v.id_kepanitiaan != null){
                            formP += '<option value="'+v.id_kepanitiaan+'" selected>'+v.nama_kepanitiaan+'</option>';
                        }                         
                        formP += '</select><div class="invalid-feedback">*wajib diisi</div></div></td><td><div class="form-group form-material mb-0" data-plugin="formMaterial">';
                        formP += '<select disabled class="form-control-plaintext section_pengurus form-control-sm pan_nim pan_nim'+count_pan+'" name="pan_nim[]" data-id="'+count_pan+'">';
                        if(v.id_kepanitiaan != null){
                            formP += '<option value="'+v.nim+'" selected>( '+v.nim+' ) '+v.nama_mahasiswa+'</option>';
                        }                           
                        formP += '</select><div class="invalid-feedback">*wajib diisi</div></div></td><td><div class="form-group form-material mb-0" data-plugin="formMaterial">';
                        formP += '<input disabled type="text" class="form-control-plaintext section_pengurus select-pan form-control-sm pan_kelas" value="'+v.nama_kelas+'" id="pan_kelas'+count_pan+'" data-id="'+count_pan+'" name="pan_kelas[]" placeholder="otomatis terisi"/>';
                        formP += '<div class="invalid-feedback">*wajib diisi</div></div></td><td><div class="form-group form-material mb-0" data-plugin="formMaterial">';
                        formP += '<input disabled type="text" class="form-control-plaintext section_pengurus select-pan form-control-sm pan_prodi" value="'+v.nama_prodi+'" id="pan_prodi'+count_pan+'" data-id="'+count_pan+'" name="pan_prodi[]" placeholder="otomatis terisi"/>';
                        formP += '<div class="invalid-feedback">*wajib diisi</div></div></td><td class="text-center pt-15 td_delete_pan" style="display:none"><button type="button" class="btn btn-danger btn-xs rmForm_pan" data-panitia_id="'+v.id+'" data-id="'+count_pan+'">x</button></td></tr>';

                    $('.kb_grub_panitia').append(formP);
                
                    $('.'+idtag_pan).each(function () { 
                        nameSelect = '.pan_sie_panitia'+count_pan;
                        selecta(nameSelect);            
                        nameSelect2 = '.pan_nim'+count_pan;
                        selecta_nim_panitia(nameSelect2);            
                        // console.log(nameSelect);
                    });
                    count_pan = count_pan + 1;                       
                });
            });
        });
        // -=============================================== READY ==============================================-

        $('#btn_edit_pan').click(function (e) { 
            e.preventDefault();

            for (let index = 0; index < $('.baris').length; index++) {
                idtag_pan = 'kebutuhan_field'+count_pan;
                $('.'+idtag_pan).each(function () { 
                    nameSelect = '.sie_panitia'+count_pan;
                    selecta(nameSelect);            
                    // console.log(nameSelect);
                });
                count_pan = count_pan + 1;                       
            }

            $(this).attr('style','display:none')
            $('#btn_excel_pan').attr('style','display:none')
            $('.btn_cancel_pan').removeAttr('style','display:none');
            $('#btnSave_pan').removeAttr('style','display:none');
            $('.td_delete_pan').removeAttr('style','display:none');
            $('.addForm_pan').removeAttr('style','display:none');
            $('.section_pengurus').removeAttr('disabled').addClass('form-control').removeClass('form-control-plaintext');
            $('.no').removeAttr('class').attr('class',"form-control-plaintext form-control-sm no");
            $('.total_p').attr('readonly','true')
            $('.total_r').attr('readonly','true')
            // console.log($('.baris').length);

            // for (let index = 0; index < $('.baris').length; index++) {
            //     idtag = 'kebutuhan_field'+count;
            //     $('.'+idtag).each(function () { 
            //         nameSelect = '.sie_panitia'+count;
            //         selecta(nameSelect);            
            //         console.log(nameSelect);
            //     });
            //     count = count + 1;                       
            // }
            
                                
        });

        function cancel() {
            $('.btn_cancel_pan').attr('style','display:none')
            $('#btn_excel_pan').removeAttr('style','display:none')
            $('#btn_edit_pan').removeAttr('style','display:none');
            $('#btnSave_pan').attr('style','display:none');
            $('.td_delete_pan').attr('style','display:none');
            $('.addForm_pan').attr('style','display:none');
            $('.section_pengurus').addClass('form-control-plaintext').attr('disabled','true').removeClass('form-control');            
        }

        $('.btn_cancel_pan').click(function (e) { 
            e.preventDefault();
            cancel()
        });

        $(document).on('change', '.pan_nim', function (e) {
            var nim = $(this).val();
            var id = $(this).data('id');

            // console.log(id);
            $.ajax({
                url: "{{url('/anggota/getkelpro/')}}/"+nim,
                type: "GET",
                dataType: 'json',
                success: function (data) {
                    
                    if(jQuery.isEmptyObject(data) == false){
                        $('#pan_kelas'+id).val(data.nama_kelas);
                        // console.log('pan_kelas'+id);
                        $('#pan_prodi'+id).val(data.nama_prodi);
                    }else{}                
                    // console.log(data);                 
                
                    
                    
                    // toastr.success(data.message);
                    },
                error: function (data) {
                    console.log('Error:', data);
                    // toastr.success(data.message);
                    //$('#modalOrg').modal('show');
                    }
            });
        });

        $(document).on('click', '#btnSave_pan', function (e) {
            checker_pan = true;
            $('.kepanitiaan').each(function () {         
                var id = $(this).data('id');
                var pan_sie_panitia = $('.pan_sie_panitia'+id).val();
                var pan_nim = $('.pan_nim'+id).val();
                var pan_kelas = $('#pan_kelas'+id).val();     
                var pan_prodi = $('#pan_prodi'+id).val();
                console.log(pan_sie_panitia);
                console.log(pan_nim);
                console.log(pan_kelas);
                console.log(pan_prodi);

                    if(pan_sie_panitia == null){
                        checker_pan = false;                            
                        $('.pan_sie_panitia'+id).addClass('is-invalid').closest('.form-material').addClass('has-danger');
                        $('.pan_sie_panitia').change(function () {
                            $('.pan_sie_panitia'+id).removeClass('is-invalid').closest('.form-material').removeClass('has-danger');
                        });                            
                    }
                    if(pan_nim == null){
                        checker_pan = false;                            
                        $('.pan_nim'+id).addClass('is-invalid').closest('.form-material').addClass('has-danger');   
                        $('.pan_nim').change(function () {
                            $('.pan_nim'+id).removeClass('is-invalid').closest('.form-material').removeClass('has-danger');
                            $('#pan_kelas'+id).removeClass('is-invalid').closest('.form-material').removeClass('has-danger');
                            $('#pan_prodi'+id).removeClass('is-invalid').closest('.form-material').removeClass('has-danger');
                        });                                   
                    }
                    if(pan_kelas == ''){
                        checker_pan = false;                            
                        $('#pan_kelas'+id).addClass('is-invalid').closest('.form-material').addClass('has-danger');  
                        $('.pan_kelas').keyup(function () {
                            $('#pan_kelas'+id).removeClass('is-invalid').closest('.form-material').removeClass('has-danger');
                        });                                    
                    }   
                    if(pan_prodi == ''){
                        checker_pan = false;                            
                        $('#pan_prodi'+id).addClass('is-invalid').closest('.form-material').addClass('has-danger');
                        $('.pan_prodi').change(function () {
                            $('#pan_prodi'+id).removeClass('is-invalid').closest('.form-material').removeClass('has-danger');
                        });                            
                    }
                                
                // console.log(id+';'+kebutuhan+';'+jumlah+';'+satuan+';'+harga_satuan+';'+sie_panitia);
            });
            console.log(checker_pan);

            if (checker_pan == true) {
                $.ajax({
                    url: "{{route('simpan_panitia')}}",
                    type: "POST",
                    dataType: 'json',
                    data: $('#form_panitia').serialize(),                
                    success: function (data) {
                        console.log(data);
                        cancel()
                            swal({
                                title: "Sukses!",
                                text: "Perubahan berhasil disimpan",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonClass: "btn-success",
                                confirmButtonText: 'OK',
                                closeOnConfirm: false
                            });                     
                        // toastr.success(data.message);
                        },
                    error: function (data) {
                        console.log('Error:', data);
                        toastr.danger(data.message);
                        //$('#modalOrg').modal('show');
                        }
                });
            }
    
        });

        function selecta_nim_panitia (idSelect_nim) { 
            $(idSelect_nim).select2({
                minimumInputLength: 0,
                // language: {
                //     inputTooShort: function() {
                //         return 'Masukkan minimal 2 digit angka';
                //     }
                // },         
                allowClear : true,
                selectionTitleAttribute: false,
                searchInputPlaceholder: 'Search...',
                placeholder: 'pilih NIM',
                ajax: {
                cache: true,
                url: "{{ url('/anggota/select2AnggotaPanitia') }}?id_proker="+'{{$id_proker}}',
                dataType: 'json',
                type: "GET",
                quietMillis: 50,
                data: function (params) {
                    // console.log(params)
                    return {
                    search: $.trim(params.term)
                    };
                },
                processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                    return {
                        text:item.nama,
                        id: item.id
                        }
                    })
                    };
                }
                }
            });     
        };    

        function tambah_form_panitia(params) {
            // console.log(count_pan);

                    idtag_pan = 'kepanitiaan'+count_pan;
                    var formP =  '<tr class="kepanitiaan '+idtag_pan+' baris" data-id="'+count_pan+'"><td><div class="form-group form-material mb-0" data-plugin="formMaterial"><input type="text" class="form-control-plaintext section_pengurus form-control-sm pan_no" name="pan_no[]" value="'+ no_pan++ +'"/></div>';
                        formP += '</td><td><div class="form-group form-material mb-0" data-plugin="formMaterial">';
                        formP += '<input hidden type="text" class="form-control-plaintext section_pengurus select-pan form-control-sm pan_id"  name="pan_id[]" value="" />';
                        formP += '<select class="form-control section_pengurus form-control-sm pan_sie_panitia pan_sie_panitia'+count_pan+'" name="pan_sie_panitia[]">';                       
                        formP += '</select><div class="invalid-feedback">*wajib diisi</div></div></td><td><div class="form-group form-material mb-0" data-plugin="formMaterial">';
                        formP += '<select class="form-control section_pengurus form-control-sm pan_nim pan_nim'+count_pan+'" name="pan_nim[]" data-id="'+count_pan+'">';                       
                        formP += '</select><div class="invalid-feedback">*wajib diisi</div></div></td><td><div class="form-group form-material mb-0" data-plugin="formMaterial">';
                        formP += '<input readonly type="text" class="form-control section_pengurus select-pan form-control-sm pan_kelas" id="pan_kelas'+count_pan+'" data-id="'+count_pan+'" name="pan_kelas[]" placeholder="otomatis terisi"/>';
                        formP += '<div class="invalid-feedback">*wajib diisi</div></div></td><td><div class="form-group form-material mb-0" data-plugin="formMaterial">';
                        formP += '<input readonly type="text" class="form-control section_pengurus select-pan form-control-sm pan_prodi" id="pan_prodi'+count_pan+'" data-id="'+count_pan+'" name="pan_prodi[]" placeholder="otomatis terisi"/>';
                        formP += '<div class="invalid-feedback">*wajib diisi</div></div></td><td class="text-center pt-15 td_delete_pan"><button type="button" class="btn  btn-danger btn-xs rmForm_pan" data-kebutuhan_id="" data-id="'+count_pan+'">x</button></td></tr>';

                    $('.kb_grub_panitia').append(formP);
                
                    $('.'+idtag_pan).each(function () { 
                        nameSelect = '.pan_sie_panitia'+count_pan;
                        selecta(nameSelect);            
                        nameSelect2 = '.pan_nim'+count_pan;
                        selecta_nim_panitia(nameSelect2);            
                        // console.log(nameSelect);
                    });
                    count_pan = count_pan + 1;   
        }

        $(document).on('click', '.rmForm_pan', function () {
            var id = $(this).data('id');
            var panitia_id = $(this).data('panitia_id');
            $(this).closest('.kepanitiaan'+id).remove();
            $('#form_panitia').append('<input type="text" name="deleted_id[]" value="'+panitia_id+'" hidden class="deleted_id">');
        });            

        $(document).on('change', '.pan_sie_panitia', function () {
            $('.select2-selection__rendered').removeAttr('title');
        });

        $('.pan_nim').hover(function () {
            $('.select2-selection__rendered').removeAttr('title');
            }, function () {
                // out
            }
        );

        $('.pan_sie_panitia').hover(function () {
            $('.select2-selection__rendered').removeAttr('title');
            }, function () {
                // out
            }
        );
        


</script>
@endpush