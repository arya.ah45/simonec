<?php $hari_ini = date("Y-m-d"); ?>

<!DOCTYPE html>
<html>

<head>
	<title>Daftar Panitia</title>

    <script src="{{asset('js/jquery-3.5.1.js')}}"></script>

	<style type="text/css">
		body {
			font-family: "Times New Roman", Times, serif;
			font-size: 12pt;
		}

		.text-center {
			text-align: center;
		}

		.text-left {
			text-align: left;
		}

		.text-right {
			text-align: right;
		}

		.text-uppercase {
			text-transform: uppercase;
		}

		.text-lowercase {
			text-transform: lowercase;
		}

		.text-capital {
			text-transform: capitalize;
		}

		.text-underline {
			text-decoration: underline;
			text-decoration-color: #000;
		}

		.font-sm {
			font-size: 12px;
		}

		.bg-red {
			background-color: red;
		}

		.bg-grey {
			background-color: rgb(220, 220, 220);
		}

		.table {
			border-collapse: collapse;
			border-spacing: 0;
			width: 100%;
			border: solid 1px black;
		}

		.table th,
		.table td {
			border: 1px solid black;
			font-size: 12px;
			padding: 5px;
		}

		.mb-0 {
			margin-bottom: 0px;
		}

		.mt-0 {
			margin-top: 0px;
		}

		.my-0 {
			margin-bottom: 0px;
			margin-top: 0px;
		}

		.mb-1 {
			margin-bottom: 1.5px;
		}

		.mar {
			margin-top: 10px;
			margin-bottom: 10px
		}

		hr {
			display: block;
			margin-top: 0.3em;
			margin-bottom: -0.2em;
			margin-left: auto;
			margin-right: auto;
			border-style: inset;
			border-width: 3px;
			background: black;
		}

		ol {
			display: block;
			margin-top: 0em;
			margin-bottom: 1em;
			margin-left: 0;
			margin-right: 0;
			padding-left: 17px;
			padding-top: -15px;
		}
	</style>
</head>

<body>

	<table border="0" style="width: 100%;">
		<tr>
			<td style="width: 15%;padding-bottom:0px;" align="left"><img src="{{ asset("assets/images/logo.png") }}" style="width: 100px;"></td>
			<td style="width: 70%;margin-bottom:0px;" class="text-center">
				<p style="font-size:12pt;margin-bottom:0px"> <b>POLITEKNIK NEGERI MALANG</b> <br> <b>PSDKU KEDIRI</b> <br> <b>ENGLISH CLUB</b> </p>
				<p style="font-size:11pt;margin-top:0px;margin-bottom:5px"> Sekretariat : Jl. Lingkar Maskumambang, Kediri 64119, Kampus 2 Gedung D <br> Telp. (0354) 683128 – Fax. (0354) 683128 <br> Email : englishclubppk2020@gmail.com </p>
			</td>
			<td style="width: 15%;padding-bottom:0px" align="right"><img src="{{ asset("assets/images/logo_polinema.png") }}" style="width: 100px;"></td>
		</tr>
	</table>
	<hr style="height: 1px;margin-top:0px;">

	<!-- &nbsp;&nbsp;&nbsp;&nbsp; -->
	<p class="text-center" style="margin-bottom:0px;"><b>DAFTAR KEBUTUHAN</b></p>

	<br>
	<table class="table" style="width: 100%;" id="tabel_panitia">
		<tr>
			<th rowspan="2" width="5%" class="text-center">No</th>
			<th rowspan="2" width="10%" class="text-center">Sie</th>
			<th rowspan="2" width="15%" class="text-center">Uraian</th>
			<th colspan="4" width="35%" class="text-center">Usulan</th>
			<th colspan="4" width="35%" class="text-center" {{$hidden2}}>Realisasi</th>
		</tr>
		<tr>
			<th style="font-size:8pt;" class="text-center">Harga satuan</th>
			<th style="font-size:8pt;" width="5%" class="text-center">Satuan</th>
			<th style="font-size:8pt;" width="5%" class="text-center">Jumlah</th>
			<th style="font-size:8pt;" class="text-center">Total</th>
			@if ($hidden2 == 'style=display:none;')
			@else
			<th style="font-size:8pt;" class="text-center">Harga satuan</th>
			<th style="font-size:8pt;" width="5%" class="text-center">Satuan</th>
			<th style="font-size:8pt;" width="5%" class="text-center">Jumlah</th>
			<th style="font-size:8pt;" class="text-center">Total</th>
			@endif
		</tr>
		@foreach ($data_keb as $key => $item)
		<tr>
			<td style="font-size:8pt;" class="text-center">{{++$key}}</td>
			<td style="font-size:8pt;">{{$item['nama_kepanitiaan']}}</td>
			<td style="font-size:8pt;">{{$item['keterangan']}}</td>
			<td style="font-size:8pt;" class="text-right">{{$item['harga_satuan']}}</td>
			<td style="font-size:8pt;" class="text-right">{{$item['satuan']}}</td>
			<td style="font-size:8pt;" class="text-right">{{$item['jumlah']}}</td>
			<td style="font-size:8pt;" class="text-right">{{$item['sub_total_p']}}</td>
			@if ($hidden2 == 'style=display:none;')
			@else
			<td style="font-size:8pt;" class="text-right">{{$item['harga_satuan_real']}}</td>
			<td style="font-size:8pt;" class="text-right">{{$item['satuan_real']}}</td>
			<td style="font-size:8pt;" class="text-right">{{$item['jumlah_real']}}</td>
			<td style="font-size:8pt;" class="text-right">{{$item['sub_total_r']}}</td>
			@endif
		</tr>
		@endforeach
		<tr>
			<td style="font-size:8pt;" colspan="3" class="abu-soft">Perbandingan</td>
			<td style="font-size:8pt;" colspan="3" class="text-right pinky1-soft">Total Usulan</td>
			<td style="font-size:8pt;" class="text-right">{{$total_p}}</td>
			@if ($hidden2 == 'style=display:none;')
			@else			
			<td style="font-size:8pt;" colspan="3" class="text-right pinky2-soft">Total Realisasi</td>
			<td style="font-size:8pt;" class="text-right">{{$total_r}}</td>
			@endif
		</tr>		
	</table>
	<br>
	<p class="text-center" ><b>REKAP DANA</b></p>
	<table class="table" style="width: 100%;" id="tabel_panitia">
		<tr>
			<th class="text-center" width="100px">Keterangan</th>
			<th class="text-center pinky1-soft" width="200px">Usulan</th>
			<th class="text-center pinky2-soft" width="200px" {{$hidden2}}>Realisasi</th>
		</tr>

		<tr>
			<td style="font-size:12pt;font-weight:bold" colspan="{{$colspan}}">Debit</td>
		</tr>
		<tr>
			<td>Dana Mandiri</td>
			<td class="text-right pinky1-soft-1">{{$data_proker->dana_mandiri}}</td>
			<td class="text-right pinky2-soft-1" {{$hidden2}}>{{$data_proker->dana_mandiri_real}}</td>
		</tr>
		<tr>
			<td>Dana Kampus</td>
			<td class="text-right pinky1-soft-2">{{$data_proker->dana_turun}}</td>
			<td class="text-right pinky2-soft-2" {{$hidden2}}>{{$data_proker->dana_turun_real}}</td>
		</tr>
		<tr>
			<td>TOTAL</td>
			<td class="text-right abu1">{{$data_proker->t_debit}}</td>
			<td class="text-right abu1" {{$hidden2}}>{{$data_proker->t_debit_r}}</td>
		</tr>
		<tr>
			<td style="font-size:12pt;font-weight:bold" colspan="{{$colspan}}">Kredit</td>
		</tr>    
		<tr>
			<td>TOTAL</td>
			<td class="text-right abu1">{{$data_proker->dana_usulan}}</td>
			<td class="text-right abu1" {{$hidden2}}>{{$data_proker->dana_terpakai}}</td>
		</tr>
		<tr>
			<td style="font-size:12pt;font-weight:bold">Selisih</td>
			<td class="text-right abu2">{{$data_proker->selisih}}</td>
			<td class="text-right abu2" {{$hidden2}}>{{$data_proker->selisih_r}}</td>
		</tr>               		
	</table>
</body>

</html>