@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card card-shadow text-center" style="background-image: linear-gradient(180deg,rgba(255, 255, 255, 0.6),rgba(255, 255, 255, 0.9));">
            <div class="card-block p-0">
                <div class="row no-space p-0">
                    <div class="col-md-6 px-0">
                        <div class="btn-block py-10 px-15">
                            <h4 class="profile-user">{{$proker->nama}}</h4>
                        </div>
                    </div>
                    <div class="col-md-3 px-0">
                        <div class="btn-block bg-grey-400 py-10 h-p100 vertical-align">
                            <div class="profile-user vertical-align-middle">
                                <span style="font-size: 20px; font-weight: 500">{{DeHelper::jumlahPanitia($id_proker)}}</span> <span>Panitia</span>    
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 px-0">
                        <div class="btn-block {{$waktu['code']}} py-10 h-p100 vertical-align">
                            <div class="profile-user text-white vertical-align-middle">{{$waktu['time']}}</div>
                        </div>
                    </div>
                </div>                
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="panel">
            <div class="panel-body nav-tabs-animate nav-tabs-horizontal px-0 py-15" data-plugin="tabs">


                <ul class="nav nav-tabs nav-tabs-line" role="tablist">
                    @if ( Session::get('data_user')['nim']  == $proker->penanggungjawab || Session::get('data_user')['flag_lv'] == 'b' || Session::get('data_user')['flag_lv'] == 'a')
                        <li class="nav-item" role="presentation"><a class="active nav-link tap_detail" data-toggle="tab" href="#detail"
                            aria-controls="detail" role="tab">Detail</a></li>
                        <li class="nav-item" role="presentation"><a class="nav-link tap_kebutuhan" data-toggle="tab" href="#kebutuhan" aria-controls="kebutuhan"
                            role="tab">Kebutuhan</a></li>
                        <li class="nav-item" role="presentation"><a class="nav-link tap_pengurus" data-toggle="tab" href="#pengurus" aria-controls="pengurus"
                            role="tab">Pengurus / Panitia</a></li>
                        <li class="nav-item" role="presentation"><a class="nav-link tap_rapat" data-toggle="tab" href="#rapat" aria-controls="rapat"
                            role="tab">Rapat</a></li>
                        {{-- <li class="nav-item" role="presentation"><a class="nav-link" data-toggle="tab" href="#foto" aria-controls="foto"
                            role="tab">Dokumentasi</a></li> --}}                    
                        <li class="nav-item dropdown">
                            <a class="dropdown-toggle nav-link" data-toggle="dropdown" href="#" aria-expanded="false">Menu </a>
                            <div class="dropdown-menu" role="menu">
                                <a class="active dropdown-item tap_detail" data-toggle="tab" href="#detail" aria-controls="detail"
                                role="tab">Detail</a>
                                <a class="dropdown-item tap_kebutuhan" data-toggle="tab" href="#kebutuhan" aria-controls="kebutuhan"
                                role="tab">Kebutuhan</a>
                                <a class="dropdown-item tap_pengurus" data-toggle="tab" href="#pengurus" aria-controls="pengurus"
                                role="tab">Pengurus / Panitia</a>
                                <a class="dropdown-item tap_rapat" data-toggle="tab" href="#rapat" aria-controls="rapat"
                                role="tab">Rapat</a>
                                {{-- <a class="dropdown-item" data-toggle="tab" href="#dokumentasi" aria-controls="dokumentasi"
                                role="tab">Dokumentasi</a> --}}
                            </div>
                        </li>                                
                    @else
                        <li class="nav-item" role="presentation"><a class="active nav-link tap_detail" data-toggle="tab" href="#detail"
                            aria-controls="detail" role="tab">Detail</a></li>       
                        <li class="nav-item dropdown">
                            <a class="dropdown-toggle nav-link" data-toggle="dropdown" href="#" aria-expanded="false">Menu </a>
                            <div class="dropdown-menu" role="menu">
                                <a class="active dropdown-item tap_detail" data-toggle="tab" href="#detail" aria-controls="detail"
                                role="tab">Detail</a>
                                {{-- <a class="dropdown-item" data-toggle="tab" href="#dokumentasi" aria-controls="dokumentasi"
                                role="tab">Dokumentasi</a> --}}
                            </div>
                        </li>           
                    @endif
                        
                    @if ( Session::get('data_user')['nim'] == $proker->penanggungjawab && session::has('is_proker') == false)
                        <div class="float-right" style="right:10px; position: absolute"><a href="{{route('event')}}" class="btn btn-outline-secondary btn-sm"><i class="icon fa-mail-reply"></i> Kembali</a></div>
                    @else
                        <div class="float-right" style="right:10px; position: absolute"><a href="{{route('proker')}}" class="btn btn-outline-secondary btn-sm"><i class="icon fa-mail-reply"></i> Kembali</a></div>
                    @endif
                </ul>                

                <div class="tab-content">
                    @include('proker.detail.section_home')
                    @include('proker.detail.section_kebutuhan')
                    @include('proker.detail.section_pengurus')
                    @include('proker.detail.section_rapat')
                    {{-- @include('proker.detail.section_dokumentasi') --}}
                    {!! $aksi !!}                
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade " id="modal_approval" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-center" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="judulModalApproval">Catatan</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body pt-0 pb-0">
        <form  id="formApproval" name="formApproval">
        @csrf
        <input hidden name="id_proker" id="id_proker_approve">
        <input hidden name="jenis" id="jenis">
        <div class="form-group row" >
          <div class="col-md-12 pt-0">
            <textarea class="form-control" name="catatan" id="textareaDefault" rows="3" style="margin-top: 0px; margin-bottom: 0px; height: 100px;"></textarea>
            <div class="invalid-feedback">*wajib diisi</div>
            </div>
          </div>
        </div>
        </form>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" id="btnApproval" class="btn btn-danger">Tolak</button>
        </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script id="detail">

    $(document).on('click', '.tap_kebutuhan', function () {
        $('.select2').removeAttr('style', 'display:none');
        // console.log('ok1');
    });

    $(document).on('click', '.tap_detail', function () {
        $('.select2').attr('style', 'display:none');
        // console.log('ok');
    });

    $(document).on('click', '.tap_pengurus', function () {
        $('.select2').removeAttr('style', 'display:none');
        // console.log('ok2');
    });

    function ubah_status(url,question) {
        swal({
            title: question,
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-warning",
            confirmButtonText: 'Iya',
            cancelButtonText: "Tidak",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                    $.ajax({
                        url: url,
                        type: "GET",
                        dataType: 'json',
                        success: function (data) {
                            console.log(data);
                            swal({
                                title: "Sukses!",
                                text: data.message,
                                type: "success",
                                showCancelButton: false,
                                confirmButtonClass: "btn-success",
                                confirmButtonText: 'OK',
                                closeOnConfirm: false
                            }, function (isConfirm) {
                                if (isConfirm) {
                                    location.reload(true);
                                    return;
                                }else {
                                    check = 0;
                                }
                            });  
                        },
                        error: function (data) {
                            console.log('Error:', 'terjadi kesalahan teknis, dimohon mencoba lagi beberapa saat');
                            toastr.error('terjadi kesalahan teknis, dimohon mencoba lagi beberapa saat');
                            //$('#modalOrg').modal('show');
                        }
                    });
            }else {
            }
        });        
    }

    $(document).on('click', '.btn_approve', function (e) {
        console.log($(this).data('id'));
        var id = $(this).data('id');
        var question = $(this).data('q');
        var jenis = $(this).data('jenis');
        var url = "{{url('proker/approval/')}}/"+jenis+'/'+id;
        ubah_status(url,question)        
        // console.log(url);
    })

    $(document).on('click', '.btn_kirim_kt', function (e) {
        console.log($(this).data('id'));
        var id = $(this).data('id');
        var question = $(this).data('q');
        var jenis = $(this).data('jenis');
        var url = "{{url('proker/approval/')}}/"+jenis+'/'+id;
        ubah_status(url,question)        
        // console.log(url);
    })

    $(document).on('click', '.btn_penyesuaian', function (e) {
        console.log($(this).data('id'));
        var id = $(this).data('id');
        var question = $(this).data('q');
        var jenis = $(this).data('jenis');
        var url = "{{url('proker/approval/')}}/"+jenis+'/'+id;
        ubah_status(url,question)        
        // console.log(url);
    })

    $(document).on('click', '.btn_review_ok', function (e) {
        console.log($(this).data('id'));
        var id = $(this).data('id');
        var question = $(this).data('q');
        var jenis = $(this).data('jenis');
        var url = "{{url('proker/approval/')}}/"+jenis+'/'+id;
        ubah_status(url,question)        
        // console.log(url);
    })

    $(document).on('click', '.btn_perbaikan', function (e) {
        console.log($(this).data('id'));
        var id = $(this).data('id');
        var question = $(this).data('q');
        var jenis = $(this).data('jenis');
        var url = "{{url('proker/approval/')}}/"+jenis+'/'+id;
        ubah_status(url,question)        
        // console.log(url);
    })

    $(document).on('click', '#btnApproval', function (e) {
        var urlT = "{{route('approval2Proker')}}";
        // console.log(urlT);
        $.ajax({
        data: $('#formApproval').serialize(),
        url: urlT,
        type: "POST",
        dataType: 'json',
        success: function(data) {
            console.log(data);
            $('#formApproval').trigger('reset');
            $('#modal_approval').modal('hide');
            swal({
                title: "Sukses!",
                text: data.message,
                type: "success",
                showCancelButton: false,
                confirmButtonClass: "btn-success",
                confirmButtonText: 'OK',
                closeOnConfirm: false
            }, function (isConfirm) {
                if (isConfirm) {
                    location.reload(true);
                    return;
                }else {
                    check = 0;
                }
            }); 
        },
        error: function(data) {
            console.log('Error:', data);
            //$('#modalRPendidikan').modal('show');
        }
        });
    })

    $(document).on('click','.btn_tolak',function(){
        var id = $(this).data('id');
        var jenis = $(this).data('jenis');
        $('#jenis').val(jenis);
        $('#id_proker_approve').val($(this).data('id'));

        $('#judulModalApproval').text('Catatan');
        $('#btnApproval').text('Tolak').removeClass('btn-info').addClass('btn-danger');

        $('#modal_approval').modal({
            backdrop: 'static',
            keyboard: false, // to prevent closing with Esc button (if you want this too)
            show: true  
        })        
    });

    $(document).on('click','.btn_approve_dc',function(){
        var id = $(this).data('id');
        var jenis = $(this).data('jenis');
        $('#jenis').val(jenis);
        $('#id_proker_approve').val($(this).data('id'));

        $('#judulModalApproval').text('Catatan');
        $('#btnApproval').text('Setujui dengan catatan').removeClass('btn-danger').addClass('btn-info');

        $('#modal_approval').modal({
            backdrop: 'static',
            keyboard: false, // to prevent closing with Esc button (if you want this too)
            show: true  
        })        
    });

</script>
@endpush