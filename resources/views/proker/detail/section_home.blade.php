<div class="tab-pane active animation-slide-right container-fluid" id="detail" role="tabpanel">
    <form name="formDetail" id="formDetail">
    <div class="row py-20">
            <input type="text" hidden name="id_proker" id="id_proker_home" value="{{$id_proker}}">
        @csrf
        <div class="col-md-12">
                    
            <h4>Detail Lengkap Proker</h4>
        </div>
        <div class="col-md-12 row pr-0">
            <div class="col-md-6">
                <div class="form-group form-material" data-plugin="formMaterial">
                    <label class="form-control-label" for="nama">Nama Proker</label>
                    <input type="text" value="{{$proker->nama}}" class="form-control-plaintext section_home" disabled id="nama" name="nama" placeholder="nama Proker" data-hint="" />
                    <div class="invalid-feedback">*wajib diisi</div> 
                </div>
            </div>
            <div class="col-md-6 pr-0">
                <div class="form-group form-material" data-plugin="formMaterial">
                    <label class="form-control-label" for="tipe_proker">Tipe Proker</label>
                    <select class="form-control-plaintext section_home" disabled id="tipe_proker" name="tipe_proker" placeholder="tipe proker">
                        <option value="0" selected disabled>Pilih Tipe</option>
                        <option {{($proker->event == 1)?'selected':''}} value="1">NORMAL</option>
                        <option {{($proker->event == 2)?'selected':''}} value="2">EVENT</option>
                    </select>
                    <input type="text" class="form-control-plaintext section_home" disabled id="tipe_proker_v" name="tipe_proker_v" data-hint="" />
                    <div class="invalid-feedback">*wajib diisi</div> 
                </div>
            </div>                                                         
        </div>    
        <div class="col-md-12 row pr-0">                                   
            <div class="col-md-6">
                <div class="form-group form-material" data-plugin="formMaterial">
                    <label class="form-control-label" for="subjek_tujuan">Proker ditujukan kepada</label>
                    <input type="text" value="{{$proker->subjek_tujuan}}" class="form-control-plaintext section_home" disabled id="subjek_tujuan" name="subjek_tujuan" placeholder="subjek proker ditujukan " data-hint="" />
                    <div class="invalid-feedback">*wajib diisi</div> 
                </div>
            </div>            
            <div class="col-md-6 pr-0">
                <div class="form-group form-material" data-plugin="formMaterial">
                    <label class="form-control-label" for="penanggungjawab">Penanggung jawab</label>
                    <input type="text" value="( {{$proker->penanggungjawab}} ) {{$proker->nama_pj}}" class="form-control-plaintext section_home" disabled id="nama_pj" name="nama_pj" data-hint="" />
                    <select type="text" class="form-control section_home" id="penanggungjawab" name="penanggungjawab">
                        @if (isset($proker->penanggungjawab))
                            <option value="{{$proker->penanggungjawab}}" selected>( {{$proker->penanggungjawab}} ) {{$proker->nama_pj}}</option>
                        @endif                        
                    </select>
                    <div class="invalid-feedback">*wajib diisi</div>  
                </div>
            </div>                        
        </div>    
        <div class="col-md-12 row pr-0">
            <div class="col-md-6">
                <div class="form-group form-material" data-plugin="formMaterial">
                    <label class="form-control-label" for="tgl_mulai">Tanggal Mulai</label>
                    <input type="date" value="{{$proker->tanggal_mulai}}" min="{{date('Y').'-01-01'}}" max="{{(date('Y')+1).'-12-31'}}" class="form-control-plaintext section_home" disabled id="tgl_mulai" name="tgl_mulai" data-hint="tanggal Proker dilaksanakan" />
                    <div class="invalid-feedback">*wajib diisi</div> 
                </div>
            </div>                                            
            <div class="col-md-6 pr-0">
                <div class="form-group form-material" data-plugin="formMaterial">
                    <label class="form-control-label" for="tgl_selesai">Tanggal Selesai</label>
                    <input type="date" value="{{$proker->tanggal_selesai}}" min="{{date('Y').'-01-01'}}" max="{{(date('Y')+1).'-12-31'}}" class="form-control-plaintext section_home" disabled id="tgl_selesai" name="tgl_selesai" data-hint="tanggal Proker selesai" />
                    <div class="invalid-feedback">*wajib diisi</div> 
                </div>
            </div>                                   
        </div>
        <div class="col-md-12 row pr-0">
            <div class="col-md-6">
                <div class="form-group form-material" data-plugin="formMaterial">
                    <label class="form-control-label" for="tujuan">Tujuan </label>
                    <textarea class="form-control-plaintext section_home" disabled id="tujuan" name="tujuan" data-hint="tujuan utama Proker diselenggarakan">{{$proker->tujuan}}</textarea>
                    <div class="invalid-feedback">*wajib diisi</div> 
                </div>
            </div>              
            <div class="col-md-6 pr-0">
                <div class="form-group form-material" data-plugin="formMaterial">
                    <label class="form-control-label" for="ket_tambahan">Keterangan tambahan</label>
                    <textarea class="form-control-plaintext section_home" disabled id="ket_tambahan" name="ket_tambahan" data-hint="Keterangan Proker" >{{$proker->ket_tambahan}}</textarea>
                    <div class="invalid-feedback">*wajib diisi</div> 
                </div>
            </div>                        
        </div>  
        @if ($waktu['time']=='Selesai')
            <div class="col-md-12 row pr-0">
                <div class="col-md-6">
                    <div class="form-group form-material" data-plugin="formMaterial">
                        <label class="form-control-label" for="tujuan">Status</label>
                        <select class="form-control-plaintext section_home" disabled id="terlaksana" name="terlaksana" >
                            <option {{($proker->terlaksana == false)?'selected':''}} value="0">TIDAK TERLAKSANA</option>
                            <option {{($proker->terlaksana == true)?'selected':''}} value="1">TERLAKSANA</option>
                        </select>
                        <input type="text" class="form-control-plaintext section_home" disabled id="terlaksana_v" name="terlaksana_v" data-hint="" />
                        <div class="invalid-feedback">*wajib diisi</div> 
                    </div>
                </div>                      
                <div class="col-md-6">
                    <div class="form-group form-material" data-plugin="formMaterial">
                        <label class="form-control-label" for="tujuan">Rating</label>
                        <div id="rateYo"></div>
                        <input type="text" hidden class="form-control-plaintext section_home" id="rating" name="rating" data-hint="" />
                        <div class="invalid-feedback">*wajib diisi</div> 
                    </div>                    
                </div>                      
            </div>  
            <div class="col-md-12 row pr-0">
                <div class="col-md-12">
                    <div class="form-group form-material" data-plugin="formMaterial">
                        <label class="form-control-label" for="tujuan">Catatan Evaluasi</label>
                        <textarea class="form-control-plaintext section_home" disabled id="evaluasi" name="evaluasi" data-hint="evaluasi program kerja">{{$proker->evaluasi}}</textarea>
                        <div class="invalid-feedback">*wajib diisi</div> 
                    </div>
                </div>                     
            </div>             
        @endif
        
        <div class="col-md-12">
            <h4>Rekap keuangan (total)</h4>
        </div>  
        <div class="col-md-12 row pr-0">
            <div class="col-md-3">
                <div class="form-group form-material" data-plugin="formMaterial">
                    <label class="form-control-label" for="dana_usulan">Total dana rencana kebutuhan</label>
                    <input readonly type="text" value="{{$proker->dana_usulan}}" class="form-control-plaintext section_home" disabled id="dana_usulan" name="dana_usulan" placeholder="dana usulan otomatis dihitung sistem" />
                </div>
            </div>                        
            <div class="col-md-3">
                <div class="form-group form-material" data-plugin="formMaterial">
                    <label class="form-control-label" for="dana_kampus">Usulan dana kampus</label>
                    <input readonly type="text" value="{{$proker->dana_turun}}"  class="form-control-plaintext section_home" disabled id="dana_kampus" name="dana_kampus" placeholder="dana yang diterima dari pengajuan proposal" />
                </div>
            </div>                        
            <div class="col-md-3 pr-0">
                <div class="form-group form-material" data-plugin="formMaterial">
                    <label class="form-control-label" for="dana_mandiri">Dana Mandiri</label>
                    <input readonly type="text" value="{{$proker->dana_mandiri}}"  class="form-control-plaintext section_home" disabled id="dana_mandiri" name="dana_mandiri" placeholder="dana yang digunakan" />
                </div>
            </div>                        
            <div class="col-md-3 pr-0">
                <div class="form-group form-material" data-plugin="formMaterial">
                    <label class="form-control-label" for="dana_terpakai">Total dana terealisasi kebutuhan</label>
                    <input readonly type="text" value="{{$proker->dana_terpakai}}"  class="form-control-plaintext section_home" disabled id="dana_terpakai" name="dana_terpakai" placeholder="dana yang digunakan" />
                </div>
            </div>                        
        </div>    
        
        <div class="col-md-12 text-left">
            {!! $btn_edit_home !!}
            @if ($waktu['time']=='Selesai')
                <button class="btn btn-sm btn-round w-100 text-white bg-purple-600" id="btn_edit_status">Edit Status</button>
            @endif                 
            <button class="btn btn-sm btn-round w-100 btn-secondary btn_cancel_home" style="display:none">Cancel</button>
            <button class="btn btn-sm btn-round w-100 btn-success btn_save_home" style="display:none">Simpan</button>
        </div>  
    </div>
</form>   

</div>

@push('scripts')
<script id="script_home">
    // variabel penting
    var count_pan = 1;
    var nameSelect = nameSelect2 = idtag_pan = text_tipe = status_laksana = '';
    var no_pan = 1;
    // -=============================================== READY ==============================================-

    function formatRupiah(angka, prefix){
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
        split           = number_string.split(','),
        sisa            = split[0].length % 3,
        rupiah          = split[0].substr(0, sisa),
        ribuan          = split[0].substr(sisa).match(/\d{3}/gi);

            // tambahkan titik jika yang di input sudah menjadi angka ribuan
            if(ribuan){
                separator = sisa ? '.' : '';
                rupiah += separator + ribuan.join('.');
            }

            rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
            return prefix == undefined ? rupiah : (rupiah ? 'Rp.' + rupiah : '');
    }

    function vld_text(id_input,col){
        if ($(id_input).val()=='') {
            $(id_input).addClass('is-invalid').closest('.col-md-'+col).find('.form-control-label').addClass('text-danger');
            $(id_input).focus(function () {
                $(this).removeClass('is-invalid')
            });
            $(id_input).keyup(function () {
                $(this).closest('.col-md-'+col).find('.form-control-label').removeClass('text-danger');
            });
        }
    }

    function vld_select(id_input,col){
        if ($(id_input).val()==null) {
            $(id_input).addClass('is-invalid').closest('.col-md-'+col).find('.form-control-label').addClass('text-danger');
            $(id_input).focus(function () {
                $(this).removeClass('is-invalid')
            });
            $(id_input).change(function () {
                $(this).closest('.col-md-'+col).find('.form-control-label').removeClass('text-danger');
            });
        }
    }

    $(document).ready(function () {
        $("#rateYo").rateYo({            
            rating:'{{$proker->rating}}',
            starWidth: "30px",
            readOnly: true,
            numStars: 5,
            multiColor: {
        
            "startColor": "#FF0000", //RED
            "endColor"  : "#fcc203"  //GREEN
            },
            halfStar: true
        });


        // fix bug title in select2
        $(document).on('mouseenter', '.select2-selection__rendered', function () {
            $('.select2-selection__rendered').removeAttr('title');
        });

        $('#penanggungjawab').attr('style', 'display:none'); 
        $('#tipe_proker').attr('style', 'display:none'); 
        $('#terlaksana').attr('style', 'display:none'); 
        $('.select2').attr('style', 'display:none');
        $('.select2').removeAttr('style', 'display:none');
        // console.log($('#tipe_proker :checked').text());

        if($('#tipe_proker :checked').text() == 'Pilih Tipe'){
            text_tipe = 'belum dipilih';
        }else{
            text_tipe = $('#tipe_proker :checked').text();
        }
        
        $('#tipe_proker_v').val(text_tipe);  
        
        status_laksana = $('#terlaksana :checked').text();
        $('#terlaksana_v').val(status_laksana);        

        $('#dana_usulan').val(formatRupiah($('#dana_usulan').val(), ''));
        $('#dana_kampus').val(formatRupiah($('#dana_kampus').val(), ''));
        $('#dana_mandiri').val(formatRupiah($('#dana_mandiri').val(), ''));
        $('#dana_terpakai').val(formatRupiah($('#dana_terpakai').val(), ''));

        var tipe = $('#tipe_proker').val();

        if (tipe == 1) {
            $('#head_kolom_sie_peng').html('Pengurus');
            $('.tap_pengurus').html('Pengurus');
        }else if(tipe == 2){
            $('#head_kolom_sie_peng').html('Sie Kepanitiaan');
            $('.tap_pengurus').html('Panitia');
        }else{}


                
    });
    // -=============================================== READY ==============================================-


    $(document).on('keyup', '#dana_usulan', function (e) {
        var data = $(this).val();
        var hasil = formatRupiah(data, '');
        $(this).val(hasil);
    });

    $(document).on('keyup', '#dana_kampus', function (e) {
        var data = $(this).val();
        var hasil = formatRupiah(data, '');
        $(this).val(hasil);
    });

    $(document).on('keyup', '#dana_mandiri', function (e) {
        var data = $(this).val();
        var hasil = formatRupiah(data, '');
        $(this).val(hasil);
    });

    $(document).on('keyup', '#dana_terpakai', function (e) {
        var data = $(this).val();
        var hasil = formatRupiah(data, '');
        $(this).val(hasil);
    });

    function vld_text(id_input,col){
        if ($(id_input).val()=='') {
            $(id_input).addClass('is-invalid').closest('.col-md-'+col).find('.form-control-label').addClass('text-danger');
            $(id_input).closest('.form-material').addClass('has-danger');
            $(id_input).focus(function () {
                $(this).removeClass('is-invalid').closest('.form-material').removeClass('has-danger');
            });
            $(id_input).keyup(function () {
                $(this).closest('.col-md-'+col).find('.form-control-label').removeClass('text-danger');
            });
        }
    }

    function vld_select(id_input,col){
        if ($(id_input).val()==null) {
            $(id_input).addClass('is-invalid').closest('.col-md-'+col).find('.form-control-label').addClass('text-danger');
            $(id_input).closest('.form-material').addClass('has-danger');                    
            $(id_input).focus(function () {
                $(this).removeClass('is-invalid').closest('.form-material').removeClass('has-danger');
            });
            $(document).on('change',id_input, function () {
                console.log('select1 ok');
                $(this).removeClass('is-invalid').closest('.col-md-'+col).find('.form-control-label').removeClass('text-danger');
                $(this).removeClass('is-invalid').closest('.form-material').removeClass('has-danger');                        
            });
        }
    }

    function vld_date(id_input,col){
        if ($(id_input).val()=='') {
            $(id_input).addClass('is-invalid').closest('.col-md-'+col).find('.form-control-label').addClass('text-danger');
            $(id_input).closest('.form-material').addClass('has-danger');                    
            $(id_input).focus(function () {
                $(this).removeClass('is-invalid').closest('.form-material').removeClass('has-danger');
            });
            $(document).on('change',id_input, function () {
                console.log('select1 ok');
                $(this).removeClass('is-invalid').closest('.col-md-'+col).find('.form-control-label').removeClass('text-danger');
                $(this).removeClass('is-invalid').closest('.form-material').removeClass('has-danger');                        
            });
        }
    }

    // aksi ketika save data dan update
    $('.btn_save_home').click(function (e) {
        e.preventDefault();

        vld_text('#nama',6);
        vld_select('#tipe_proker',6);
        vld_text('#subjek_tujuan',6);
        vld_select('#penanggungjawab',6);
        vld_date('#tgl_mulai',6);
        vld_date('#tgl_selesai',6);
        vld_text('#tujuan',6);


        if(
            $('#nama').val() != '' &&
            $('#tipe_proker').val() != null &&
            $('#subjek_tujuan').val() != '' &&
            $('#penanggungjawab').val() != null &&
            $('#tgl_mulai').val() != '' &&
            $('#tgl_selesai').val() != '' &&
            $('#tujuan').val() != ''
        ){
            $(this).html('Sending..');
            $(this).attr('disabled',true);
            $.ajax({
                data: $('#formDetail').serialize(),
                url: "{{route('updateDetail')}}",
                type: "POST",
                dataType: 'json',
                success: function (data) {
                    console.log(data);
                    $("#rateYo").rateYo("option", "readOnly", false); //returns a jQuery Element                    
                    // $('#formRapat').trigger('reset');
                    swal({
                        title: "Sukses!",
                        text: "data detail proker berhasil diupdate",
                        type: "success",
                        showCancelButton: false,
                        confirmButtonClass: "btn-success",
                        confirmButtonText: 'OK',
                        closeOnConfirm: false
                    }); 
                    cancel_detail()
                    $('.btn_save_home').html('Simpan');
                    $('.btn_save_home').removeAttr('disabled',true);
                },
                error: function (data) {
                    console.log('Error:', data);
                    $('.btn_save_home').html('Simpan');
                    toastr.error('Kelola Data Gagal');
                }
            })
        }

        
    });  

    function cancel_detail() {
        $('.btn_cancel_home').attr('style','display:none')
        $('#btn_edit_home').removeAttr('style','display:none')
        $('#btn_edit_status').removeAttr('style','display:none')
        $('.btn_save_home').attr('style','display:none');

        $('#formDetal').trigger('reset');
        $('#tipe_proker_v').removeAttr('style', 'display:none'); 
        pj = $('#penanggungjawab :checked').text();
        $('#nama_pj').val(pj).removeAttr('style', 'display:none'); 
        
        $('#penanggungjawab').attr('style', 'display:none'); 
        $('#tipe_proker').attr('style', 'display:none'); 
        $('.select2').attr('style', 'display:none');
        // console.log($('#tipe_proker :checked').text());
        $('.form-control').attr('disabled','true').addClass('form-control-plaintext').removeClass('form-control');

        if($('#tipe_proker :checked').text() == 'Pilih Tipe'){
            text_tipe = 'belum dipilih';
        }else{
            text_tipe = $('#tipe_proker :checked').text();
        }
        $('#tipe_proker_v').val(text_tipe);
        
        status_laksana = $('#terlaksana :checked').text();
        $('#terlaksana_v').val(status_laksana);             
        $('#terlaksana_v').removeAttr('style', 'display:none'); 
    }

    $('.btn_cancel_home').click(function (e) { 
        e.preventDefault();            
        cancel_detail()
    });

    $('#btn_edit_status').click(function (e) { 
        e.preventDefault();

        $('.select2').removeAttr('style', 'display:none');
        $('#terlaksana').select2();
        $('#terlaksana_v').attr('style','display:none')
        $(this).attr('style','display:none')

        $('.btn_cancel_home').removeAttr('style','display:none')
        $('.btn_save_home').removeAttr('style','display:none');
        $('#terlaksana').removeAttr('disabled').addClass('form-control').removeClass('form-control-plaintext');
        $('#evaluasi').removeAttr('disabled').addClass('form-control').removeClass('form-control-plaintext');
        $('#dana_usulan').attr('readonly','true')
        $('#dana_terpakai').attr('readonly','true')

        var laksa = $('#terlaksana').val();
        if (laksa == 1) {
            $("#rateYo").rateYo('destroy');
            $("#rateYo").rateYo({
                rating:'{{$proker->rating}}',                
                starWidth: "30px",
                readOnly: false,
                numStars: 5,
                multiColor: {
                    "startColor": "#FF0000", //RED
                    "endColor"  : "#fcc203"  //GREEN
                },
                halfStar: true
            });           
            $("#rateYo").rateYo("option", "onChange", function (rating, rateYoInstance) {
                $('#rating').val(rating);
                console.log("nilai rating : "+rating);
            }); //returns a jQuery Element
                            
        }        
    });

    $('#btn_edit_home').click(function (e) { 
        e.preventDefault();
        $('.select2').removeAttr('style', 'display:none');
        
        selecta_nim ('#penanggungjawab')
        $('#tipe_proker').select2();
        $('#terlaksana').select2();
        $('#terlaksana_v').attr('style','display:none')

        $('#nama_pj').attr('style','display:none')
        $('#tipe_proker_v').attr('style','display:none')
        $(this).attr('style','display:none')

        $('.btn_cancel_home').removeAttr('style','display:none')
        $('.btn_save_home').removeAttr('style','display:none');
        $('.section_home').removeAttr('disabled').addClass('form-control').removeClass('form-control-plaintext');
        $('#dana_usulan').attr('readonly','true')
        $('#dana_terpakai').attr('readonly','true')

                            
    });

    function cancel() {
        $('.btn_cancel_pan').attr('style','display:none')
        $('#btn_excel_pan').removeAttr('style','display:none')
        $('#btn_edit_pan').removeAttr('style','display:none');
        $('#btnSave_pan').attr('style','display:none');
        $('.td_delete_pan').attr('style','display:none');
        $('.addForm_pan').attr('style','display:none');
        $('.section_home').addClass('form-control-plaintext').attr('disabled','true');
        $('.section_home').removeClass('form-control');            
    }

    function selecta_nim (idSelect_nim) { 
        $(idSelect_nim).select2({
            minimumInputLength: 0,
            // language: {
            //     inputTooShort: function() {
            //         return 'Masukkan minimal 2 digit angka';
            //     }
            // },         
            allowClear : true,
            searchInputPlaceholder: 'Search...',
            placeholder: 'pilih NIM',
            ajax: {
            cache: true,
            url: "{{ url('/anggota/select2') }}",
            dataType: 'json',
            type: "GET",
            quietMillis: 50,
            data: function (params) {
                // console.log(params)
                return {
                search: $.trim(params.term)
                };
            },
            processResults: function (data) {
            return {
                results: $.map(data, function (item) {
                return {
                    text:item.nama,
                    id: item.id
                    }
                })
                };
            }
            }
        });     
    };  

    function selecta (idSelect) { 
        // console.log($('#tipe_proker').val());
        $(idSelect).select2({
            minimumInputLength: 0,
            // language: {
            //     inputTooShort: function() {
            //         return 'Masukkan minimal 2 digit angka';
            //     }
            // },         
            allowClear : true,
            searchInputPlaceholder: 'Search...',
            placeholder: 'pilih sie',
            ajax: {
            cache: true,
            url: "{{ url('master/kepanitiaan/select2') }}?tipe_event="+$('#tipe_proker').val(),
            dataType: 'json',
            type: "GET",
            quietMillis: 50,
            data: function (params) {
                // console.log(params)
                return {
                search: $.trim(params.term)
                };
            },
            processResults: function (data) {
            return {
                results: $.map(data, function (item) {
                return {
                    text:item.nama,
                    id: item.id
                    }
                })
                };
            }
            }
        });     
    };    


    $(document).on('change', '#tipe_proker', function (e) {
        e.preventDefault();
        
        selecta('.sie_panitia');   
        selecta('.pan_sie_panitia');
        var tipe = $('#tipe_proker').val();

        if (tipe == 1) {
            $('#head_kolom_sie_peng').html('Pengurus');
            $('.tap_pengurus').html('Pengurus');
        }else if(tipe == 2){
            $('#head_kolom_sie_peng').html('Sie Kepanitiaan');
            $('.tap_pengurus').html('Panitia');
        }else{}
    });        

    $(document).on('change', '#terlaksana', function (e) {
        e.preventDefault();
        var laksa = $(this).val();
        var tipe = $('#terlaksana_v').val(laksa);
        if (laksa == 1) {
            $("#rateYo").rateYo('destroy');
            $("#rateYo").rateYo({
                starWidth: "30px",
                rating:'{{$proker->rating}}',                  
                readOnly: false,
                numStars: 5,
                multiColor: {
                    "startColor": "#FF0000", //RED
                    "endColor"  : "#fcc203"  //GREEN
                },
                halfStar: true
            });            
        }else{
            $("#rateYo").rateYo('destroy');
            $("#rateYo").rateYo({
                starWidth: "30px",
                readOnly: true,
                numStars: 5,
                multiColor: {
                    "startColor": "#FF0000", //RED
                    "endColor"  : "#fcc203"  //GREEN
                },
                halfStar: true
            });               
        }
        $("#rateYo").rateYo("option", "onChange", function (rating, rateYoInstance) {
            $('#rating').val(rating);
            console.log("nilai rating : "+rating);
        }); //returns a jQuery Element
                    
        console.log(laksa);
    });        

    $(document).on('change', '#tgl_mulai', function (e) {
        // console.log(this);
        var tgl_ = $(this).val();
        $('#tgl_selesai').attr('min', tgl_);
    });


</script>
@endpush