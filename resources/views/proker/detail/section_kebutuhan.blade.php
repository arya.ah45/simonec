@php
    if($waktu['realisasi']==true){
        $hidden = '';
        $id_pdf = 1;
        $colspan = 3;
    }else{
        $hidden = "style=display:none";
        $id_pdf = 0;
        $colspan = 2;
    }
@endphp
<div class="tab-pane  animation-slide-right container-fluid" id="kebutuhan" role="tabpanel">
    <form id="form_kebutuhan" class="form_kebutuhan">    
    <div class="row py-20">
        <div class="col-md-6">
            <h4>Rekap keuangan (total)</h4>
        </div>  
        <div class="col-md-6 text-right">
            {!! $btn_edit_keb!!}
            @if ($waktu['time']=='Selesai')
                <button class="btn btn-xs btn-round w-100 text-white bg-purple-700" id="btn_edit_real">Edit Realiasi</button>
            @endif            
            <a href="{{route('pdfKebutuhan',[$id_proker,$id_pdf])}}" class="btn btn-xs btn-round w-100 bg-red-600 text-white" id="btn_excel_pan">cetak PDF</a>
            <button class="btn btn-xs btn-round w-60 btn-secondary btn_cancel_keb" style="display:none">cancel</button>
        </div>          
        @csrf            
            <input type="text" name="id_proker"  value="{{$id_proker}}" hidden/>

        <div class="col-md-12 table-responsive-xxl">


            <table class="table table-light table-hover table-striped table-bordered" style="font-size: 12px;color: black;">
                <thead>
                    <tr>
                        <th class="text-center"></th>
                        <th class="text-center pinky1-soft">Usulan</th>
                        <th class="text-center pinky2-soft" {{$hidden}}>Realisasi</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td colspan="{{$colspan}}">Debit</td>
                    </tr>
                    <tr>
                        <td>Dana Mandiri</td>
                        <td class="pinky1-soft-1">
                            <div class="form-group mb-0 form-material" data-plugin="formMaterial">
                                <input type="text" value="{{$proker->dana_mandiri}}" style="text-align: right;" class="form-control-plaintext section_kebutuhan" disabled name="dana_mandiri" id="keb_dana_mandiri" placeholder="rencana dana dari internal organisasi" />
                            </div>                            
                        </td>
                        <td class="pinky2-soft-1" {{$hidden}}>
                            <div class="form-group mb-0 form-material" data-plugin="formMaterial">
                                <input type="text" value="{{$proker->dana_mandiri_real}}" style="text-align: right;" class="form-control-plaintext section_kebutuhan" disabled name="dana_mandiri_real" id="keb_dana_mandiri_real" placeholder="dana dari internal organisasi" />
                            </div>                            
                        </td>
                    </tr>
                    <tr>
                        <td>Dana Kampus</td>
                        <td class="pinky1-soft-2">
                            <div class="form-group mb-0 form-material" data-plugin="formMaterial">
                                <input type="text" value="{{$proker->dana_turun}}" style="text-align: right;" class="form-control-plaintext section_kebutuhan" disabled name="dana_turun" id="keb_dana_turun" placeholder="rencana dana yang diterima dari kampus" />
                            </div>                            
                        </td>
                        <td class="pinky2-soft-2" {{$hidden}}>
                            <div class="form-group mb-0 form-material" data-plugin="formMaterial">
                                <input type="text" value="{{$proker->dana_turun_real}}" style="text-align: right;" class="form-control-plaintext section_kebutuhan" disabled name="dana_turun_real" id="keb_dana_turun_real" placeholder="dana yang diterima dari kampus" />
                            </div>                            
                        </td>
                    </tr>
                    <tr>
                        <td class="abu1">TOTAL</td>
                        <td class="abu1" class="pinky1-soft-1">
                            <div class="form-group mb-0 form-material" data-plugin="formMaterial">
                                <input type="text" readonly style="text-align: right;" class="form-control-plaintext section_kebutuhan" disabled name="total_debit" id="keb_total_debit" placeholder="todal rencana dana debit" />
                            </div>                            
                        </td>
                        <td class="abu1" class="pinky2-soft-1" {{$hidden}}>
                            <div class="form-group mb-0 form-material" data-plugin="formMaterial">
                                <input type="text" readonly style="text-align: right;" class="form-control-plaintext section_kebutuhan" disabled name="total_debit_real" id="keb_total_debit_real" placeholder="todal dana debit" />
                            </div>                            
                        </td>
                    </tr>
                    <tr>
                        <td colspan="{{$colspan}}">Kredit</td>
                    </tr>    
                    <tr>
                        <td class="abu1">TOTAL</td>
                        <td class="abu1">
                            <div class="form-group mb-0 form-material" data-plugin="formMaterial">
                                <input type="text" readonly value="{{$proker->dana_usulan}}" style="text-align: right;" class="form-control-plaintext section_kebutuhan" disabled name="dana_usulan" id="keb_dana_usulan" placeholder="dana yang diterima dari pengajuan proposal" />
                            </div>                            
                        </td>
                        <td class="abu1" {{$hidden}}>
                            <div class="form-group mb-0 form-material" data-plugin="formMaterial">
                                <input type="text" readonly value="{{$proker->dana_terpakai}}" style="text-align: right;" class="form-control-plaintext section_kebutuhan" disabled name="dana_terpakai" id="keb_dana_terpakai" placeholder="dana yang diterima dari pengajuan proposal" />
                            </div>                            
                        </td>
                    </tr>
                    <tr>
                        <td class="abu2">Selisih</td>
                        <td class="abu2">
                            <div class="form-group mb-0 form-material" data-plugin="formMaterial">
                                <input type="text"  style="text-align: right;" class="form-control-plaintext section_kebutuhan" disabled name="dana_selisih" id="keb_dana_selisih" placeholder="dana yang diterima dari pengajuan proposal" />
                            </div>                            
                        </td>
                        <td class="abu2" {{$hidden}}>
                            <div class="form-group mb-0 form-material" data-plugin="formMaterial">
                                <input type="text" readonly style="text-align: right;" class="form-control-plaintext section_kebutuhan" disabled name="dana_selisih_real" id="keb_dana_selisih_real" placeholder="dana yang diterima dari pengajuan proposal" />
                            </div>                            
                        </td>
                    </tr>                                                        
                </tbody>
            </table>   
        </div>            

        <div class="col-md-6">
            <h4>Tabel Kebutuhan</h4>
        </div>  
        <div class="col-md-6 text-right">
            <button class="btn btn-xs btn-round w-100 btn-info addForm" style="display:none">+ tambah data</button>
        </div>                   
        <div class="col-md-12 table-responsive-xxl">

            <table class="table table-light table-striped table-bordered" style="font-size: 11px;color: black">
                <thead class="abu-soft">
                    <tr>
                        <th rowspan="2" width="15" class="text-center">No</th>
                        <th rowspan="2" width="150" class="text-center">Sie</th>
                        <th rowspan="2" width="250" class="text-center">Uraian</th>
                        <th colspan="4" width="100" class="text-center pinky1-soft">Usulan</th>
                        <th colspan="4" class="text-center pinky2-soft" {{$hidden}}>Realisasi</th>
                        <th rowspan="2" width="15" class="text-center bg-red-500 text-white td_delete" style="display:none" ><i class="icon fa-trash" aria-hidden="true"></i></th>
                    </tr>
                    <tr>
                        <th width="100" class="text-center">Harga satuan</th>
                        <th width="50" class="text-center">Satuan</th>
                        <th width="50" class="text-center">Jumlah</th>
                        <th width="120" class="text-center">Total</th>
                        <th width="100" class="text-center" {{$hidden}}>Harga satuan</th>
                        <th width="50" class="text-center" {{$hidden}}>Satuan</th>
                        <th width="50" class="text-center" {{$hidden}}>Jumlah</th>
                        <th width="120" class="text-center" {{$hidden}}>Total</th>
                    </tr>

                </thead>
                <tbody class="kb_grub">
                    <tr class="kebutuhan_field kebutuhan_field0 baris" data-id="0">
                        <td><div class="form-group form-material mb-0" data-plugin="formMaterial"><input type="text" class="form-control-plaintext section_kebutuhan form-control-sm no" name="no[]" value="1"/></div></td><td><div class="form-group form-material mb-0" data-plugin="formMaterial">
                        <select class="form-control-plaintext section_kebutuhan form-control-sm sie_panitia sie_panitia0" disabled name="sie_panitia[]"></select></div>
                        <div class="invalid-feedback">*wajib diisi</div> 
                        </td><td><div class="form-group form-material mb-0" data-plugin="formMaterial">
                        <input hidden type="text" class="form-control-plaintext section_kebutuhan form-control-sm"  name="id[]" />
                        <select disabled class="form-control section_kebutuhan kabel_id" id="kabel_id0" name="kabel_id[]" data-id="0"></select>
                        <input hidden type="text" class="form-control-plaintext section_kebutuhan form-control-sm uraian" data-id="0" id="uraian0" name="uraian[]" placeholder="uraian kebutuhan"/></div>
                        <div class="invalid-feedback">*wajib diisi</div> 
                        </td><td><div class="form-group form-material mb-0" data-plugin="formMaterial">
                        <input disabled type="text" class="form-control-plaintext section_kebutuhan form-control-sm harga_satuan_p" data-id="0" id="harga_satuan_p0"  name="harga_satuan_p[]" placeholder="Rp."/></div>
                        <div class="invalid-feedback">*wajib diisi</div> 
                        </td><td><div class="form-group form-material mb-0" data-plugin="formMaterial">
                        <input disabled type="text" class="form-control-plaintext section_kebutuhan form-control-sm satuan_p" id="satuan_p0" name="satuan_p[]" placeholder="nama satuan"/></div>
                        <div class="invalid-feedback">*wajib diisi</div> 
                        </td><td><div class="form-group form-material mb-0" data-plugin="formMaterial">
                        <input disabled type="text" class="form-control-plaintext section_kebutuhan form-control-sm jumlah_p" data-id="0" id="jumlah_p0" name="jumlah_p[]" placeholder="banyak item"/></div>
                        <div class="invalid-feedback">*wajib diisi</div> 
                        </td><td><div class="form-group form-material mb-0" data-plugin="formMaterial">
                        <input disabled type="text" class="form-control-plaintext section_kebutuhan form-control-sm total_p" readonly name="total_p[]" id="total_p0" placeholder="Rp. total"/></div>
                        <div class="invalid-feedback">*wajib diisi</div> 
                        </td><td {{$hidden}}><div class="form-group form-material mb-0" data-plugin="formMaterial">
                        <input disabled type="text" class="form-control-plaintext section_kebutuhan form-control-sm harga_satuan_r"  data-id="0" id="harga_satuan_r0"  name="harga_satuan_r[]" placeholder="Rp."/></div>
                        <div class="invalid-feedback">*wajib diisi</div> 
                        </td><td {{$hidden}}><div class="form-group form-material mb-0" data-plugin="formMaterial">
                        <input disabled type="text" class="form-control-plaintext section_kebutuhan form-control-sm satuan_r" id="satuan_r0" name="satuan_r[]" placeholder="nama satuan"/></div>
                        <div class="invalid-feedback">*wajib diisi</div> 
                        </td><td {{$hidden}}><div class="form-group form-material mb-0" data-plugin="formMaterial">
                        <input disabled type="text" class="form-control-plaintext section_kebutuhan form-control-sm jumlah_r" data-id="0" id="jumlah_r0" name="jumlah_r[]" placeholder="banyak item"/></div>
                        <div class="invalid-feedback">*wajib diisi</div> 
                        </td><td {{$hidden}}><div class="form-group form-material mb-0" data-plugin="formMaterial">
                        <input disabled type="text" class="form-control-plaintext section_kebutuhan form-control-sm total_r" readonly name="total_r[]" id="total_r0" placeholder="Rp. total"/></div>
                        <div class="invalid-feedback">*wajib diisi</div> 
                        </td><td class="text-center pt-15 td_delete" style="display:none"><button type="button" class="btn  btn-danger btn-xs rmForm" data-kebutuhan_id="" data-id="0">x</button></td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="2" class="abu-soft">Perbandingan</td>
                        <td colspan="4" class="text-right pinky1-soft">Total Usulan</td>
                        <td class="abu-soft">
                            <div id="show_total_p"></div>
                                <input type="text" hidden class="form-control-plaintext section_kebutuhan" value="0" name="total_akhir_p" id="total_akhir_p">
                        </td>
                        <td colspan="{{$colspan}}" class="text-right pinky2-soft" {{$hidden}}>Total Realisasi</td>
                        <td class="abu-soft" {{$hidden}}>
                            <div id="show_total_r"></div>
                                <input type="text" hidden class="form-control-plaintext section_kebutuhan" value="0" name="total_akhir_r" id="total_akhir_r">
                        </td>
                        <td class="text-center bg-red-500 text-white td_delete" style="display:none" ></td>
                    </tr>
                </tfoot>
            </table>  
        </div>  
        
        <div class="col-md-12 text-right">
            <button class="btn btn-xs btn-round w-60 btn-success" id="btnSave_keb" style="display:none">simpan</button>
            <button class="btn btn-xs btn-round w-60 btn-secondary btn_cancel_keb" style="display:none">cancel</button>
        </div>  
    </div>
</form>
</div>

@push('scripts')
<script id="script_kebutuhan">
        // variabel penting
        var count = 1;
        var nameSelect = '';
        var subBiaya = 0;
        var total = 0;
        var idtag = '';
        var cek = 1;
        var table = '';
        var no = 1;
        var checker_keb = true;

        function formatRupiah(angka, prefix){
            var number_string = angka.replace(/[^,\d]/g, '').toString(),
            split           = number_string.split(','),
            sisa            = split[0].length % 3,
            rupiah          = split[0].substr(0, sisa),
            ribuan          = split[0].substr(sisa).match(/\d{3}/gi);

                // tambahkan titik jika yang di input sudah menjadi angka ribuan
                if(ribuan){
                    separator = sisa ? '.' : '';
                    rupiah += separator + ribuan.join('.');
                }

                rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
                return prefix == undefined ? rupiah : (rupiah ? 'Rp.' + rupiah : '');
        }

        function rupiah2number(uang) {
            return parseInt((((uang.replace('.','')).replace('.','')).replace('.','')).replace('Rp',''));            
            
        }

        function sum_total() {
            total_p = 0;
            total_r = 0;
            subBiaya_p = 0;
            subBiaya_r = 0;
            hitung = 0;
            $('.uraian').each(function(){
                var jumlah_p = $('#jumlah_p'+$(this).data('id')).val();
                var jumlah_r = $('#jumlah_r'+$(this).data('id')).val();
                
                var harga_satuan_p = $('#harga_satuan_p'+$(this).data('id')).val();
                var harga_satuan_r = $('#harga_satuan_r'+$(this).data('id')).val();

                // console.log(kali+' '+$(this).val());
                subBiaya_p = jumlah_p * rupiah2number(harga_satuan_p);
                subBiaya_r = jumlah_r * rupiah2number(harga_satuan_r);
                if(isNaN(subBiaya_p)) {
                    subBiaya_p = 0;
                }    
                if(isNaN(subBiaya_r)) {
                    subBiaya_r = 0;
                }    
                $('#total_p'+$(this).data('id')).val('Rp.'+subBiaya_p.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
                $('#total_r'+$(this).data('id')).val('Rp.'+subBiaya_r.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));

                total_p += subBiaya_p;
                total_r += subBiaya_r;
                // hitung = hitung + 1;
                // console.log(total_p);
                // console.log(total_r);
            });

            if(isNaN(total_p)) {
                total_p = 0;
            }    
            if(isNaN(total_r)) {
                total_r = 0;
            } 

            $("input[name='total_akhir_p']").val(total_p);           
            $("input[name='total_akhir_r']").val(total_r);           
            $('#keb_dana_usulan').val('Rp.'+total_p.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));            
            $('#keb_dana_terpakai').val('Rp.'+total_r.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));             
            $("#show_total_p").text('Rp.'+total_p.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
            $("#show_total_r").text('Rp.'+total_r.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
        }

        function select2Pagu (id_tag) { 
            $(id_tag).select2({
                minimumInputLength: 0,
                // language: {
                //     inputTooShort: function() {
                //         return 'Masukkan minimal 2 digit angka';
                //     }
                // },         
                allowClear : true,
                searchInputPlaceholder: 'Search...',
                placeholder: 'pilih Kategori Anggaran',
                ajax: {
                    cache: true,
                    url: "{{ route('select2Pagu') }}",
                    dataType: 'json',
                    type: "GET",
                    quietMillis: 50,
                    data: function (params) {
                        // console.log(params)
                        return {
                        search: $.trim(params.term)
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: $.map(data, function (item) {
                            return {
                                text:item.nama,
                                id: item.id,
                                satuan:item.satuan
                                }
                            })
                        };
                    },             
                }
            });     
        };  

        function tambah_form(params) {
            // console.log(count);

            idtag = 'kebutuhan_field'+count;
            var form =  '<tr class="kebutuhan_field '+idtag+' baris" data-id="'+count+'"><td><div class="form-group form-material mb-0" data-plugin="formMaterial"><input type="text" class="form-control-plaintext section_kebutuhan form-control-sm no" name="no[]" value="'+ no++ +'"/></div></td><td><div class="form-group form-material mb-0" data-plugin="formMaterial">';
                form += '<select class="form-control section_kebutuhan form-control-sm sie_panitia sie_panitia'+count+'"  name="sie_panitia[]"></select>';
                form += '<div class="invalid-feedback">*wajib diisi</div> </div></td><td><div class="form-group form-material mb-0" data-plugin="formMaterial">';
                form += '<input hidden type="text" class="form-control section_kebutuhan form-control-sm"  name="id[]"/>';
                form += '<select class="form-control section_kebutuhan kabel_id" id="kabel_id'+count+'" name="kabel_id[]" data-id="'+count+'"></select>';
                form += '<input hidden type="text" class="form-control section_kebutuhan form-control-sm uraian" data-id="'+count+'" id="uraian'+count+'" name="uraian[]" placeholder="uraian kebutuhan"/>';
                form += '<div class="invalid-feedback">*wajib diisi</div> </div> </td><td><div class="form-group form-material mb-0" data-plugin="formMaterial">';
                form += '<input type="text" class="form-control section_kebutuhan form-control-sm harga_satuan_p" data-id="'+count+'" id="harga_satuan_p'+count+'" name="harga_satuan_p[]" placeholder="Rp."/>';
                form += '<div class="invalid-feedback">*wajib diisi</div> </div> </td><td><div class="form-group form-material mb-0" data-plugin="formMaterial">';
                form += '<input type="text" class="form-control section_kebutuhan form-control-sm satuan_p" id="satuan_p'+count+'" name="satuan_p[]" placeholder="nama satuan"/>';
                form += '<div class="invalid-feedback">*wajib diisi</div> </div> </td><td><div class="form-group form-material mb-0" data-plugin="formMaterial">';
                form += '<input type="text" class="form-control section_kebutuhan form-control-sm jumlah_p" data-id="'+count+'" id="jumlah_p'+count+'" name="jumlah_p[]" placeholder="banyak item"/>';
                form += '<div class="invalid-feedback">*wajib diisi</div> </div> </td><td><div class="form-group form-material mb-0" data-plugin="formMaterial">';
                form += '<input type="text" class="form-control section_kebutuhan form-control-sm total_p" readonly name="total_p[]" id="total_p'+count+'" placeholder="Rp. total"/>';
                form += '<div class="invalid-feedback">*wajib diisi</div> </div> </td><td {{$hidden}}><div class="form-group form-material mb-0" data-plugin="formMaterial">';
                form += '<input type="text" class="form-control section_kebutuhan form-control-sm harga_satuan_r"  data-id="'+count+'" id="harga_satuan_r'+count+'"  name="harga_satuan_r[]" placeholder="Rp."/>';
                form += '<div class="invalid-feedback">*wajib diisi</div> </div> </td><td {{$hidden}}><div class="form-group form-material mb-0" data-plugin="formMaterial">';
                form += '<input type="text" class="form-control section_kebutuhan form-control-sm satuan_r" id="satuan_r'+count+'" name="satuan_r[]" placeholder="nama satuan"/>';
                form += '<div class="invalid-feedback">*wajib diisi</div> </div> </td><td {{$hidden}}><div class="form-group form-material mb-0" data-plugin="formMaterial">';
                form += '<input type="text" class="form-control section_kebutuhan form-control-sm jumlah_r" data-id="'+count+'" id="jumlah_r'+count+'" name="jumlah_r[]" placeholder="banyak item"/>';
                form += '<div class="invalid-feedback">*wajib diisi</div> </div> </td><td {{$hidden}}><div class="form-group form-material mb-0" data-plugin="formMaterial">';
                form += '<input type="text" class="form-control section_kebutuhan form-control-sm total_r" readonly name="total_r[]" id="total_r'+count+'" placeholder="Rp. total"/>';
                form += '<div class="invalid-feedback">*wajib diisi</div> </div> </td><td class="text-center pt-15 td_delete"><button type="button" class="btn  btn-danger btn-xs rmForm" data-kebutuhan_id="" data-id="'+count+'">x</button></td></tr>';

            $('.kb_grub').append(form);
        
            $('.'+idtag).each(function () { 
                nameSelect = '.sie_panitia'+count;
                selecta(nameSelect);            
                console.log(nameSelect);
            });
            select2Pagu('#kabel_id'+count);

            count = count + 1;            
        }        

        function update_rekap() {
            var dana_mandiri_p = rupiah2number($('#keb_dana_mandiri').val());
            var dana_mandiri_r = rupiah2number($('#keb_dana_mandiri_real').val());
            var dana_kampus_p = rupiah2number($('#keb_dana_turun').val());
            var dana_kampus_r = rupiah2number($('#keb_dana_turun_real').val());
            var kredit_p = rupiah2number($('#keb_dana_usulan').val());
            var kredit_r = rupiah2number($('#keb_dana_terpakai').val());
            var debit_p = parseInt(dana_mandiri_p) + parseInt(dana_kampus_p);
            var debit_r = parseInt(dana_mandiri_r) + parseInt(dana_kampus_r);

            var selisih_p = parseInt(debit_p) - parseInt(kredit_p);
            var selisih_r = formatRupiah((parseInt(debit_r) - parseInt(kredit_r)).toString(), '');

            if ((parseInt(debit_r) - parseInt(kredit_r)).toString().substr(0, 1) == '-') {
                selisih_r = '( - ) '+selisih_r;
            }else{}

            $('#keb_total_debit').val(formatRupiah(debit_p.toString(), ''));            
            $('#keb_total_debit_real').val(formatRupiah(debit_r.toString(), ''));  
            $('#keb_dana_selisih').val(formatRupiah(selisih_p.toString(), ''));     
            // console.log(selisih_r);
            $('#keb_dana_selisih_real').val(selisih_r);              
        }

        function reload_tabel_kebutuhan() {
            var url = "{{url('DTkebutuhan/')}}/"+"{{$id_proker}}";
            no = 1;
            $.getJSON(url , function (data) {
                // console.log(data);
                $.each(data, function (i, v) { 
                    // console.log(v);
                    if ($('.kebutuhan_field0').length)
                    {
                        // console.log('enek');
                        $('.kebutuhan_field0').remove();                                              
                    }
                    // console.log(no);
                    var sub_tot_p = (v.sub_total_p != null) ? v.sub_total_p.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.") : '';
                    var sub_tot_r = (v.sub_total_r != null) ? v.sub_total_r.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.") : '';
                    var sub_total_p = 'Rp. '+sub_tot_p;
                    var sub_total_r = 'Rp. '+sub_tot_r;
                    
                    var harga_satuan = (v.harga_satuan != null) ? v.harga_satuan.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.") : '';
                    var harga_satuan_real = (v.harga_satuan_real != null) ? v.harga_satuan_real.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.") : '';
                    var harga_satuan_p = 'Rp. '+harga_satuan;
                    var harga_satuan_r = 'Rp. '+harga_satuan_real;

                    idtag = 'kebutuhan_field'+count;
                    var form =  '<tr class="kebutuhan_field '+idtag+' baris" data-id="'+count+'"><td><div class="form-group form-material mb-0" data-plugin="formMaterial"><input disabled type="text" class="form-control-plaintext section_kebutuhan form-control-sm no" name="no[]" value="'+ no++ +'"/></div></td><td><div class="form-group form-material mb-0" data-plugin="formMaterial">';
                        form += '<select disabled class="form-control-plaintext section_kebutuhan form-control-sm sie_panitia sie_panitia'+count+'"  name="sie_panitia[]">';
                        if(v.id_sie_kepanitiaan != null){
                            form += '<option value="'+v.id_sie_kepanitiaan+'" selected>'+v.nama_kepanitiaan+'</option>';
                        }                        
                        form += '</select></div><div class="invalid-feedback">*wajib diisi</div> </td><td><div class="form-group form-material mb-0" data-plugin="formMaterial">';
                        form += '<input hidden type="text" class="form-control-plaintext section_kebutuhan form-control-sm"  name="id[]" value="'+v.id+'" />';
                        form += '<select disabled class="form-control section_kebutuhan kabel_id" id="kabel_id'+count+'" name="kabel_id[]" data-id="'+count+'">';
                        if(v.id_kabel != null){
                            form += '<option value="'+v.id_kabel+'" selected>'+v.keterangan+'</option>';
                        }                        
                        form += '</select><div class="invalid-feedback">*wajib diisi</div>';                        
                        form += '<input hidden type="text" class="form-control section_kebutuhan form-control-sm uraian" value="'+v.keterangan+'" data-id="'+count+'" id="uraian'+count+'" name="uraian[]" placeholder="uraian kebutuhan"/></div>';                        
                        form += '<div class="invalid-feedback">*wajib diisi</div> </td><td><div class="form-group form-material mb-0" data-plugin="formMaterial">';
                        form += '<input disabled type="text" class="form-control-plaintext section_kebutuhan form-control-sm harga_satuan_p" data-id="'+count+'" value="'+harga_satuan_p+'" id="harga_satuan_p'+count+'"  name="harga_satuan_p[]" placeholder="Rp."/></div>';
                        form += '<div class="invalid-feedback">*wajib diisi</div> </td><td><div class="form-group form-material mb-0" data-plugin="formMaterial">';
                        form += '<input disabled type="text" class="form-control-plaintext section_kebutuhan form-control-sm satuan_p" id="satuan_p'+count+'" value="'+v.satuan+'" name="satuan_p[]" placeholder="nama satuan"/></div>';
                        form += '<div class="invalid-feedback">*wajib diisi</div> </td><td><div class="form-group form-material mb-0" data-plugin="formMaterial">';
                        form += '<input disabled type="text" class="form-control-plaintext section_kebutuhan form-control-sm jumlah_p" data-id="'+count+'" value="'+v.jumlah+'" id="jumlah_p'+count+'" name="jumlah_p[]" placeholder="banyak item"/></div>';
                        form += '<div class="invalid-feedback">*wajib diisi</div> </td><td><div class="form-group form-material mb-0" data-plugin="formMaterial">';
                        form += '<input disabled type="text" class="form-control-plaintext section_kebutuhan form-control-sm total_p" value="'+sub_total_p+'" name="total_p[]" id="total_p'+count+'" placeholder="Rp. total"/></div>';
                        form += '<div class="invalid-feedback">*wajib diisi</div> </td><td {{$hidden}}><div class="form-group form-material mb-0" data-plugin="formMaterial">';
                        form += '<input disabled type="text" class="form-control-plaintext section_kebutuhan form-control-sm harga_satuan_r"  data-id="'+count+'" value="'+harga_satuan_r+'" id="harga_satuan_r'+count+'"  name="harga_satuan_r[]" placeholder="Rp."/></div>';
                        form += '<div class="invalid-feedback">*wajib diisi</div> </td><td {{$hidden}}><div class="form-group form-material mb-0" data-plugin="formMaterial">';
                        form += '<input disabled type="text" class="form-control-plaintext section_kebutuhan form-control-sm satuan_r" id="satuan_r'+count+'" value="'+v.satuan_real+'" name="satuan_r[]" placeholder="nama satuan"/></div>';
                        form += '<div class="invalid-feedback">*wajib diisi</div> </td><td {{$hidden}}><div class="form-group form-material mb-0" data-plugin="formMaterial">';
                        form += '<input disabled type="text" class="form-control-plaintext section_kebutuhan form-control-sm jumlah_r" data-id="'+count+'" value="'+v.jumlah_real+'" id="jumlah_r'+count+'" name="jumlah_r[]" placeholder="banyak item"/></div>';
                        form += '<div class="invalid-feedback">*wajib diisi</div> </td><td {{$hidden}}><div class="form-group form-material mb-0" data-plugin="formMaterial">';
                        form += '<input disabled type="text" class="form-control-plaintext section_kebutuhan form-control-sm total_r" value="'+sub_total_r+'" name="total_r[]" id="total_r'+count+'" placeholder="Rp. total"/></div>';
                        form += '<div class="invalid-feedback">*wajib diisi</div> </td><td class="text-center pt-15 td_delete" style="display:none"><button type="button" data-kebutuhan_id="'+v.id+'" class="btn  btn-danger btn-xs rmForm" data-id="'+count+'">x</button></td></tr>';
                    // console.log(form);
                    $('.kb_grub').append(form);
                    select2Pagu('#kabel_id'+count);
                    $('.'+idtag).each(function () { 
                        nameSelect = '.sie_panitia'+count;
                        selecta(nameSelect);            
                        // console.log(nameSelect);
                    });
                    count = count + 1;                       
                });
                sum_total();                   
            });            
        }

        // -=============================================== READY ==============================================-
        $(document).ready(function () {
            $('.addForm').click(function(e) {
                e.preventDefault();                
                tambah_form();
            });            

            $(".sie_panitia0").select2({
                minimumInputLength: 0,
                // language: {
                //     inputTooShort: function() {
                //         return 'Masukkan minimal 2 digit angka';
                //     }
                // },         
                allowClear : true,
                searchInputPlaceholder: 'Search...',
                placeholder: 'pilih sie',
                ajax: {
                cache: true,
                url: "{{ url('master/kepanitiaan/select2') }}",
                dataType: 'json',
                type: "GET",
                quietMillis: 50,
                data: function (params) {
                    // console.log(params)
                    return {
                    search: $.trim(params.term)
                    };
                },
                processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                    return {
                        text:item.nama,
                        id: item.id
                        }
                    })
                    };
                }
                }
            }); 

            reload_tabel_kebutuhan()
            // sum_total();            
            update_rekap()

            var dana_mandiri_p = $('#keb_dana_mandiri').val();
            var dana_mandiri_r = $('#keb_dana_mandiri_real').val();
            var dana_kampus_p = $('#keb_dana_turun').val();
            var dana_kampus_r = $('#keb_dana_turun_real').val();
            var kredit_p = $('#keb_dana_usulan').val();
            var kredit_r = $('#keb_dana_terpakai').val();
            
            var debit_p = parseInt(dana_mandiri_p) + parseInt(dana_kampus_p);
            var debit_r = parseInt(dana_mandiri_r) + parseInt(dana_kampus_r);
            // console.log(debit_r);
            var selisih_p = parseInt(debit_p) - parseInt(kredit_p);
            var selisih_r = parseInt(debit_r) - parseInt(kredit_r);

            $('#keb_dana_mandiri').val(formatRupiah($('#keb_dana_mandiri').val(), ''));            
            $('#keb_dana_turun').val(formatRupiah(dana_kampus_p.toString(), ''));            
            // $('#keb_total_debit').val(formatRupiah(debit_p.toString(), ''));            
            $('#keb_dana_usulan').val(formatRupiah(kredit_p.toString(), ''));            
            // $('#keb_dana_selisih').val(formatRupiah(selisih_p.toString(), ''));            
            $('#keb_dana_mandiri_real').val(formatRupiah($('#keb_dana_mandiri_real').val(), ''));            
            $('#keb_dana_turun_real').val(formatRupiah(dana_kampus_r.toString(), ''));            
            // $('#keb_total_debit_real').val(formatRupiah(debit_r.toString(), ''));            
            $('#keb_dana_terpakai').val(formatRupiah(kredit_r.toString(), ''));            
            // $('#keb_dana_selisih_real').val(formatRupiah(selisih_r.toString(), ''));              
            // $('#keb_dana_mandiri_real').val('Rp.0');
            // $('#keb_dana_turun_real').val('Rp.0');
            // $('#keb_total_debit_real').val('Rp.0');
            // $('#keb_dana_terpakai').val('Rp.0');
            // $('#keb_dana_selisih_real').val('Rp.0');


        });
        // -=============================================== READY ==============================================-

        $(document).on('change', '.kabel_id', function (e) {
            // console.log(this);
            kabel_id = $(this).val();
            id = $(this).data('id');
            // console.log(id);
            var url = "{{route('getAnggaranDetail')}}?id_kabel="+kabel_id;
            $.getJSON(url , function (data) {
                // console.log(data);
                $('#uraian'+id).val(data.kategori_belanja);
                $('#satuan_p'+id).val(data.satuan);
                $('#satuan_r'+id).val(data.satuan);
            });            
        });

        $('#keb_dana_mandiri').keyup(function (e) { 
            var data = $(this).val();
            var hasil = formatRupiah(data, '');
            $(this).val(hasil);            
            update_rekap()
        });

        $('#keb_dana_mandiri_real').keyup(function (e) { 
            var data = $(this).val();
            var hasil = formatRupiah(data, '');
            $(this).val(hasil);            
            update_rekap()
        });

        $('#keb_dana_turun').keyup(function (e) { 
            var data = $(this).val();
            var hasil = formatRupiah(data, '');
            $(this).val(hasil);            
            update_rekap()
        });

        $('#keb_dana_turun_real').keyup(function (e) { 
            var data = $(this).val();
            var hasil = formatRupiah(data, '');
            $(this).val(hasil);            
            update_rekap()
        });

        $('#btn_edit_real').click(function (e) { 
            e.preventDefault();
    
            $(this).attr('style','display:none')
            $('#btn_excel_keb').attr('style','display:none')
            $('.btn_cancel_keb').removeAttr('style','display:none');
            $('#btnSave_keb').removeAttr('style','display:none');
            $('.td_delete').removeAttr('style','display:none');
            $('.section_kebutuhan').removeAttr('disabled').addClass('form-control').removeClass('form-control-plaintext');
            $('.no').removeAttr('class').attr('class',"form-control-plaintext form-control-sm no");
            $('.total_p').attr('readonly','true')
            $('.total_r').attr('readonly','true')
            $('.kabel_id').attr('disabled',true).removeClass('form-control').addClass('form-control-plaintext')
            $('.sie_panitia').attr('disabled',true).removeClass('form-control').addClass('form-control-plaintext')
            $('.harga_satuan_p').attr('disabled',true).removeClass('form-control').addClass('form-control-plaintext')
            $('.satuan_p').attr('disabled',true).removeClass('form-control').addClass('form-control-plaintext')
            $('.jumlah_p').attr('disabled',true).removeClass('form-control').addClass('form-control-plaintext')
            $('.total_p').attr('disabled',true).removeClass('form-control').addClass('form-control-plaintext')
            $('#keb_dana_mandiri').attr('disabled',true).removeClass('form-control').addClass('form-control-plaintext')
            $('#keb_dana_turun').attr('disabled',true).removeClass('form-control').addClass('form-control-plaintext')
            $('#keb_total_debit').attr('disabled',true).removeClass('form-control').addClass('form-control-plaintext')
            $('#keb_dana_usulan').attr('disabled',true).removeClass('form-control').addClass('form-control-plaintext')
            $('#keb_dana_selisih').attr('disabled',true).removeClass('form-control').addClass('form-control-plaintext')
            console.log('real');
        });

        $('#btn_edit_keb').click(function (e) { 
            e.preventDefault();

            $(this).attr('style','display:none')
            $('#btn_excel_keb').attr('style','display:none')
            $('.btn_cancel_keb').removeAttr('style','display:none');
            $('#btnSave_keb').removeAttr('style','display:none');
            $('.td_delete').removeAttr('style','display:none');
            $('.addForm').removeAttr('style','display:none');
            $('.section_kebutuhan').removeAttr('disabled').addClass('form-control').removeClass('form-control-plaintext');
            $('.no').removeAttr('class').attr('class',"form-control-plaintext form-control-sm no");
            $('.total_p').attr('readonly','true')
            $('.total_r').attr('readonly','true')
        });

        function cancel_kebutuhan() {
            $('.btn_cancel_keb').attr('style','display:none')
            $('#btn_excel_keb').removeAttr('style','display:none')
            $('#btn_edit_keb').removeAttr('style','display:none');
            $('#btn_edit_real').removeAttr('style','display:none');
            $('#btnSave_keb').attr('style','display:none');
            $('.td_delete').attr('style','display:none');
            $('.addForm').attr('style','display:none');
            $('.section_kebutuhan').addClass('form-control-plaintext').attr('disabled','true').removeClass('form-control');            
        }

        $('.btn_cancel_keb').click(function (e) { 
            e.preventDefault();
            cancel_kebutuhan();
        });

        $(document).on('click', '#btnSave_keb', function (e) {
            e.preventDefault();
            // console.log('{{$hidden}}');

            var hidden = '{{$hidden}}';

            checker_keb = true;
            $('.kebutuhan_field').each(function () {         
                var id = $(this).data('id');
                var jumlah_p = $('#jumlah_p'+id).val();
                var satuan_p = $('#satuan_p'+id).val();
                var harga_satuan_p = $('#harga_satuan_p'+id).val();     
                var jumlah_r = $('#jumlah_r'+id).val();
                var satuan_r = $('#satuan_r'+id).val();
                var harga_satuan_r = $('#harga_satuan_r'+id).val();    
                var kabel_id = $('#kabel_id'+id).val();
                var sie_panitia = $('.sie_panitia'+id).val();

                if(kabel_id == null){
                    checker_keb = false;
                    $('#kabel_id'+id).addClass('is-invalid').closest('.form-material').addClass('has-danger');   
                    console.log('#kabel_id'+id);
                    $('.kabel_id').change(function () {
                        $('#kabel_id'+id).removeClass('is-invalid').closest('.form-material').removeClass('has-danger');
                            $('#satuan_p'+id).removeClass('is-invalid').closest('.form-material').removeClass('has-danger');
                            $('#satuan_r'+id).removeClass('is-invalid').closest('.form-material').removeClass('has-danger');
                    });
                }
                if(sie_panitia == null){
                    checker_keb = false;                            
                    $('.sie_panitia'+id).addClass('is-invalid').closest('.form-material').addClass('has-danger'); 
                    $('.sie_panitia').change(function () {
                        $('.sie_panitia'+id).removeClass('is-invalid').closest('.form-material').removeClass('has-danger');
                    });                                     
                }                                               
                if (hidden == '') {
                    if(jumlah_p == ''){
                        checker_keb = false;                            
                        $('#jumlah_p'+id).addClass('is-invalid').closest('.form-material').addClass('has-danger');
                        $('.jumlah_p').change(function () {
                            $('#jumlah_p'+id).removeClass('is-invalid').closest('.form-material').removeClass('has-danger');
                        });                            
                    }
                    if(satuan_p == ''){
                        checker_keb = false;                            
                        $('#satuan_p'+id).addClass('is-invalid').closest('.form-material').addClass('has-danger');   
                        $('.satuan_p').change(function () {
                            $('#satuan_p'+id).removeClass('is-invalid').closest('.form-material').removeClass('has-danger');
                        });                                   
                    }
                    if(harga_satuan_p == ''){
                        checker_keb = false;                            
                        $('#harga_satuan_p'+id).addClass('is-invalid').closest('.form-material').addClass('has-danger');  
                        $('.harga_satuan_p').keyup(function () {
                            $('#harga_satuan_p'+id).removeClass('is-invalid').closest('.form-material').removeClass('has-danger');
                        });                                    
                    }   
                    if(jumlah_r == ''){
                        checker_keb = false;                            
                        $('#jumlah_r'+id).addClass('is-invalid').closest('.form-material').addClass('has-danger');
                        $('.jumlah_r').change(function () {
                            $('#jumlah_r'+id).removeClass('is-invalid').closest('.form-material').removeClass('has-danger');
                        });                            
                    }
                    if(satuan_r == ''){
                        checker_keb = false;                            
                        $('#satuan_r'+id).addClass('is-invalid').closest('.form-material').addClass('has-danger');   
                        $('.satuan_r').change(function () {
                            $('#satuan_r'+id).removeClass('is-invalid').closest('.form-material').removeClass('has-danger');
                        });                                   
                    }
                    if(harga_satuan_r == ''){
                        checker_keb = false;                            
                        $('#harga_satuan_r'+id).addClass('is-invalid').closest('.form-material').addClass('has-danger');  
                        $('.harga_satuan_r').keyup(function () {
                            $('#harga_satuan_r'+id).removeClass('is-invalid').closest('.form-material').removeClass('has-danger');
                        });                                    
                    }                    
                }else{
                    if(jumlah_p == ''){
                        checker_keb = false;                            
                        $('#jumlah_p'+id).addClass('is-invalid').closest('.form-material').addClass('has-danger');
                        $('.jumlah_p').change(function () {
                            $('#jumlah_p'+id).removeClass('is-invalid').closest('.form-material').removeClass('has-danger');
                        });                            
                    }
                    if(satuan_p == ''){
                        checker_keb = false;                            
                        $('#satuan_p'+id).addClass('is-invalid').closest('.form-material').addClass('has-danger');   
                        $('.satuan_p').change(function () {
                            $('#satuan_p'+id).removeClass('is-invalid').closest('.form-material').removeClass('has-danger');
                        });                                   
                    }
                    if(harga_satuan_p == ''){
                        checker_keb = false;                            
                        $('#harga_satuan_p'+id).addClass('is-invalid').closest('.form-material').addClass('has-danger');  
                        $('.harga_satuan_p').keyup(function () {
                            $('#harga_satuan_p'+id).removeClass('is-invalid').closest('.form-material').removeClass('has-danger');
                        });                                    
                    }   
                }


                                
                // console.log(id+';'+kebutuhan+';'+jumlah+';'+satuan+';'+harga_satuan+';'+sie_panitia);
            });
            console.log(checker_keb);

            if (checker_keb == true) {
                $.ajax({
                    url: "{{route('simpan_kebutuhan')}}",
                    type: "POST",
                    dataType: 'json',
                    data: $('#form_kebutuhan').serialize(),                
                    success: function (data) {
                        console.log(data);
                        cancel_kebutuhan()
                        $('.kb_grub').html('');                    
                        reload_tabel_kebutuhan()                    
                            swal({
                                title: "Sukses!",
                                text: "Perubahan berhasil disimpan",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonClass: "btn-success",
                                confirmButtonText: 'OK',
                                closeOnConfirm: false
                            });                     
                        // toastr.success(data.message);
                        },
                    error: function (data) {
                        console.log('Error:', data);
                        // toastr.success(data.message);
                        //$('#modalOrg').modal('show');
                        }
                });
            }
    
        });

        $(document).on('keyup', '.harga_satuan_p', function (e) {
            var data = $(this).val();
            var hasil = formatRupiah(data, '');
            $(this).val(hasil);
            sum_total();
            update_rekap();
        });

        $(document).on('keyup', '.harga_satuan_r', function (e) {
            var data = $(this).val();
            var hasil = formatRupiah(data, '');
            $(this).val(hasil);
            sum_total();
            update_rekap();
        });

        $(document).on('keyup', '.jumlah_p', function (e) {
            sum_total();
            update_rekap();            
        });

        $(document).on('keyup', '.jumlah_r', function (e) {
            sum_total();
            update_rekap();            
        });

        $(document).on('click', '.rmForm', function () {
            var id = $(this).data('id');
            var kebutuhan_id = $(this).data('kebutuhan_id');
            $(this).closest('.kebutuhan_field'+id).remove();
            $('#form_kebutuhan').append('<input type="text" name="deleted_id[]" value="'+kebutuhan_id+'" hidden class="deleted_id">');
            sum_total();
        });            
</script>
@endpush