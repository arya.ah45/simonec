<div class="tab-pane animation-slide-right container-fluid" id="rapat" role="tabpanel">
    <div class="row py-20">           
        <div class="col-md-6">
            <h4>Tabel Rapat</h4>
        </div>  
        <div class="col-md-6 text-right">
            {{-- <button class="btn btn-xs btn-round w-100 bg-teal-600 text-white" id="btn_excel_pan">cetak excel</button> --}}
            {!!$btn_add_rapat!!}
        </div>  
        <div class="col-md-12 table-responsive-xxl">
            <table class="table table-light table-striped table-bordered" style="font-size: 12px;color: black" id="tb_rapat">
                <thead class="abu-soft">
                    <tr>
                        <th class="text-center">No</th>
                        <th class="text-center">Jam / Tanggal</th>
                        <th class="text-center">Pokok bahasan</th>
                        <th class="text-center">Moderator</th>
                        <th class="text-center">Jumlah Peserta</th>
                        <th class="text-center">Jenis Rapat</th>
                        <th class="text-center">Absensi</th>
                        <th class="text-center">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>  
        </div>   
    </div>
</div>


<!-- Modal -->
<div class="modal fade modal-slide-from-bottom p-0" id="modalRapat" aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-simple modal-center">
        <div class="modal-content">

            <div class="modal-header">
                <div class="ribbon ribbon-clip px-10 w-300 h-50" style="z-index:100">
                    <span class="ribbon-inner">
                        <h4 id="judulModal" class="modal-title text-white"></h4>
                    </span>
                </div>                   
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                
            </div>

            <div class="modal-body pt-20 pl-20" data-keyboard="false" data-backdrop="static" style="height:500px;overflow-y: auto;">
                <form id="formRapat" name="formRapat">
                    @csrf
                    <input hidden name="id_rapat" id="id_rapat">       
                    <input hidden name="id_proker" id="id_proker" value="{{$id_proker}}">       

                    <div class="row pt-10 px-20">
                        
                        <div class="col-md-12">
                            <div class="form-group form-material" data-plugin="formMaterial">
                                <label class="form-control-label" for="pokok_bahasan">Pokok Bahasan</label>
                                <input type="text" class="form-control" id="pokok_bahasan" name="pokok_bahasan" placeholder="pokok bahasan rapat"/>
                                <div class="invalid-feedback">*wajib diisi</div>                                        
                            </div>
                        </div>        
                        
                        <div class="col-md-12">
                            <div class="form-group form-material" data-plugin="formMaterial">
                                <label class="form-control-label" for="moderator">Pemimpin Rapat</label>
                                <select type="text" class="form-control" id="moderator" name="moderator"></select>
                                <div class="invalid-feedback">*wajib diisi</div>  
                            </div>
                        </div>
                        
                        <div class="col-md-12">
                            <div class="form-group form-material" data-plugin="formMaterial">
                                <label class="form-control-label" for="tipe">Tipe Rapat</label>
                                <select class="form-control" id="tipe" name="tipe" placeholder="pokok bahasan rapat">
                                    <option value="" selected disabled>pilih Tipe </option>
                                    <option value="0">ONLINE</option>
                                    <option value="1">OFFLINE</option>
                                </select>
                                <div class="invalid-feedback">*wajib diisi</div>  
                            </div>
                        </div>
                        
                        <div class="col-md-12">
                            <div class="form-group form-material" data-plugin="formMaterial">
                                <label class="form-control-label" for="tempat">Tempat Pelaksanaan</label>
                                <input type="text" class="form-control" id="tempat" name="tempat" placeholder="Alamat / nama tempat"/>
                                <div class="invalid-feedback">*wajib diisi</div>  
                            </div>
                        </div>
                        
                        <div class="col-md-6">
                            <div class="form-group form-material" data-plugin="formMaterial">
                                <label class="form-control-label" for="tanggal_rapat">Tanggal Rapat</label>
                                <input type="date" class="form-control" id="tanggal_rapat" name="tanggal_rapat" placeholder="pokok bahasan rapat"/>
                                <div class="invalid-feedback">*wajib diisi</div>  
                            </div>                                
                        </div>    
                        <div class="col-md-6">
                            <div class="form-group form-material" data-plugin="formMaterial">
                                <label class="form-control-label" for="jam">Jam Pelaksanaan</label>
                                <input type="time" class="form-control" id="jam" name="jam" placeholder="pokok bahasan rapat"/>
                                <div class="invalid-feedback">*wajib diisi</div>  
                            </div>
                        </div>       
                        
                        <div class="col-md-12">
                            <div class="form-group " data-plugin="formMaterial">
                                <label class="form-control-label" for="hasil_rapat">Hasil Rapat</label>
                                <textarea class="form-control" id="hasil_rapat" name="hasil_rapat" placeholder="Hasil notulensi"></textarea>
                            </div>
                        </div>   
                        
                        <div class="col-md-12">
                            <div class="form-group " data-plugin="formMaterial">
                                <label class="form-control-label" for="catatan">Catatan</label>
                                <textarea class="form-control" id="catatan" name="catatan" placeholder="catatan-catatan"></textarea>
                            </div>
                        </div>  

                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-outline btn-outline-secondary" data-dismiss="modal" id="tutup">Tutup</button>
                <button type="button" id="btnSave" class="btn bg-purple-400 text-white">Simpan</button>
            </div>
        </div>
    </div>

</div>

<!-- Modal -->
<div class="modal fade modal-slide-from-bottom p-0" id="modalAbsensi0" aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-simple modal-center">
        <div class="modal-content">

            <div class="modal-header">
                <div class="ribbon ribbon-clip px-10 w-300 h-50" style="z-index:9000">
                    <span class="ribbon-inner">
                        <h4 id="judulModal" class="modal-title text-white">Presensi Rapat Online</h4>
                    </span>
                </div>                   
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                
            </div>

            <div class="modal-body pt-20 pl-20" data-keyboard="false" data-backdrop="static" style="height:500px;overflow-y: auto;">
                <form id="formAbon" name="formAbon">
                    @csrf
                    <input hidden name="id_rapat" class="id_rapat_abon">       
                    <input hidden name="tipe_absensi" value="0">       
                    <input hidden name="id_proker" class="id_proker_abon" value="{{$id_proker}}">       

                    <table class="table table-bordered">
                        <thead class="thead-light">
                            <tr>
                                <th width="10%">#</th>
                                <th width="85%" id="absen_header_nim">NIM / NAMA</th>
                                <th width="5%">X</th>
                            </tr>
                        </thead>
                        <tbody id="abon_row">
                            <tr class="absen_row" id="absen_row0" data-id="0"><td><div class="form-group form-material mb-0" data-plugin="formMaterial">
                            <input type="text" class="form-control-plaintext form-control-sm abon_no" id="abon_no0" disabled name="abon_no[]" value="1"/></td>
                            </div><td><div class="form-group form-material mb-0" data-plugin="formMaterial">
                            <select type="text" class="form-control" id="abon_nim0" name="nim[]"></select><div class="invalid-feedback">*wajib diisi</div>
                            </div></td><td class="pt-15"><button type="button" class="btn  btn-danger btn-xs rmForm_absen_on" data-kebutuhan_id="" data-id="0">x</button></td></tr>
                        </tbody>
                    </table>
                    <button class="btn btn-xs btn-round w-100 btn-info addForm_absen_on">+ tambah data</button>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-outline btn-outline-secondary" data-dismiss="modal" id="tutup">Tutup</button>
                <button type="button" id="btnSave_abon" class="btn bg-purple-400 text-white">Simpan</button>
            </div>
        </div>
    </div>

</div>

<!-- Modal -->
<div class="modal fade modal-slide-from-bottom p-0" id="modalAbsensi1" aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-simple modal-center">
        <div class="modal-content">

            <div class="modal-header">
                <div class="ribbon ribbon-clip px-10 w-300 h-50" style="z-index:9000">
                    <span class="ribbon-inner">
                        <h4 id="judulModal" class="modal-title text-white">Presensi Rapat Offline</h4>
                    </span>
                </div>                   
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                
            </div>

            <div class="modal-body pt-20 pl-20" data-keyboard="false" data-backdrop="static">
                <form id="formAboff" name="formAboff">
                    @csrf
                    <input hidden name="id_rapat" class="id_rapat_aboff"> 
                    <input hidden name="tipe_absensi" value="1">       
                    <input hidden name="id_proker" class="id_proker_aboff" value="{{$id_proker}}">       
                    <div class="row">
                        <div class="col-md-12">
                            <video id="preview3"></video>
                        </div>                        
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" id="ins_manual" class="btn btn-xs btn-primary">Insert manual ?</button>
                        </div>                        
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group form-material mb-0 pt-15" id="manual_ins" style="display:none" data-plugin="formMaterial">
                                <label class="form-control-label" for="nim_absen">NIM</label>
                                <input maxlength="10" type="text" class="form-control form-control-sm nim_absen" name="nim_absen" placeholder="isi NIM secara manual disini jika dibutuhkan"/>
                                <input type="text" hidden class="form-control form-control-sm nim_absen_h" name="nim"/>
                            </div>
                        </div>                        
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-outline btn-outline-secondary" data-dismiss="modal" id="tutup">Tutup</button>
                <button type="button" id="btnSave_aboff" class="btn bg-purple-400 text-white">Simpan</button>
            </div>
        </div>
    </div>

</div>

<!-- Modal -->
<div class="modal fade modal-slide-from-bottom p-0" id="modalAbsensi" aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-simple modal-center">
        <div class="modal-content">

            <div class="modal-header">
                <div class="ribbon ribbon-clip px-10 w-300 h-50" style="z-index:9000">
                    <span class="ribbon-inner">
                        <h4 id="judulModal" class="modal-title text-white">Presensi Rapat Offline</h4>
                    </span>
                </div>                   
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                
            </div>

            <div class="modal-body pt-20 pl-20" data-keyboard="false" data-backdrop="static">
                <form id="formAbsen" name="formAbsen">
                    @csrf
                    <input hidden name="id_rapat" class="id_rapat_aboff"> 
                    <input hidden name="tipe_absensi" class="tipe_absensi">       
                    <input hidden name="id_proker" class="id_proker_aboff" value="{{$id_proker}}">     
                    
                    <table class="table table-bordered">
                        <thead class="thead-light">
                            <tr>
                                <th width="10%">#</th>
                                <th width="85%" id="absen_header_nim">NIM / NAMA</th>
                                <th width="5%" class="absen_del" style="display: none">X</th>
                            </tr>
                        </thead>
                        <tbody id="absensi_row">
                            
                        </tbody>
                    </table>
                    <input hidden type="text" name="deleted_id" id="absen_deleted_id">
                </form>
            </div>
            <div class="modal-footer" style="display: block">
                <div class="row">
                    <div class="col-md-6 text-left">
                        <button type="button" class="btn btn-warning" id="edit_absen">Edit</button>
                    </div>
                    <div class="col-md-6 text-right">
                        <button type="button" class="btn btn-outline btn-outline-secondary" data-dismiss="modal" id="tutup">Tutup</button>
                        <button type="button" id="btnSave_absen" class="btn bg-purple-400 text-white absen_del" style="display: none">Simpan</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

@push('scripts')
<script id="script_rapat">
        // variabel penting
        var count_absen_on = 1;
        var no_abon = 2;
        var nameSelect = '';
        var nameSelect2 = '';
        var idtag_pan = '';
        var id_deleted = '';
        var table = '';
        // Restricts input for the set of matched elements to the given inputFilter function.
        (function($) {
        $.fn.inputFilter = function(inputFilter) {
            return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
            if (inputFilter(this.value)) {
                this.oldValue = this.value;
                this.oldSelectionStart = this.selectionStart;
                this.oldSelectionEnd = this.selectionEnd;
            } else if (this.hasOwnProperty("oldValue")) {
                this.value = this.oldValue;
                this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
            } else {
                this.value = "";
            }
            });
        };
        }(jQuery));

        // -=============================================== READY ==============================================-
        $(document).ready(function () {

            $('.nim_absen').inputFilter(function(value) {
                return /^\d*$/.test(value);    // Allow digits only, using a RegExp
            });

            selecta_nim_absen('#abon_nim0')
            table = $('#tb_rapat').DataTable({
                processing: true,
                serverSide: true,
                "scrollY": "250px",
                "scrollx": "1050px",
                "scrollCollapse": true,
                ajax: "{{ route('DTrapat') }}?id_proker="+"{{$id_proker}}",
                language: {
                    processing: "Sedang diproses...<img height=50 src='{{asset('assets/images/load3.gif')}}'>"
                },
                columns: [
                    {
                        data: 'no',
                        name: 'no',
                        searchable : false,
                        orderable:true
                    }, {
                        data: 'jam_tanggal',
                        name: 'jam_tanggal',
                        orderable:true
                    },{
                        data: 'pokok_bahasan',
                        name: 'pokok_bahasan',
                        orderable: false,
                        searchable: false
                    },{
                        data: 'nama_mahasiswa',
                        name: 'moderator',
                        orderable: false,
                        searchable: false
                    },{
                        data: 'jumlah_peserta',
                        name: 'jumlah_peserta',
                        orderable: false,
                        searchable: false
                    },{
                        data: 'tipe',
                        name: 'tipe',
                        orderable: false,
                        searchable: false
                    },{
                        data: 'absensi',
                        name: 'absensi',
                        orderable: false,
                        searchable: false
                    },{
                        data: 'aksi',
                        name: 'aksi',
                        orderable: false,
                        searchable: false
                    },
                ],
                columnDefs: [
                    { className: 'text-center', targets: [0,1,2,3,4,5,6,7] },
                ]
            });  

            $(document).on('click', '.tap_rapat', function () {
                console.log('ok');
                table.ajax.reload();
            });
            
            $(document).on('click', '.showModalRapat', function () {
                selecta_nim('#moderator');
                $('#tipe').select2();
                $('#judulModal').html('Tambah data rapat');
                $('#id').val('');

                    $('#modalRapat').modal({
                        backdrop: 'static',
                        keyboard: false,
                        show: true
                    })
            });    

            function vld_text(id_input,col){
                if ($(id_input).val()=='') {
                    $(id_input).addClass('is-invalid').closest('.col-md-'+col).find('.form-control-label').addClass('text-danger');
                    $(id_input).focus(function () {
                        $(this).removeClass('is-invalid')
                    });
                    $(id_input).keyup(function () {
                        $(this).closest('.col-md-'+col).find('.form-control-label').removeClass('text-danger');
                    });
                }
            }

            function vld_select(id_input,col){
                if ($(id_input).val()==null) {
                    $(id_input).addClass('is-invalid').closest('.col-md-'+col).find('.form-control-label').addClass('text-danger');
                    $(id_input).focus(function () {
                        $(this).removeClass('is-invalid')
                    });
                    $(id_input).change(function () {
                        $(this).closest('.col-md-'+col).find('.form-control-label').removeClass('text-danger');
                    });
                }
            }

            $('#btnSave').click(function (e) {
                e.preventDefault();


                vld_text('#pokok_bahasan',12);
                vld_select('#moderator',12);
                vld_select('#tipe',12);
                vld_text('#tempat',12);
                vld_text('#tanggal_rapat',6);
                vld_text('#jam',6);

                if(
                    $('#pokok_bahasan').val()!='' &&
                    $('#moderator').val()!=null &&                    
                    $('#tipe').val()!=null &&                    
                    $('#tempat').val()!='' &&                    
                    $('#tanggal_rapat').val()!='' &&                    
                    $('#jam').val()!=''                  
                ){
                    $(this).html('Sending..');
                    $(this).attr('disabled',true);
                    $.ajax({
                        data: $('#formRapat').serialize(),
                        url: "{{route('simpan_rapat')}}",
                        type: "POST",
                        dataType: 'json',
                        success: function (data) {
                            console.log(data);
                            $('#formRapat').trigger('reset');

                                if(data == 1){
                                    swal({
                                        title: "Sukses!",
                                        text: "data rapat berhasil disimpan",
                                        type: "success",
                                        showCancelButton: false,
                                        confirmButtonClass: "btn-success",
                                        confirmButtonText: 'OK',
                                        closeOnConfirm: false
                                    });  
                                }else{
                                    swal({
                                        title: "Sukses!",
                                        text: "data rapat berhasil diupdate",
                                        type: "success",
                                        showCancelButton: false,
                                        confirmButtonClass: "btn-success",
                                        confirmButtonText: 'OK',
                                        closeOnConfirm: false
                                    });  
                                }
                
                            $('#btnSave').html('Simpan');
                            $('#btnSave').removeAttr('disabled',false);
                            $('#modalRapat').modal('hide');
                            table.ajax.reload();
                        },
                        error: function (data) {
                            console.log('Error:', data);
                            $('#btnSave').html('Simpan');
                            toastr.error('Kelola Data Gagal');
                        }
                    })
                }
                
            });  

            $('#btnSave_abon').click(function (e) {
                e.preventDefault();

                var report = [];
                $('.absen_row').each(function (index, element) {
                    var id = $(this).data('id');
                    var value = $('#abon_nim'+id).val();
                    if(value != null){
                        report[index] = true; 
                    }else{
                        $('#abon_nim'+id).addClass('is-invalid')
                        report[index] = false; 
                    }
                });

                // console.log(report);
                // console.log($.inArray( false, report));

                if ($.inArray( false, report) == -1) {
                    $(this).html('Sending..');
                    $(this).attr('disabled',true);                
                    $.ajax({
                        data: $('#formAbon').serialize(),
                        url: "{{route('simpanAbsensifromProker')}}",
                        type: "POST",
                        dataType: 'json',
                        success: function (data) {
                            console.log(data);
                            $('#modalAbsensi0').modal('hide');
                            swal({
                                title: "Sukses!",
                                text: data.message,
                                type: "success",
                                showCancelButton: false,
                                confirmButtonClass: "btn-success",
                                confirmButtonText: 'OK',
                                closeOnConfirm: false
                            });                          
                            $('#formAbon').trigger('reset');
                            $('#btnSave').html('Simpan');
                            $('#btnSave').removeAttr('disabled',true);
                            table.ajax.reload();                        
                        },
                        error: function (data) {
                            console.log('Error:', data);
                            $('#btnSave').removeAttr('disabled',true);
                            $('#btnSave').html('Simpan');
                            toastr.error('Kelola Data Gagal');
                        }
                    })
                }else{
                    $('#absen_header_nim').addClass('text-danger');
                }
            });  
            
            $('#btnSave_aboff').click(function (e) {
                e.preventDefault();
                $(this).html('Sending..');
                $(this).attr('disabled',true);

                $.ajax({
                    data: $('#formAboff').serialize(),
                    url: "{{route('simpanAbsensifromProker')}}",
                    type: "POST",
                    dataType: 'json',
                    success: function (data) {
                        console.log(data);
                        if (data.code == 0) {
                            toastr.error(data.message);
                        }else{
                            toastr.success(data.message);
                        }
                        // $('#formAboff').trigger('reset');
                        $('.nim_absen').val('');
                        $('#btnSave_aboff').html('Simpan');
                        $('#btnSave_aboff').removeAttr('disabled',true);
                        table.ajax.reload();
                        // $('#modalAbsensi1').modal('hide');                    
                    },
                    error: function (data) {
                        console.log('Error:', data);
                        $('#btnSave_aboff').removeAttr('disabled',true);
                        $('#btnSave_aboff').html('Simpan');
                        toastr.error('Kelola Data Gagal');
                    }
                })
            });  

            $('#btnSave_absen').click(function (e) {
                e.preventDefault();
                $(this).html('Sending..');
                $(this).attr('disabled',true);

                $.ajax({
                    data: $('#formAbsen').serialize(),
                    url: "{{route('delete_absen')}}",
                    type: "POST",
                    dataType: 'json',
                    success: function (data) {
                        console.log(data);
                        table.ajax.reload();
                            swal({
                                title: "Sukses!",
                                text: data.message,
                                type: "success",
                                showCancelButton: false,
                                confirmButtonClass: "btn-success",
                                confirmButtonText: 'OK',
                                closeOnConfirm: false
                            });                       
                        // $('#formAboff').trigger('reset');
                        $('#absen_deleted_id').val('');
                        $('#btnSave_absen').html('Simpan');
                        $('#btnSave_absen').removeAttr('disabled',true);
                        $('#modalAbsensi').modal('hide'); 
                    },
                    error: function (data) {
                        console.log('Error:', data);
                        $('#btnSave_absen').removeAttr('disabled',true);
                        $('#btnSave_absen').html('Simpan');
                        toastr.error('Kelola Data Gagal');
                    }
                })
            });  

            $(document).on('click', '.btnDelete', function () {
                console.log('deleted');
                var url = "{{url('proker/detail/rapat/hapus/')}}/"+$(this).data('id');
                // console.log(url);
                swal({
                title: "Apakah anda yakin menghapus data ini?",
                text: "Data yang akan dihapus secara permanen",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-warning",
                confirmButtonText: 'Iya',
                cancelButtonText: "Tidak",
                closeOnConfirm: true,
                closeOnCancel: true
                }, function (isConfirm) {
                    if (isConfirm) {
                            $.ajax({
                                url: url,
                                type: "GET",
                                dataType: 'json',
                                success: function (data) {
                                    // console.log(data);
                                    // toastr.success(data.message);
                                        swal({
                                            title: "Sukses!",
                                            text: "data berhasil dihapus",
                                            type: "success",
                                            showCancelButton: false,
                                            confirmButtonClass: "btn-success",
                                            confirmButtonText: 'OK',
                                            closeOnConfirm: false
                                        });                                  
                                    table.ajax.reload();
                                    },
                                error: function (data) {
                                    console.log('Error:', data);
                                    toastr.success(data.message);
                                    //$('#modalOrg').modal('show');
                                    }
                            });
                    }else {
                    }
                });
            });
        });
        // -=============================================== READY ==============================================-

        // aksi ketika save data dan update
        $(document).on('click', '.btnEdit', function () {
            // console.log('sjfhdav');
            $('#judulModal').html('Edit data Rapat');
            selecta_nim('#moderator');
            $('#tipe').select2();
            $('#id_rapat').val($(this).data('id_rapat'));
            $('#pokok_bahasan').val($(this).data('pokok_bahasan'));
            $('#moderator').html('<option value="'+$(this).data('moderator')+'" selected>( '+$(this).data('moderator')+' ) '+$(this).data('nama_mahasiswa')+'</option>');
            $('#tipe').val($(this).data('tipe')).trigger('change');
            $('#tempat').val($(this).data('tempat'));
            $('#tanggal_rapat').val($(this).data('tanggal_rapat'));
            $('#jam').val($(this).data('jam'));
            $('#hasil_rapat').val($(this).data('hasil_rapat'));
            $('#catatan').val($(this).data('catatan'));

            $('#modalRapat').modal({
                backdrop: 'static',
                keyboard: false, // to prevent closing with Esc button (if you want this too)
                show: true
            })
        });

        $(document).on('click', '.btnDetail', function () {
            // console.log('sjfhdav');
            $('#judulModal').html('Detail data Rapat');
            selecta_nim('#moderator');
            $('#tipe').select2();
            $('#id_rapat').val($(this).data('id_rapat')).attr('disabled',true);
            $('#pokok_bahasan').val($(this).data('pokok_bahasan')).attr('disabled',true);
            $('#moderator').html('<option value="'+$(this).data('moderator')+'" selected>( '+$(this).data('moderator')+' ) '+$(this).data('nama_mahasiswa')+'</option>').attr('disabled',true);
            $('#tipe').val($(this).data('tipe')).trigger('change').attr('disabled',true);
            $('#tempat').val($(this).data('tempat')).attr('disabled',true);
            $('#tanggal_rapat').val($(this).data('tanggal_rapat')).attr('disabled',true);
            $('#jam').val($(this).data('jam')).attr('disabled',true);
            $('#hasil_rapat').val($(this).data('hasil_rapat')).attr('disabled',true);
            $('#catatan').val($(this).data('catatan')).attr('disabled',true);
            
            $('#btnSave').attr('style', 'display:none');

            $('#modalRapat').modal({
                backdrop: 'static',
                keyboard: false, // to prevent closing with Esc button (if you want this too)
                show: true
            })
        });

        $("#modalRapat").on("hidden.bs.modal", function() {
            $('.form-control').removeAttr('disabled', true).removeClass('is-invalid');
            $('#id_rapat').attr('disabled',false);
            $('#id_proker').attr('disabled',false);
            $('#moderator').html('');
            $('#btnSave').removeAttr('style', 'display:none');
            $('#formRapat').trigger('reset');
            $('.form-control-label').removeClass('text-danger')
        });

        function selecta_nim_absen (idSelect_nim) { 
            $(idSelect_nim).select2({
                minimumInputLength: 0,
                // language: {
                //     inputTooShort: function() {
                //         return 'Masukkan minimal 2 digit angka';
                //     }
                // },         
                allowClear : true,
                searchInputPlaceholder: 'Search...',
                placeholder: 'pilih NIM',
                ajax: {
                cache: true,
                url: "{{ url('/anggota/select2') }}",
                dataType: 'json',
                type: "GET",
                quietMillis: 50,
                data: function (params) {
                    // console.log(params)
                    return {
                    search: $.trim(params.term)
                    };
                },
                processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                    return {
                        text:item.nama,
                        id: item.id
                        }
                    })
                    };
                }
                }
            });     
        };    

        function tambah_form_absen_on(params) {
            // console.log(count_absen_on);

                    idtag_pan = 'absen_row'+count_absen_on;
                    var formP =  '<tr class="absen_row" id="absen_row'+count_absen_on+'" data-id="'+count_absen_on+'"><td><div class="form-group form-material mb-0" data-plugin="formMaterial">';
                        formP += '<input type="text" class="form-control-plaintext form-control-sm abon_no" id="abon_no'+count_absen_on+'" disabled name="abon_no[]" value="'+ no_abon++ +'"/></td>';
                        formP += '</div><td><div class="form-group form-material mb-0" data-plugin="formMaterial">';
                        formP += '<select type="text" class="form-control" id="abon_nim'+count_absen_on+'" name="nim[]"></select><div class="invalid-feedback">*wajib diisi</div>';
                        formP += '</div></td><td class="pt-15"><button type="button" class="btn  btn-danger btn-xs rmForm_absen_on" data-id="'+count_absen_on+'">x</button></td></tr>';

                    $('#abon_row').append(formP);
                
                        nameSelect = '#abon_nim'+count_absen_on;
                        selecta_nim_absen(nameSelect);            
                        // console.log(nameSelect);
                    count_absen_on = count_absen_on + 1;   
        }

        $(document).on('click', '.addForm_absen_on', function (e) {
            e.preventDefault();            
            tambah_form_absen_on();     
        });

        $(document).on('click', '.rmForm_absen_on', function (e) {
            e.preventDefault();            
            var id = $(this).data('id');
            var absen_id = $(this).data('absen_id');
            id_deleted += ','+absen_id;
            
            $('#absen_deleted_id').val(id_deleted);

            $('#absen_row'+id).remove();

            var last_count_abon = 1;

            $('.absen_row').each(function () {
                var id = $(this).data('id');
                $('#abon_no'+id).val(last_count_abon);
                last_count_abon++;          
            });

            // console.log(last_count_abon);
            no_abon = last_count_abon;
            // console.log(count_absen_on);
        });

        $(document).on('click', '.btnAddAbsensi', function () {

            var id_rapat = $(this).data('id_rapat');
            var tipe = $(this).data('tipe');

            no_abon = 2;

            if (tipe == 0) {
                $('.id_rapat_abon').val(id_rapat);
                $('#modalAbsensi0').modal({
                    backdrop: 'static',
                    keyboard: false, // to prevent closing with Esc button (if you want this too)
                    show: true
                })            
            } else {
                scannerInstanscan();
                $('.id_rapat_aboff').val(id_rapat);
                $('#modalAbsensi1').modal({
                    backdrop: 'static',
                    keyboard: false, // to prevent closing with Esc button (if you want this too)
                    show: true
                })            
            }
        });

        $("#modalAbsensi1").on("hidden.bs.modal", function() {
            // $('#formAbon1').trigger('reset');
            scanner.stop();
        });

        $("#modalAbsensi0").on("hidden.bs.modal", function() {
            // $('#formAbon0').trigger('reset');
            $('.form-control').removeClass('is-invalid');
            $('#absen_header_nim').removeClass('text-danger');
            $('#abon_row').html('');
            tambah_form_absen_on();
            var last_count_abon = 1;

            $('.absen_row').each(function () {
                var id = $(this).data('id');
                $('#abon_no'+id).val(last_count_abon);
                last_count_abon++;          
            });

            // console.log(last_count_abon);
            no_abon = last_count_abon;            
        });

        $(document).on('click', '#ins_manual', function () {
            $('#manual_ins').removeAttr('style','display:none');
        });

        $(document).on('click', '.btnShowAbsensi', function () {
            id_rapat = $(this).data('id_rapat');
            tipe_absensi = $(this).data('tipe');

            $('.tipe_absensi').val(tipe_absensi);
            $('#deleted_id').val('');            

            no_abon = 1;

            var url_get_abseni = "{{url('absensi/proker/get/')}}/"+"{{$id_proker}}"+'/'+id_rapat;
            $.getJSON(url_get_abseni , function (data) {
                console.log(data);
                $.each(data, function (i, v) { 

                    idtag_pan = 'absen_row'+count_absen_on;
                    var formAbsen =  '<tr class="absen_row" id="absen_row'+count_absen_on+'" data-id="'+count_absen_on+'"><td><div class="form-group form-material mb-0" data-plugin="formMaterial">';
                        formAbsen += '<input type="text" class="form-control-plaintext form-control-sm absensi_no" id="absensi_no'+count_absen_on+'" disabled name="absensi_no[]" value="'+ no_abon++ +'"/></td>';
                        formAbsen += '</div><td><div class="form-group form-material mb-0" data-plugin="formMaterial">';
                        formAbsen += '<select disabled type="text" class="form-control select_nim" id="absensi_nim'+count_absen_on+'" name="nim[]">';
                        if(v.nim != null){
                            formAbsen += '<option value="'+v.nim+'" selected>( '+v.nim+' ) '+v.nama+'</option>';
                        }                               
                        formAbsen += '</select><input hidden type="text" class="form-control-plaintext" id="absensi_id'+count_absen_on+'" name="absensi_id[]" value="'+ v.id +'"/><div class="invalid-feedback">*wajib diisi</div>';
                        formAbsen += '</div></td><td class="pt-15 absen_del" style="display: none"><button type="button" class="btn  btn-danger btn-xs rmForm_absen_on" data-id="'+count_absen_on+'" data-absen_id="'+ v.id +'">x</button></td></tr>';

                    $('#absensi_row').append(formAbsen);

                    nameSelect = '#absensi_nim'+count_absen_on;
                    selecta_nim_absen(nameSelect);            
                    // console.log(nameSelect);
                    count_absen_on = count_absen_on + 1;                 
                });
            });

            $('#modalAbsensi').modal({
                backdrop: 'static',
                keyboard: false, // to prevent closing with Esc button (if you want this too)
                show: true
            })                         
        });
        
        $("#modalAbsensi").on("hidden.bs.modal", function() {
            $('#absensi_row').html('');
            $('.absen_del').attr('style','display:none');
        });

        $(document).on('click', '#edit_absen', function (e) {
            e.preventDefault();
            $('.absen_del').removeAttr('style','display:none');
        });        

        function maskInput(e) {
            //check if we have "e" or "window.event" and use them as "event"
                //Firefox doesn't have window.event 
            var event = e || window.event 

            var key_code = event.keyCode;
            var oElement = e ? e.target : window.event.srcElement;
            if (!event.shiftKey && !event.ctrlKey && !event.altKey) {
                if ((key_code > 47 && key_code < 58) ||
                    (key_code > 95 && key_code < 106)) {

                    if (key_code > 95)
                        key_code -= (95-47);
                    oElement.value = oElement.value;
                } else if(key_code == 8) {
                    oElement.value = oElement.value;
                } else if(key_code != 9) {
                    event.returnValue = false;
                }
            }
        }        

        var scanner = new Instascan.Scanner({
            video: document.getElementById('preview3'),
            scanPeriod: 5,
            mirror: false
        });

        function animasi_leave(this_) {
            $(this_).css({
                "border": "solid",
                "border-color": "#3e8ef7"
            });
        }

        function animasi_enter(this_) {
            $(this_).css({
                "border": "",
                "border-color": ""
            });
        }

        // FUNCTION SCANNER
        function scannerInstanscan() {
            Instascan.Camera.getCameras().then(function(cameras) {
                if (cameras.length > 0) {
                    if (cameras.length > 1) {
                        $("#div_select").show();
                        $("#popup_camera").html("");
                        $("#popup_camera").append("<option value=''> -- Pilih Kamera -- </option>");
                        for (i in cameras) {
                            $("#popup_camera").append("<option value='" + [i] + "'>" + cameras[i].name + "</option>");
                        }

                        if (cameras[1] != "") {
                            scanner.start(cameras[1]);
                            $("#popup_camera").val(1).change();
                        } else {
                            alert('Kamera tidak ditemukan!');
                        }
                    } else {
                        if (cameras[0] != "") {
                            scanner.start(cameras[0]);
                            $("#popup_camera").val(0).change();
                        } else {
                            alert('Kamera tidak ditemukan!');
                        }
                    }

                    $("#popup_camera").change(function() {
                        var id = $("#popup_camera :selected").val();

                        if (id != '') {
                            if (cameras[id] != "") {
                                scanner.start(cameras[id]);
                            } else {
                                alert('Kamera tidak ditemukan!');
                            }
                        }
                    })

                } else {
                    console.error('No cameras found.');
                    alert('No cameras found.');
                }
            }).catch(function(e) {
                console.error(e);
                alert(e);
            });
        }

        scanner.addListener('scan', function(content) {
            if (content != '') {
                console.log(content);
                $('.nim_absen_h').val(content);
                
                $.ajax({
                    data: $('#formAboff').serialize(),
                    url: "{{route('simpanAbsensifromProker')}}",
                    type: "POST",
                    dataType: 'json',
                    success: function (data) {
                        console.log(data);
                        if (data.code == 0) {
                            toastr.error(data.message);
                        }else{
                            toastr.success(data.message);
                        }
                        // $('#formAboff').trigger('reset');
                        $('.nim_absen').val('');
                        $('#btnSave_aboff').html('Simpan');
                        $('#btnSave_aboff').removeAttr('disabled',true);
                        table.ajax.reload();
                        // $('#modalAbsensi1').modal('hide');
                    },
                    error: function (data) {
                        console.log('Error:', data);
                        $('#btnSave_aboff').removeAttr('disabled',true);
                        $('#btnSave_aboff').html('Simpan');
                        toastr.error('Kelola Data Gagal');
                    }
                })
            } else {
                var item = content.split("||");
                // window.location.href = "/" + item;
                console.log(item);
                setTimeout(function() {
                    swal({
                        title: "Data Error!",
                        text: "Content dari Kode QR tersebut Kosong!",
                        type: "error"
                    }, function() {
                        $("#modalAbsensi1").modal("hide");
                    }, 1000);
                })
            }
        });
</script>
@endpush