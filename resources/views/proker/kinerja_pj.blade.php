@extends('layouts.app')

@section('content')
@php
        $user 		= Session::get("data_user");
        $flag_lv = $user['flag_lv'];

@endphp
<div class="row" data-plugin="matchHeight" data-by-row="true">
    <div class="col-xxl-12 col-lg-12">
        <div class="panel shadow panel-bordered">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        List Kinerja Penanggung Jawab    
                        </h3>
                    <div class="panel-actions ">
                        <a class="panel-action icon wb-minus" aria-expanded="true" data-toggle="panel-collapse" aria-hidden="true"></a>
                        <a class="panel-action icon wb-expand" data-toggle="panel-fullscreen" aria-hidden="true"></a>
                    </div>
                </div>
                <div class="panel-body pb-15 pt-15 ">
                    <div class="row pt-3">
                        <div class="col-xxl-6 col-lg-6" style="border-right: solid rgba(182, 180, 180, 0.496) 1px">
                            <h5>Filter</h5>
                            <dl class="dl-horizontal row">
                                <dt class="col-sm-1">Tahun</dt>
                                <dd class="col-sm-4"><select type="text" class="form-control form-control-sm" id="filter_tahun" name="filter_tahun">
                                </select></dd>                                
                                <dt class="col-sm-1">Bulan</dt>
                                <dd class="col-sm-6"><select type="text" class="form-control form-control-sm" id="filter_bulan" name="filter_bulan">
                                </select></dd>
                            </dl>                                                                                  
                        </div>
                    </div>                    
                    <div class="row pt-3">
                        <div class="col-xxl-12 col-lg-12">
                        
                        </div>
                        <div class="col-xxl-12 col-lg-12 table-responsive">
                            <table class="table table-bordered table-hover table-striped" cellspacing="0" id="tb_kinerja">
                                <thead class="text-center">
                                    <tr>
                                        <th data-priority="1">No</th>
                                        <th data-priority="2">Penanggung Jawab</th>
                                        <th data-priority="3">Proker</th>
                                        <th data-priority="4">Tanggal</th>
                                        <th data-priority="5">Jenis</th>
                                        <th data-priority="6">Status Pelaksanaan</th>
                                        <th data-priority="7">Status LPJ</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                                </table>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</div>

@endsection

@push('scripts')
<script>
        function select2filter(id_tag,url,placeholder) {
            $(id_tag).select2({
                minimumInputLength: 0,
                // language: {
                //     inputTooShort: function() {
                //         return 'Masukkan minimal 2 digit angka';
                //     }
                // },         
                allowClear : true,
                searchInputPlaceholder: 'Search...',
                placeholder: placeholder,
                ajax: {
                cache: true,
                url: url,
                dataType: 'json',
                type: "GET",
                quietMillis: 50,
                data: function (params) {
                    // console.log(params)
                    return {
                    search: $.trim(params.term)
                    };
                },
                processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                    return {
                        text:item.nama,
                        id: item.id
                        }
                    })
                    };
                }
                }
            });               
        }
        $(document).ready(function () {

            // fix bug title in select2
            $(document).on('mouseenter', '.select2-selection__rendered', function () {
                $('.select2-selection__rendered').removeAttr('title');
            });
                        
            select2filter('#filter_bulan',"{{route('select2bulanProker')}}?tahun=0",'semua bulan');
            select2filter('#filter_tahun',"{{route('select2tahunProker')}}?bulan=0",'semua tahun');


                var table = $('#tb_kinerja').DataTable({
                    "paging": true,                    
                    processing: true,
                    serverSide: true,
                    // responsive: true,        
                    ajax: "{{ route('getKinerjaPJ') }}?nim="+"{{(isset($nim)) ? $nim : ''}}",
                    language: {
                        processing: "Sedang diproses...<img height=50 src='{{asset('assets/images/load3.gif')}}'>"
                    },
                    columns: [
                        {
                            data: 'no',
                            name: 'no',
                            searchable : true,
                            orderable:true
                        }, {
                            data: 'nama_pj',
                            name: 'nama_pj',
                            orderable:true
                        },{
                            data: 'nama',
                            name: 'nama',
                            orderable: true,
                            searchable: true                            
                        },{
                            data: 'tanggal',
                            name: 'tanggal',
                            orderable: true,
                            searchable: true
                        },{
                            data: 'stat_event',
                            name: 'stat_event',
                            orderable: true,
                            searchable: true
                        },{
                            data: 'terlaksana',
                            name: 'terlaksana',
                            orderable: true,
                            searchable: true
                        },{
                            data: 'status_lpj',
                            name: 'status_lpj',
                            orderable: true,
                            searchable: true
                        },
                    ],
                    columnDefs: [
                        { className: 'text-center', targets: [0,4,6] },
                    ],
                    initComplete: function(settings, json) {
                        $("#tb_kinerja tbody tr").each(function() {
                            var rating = $(this).find(".rating").data('rating');
                            $(this).find(".rating").rateYo({
                                starWidth: "10px",
                                readOnly: true,
                                numStars: 5,
                                rating: rating,
                                multiColor: {
                                    "startColor": "#FF0000", //RED
                                    "endColor"  : "#fcc203"  //GREEN
                                },
                                halfStar: true
                            });    
                            
                            var catatan = $(this).find(".st_pengajuan").data('catatan');
                            $(this).find(".st_pengajuan").webuiPopover({title:'Catatan',content:catatan,closeable:true,trigger:'hover'});

                            var evaluasi = $(this).find(".st_eval").data('evaluasi');
                            $(this).find(".st_eval").webuiPopover({title:'Evaluasi',content:evaluasi,closeable:true,trigger:'hover'});
                        });  

                    },
                    fnDrawCallback: function( oSettings ) {
                        $("#tb_kinerja tbody tr").each(function() {
                            var rating = $(this).find(".rating").data('rating');
                            $(this).find(".rating").rateYo({
                                starWidth: "10px",
                                readOnly: true,
                                numStars: 5,
                                rating: rating,
                                multiColor: {
                                    "startColor": "#FF0000", //RED
                                    "endColor"  : "#fcc203"  //GREEN
                                },
                                halfStar: true
                            });    
                            
                            var catatan = $(this).find(".st_pengajuan").data('catatan');
                            $(this).find(".st_pengajuan").webuiPopover({title:'Catatan',content:catatan,closeable:true,trigger:'hover'});

                            var evaluasi = $(this).find(".st_eval").data('evaluasi');
                            $(this).find(".st_eval").webuiPopover({title:'Evaluasi',content:evaluasi,closeable:true,trigger:'hover'});
                            
                        });  
                    }                    
                });                


            $(document).on('change', '#filter_bulan', function (e) {
                e.preventDefault();
                // console.log($(this).val());
                var bulan = $(this).val();
                var tahun = $('#filter_tahun').val();

                select2filter('#filter_tahun',"{{route('select2tahunProker')}}?bulan="+bulan,'semua tahun');
                table.ajax.url( "{{ route('getKinerjaPJ') }}?nim="+"{{(isset($nim)) ? $nim : ''}}&tahun="+tahun+"&bulan="+bulan ).load();
            });    

            $(document).on('change', '#filter_tahun', function (e) {
                e.preventDefault();
                // console.log($(this).val());
                var bulan = $('#filter_bulan').val();
                var tahun = $(this).val();

                select2filter('#filter_bulan',"{{route('select2bulanProker')}}?tahun="+tahun,'semua bulan');
                table.ajax.url( "{{ route('getKinerjaPJ') }}?nim="+"{{(isset($nim)) ? $nim : ''}}&tahun="+tahun+"&bulan="+bulan ).load();
            });    

        });

</script>
@endpush