"use strict";

// Class Definition
var KTAddUser = function() {
    // Private Variables
    var _wizardEl;
    var _formEl;
    var _wizardObj;
    var _avatar;
    var _validations = [];

    // Private Functions
    var _initWizard = function() {
        // Initialize form wizard
        _wizardObj = new KTWizard(_wizardEl, {
            startStep: 1, // initial active step number
            clickableSteps: false // allow step clicking
        });

        // Validation before going to next page
        _wizardObj.on('change', function(wizard) {
            if (wizard.getStep() > wizard.getNewStep()) {
                return; // Skip if stepped back
            }

            // Validate form before change wizard step
            var validator = _validations[wizard.getStep() - 1]; // get validator for currnt step

            if (validator) {
                validator.validate().then(function(status) {
                    if (status == 'Valid') {
                        wizard.goTo(wizard.getNewStep());

                        KTUtil.scrollTop();
                    } else {
                        Swal.fire({
                            text: "Maaf, kelihatannya masih ada kesalahan, dimohon meneliti kembali.",
                            icon: "error",
                            buttonsStyling: false,
                            confirmButtonText: "Ok, Mengerti!",
                            customClass: {
                                confirmButton: "btn font-weight-bold btn-light"
                            }
                        }).then(function() {
                            KTUtil.scrollTop();
                        });
                    }
                });
            }

            return false; // Do not change wizard step, further action will be handled by he validator
        });

        // Change event
        _wizardObj.on('changed', function(wizard) {
            KTUtil.scrollTop();
        });

        // Submit event
        _wizardObj.on('submit', function(wizard) {

        });
    }

    var _initValidations = function() {
        // Init form validation rules. For more info check the FormValidation plugin's official documentation:https://formvalidation.io/

        // Validation Rules For Step 1
        _validations.push(FormValidation.formValidation(
            _formEl, {
                fields: {
                    foto: {
                        validators: {
                            notEmpty: {
                                message: 'Foto tidak boleh kosong'
                            }
                        }
                    },
                    nim: {
                        validators: {
                            notEmpty: {
                                message: 'NIM tidak boleh kosong'
                            },
                            stringLength: {
                                max: 10,
                                message: 'NIM tidak boleh lebih dari 10 karakter'
                            }
                        }
                    },
                    nama: {
                        validators: {
                            notEmpty: {
                                message: 'Nama tidak boleh kosong'
                            }
                        }
                    },
                    tanggal_lahir: {
                        validators: {
                            notEmpty: {
                                message: 'Tanggal lahir tidak boleh kosong'
                            }
                        }
                    },
                    jenis_kelamin: {
                        validators: {
                            notEmpty: {
                                message: 'Wajib memilih jenis kelamin'
                            },
                            choice: {
                                min: 1,
                                message: 'Wajib memilih jenis kelamin'
                            }
                        }
                    },
                    agama: {
                        validators: {
                            notEmpty: {
                                message: 'Wajib memilih Agama'
                            },
                            choice: {
                                min: 1,
                                message: 'Wajib memilih Agama'
                            }
                        }
                    },
                    no_hp: {
                        validators: {
                            notEmpty: {
                                message: 'No HP tidak boleh kosong'
                            },
                        }
                    },
                    email: {
                        validators: {
                            notEmpty: {
                                message: 'Email tidak boleh kosong'
                            },
                            emailAddress: {
                                message: 'Format email salah'
                            }
                        }
                    }
                },
                plugins: {
                    trigger: new FormValidation.plugins.Trigger(),
                    // Bootstrap Framework Integration
                    bootstrap: new FormValidation.plugins.Bootstrap({
                        //eleInvalidClass: '',
                        eleValidClass: '',
                    })
                }
            }
        ));

        _validations.push(FormValidation.formValidation(
            _formEl, {
                fields: {
                    // Step 2
                    id_prodi: {
                        validators: {
                            choice: {
                                min: 1,
                                message: 'Wajib memilih Prodi'
                            }
                        }
                    },
                    id_kelas: {
                        validators: {
                            notEmpty: {
                                message: 'Wajib memilih Kelas'
                            }
                        }
                    }
                },
                plugins: {
                    trigger: new FormValidation.plugins.Trigger(),
                    // Bootstrap Framework Integration
                    bootstrap: new FormValidation.plugins.Bootstrap({
                        //eleInvalidClass: '',
                        eleValidClass: '',
                    })
                }
            }
        ));

        _validations.push(FormValidation.formValidation(
            _formEl, {
                fields: {
                    id_provinsi: {
                        validators: {
                            choice: {
                                min: 1,
                                message: 'Wajib memilih Provinsi'
                            }
                        }
                    },
                    id_kota: {
                        validators: {
                            choice: {
                                min: 1,
                                message: 'Wajib memilih Kota'
                            }
                        }
                    },
                    id_kecamatan: {
                        validators: {
                            choice: {
                                min: 1,
                                message: 'Wajib memilih Kecamatan'
                            }
                        }
                    },
                    id_desa: {
                        validators: {
                            choice: {
                                min: 1,
                                message: 'Wajib memilih Desa'
                            }
                        }
                    },
                    alamat: {
                        validators: {
                            notEmpty: {
                                message: 'alamat tidak boleh kosong'
                            }
                        }
                    }
                },
                plugins: {
                    trigger: new FormValidation.plugins.Trigger(),
                    // Bootstrap Framework Integration
                    bootstrap: new FormValidation.plugins.Bootstrap({
                        //eleInvalidClass: '',
                        eleValidClass: '',
                    })
                }
            }
        ));
    }

    var _initAvatar = function() {
        _avatar = new KTImageInput('kt_user_add_avatar');
    }

    return {
        // public functions
        init: function() {
            _wizardEl = KTUtil.getById('kt_wizard');
            _formEl = KTUtil.getById('pendaftaranForm');

            _initWizard();
            _initValidations();
            _initAvatar();
        }
    };
}();

jQuery(document).ready(function() {
    KTAddUser.init();
});